<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutBrideGroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_bride_grooms', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 60)->unique()->nullable();
            $table->unsignedBigInteger('celebrate_detail_id')->nullable();
            $table->string('name_groom',60);
            $table->string('name_bride',60);

            $table->text('description_groom')->nullable();
            $table->text('description_bride')->nullable();

            $table->string('img_groom')->nullable();
            $table->string('img_bride')->nullable();

            $table->text('introduction')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('celebrate_detail_id')
                ->references('id')
                ->on('celebrate_details')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_bride_grooms');
    }
}
