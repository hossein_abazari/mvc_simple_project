<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('slug',30)->nullable()->unique();;
            $table->enum('type',['unpaid','cash','card','port'])->default('unpaid');
            $table->dateTime('payment_date')->nullable();
            $table->bigInteger('amount')->default(0)->comment('if 0 >= is Creditor and 0 <= is Debtor');
            $table->text('description')->nullable();
            $table->string('tracking_code',80)->nullable();
            $table->boolean('payment')->default(false)->comment('if payment is true record for pay');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
