<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCelebrateDetailIdToGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guests', function (Blueprint $table) {
            $table->unsignedBigInteger('celebrate_detail_id')->nullable()->after('user_id');
            $table->unsignedBigInteger('celebration_id')->nullable()->after('user_id');

            $table->foreign('celebrate_detail_id')
                ->references('id')
                ->on('celebrate_details')
                ->onDelete('cascade');

            $table->foreign('celebration_id')
                ->references('id')
                ->on('celebrations')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guests', function (Blueprint $table) {
            //
        });
    }
}
