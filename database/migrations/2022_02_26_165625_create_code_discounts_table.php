<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodeDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_discounts', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 60)->unique()->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('name','100')->nullable();
            $table->string('code_discount','100')->nullable();
            $table->bigInteger('cent_discount')->nullable();
            $table->bigInteger('amount_discount')->nullable();
            $table->boolean('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_discounts');
    }
}
