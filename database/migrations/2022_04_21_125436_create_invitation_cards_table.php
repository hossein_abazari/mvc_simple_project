<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitationCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation_cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('celebrate_detail_id')->unique();
            $table->bigInteger('number_invitation_card')->default(50);
            $table->bigInteger('price')->default(0);
            $table->timestamps();

            $table->foreign('celebrate_detail_id')
                ->references('id')
                ->on('celebrate_details')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitation_card');
    }
}
