<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("mysql_log")->create('user_logins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug',30)->nullable()->unique();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->dateTime('login')->useCurrent();
            $table->string('key',500)->nullable();
            $table->ipAddress('ip_address')->nullable();
            $table->string('user_agent', 1023)->nullable();
            $table->string('country',40)->nullable();
            $table->string('city',40)->nullable();
            $table->string('os',40)->nullable();
            $table->string('browser',40)->nullable();
            $table->enum('action',['login','logout','registration','activation','api','fail'])->default('login');
            $table->timestamps();

            $table->index(['user_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_logins');
    }
}
