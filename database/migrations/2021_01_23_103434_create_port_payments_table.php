<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('port_payments', function (Blueprint $table) {
            $table->id();
            $table->string('slug',30)->nullable()->unique();;
            $table->bigInteger('celebration_id')->unsigned();
            $table->bigInteger('invoice_id')->unsigned()->nullable();
            /* celebrate_detail_id in add table */
            $table->string('authority',100)->nullable();
            $table->bigInteger('amount')->default(0);
            $table->bigInteger('discount')->default(0);
            $table->integer('discount_cent')->default(0);
            $table->text('description')->nullable();
            $table->integer('payment')->default(false);
            $table->string('tracking_code_error',100)->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('celebration_id')
                ->references('id')
                ->on('celebrations')
                ->onDelete('cascade');

            $table->foreign('invoice_id')
                ->references('id')
                ->on('invoices')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('port_payments');
    }
}
