<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddThereIsGalleryToCelebrateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('celebrate_details', function (Blueprint $table) {
            $table->boolean('there_is_gallery')->default(1)->after('background_img');
            $table->boolean('free')->default(0)->after('there_is_gallery');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('celebrate_details', function (Blueprint $table) {
            //
        });
    }
}
