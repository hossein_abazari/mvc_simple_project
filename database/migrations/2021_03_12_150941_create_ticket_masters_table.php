<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_masters', function (Blueprint $table) {
            $table->id();
            $table->string('slug',30)->nullable()->unique();
            $table->bigInteger('celebration_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('title')->nullable();
            $table->boolean('close')->default(0)->comment('if is 1 mean closed');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('celebration_id')->references('id')->on('celebrations')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_masters');
    }
}
