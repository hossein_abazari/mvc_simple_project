<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCodeDiscountIdToCelebrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('celebrations', function (Blueprint $table) {
            $table->unsignedBigInteger('code_discount_id')->nullable()->after('id');

            $table->foreign('code_discount_id')->references('id')->on('code_discounts')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('celebrations', function (Blueprint $table) {
            //
        });
    }
}
