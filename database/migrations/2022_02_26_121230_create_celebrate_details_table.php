<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCelebrateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('celebrate_details', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 60)->unique()->nullable();
            $table->unsignedBigInteger('celebration_id')->nullable();
            $table->enum('type_celebrate',['wedding','birth','other'])->default('other'); /* type celebration */
            $table->string('name','100')->nullable();
            $table->string('name_logo','100')->nullable(); /* name logo english */
            $table->dateTime('date_ceremony')->nullable(); /* from input */
            $table->text('text_invite')->nullable();
            $table->text('text_footer')->nullable();
            $table->boolean('survey')->default(0);
            $table->text('text_survey')->nullable();
            $table->string('link_name',80)->unique()->nullable();
            $table->boolean('memories')->default(0);
            $table->string('venue_ceremony','100')->nullable();
            $table->text('address')->nullable();
            $table->text('location')->nullable();
            $table->dateTime('expire')->nullable();
            $table->enum('free',['Y','N'])->default('N');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('celebration_id')->references('id')->on('celebrations')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('celebrate_details');
    }
}
