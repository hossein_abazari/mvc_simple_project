<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug','30')->unique()->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('name','100')->nullable();
            $table->string('extra_name','100')->nullable();
            $table->boolean('status')->default(0);
            $table->enum('family',['bride','groom'])->default('bride');
            $table->bigInteger('count_family')->default(1);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guests');
    }
}
