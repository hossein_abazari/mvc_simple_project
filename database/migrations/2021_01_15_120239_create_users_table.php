<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('slug',30)->nullable()->unique();;
            $table->bigInteger('celebration_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('mobile',11)->unique()->nullable();
            $table->enum('type',['admin','celebrationAdmin','celebration'])->default('celebration'); //
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('celebration_id')->references('id')->on('celebrations')->onDelete('cascade');
        });

        $user = new \App\Models\User();
        $user->name = 'admin';
        $user->email = 'hossein.abazarikha@gmail.com';
        $user->password = 'Hossein@bazari5$#';
        $user->type = 'admin';
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
