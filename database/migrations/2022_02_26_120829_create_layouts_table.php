<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layouts', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 60)->unique()->nullable();
            $table->integer('directory_id')->unique(); /* directory id */
            $table->enum('type',['wedding','birth','other'])->default('wedding'); /* type celebration */
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('directory')->nullable();
            $table->string('img_temp')->nullable();
            $table->enum('free',['Y','N'])->default('N');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layouts');
    }
}
