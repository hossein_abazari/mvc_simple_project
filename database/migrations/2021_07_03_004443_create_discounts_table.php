<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 60)->unique()->nullable();
            $table->string('name', 30)->nullable();
            $table->integer('number')->default(0);
            $table->integer('discount_cent')->default('0');  // discounts
            $table->dateTime('expire_date')->nullable();
            $table->boolean('status')->default(true)->comment('active or deactive');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
