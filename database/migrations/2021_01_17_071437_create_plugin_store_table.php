<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePluginStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plugin_store', function (Blueprint $table) {
            $table->bigInteger('plugin_id')->unsigned();
            $table->bigInteger('celebration_id')->unsigned();

            $table->foreign('plugin_id')->references('id')->on('plugins')->onDelete('cascade');

            $table->foreign('celebration_id')->references('id')->on('celebrations')->onDelete('cascade');

            $table->primary(['plugin_id' , 'celebration_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plugin_store');
    }
}
