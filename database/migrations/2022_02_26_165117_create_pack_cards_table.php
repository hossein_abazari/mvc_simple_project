<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pack_cards', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 60)->unique()->nullable();
            $table->string('name','100')->nullable();
            $table->integer('number_card')->default(1);
            $table->bigInteger('amount')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pack_cards');
    }
}
