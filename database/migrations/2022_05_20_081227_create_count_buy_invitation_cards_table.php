<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountBuyInvitationCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('count_buy_invitation_cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('celebrate_detail_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->bigInteger('number_card')->default(0);

            $table->timestamps();

            $table->foreign('celebrate_detail_id')
                ->references('id')
                ->on('celebrate_details')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('count_buy_invitation_cards');
    }
}
