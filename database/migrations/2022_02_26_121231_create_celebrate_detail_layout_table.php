<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCelebrateDetailLayoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('celebrate_detail_layout', function (Blueprint $table) {
            $table->unsignedBigInteger('celebrate_detail_id')->unique();
            $table->unsignedBigInteger('layout_id');


            $table->primary(['layout_id' , 'celebrate_detail_id']);

            $table->foreign('layout_id')
                ->references('id')
                ->on('layouts')
                ->onDelete('cascade');

            $table->foreign('celebrate_detail_id')
                ->references('id')
                ->on('celebrate_details')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layout_user');
    }
}
