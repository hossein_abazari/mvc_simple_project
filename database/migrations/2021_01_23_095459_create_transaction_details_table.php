<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->bigIncrements('transaction_id')->unsigned();
            $table->bigInteger('celebration_id')->unsigned();
            $table->bigInteger('invoice_id')->unsigned()->nullable();
            $table->bigInteger('celebrate_detail_id')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
            $table->foreign('celebration_id')->references('id')->on('celebrations')->onDelete('cascade');
            $table->foreign('celebrate_detail_id')->references('id')->on('celebrate_details')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
