<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCelebrationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('celebration_settings', function (Blueprint $table) {
            $table->bigIncrements('celebration_id')->unsigned();
            $table->dateTime('from_date')->nullable();
            $table->dateTime('to_date')->nullable();
            $table->string('name_link')->nullable(); //link address shop
            $table->boolean('status')->default(true);
            $table->text('address')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('celebration_id')->references('id')->on('celebrations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('celebration_settings');
    }
}
