<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Settings', function (Blueprint $table) {
            $table->id();
            $table->string('slug',30)->nullable()->unique();
            $table->string('locale',2);
            $table->text('language'); /* json langueage ['fa','en'] */
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
        $setting = new \App\Models\Setting();
        $setting->locale = 'fa';
        $setting->language = '';
        $setting->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Settings');
    }
}
