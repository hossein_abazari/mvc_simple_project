<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('slug',30)->nullable()->unique();;
            $table->bigInteger('celebration_id')->unsigned();
            $table->string('invoice_code',11)->nullable()->comment('code manual for invoice');
            $table->dateTime('invoice_date')->nullable();
            $table->string('name',50)->nullable();
            $table->text('description')->nullable();
            $table->bigInteger('amount')->default(0);
            $table->enum('type',['celebrationRecord','manualRecord','other'])->default('other'); /* for type insert factor */


            $table->softDeletes();
            $table->timestamps();

            $table->foreign('celebration_id')->references('id')->on('celebrations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_stores');
    }
}
