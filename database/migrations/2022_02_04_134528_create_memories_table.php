<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug','30')->nullable();
            $table->bigInteger('guest_id')->unsigned();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('celebrate_detail_id')->nullable();
            $table->text('message')->nullable();
            $table->enum('answer',['Y','N'])->default('N');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('guest_id')->references('id')->on('guests')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('celebrate_detail_id')->references('id')->on('celebrate_details')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memories');
    }
}
