<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("mysql_log")->create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug',60)->nullable();
            $table->unsignedBigInteger('guest_id')->nullable();
            $table->unsignedBigInteger('celebrate_detail_id')->nullable();
            $table->string('event')->nullable();
            $table->text('url')->nullable();
            $table->ipAddress('ip_address')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('tags')->nullable();
            $table->timestamps();

            $table->index(['guest_id']);
            $table->index(['celebrate_detail_id']);

//            $table->foreign('celebrate_detail_id')
//                ->references('id')
//                ->on('celebrate_details')
//                ->onDelete('cascade');
//
//            $table->foreign('guest_id')
//                ->references('id')
//                ->on('guests')
//                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audits');
    }
}
