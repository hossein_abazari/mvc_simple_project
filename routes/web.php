<?php

use App\Http\Controllers;
use App\Http\Controllers\Admin\GeneralController;
use App\Http\Controllers\AdminStore\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware("language.setting")->group(function () {

//    Route::get('/', function () {
//        return view('welcome');
//    })->name('first.page');

    Route::get('/', [Controllers\RootController::class, 'index'])->name('root.index');



    Route::get('/login', function () {
        return view('Celebration.login.insertEmail');
    })->name('celebration.login');


//Route::get('audits', 'AuditController@index');
    /* login admin  */
    Route::get('/admin/auth', [Controllers\Admin\LoginController::class, 'showLogin'])->name('admin.show.login');
    Route::post('/admin/auth', [Controllers\Admin\LoginController::class, 'login'])->name('admin.login');
    Route::post('/admin/logout', [Controllers\Admin\LoginController::class, 'logout'])->name('admin.logout');

    /* login admin Store  */
    Route::get('/celebration/login', [Controllers\AdminStore\LoginController::class, 'showLogin'])->name('admin.celebration.show.login');
    Route::post('/celebration/login', [Controllers\AdminStore\LoginController::class, 'login'])->name('admin.celebration.login');
    Route::get('/celebration/register', [Controllers\AdminStore\RegisterController::class, 'index'])->name('admin.celebration.register');
    Route::post('/celebration/register', [Controllers\AdminStore\RegisterController::class, 'store'])->name('admin.celebration.register.store');
    Route::post('/celebration/logout', [Controllers\AdminStore\LoginController::class, 'logout'])->name('admin.celebration.logout');
    /* User Celebration login */
    Route::middleware("celebration.login")->group(function () {
        Route::get('/login', [Controllers\Celebration\LoginController::class, 'showLogin'])->name('celebration.show.login');
        Route::post('/login', [Controllers\Celebration\LoginController::class, 'login'])->name('celebration.login');
        Route::resource('register', Controllers\Celebration\RegisterController::class)->names('celebration.register');
    });
    Route::post('/login/next/validate', [Controllers\Celebration\LoginController::class, 'nextValidate'])->name('celebration.login.next.validate');
    Route::post('/register/next/validate', [Controllers\Celebration\RegisterController::class, 'nextValidate'])->name('celebration.register.next.validate');
    Route::post('/logout', [Controllers\Celebration\LoginController::class, 'logout'])->name('celebration.logout');




    Route::middleware("auth")->group(function () {
        /* panel admin */
        Route::middleware("auth.admin")->group(function () {
            Route::prefix("admin")->group(function () {
                Route::get('/dashboard', [Controllers\Admin\DashboardController::class, 'index'])->name('admin.dashboard');

                Route::get('/dashboard/chart', [Controllers\Admin\DashboardController::class, 'chart'])->name('admin.dashboard.chart');

                Route::get('/general/cookie', [GeneralController::class, 'themeCookie'])->name('admin.general.cookie');

                /* edit all modal for table */
                Route::get('/general/edit/modal', [GeneralController::class, 'editModal'])->name('admin.general.edit.modal');
                /* language setting all page */
                Route::post('/general/language/{locale}', [GeneralController::class, 'language'])->name('admin.general.language');
                /* search all pages for store */
                Route::post('/general/store/search', [GeneralController::class, 'storeSearch'])->name('admin.general.store.search');
                /* autocomplete search all */
                Route::get('/general/autocomplete', [GeneralController::class, 'autocomplete'])->name('general.autocomplete');
                /* deadline part in admin */
                Route::resource('deadlines', Controllers\Admin\DeadlineController::class)->names('admin.deadline');
                /* PackCard part in admin */
                Route::resource('pack/cards', Controllers\Admin\PackCardController::class)->names('admin.pack.card');
                /* Layouts part in admin */
                Route::resource('layouts', Controllers\Admin\LayoutController::class)->names('admin.layout');
                /* Layouts part in code_discount */
                Route::resource('code/discount', Controllers\Admin\CodeDiscountController::class)->names('admin.code.discount');
                /* discount part in admin */
                Route::resource('discounts', Controllers\Admin\DiscountController::class)->names('admin.discount');
                /* plugin part in admin */
                Route::resource('plugins', Controllers\Admin\PluginController::class)->names('admin.plugin');
                /* Store part in admin */
                Route::resource('celebrations', Controllers\Admin\CelebrationController::class)->names('admin.celebration');
                Route::get('/amount/deadline', [Controllers\Admin\CelebrationController::class, 'amountDeadline'])->name('admin.amount.deadline');
                /* User part in admin */
                Route::resource('users', Controllers\Admin\UserController::class)->names('admin.user');
                /* Invoice part in admin */
                Route::resource('invoices', Controllers\Admin\InvoiceController::class)->names('admin.invoice');
                /* Transaction Store part in admin */
                Route::resource('transaction', Controllers\Admin\TransactionController::class)->names('admin.transaction');

                Route::get('/audit', [Controllers\Admin\AuditController::class, 'index'])->name('admin.audit.logs');
                Route::get('/audit/view', [Controllers\Admin\AuditController::class, 'view'])->name('admin.audit.view');
                /* Ticket Master part in admin */
                Route::resource('ticket/master', Controllers\Admin\TicketMasterController::class)->names('admin.ticket.master');
                Route::post('/show/store/{TicketMaster:slug}', [Controllers\Admin\TicketMasterController::class, 'showStore'])->name('admin.ticket.master.show.store');
                /* User Login part in admin */
                Route::resource('user/login', Controllers\Admin\UserLoginController::class)->names('admin.user.login');

                /* Settings part in admin */
                Route::get('/setting', [Controllers\Admin\SettingController::class, 'index'])->name('admin.setting.index');
                Route::post('/setting/store', [Controllers\Admin\SettingController::class, 'store'])->name('admin.setting.store');
                Route::post('/setting/profile', [Controllers\Admin\SettingController::class, 'profile'])->name('admin.setting.profile');
                Route::post('/setting/change/password', [Controllers\Admin\SettingController::class, 'changePassword'])->name('admin.setting.change.password');


            });
        });

        /* panel Admin Store */
        Route::middleware("auth.admin.celebration")->group(function () {
            Route::prefix("admin/celebration")->group(function () {
                /* profile */
                Route::get('/profile', [Controllers\AdminStore\ProfileController::class, 'index'])->name('admin.celebration.profile');
                Route::post('/profile', [Controllers\AdminStore\ProfileController::class, 'update'])->name('admin.celebration.profile.update');
                /* deadline tariff for sel */
                Route::get('/tariff', [Controllers\AdminStore\TariffController::class, 'index'])->name('admin.celebration.tariff');
                Route::get('/tariff/buy/{buy}', [Controllers\AdminStore\TariffController::class, 'buy'])->name('admin.celebration.tariff.buy');
                Route::post('/tariff/port/verify/{buy}', [Controllers\AdminStore\TariffController::class, 'portVerify'])->name('admin.celebration.tariff.port.verify');
                Route::get('/tariff/port/checker/{buy}', [Controllers\AdminStore\TariffController::class, 'portChecker'])->name('admin.celebration.tariff.port.checker');

                /* when redirect if profile is complete and exists expire and deadline */
                Route::middleware("expire.profile")->group(function () {
                    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.celebration.dashboard');
                });
            });
        });

        Route::middleware("auth.celebration")->group(function () {
            Route::prefix("celebration")->group(function () {
                /* continue register  */
                Route::get('/profile/register', [Controllers\Celebration\ProfileController::class, 'register'])->name('celebration.profile.register');
                Route::post('/profile/register', [Controllers\Celebration\ProfileController::class, 'registerStore'])->name('celebration.profile.register.store');
//                Route::get('/celebration/details', [Controllers\Celebration\ProfileController::class, 'celebrationDetails'])->name('celebration.details');

                Route::resource('/celebrate/details', Controllers\Celebration\CelebrateDetailController::class)->names('celebration.celebrate.details');
                Route::get('/celebration/details/layout', [Controllers\Celebration\CelebrateDetailController::class, 'getLayoutType'])->name('celebration.details.layout');
                Route::get('/celebration/details/card/{celebrateDetail}', [Controllers\Celebration\CelebrateDetailController::class, 'card'])->name('celebration.details.card');
                Route::get('/celebration/test/template/{layout}', [Controllers\Celebration\CelebrateDetailController::class, 'viewTemplate'])->name('celebration.test.template');

                Route::middleware("celebration.registered")->group(function () {
                    Route::get('/dashboard', [Controllers\Celebration\DashboardController::class, 'index'])->name('celebration.dashboard');
                    Route::get('/payment/{slug}', [Controllers\Celebration\PaymentController::class, 'view'])->name('celebration.payment');
                    Route::post('/port/{slug}', [Controllers\Celebration\PaymentController::class, 'portVerify'])->name('celebration.port');
                    Route::get('/port/payment', [Controllers\Celebration\PaymentController::class, 'portPayment'])->name('celebration.port.payment');

                    Route::resource('ticket', Controllers\Celebration\TicketController::class)->names('celebration.ticket');
                    Route::post('ticket/show/store/{ticket}',[Controllers\Celebration\Cards\CelebrateSettingController::class,'update'])->name('celebration.tickets.show.store');

                    Route::middleware("card.celebrate")->group(function () {
                        Route::prefix("card/{slug}")->group(function () {
                            Route::resource('/guest', Controllers\Celebration\Cards\GuestController::class)->names('celebration.card');
                            Route::get('/guest/log/{guest}', [Controllers\Celebration\Cards\GuestController::class, 'logs'])->name('celebration.card.log');
                            Route::get('messages',[Controllers\Celebration\Cards\MemoryController::class,'index'])->name('messages.card');
                            Route::resource('galleries', Controllers\Celebration\Cards\GalleryController::class)->names('celebration.card.galleries');

                            Route::get('about/bride/groom',[Controllers\Celebration\Cards\AboutBrideGroomController::class,'index'])->name('about.bride.groom.index');
                            Route::get('about/bride/groom/introduction',[Controllers\Celebration\Cards\AboutBrideGroomController::class,'rowIntroduction'])->name('about.bride.groom.introduction');
                            Route::post('about/bride/groom',[Controllers\Celebration\Cards\AboutBrideGroomController::class,'store'])->name('about.bride.groom.store');

                            Route::get('celebrate/setting',[Controllers\Celebration\Cards\CelebrateSettingController::class,'index'])->name('celebrate.setting.index');
                            Route::post('celebrate/setting/update',[Controllers\Celebration\Cards\CelebrateSettingController::class,'update'])->name('celebrate.setting.update');

                        });
                    });

                });
            });
        });

    });

    Route::get('/{nameLink}/{slug}', [\App\Http\Controllers\Samples\TemplateController::class, 'index'])->name('main.index');
    Route::get('/{nameLink}/{slug}/about', [\App\Http\Controllers\Samples\TemplateController::class, 'about'])->name('main.about');
    Route::get('/{nameLink}/{slug}/gallery', [\App\Http\Controllers\Samples\TemplateController::class, 'gallery'])->name('main.gallery');
    Route::get('/{nameLink}/{slug}/location', [\App\Http\Controllers\Samples\TemplateController::class, 'location'])->name('main.location');
    Route::post('/{nameLink}/{slug}/survey', [\App\Http\Controllers\Samples\TemplateController::class, 'survey'])->name('main.survey');
    Route::get('/{nameLink}/{slug}/memories', [\App\Http\Controllers\Samples\TemplateController::class, 'memories'])->name('main.memories');
    Route::post('/{nameLink}/{slug}/memories', [\App\Http\Controllers\Samples\TemplateController::class, 'memoryStore'])->name('main.memory.store');


//Route::get('admin/dashboard','Admin\DashboardController@index');
//Route::get('admin/store/dashboard','AdminStore\DashboardController@index');
});


Route::get('/run-migrations', function () {
    return Artisan::call('migrate', ["--force" => true ]);
});
