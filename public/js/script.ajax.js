function ajaxJquery(elem,dataReturn = false,ajaxValid = 1,classClosets=null) {
    $('.help-block').remove();
    $('.error.err-block').remove();
    $('.error-input-border').removeClass('error-input-border');
    if ($('.nav-link').hasClass('border-danger-error')) {
        $('.nav-link').removeClass('border-danger-error');
    }
    // $("input,.select2-selection,textarea").css({
    //     "border-width": "1px",
    //     "border-color": "rgba(170, 170, 170, .3)",
    //     "border-style": "solid"
    // });
    // if (spinner != 1) {
    //     $(elem).html("<i class='fa fa-spinner fa-spin'></i>لطفا منتظر بمانید....");
    // }
    // $(elem).prop('disabled',true);
    // var elem = $(this);
    // let pic = $('#file-upload')[0].files[0];
    // console.log(pic);
    var url =$(elem).closest('form').attr('action');

    $.ajax({
        type: $(elem).closest('form').attr('method'),
        url: url,
        data: $(elem).closest('form').serialize(),
        success: function (response) {
            if (dataReturn) {
               $(dataReturn).html(response);
            }
            else {
                var res = JSON.parse(response);
                if (res.status === 100) {
                    window.location = res.url
                }
                if (res.status === 200) {

                    $('.alert-ajax').html(
                        "<div class=\"alert alert-success knob-animate\"> <i class=\"fa fa-check-circle\"></i>\n" +
                        res.message+
                        "    <button type=\"button\" class=\"close\" style=\"float: left!important;\" data-dismiss=\"alert\" aria-label=\"Close\"> <span aria-hidden=\"true\">×</span> </button>\n" +
                        "</div>"
                    );

                }
            }

        },
        error: function (xhr) {
            if (ajaxValid === 1 ){
                errorAjax(xhr,classClosets)
            }
            else {
                errorAjaxSubmit(xhr)
            }
        }
    })

}
function submitGeneralAjax( url, serialize, method = 'post') {
    $.ajax({
        type: method,
        url: url,
        data: serialize,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status === 100){
                return true;
            }
            return false;
        },
        error: function (xhr) {
            errorAjax(xhr)
        }
    })
}

/* function error data */
function errorAjax(xhr,classClosets = null){
    $('.help-block').remove();

    if ($('.nav-link').hasClass('border-danger-error')) {
        $('.nav-link').removeClass('border-danger-error');
    }
    var errorsValidation = JSON.parse(xhr.responseText).errors;
    $.each(errorsValidation, function (key, value) {
        var errArray = key.split('.');
        if (typeof errArray[1] === 'undefined') {
            if (($("[name='" + key + "']").is("select"))) {
                ($("[name='" + key + "']").closest(".form-group").append("<div class='help-block text-danger mt-2'>" + value + "</div>"));
                $("[name='" + key + "']").closest(".form-group").find('.select2-selection').addClass('error-input-border')
            } else {
                if($("[name='" + key + "']").hasClass('upload-avatar')){
                    ($("[name='" + key + "']").closest('.avatar-upload').after("<div class='help-block text-danger avatar-style mt-2'>" + value + "</div>"));
                }
                else {
                    if(classClosets){
                        ($("[name='" + key + "']").closest('.wrap-input100').after("<div class='help-block text-error-log'>" + value + "</div>"));
                    }
                    else
                    ($("[name='" + key + "']").after("<div class='help-block text-danger  mt-2'>" + value + "</div>"));
                }

                $("[name=" + "'" + errArray[0] + "']").addClass('error-input-border');
            }

            if ($("[name=" + "'" + errArray[0] + "']").closest('.tab-pane').attr('id') !== 'undefined') {
                $('#' + $("[name=" + "'" + errArray[0] + "']").closest('.tab-pane').attr('id') + "-tab").addClass('border-danger-error')
            }
        }
        else {
            if ($("[name^='" + errArray[0] + "']").is("select")) {
                ($("[name^='" + errArray[0] + "']").eq(errArray[1]).closest(".form-group").append("<div class='help-block text-danger mt-2'>" + value + "</div>"));
                ($("[name^='" + errArray[0] + "']").eq(errArray[1]).closest(".form-group").find('.select2-selection')).addClass('error-input-border')
            } else {
                ($("[name^='" + errArray[0] + "']").eq(errArray[1]).after("<div class='help-block text-danger mt-2'>" + value + "</div>"));
                $("[name^='" + errArray[0] + "']").eq(errArray[1]).addClass('error-input-border')
            }

            if ($("[name=" + "'" + errArray[0] + "']").closest('.tab-pane').attr('id') !== 'undefined') {
                $('#' + $("[name=" + "'" + errArray[0] + "']").closest('.tab-pane').attr('id') + "-tab").addClass('border-danger-error')
            }

        }
    })
}

function upload_image(clickElement, dataReturn = false,ajaxValid = 1) {
    $('.help-block').remove();
    $('input').removeClass('border border-danger');
    $("input").closest(".form-group").find('label').removeClass('text-danger font-weight-bold');

    $.ajax({
        type: $(clickElement).closest('form').attr('method'),
        url: $(clickElement).closest('form').attr('action'),
        data: new FormData($('.form')[0]),
        contentType: false,
        processData: false,
        success: function (response) {
            // try {
                var res = JSON.parse(response);
                if (res.status === 100) {
                    window.location = res.url
                }
                if (res.status === 200) {

                    $('.alert-ajax').html(
                        "<div class=\"alert alert-success knob-animate\"> <i class=\"fa fa-check-circle\"></i>\n" +
                        res.message+
                        "    <button type=\"button\" class=\"close\" style=\"float: left!important;\" data-dismiss=\"alert\" aria-label=\"Close\"> <span aria-hidden=\"true\">×</span> </button>\n" +
                        "</div>"
                    );

                }

            // } catch (e) {
            //     $('body').append(`<div class='dev-box'> <a class="closeDev">[بستن]</a> <h3>خطاهای کدنویسی</h3> ${response} </div>`);
            //
            //     $(document).on('click', '.closeDev', function () {
            //         $('.dev-box').remove();
            //     })
            // }

        },
        error: function (xhr) {
            if (ajaxValid === 1 ){
                errorAjax(xhr)
            }
            else {
                errorAjaxSubmit(xhr)
            }

        }
    })

}


function errorAjaxSubmit(xhr){
    $('.error.err-block').remove();

    if ($('.nav-link').hasClass('border-danger-error')) {
        $('.nav-link').removeClass('border-danger-error');
    }
    var errorsValidation = JSON.parse(xhr.responseText).errors;
    $.each(errorsValidation, function (key, value) {
        var errArray = key.split('.');
        if (typeof errArray[1] === 'undefined') {
            if (($("[name='" + key + "']").is("select"))) {
                ($("[name='" + key + "']").closest(".form-group").prepend("<span class='error err-block'>" + value + "</span>"));
                $("[name='" + key + "']").closest(".form-group").find('.select2-selection').addClass('error-input-border');
            } else {
                ($("[name='" + key + "']").after("<span class='error err-block'>" + value + "</span>"));
                $("[name=" + "'" + errArray[0] + "']").addClass('error-input-border');
            }

            if ($("[name=" + "'" + errArray[0] + "']").closest('.tab-pane').attr('id') !== 'undefined') {
                $('#' + $("[name=" + "'" + errArray[0] + "']").closest('.tab-pane').attr('id') + "-tab").addClass('border-danger-error')
            }
            $("[name='" + key + "']").addClass('error')
        }
        else {
            if ($("[name^='" + errArray[0] + "']").is("select")) {
                ($("[name^='" + errArray[0] + "']").eq(errArray[1]).closest(".form-group").append("<span class='error err-block'>" + value + "</span>"));
                ($("[name^='" + errArray[0] + "']").eq(errArray[1]).closest(".form-group").find('.select2-selection')).addClass('error-input-border')
            } else {
                ($("[name^='" + errArray[0] + "']").eq(errArray[1]).after("<span class='error err-block'>" + value + "</span>"));
                $("[name^='" + errArray[0] + "']").eq(errArray[1]).addClass('error-input-border')
            }

            if ($("[name=" + "'" + errArray[0] + "']").closest('.tab-pane').attr('id') !== 'undefined') {
                $('#' + $("[name=" + "'" + errArray[0] + "']").closest('.tab-pane').attr('id') + "-tab").addClass('border-danger-error')
            }
            $("[name='" + key + "']").addClass('error')

        }
    })

}




