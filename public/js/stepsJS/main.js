function wizardForm(rules = {},labels = {}) {

    (function ($) {
        "use strict";

        /*==================================================================
        [ Validate ]*/
        var input = $('.validate-input .input100');

        $('.validate-form').on('submit',function(){
            var check = true;

            for(var i=0; i<input.length; i++) {
                if(validate(input[i]) == false){
                    showValidate(input[i]);
                    check=false;
                }
            }

            return check;
        });


        $('.validate-form .input100').each(function(){
            $(this).focus(function(){
                hideValidate(this);
            });
        });

        function validate (input) {
            if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
                if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                    return false;
                }
            }
            else {
                if($(input).val().trim() == ''){
                    return false;
                }
            }
        }

        function showValidate(input) {
            var thisAlert = $(input).parent();

            $(thisAlert).addClass('alert-validate');
        }

        function hideValidate(input) {
            var thisAlert = $(input).parent();

            $(thisAlert).removeClass('alert-validate');
        }

        $.validator.addMethod("authentic_english_text", function(value, element) {
            const regex = new RegExp(/(^([a-zA-Z-_]+)(\d+)?$)/u);
            if(value) {
                return regex.test(value);
            }
            return true;
        }, 'نام لینک یک فرمت معتبر نیست.');

        $.validator.addMethod("mobile_format", function(value, element) {
            const regex = new RegExp(/(09)[0-9]{9}/);
            if(value) {
                return regex.test(value);
            }
            return true;
        }, 'فرمت موبایل درست نیست.');

        var form = $("#signup-form");
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            rules: rules,
            onfocusout: function(element) {
                $(element).valid();
            },
            highlight : function(element, errorClass, validClass) {
                $('.error.err-block').remove();
                $(element.form).find('.actions').addClass('form-error');
                $(element).removeClass('valid');
                $(element).addClass('error .err-block');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element.form).find('.actions').removeClass('form-error');
                $(element).removeClass('error');
                $(element).addClass('valid');
            }
        });

        form.steps({
            headerTag: "h3",
            bodyTag: "fieldset",
            transitionEffect: "fade",
            labels: labels,
            titleTemplate : '<div class="title"><span class="title-text">#title#</span><span class="title-number">0#index#</span></div>',
            onStepChanging: function (event, currentIndex, newIndex)
            {
                $('.field-type').val(newIndex);
                if ($(this).hasClass('next')) {
                    var type = $('.body.current').data('type');
                }

                form.validate().settings.ignore = ":disabled,:hidden";

                return form.valid();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                // alert('Sumited');
            },
            // onInit : function (event, currentIndex) {
            //     event.append('demo');
            // }
        });

    })(jQuery);

}
