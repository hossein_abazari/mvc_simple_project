$(document).ready(function () {
    $(document).on('change','.input-radio-check',function () {
        var is_checked = $(this).is(':checked');
        $('.box-card-design').removeClass('active-box');

        if (is_checked) {
            $(this).closest('.box-card-design').addClass('active-box');
        }

    });

    $(document).on('change','.input-checkbox-check',function () {
        var is_checked = $(this).is(':checked');

        if (is_checked) {
            $(this).closest('.box-card-design').addClass('active-box');
        }
        else {
            $(this).closest('.box-card-design').removeClass('active-box');
        }

    });
})

