<?php

namespace App\Console\Commands;

use App\Classes\ReadFileAndDirectory;
use App\Models\Layout;
use App\Repositories\LayoutRepository;
use App\Repositories\LayoutRepositoryInterface;
use Illuminate\Console\Command;

class GenerateTemplateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:template';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Template';

    protected $repository;
    protected $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->repository = new LayoutRepository();
        $this->file = new ReadFileAndDirectory(new Layout());
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       $results = $this->file->getReadFile('directory_id');

        foreach ($results as $key => $value){
            $value['directory_id'] = $value;
            $value['img_temp'] = $value.'.jpg';

            $this->repository->setCreate($value);
        }
        return 0;
    }
}
