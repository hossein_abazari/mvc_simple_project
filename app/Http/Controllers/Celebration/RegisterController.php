<?php

namespace App\Http\Controllers\Celebration;

use App\Http\Controllers\Controller;
use App\Http\Requests\NextRegisterRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Traits\AdminStore\RegisterTrait;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    use RegisterTrait;
    public $repository;

    /**
     * RegisterController constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Celebration.Auth.register');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NextRegisterRequest $request)
    {
        if($this->repository->setRegister($request)) {

            return json_encode([
                'status' => 100,
                'url' => $this->indexRoute()
            ]);
        }
    }

}
