<?php

namespace App\Http\Controllers\Celebration;

use App\Http\Controllers\Controller;
use App\Http\Requests\LayoutRequest;
use App\Http\Requests\NextAuthRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\Login;
use Illuminate\Support\Facades\Hash;

class oginController extends Controller
{
    use Login;


    public $redirect = '/celebration/celebrate/details';

    public function __construct()
    {
        $this->user_name = 'mobile';

        $this->middleware('guest')->except('logout');
    }
    public function showLogin(Request $request)
    {
        return view('Celebration.Auth.login');
    }

    /**
     * @param Request $request
     */
    public function login(NextAuthRequest $request)
    {
        $this->validateLogin($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        if ($this->attemptLoginUserCelebration($request)) {
            if ($this->sendLoginResponse($request)){
                return json_encode([
                    'status' => 100,
                    'url' => $this->redirect
                ]);

            }
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout()
    {
        $this->getLogout(Request());
        return redirect(route("celebration.login"));
    }

    public function nextValidate(NextAuthRequest $request)
    {
        return json_encode(['status' => 100]);
    }
}
