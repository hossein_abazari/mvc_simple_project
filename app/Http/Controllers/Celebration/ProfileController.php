<?php

namespace App\Http\Controllers\celebration;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Repositories\CelebrationRepository;
use App\Repositories\Elequents\CodeDiscountRepository;
use App\Repositories\Interfaces\CodeDiscountRepositoryInterface;
use App\Repositories\UserRepository;
use App\Traits\AdminStore\ProfileTrait;
use App\Traits\CelebrationTrait;
use App\Traits\ProfileCelebrationTrait;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    use ProfileCelebrationTrait;
    public $celebrationRepository;

    protected $userRepository;
    protected $codeDiscountRepository;

    /**
     * ProfileController constructor.
     * @param CelebrationRepository $celebrationRepository
     * @param UserRepository $userRepository
     * @param CodeDiscountRepositoryInterface $codeDiscountRepository
     */
    public function __construct(CelebrationRepository $celebrationRepository,UserRepository $userRepository,
                                CodeDiscountRepositoryInterface $codeDiscountRepository)
    {
        $this->celebrationRepository = $celebrationRepository;
        $this->userRepository = $userRepository;
        $this->codeDiscountRepository = $codeDiscountRepository;
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function register()
    {
       return view('Celebration.Profile.register');
    }

    /**
     * @param ProfileRequest $request
     */
    public function registerStore(ProfileRequest $request)
    {

        $data = array_merge($request->all(),[
            'code_discount_id' => isset($request->referral) ? $this->codeDiscountRepository->getCodeDiscountIdWithCode($request->referral) : null
        ]);

        $celebration = $this->celebrationRepository->setCreate($data);

        $this->userRepository->setUpdate([
            'celebration_id' => $celebration->id
        ],\Auth::getUser()->slug);

        flash(vl('flash','success'),'success');

        return json_encode([
        'status' => 100,
        'url' => $this->indexRouteCelebrationDetails()
         ]);
    }

}
