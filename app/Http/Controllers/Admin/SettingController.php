<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use App\Traits\SettingTrait;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    use SettingTrait;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('Admin.Settings.index');
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $this->insertUpdateSetting($request);

        flash(vl('flash','changed'),'success');

        return json_encode([
            'status' => 100,
            'url' => $this->indexRoute()
        ]);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function profile(Request $request)
    {
        $this->validateRequestProfile($request);

        $this->updateProfile($request);

        return json_encode([
            'status' => 200,
            'message' => vl('flash','success')
        ]);
    }

    /**
     * @param Request $request
     */
    public function changePassword(Request $request)
    {
        $this->validateRequestChangePassword($request);

        $this->getChangePassword($request);

        return json_encode([
            'status' => 200,
            'message' => vl('flash','passwordChanged')
        ]);
    }
}
