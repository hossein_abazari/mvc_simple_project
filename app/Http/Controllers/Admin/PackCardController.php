<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PackCardRequest;
use App\Models\PackCard;
use App\Repositories\PackCardRepository;
use App\Traits\PackCardTrait;
use Illuminate\Http\Request;

class PackCardController extends Controller
{
    use PackCardTrait;
    public $repository;

    public function __construct(PackCardRepository $repository)
    {
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packCards = $this->repository->getPaginate();

        return view('Admin.PackCards.index',compact('packCards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackCardRequest $request)
    {
        $this->repository->setCreate($request->all());

        flash(vl('flash','success'),'success');

        return json_encode([
            'status' => 100,
            'url' => $this->indexRoute()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PackCard  $packCard
     * @return \Illuminate\Http\Response
     */
    public function show(PackCard $packCard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PackCard  $packCard
     * @return \Illuminate\Http\Response
     */
    public function edit(PackCard $packCard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PackCard  $packCard
     * @return \Illuminate\Http\Response
     */
    public function update(PackCardRequest $request, $packCard)
    {
        $this->repository->setUpdate($request,$packCard);

        flash(vl('flash','edited'),'success');

        return json_encode([
            'status' => 100,
            'url' => $this->indexRoute()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PackCard  $packCard
     * @return \Illuminate\Http\Response
     */
    public function destroy($packCard)
    {
        $this->repository->setDelete($packCard);

        flash(vl('flash','deleted'),'success');

        return json_encode([
            'status' => 1,
            'url' => $this->indexRoute()
        ]);
    }
}
