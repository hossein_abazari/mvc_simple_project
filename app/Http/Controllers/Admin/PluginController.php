<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Plugin;
use App\Traits\PluginTrait;
use App\Traits\ToolTrait;
use App\Traits\UserTrait;
use Illuminate\Http\Request;

class PluginController extends Controller
{
    use PluginTrait;
    use ToolTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plugins = Plugin::latest()->paginate($this->countPaginate);
        return view('Admin.Plugins.index',compact('plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Celebration a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        Plugin::create($request->all());

        flash(vl('flash','success'),'success');

        return json_encode([
            'status' => 100,
            'url' => $this->indexRoute()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plugin  $plugin
     * @return \Illuminate\Http\Response
     */
    public function show(Plugin $plugin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plugin  $plugin
     * @return \Illuminate\Http\Response
     */
    public function edit(Plugin $plugin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Plugin  $plugin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $plugin)
    {
        $this->validateRequest($request);
        $plugin = Plugin::whereSlug($plugin)->first();

        $plugin->update($request->all());

        flash(vl('flash','edited'),'success');

        return json_encode([
            'status' => 100,
            'url' => $this->indexRoute()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plugin  $plugin
     * @return \Illuminate\Http\Response
     */
    public function destroy($plugin)
    {
        $plugin = Plugin::whereSlug($plugin)->first();

        $plugin->delete();

        flash(vl('flash','deleted'),'success');

        return json_encode([
            'status' => 1,
            'url' => $this->indexRoute()
        ]);
    }
}
