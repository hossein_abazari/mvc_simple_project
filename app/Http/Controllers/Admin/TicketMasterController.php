<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TicketDetail;
use App\Models\TicketMaster;
use App\Traits\TicketMasterTrait;
use App\Traits\ToolTrait;
use Illuminate\Http\Request;

class TicketMasterController extends Controller
{
    use ToolTrait;
    use TicketMasterTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ticketMasters = TicketMaster::latest();

        $this->search(Request(),$ticketMasters);

        $ticketMasters = $ticketMasters->paginate($this->countPaginate);

        return view('Admin.TicketMasters.index',compact('ticketMasters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Celebration a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TicketMaster  $ticketMaster
     * @return \Illuminate\Http\Response
     */
    public function show($ticketMaster)
    {
       $ticketMaster = TicketMaster::whereSlug($ticketMaster)->first();

       $ticketDetails = TicketDetail::whereTicketMasterId($ticketMaster->id)->get();

       $this->viewTicket($ticketMaster);

       return view('Admin.TicketMasters.show',compact('ticketDetails','ticketMaster'));

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\TicketMaster $ticketMaster
     * @param Request $request
     * @return void
     */
    public function showStore($ticketMaster,Request $request)
    {
       $this->validateRequest($request);

        $modelDetails = $this->insertDetails($ticketMaster,$request);

        return view('Generals.listMessage',compact('modelDetails')) ;

       return json_encode(['ticket'=>true,'data'=>'']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TicketMaster  $ticketMaster
     * @return \Illuminate\Http\Response
     */
    public function edit(TicketMaster $ticketMaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TicketMaster  $ticketMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TicketMaster $ticketMaster)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TicketMaster  $ticketMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(TicketMaster $ticketMaster)
    {
        //
    }

}
