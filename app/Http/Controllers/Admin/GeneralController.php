<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Deadline;
use App\Models\Celebration;
use App\Traits\GeneralTrait;
use App\Traits\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Response;
use function GuzzleHttp\Psr7\try_fopen;

class GeneralController extends Controller
{
    use GeneralTrait;
    /**
     * @param Request $request
     * @return Response
     *
     * set cookie for colors theme
     */
    public function themeCookie(Request $request)
    {
        Cookie::queue('colors', $request->colors, 525600);

        return true;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function editModal(Request $request)
    {
        $modal = json_decode($request->modal,true);

        $data = $this->getDataModal($request,$modal);

        $nameModal = 'update-modal';
        $mode = 'update';

        return view('Admin.Generals.modal',compact('modal','data','nameModal','mode'));
    }

    public function language(Request $request)
    {
        $this->updateLanguage($request);

        flash(vl('flash','changed'),'success');

        return back();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * search for stores
     */
    public function storeSearch(Request $request)
    {
        $stores = Celebration::latest()->where('name','like','%'.$request->search_store.'%')->get();

        if ($stores->count() == 1){
            return $this->redirectSearchStoreDetail($stores);
        }
        else{
            return $this->redirectSearchStoreIndex($request);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        $data = Celebration::select("name")
            ->where("name","LIKE","%{$request->input('query')}%")
            ->get();

        return response()->json($data);
    }

}
