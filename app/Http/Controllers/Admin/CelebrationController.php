<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Deadline;
use App\Models\Invoice;
use App\Models\CelebrationSetting;
use App\Models\TicketMaster;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Celebration;
use App\Rules\SlugExistsValidate;
use App\Traits\DeadlineTrait;
use App\Traits\CelebrationTrait;
use App\Traits\ToolTrait;
use App\Traits\UserLoginTrait;
use Illuminate\Http\Request;

class CelebrationController extends Controller
{
    use CelebrationTrait;
    use ToolTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $celebrations = Celebration::with('celebrationSetting')->latest();

        $celebrations = $this->search(Request(),$celebrations);

        $celebrations = $celebrations->paginate($this->countPaginate);

        return view('Admin.Celebrations.index',compact('celebrations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Celebration a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $this->insertCelebration($request);

        flash(vl('flash','success'),'success');

        return json_encode([
            'status' => 100,
            'url' => $this->indexRoute()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Celebration $celebration
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show($celebration,Request $request)
    {
        $celebration = Celebration::whereSlug($celebration)->first();

        $storeSetting = $celebration->celebrationSetting;

        $users = $celebration->users()->get();

        $countLimit = 20;

        $transactions = Transaction::leftJoin('transactions', function ($join) {
            $join->on('transactions.transaction_id', '=', 'transactions.id');
        })->where('transactions.celebration_id', $celebration->id)->orderByDesc('id')->paginate($countLimit);

        $invoiceStores = Invoice::whereCelebrationId($celebration->id)->latest()->paginate($countLimit);

        $ticketMasters = TicketMaster::latest()->whereCelebrationId($celebration->id)->paginate($countLimit);

        $autoCode = $this->autoCode('invoice','invoice_code');

        $active = $this->getSection($request);


        return view('Admin.Celebrations.show',compact('celebration',
            'celebrationSetting',
            'users',
            'transactions',
            'invoices',
            'ticketMasters',
            'autoCode',
            'active'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Celebration  $celebration
     * @return \Illuminate\Http\Response
     */
    public function edit(Celebration $celebration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Celebration  $celebration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$celebration)
    {
        $this->validateUpdateRequest($request,$celebration);

        $this->updateCelebration($request,$celebration);

        flash(vl('flash','edited'),'success');

        return json_encode([
            'status' => 100,
            'url' => $this->indexRoute()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Celebration $celebration
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($celebration)
    {
        $this->deleteCelebration($celebration);

        flash(vl('flash','deleted'),'success');

        return json_encode([
            'status' => 1,
            'url' => $this->indexRoute()
        ]);
    }

    public function amountDeadline(Request $request)
    {
        return $this->showAmountDeadline($request);
    }
}
