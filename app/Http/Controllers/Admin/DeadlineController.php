<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Deadline;
use App\Traits\ToolTrait;
use App\Traits\DeadlineTrait;
use Illuminate\Http\Request;
use function Ramsey\Uuid\v1;


class DeadlineController extends Controller
{
    use DeadlineTrait;
    use ToolTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deadlines = Deadline::latest()->paginate($this->countPaginate);
        return view('Admin.Deadlines.index',compact('deadlines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        Deadline::create($request->all());

        flash(vl('flash','success'),'success');

        return json_encode([
            'status' => 100,
            'url' => $this->indexRoute()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Deadline  $deadline
     * @return \Illuminate\Http\Response
     */
    public function show(Deadline $deadline)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Deadline  $deadline
     * @return \Illuminate\Http\Response
     */
    public function edit(Deadline $deadline)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Deadline  $deadline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$deadline)
    {
        $this->validateRequest($request);
        $deadline = Deadline::whereSlug($deadline)->first();

        $deadline->update($request->all());

        flash(vl('flash','edited'),'success');

        return json_encode([
            'status' => 100,
            'url' => $this->indexRoute()
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Deadline  $deadline
     * @return \Illuminate\Http\Response
     */
    public function destroy($deadline)
    {
        $deadline = Deadline::whereSlug($deadline)->first();

        $deadline->delete();

        flash(vl('flash','deleted'),'success');

        return json_encode([
            'status' => 1,
            'url' => $this->indexRoute()
        ]);
    }
}
