<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\AdminDashboard;
use OwenIt\Auditing\Models\Audit;

class DashboardController extends Controller
{
    use AdminDashboard;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $topStatistics = $this->topStatistics();
        $lastData = $this->lastData();

        return view('Admin.Dashboard.index' , compact('topStatistics','lastData'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function chart(Request $request)
    {
        $statisticChart = $this->statisticChart()[$request->strData];

        return json_encode([
            'date' => $statisticChart->keys(),
            'result' => $statisticChart->values(),
        ]);

    }
}
