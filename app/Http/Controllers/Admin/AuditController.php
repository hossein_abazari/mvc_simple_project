<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\AuditTrait;
use App\Traits\ToolTrait;
use Illuminate\Http\Request;
use OwenIt\Auditing\Models\Audit;

class AuditController extends Controller
{
    use AuditTrait;
    use ToolTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $audits = Audit::latest();

        $audits = $this->search(Request() , $audits);

        $audits = $audits->paginate($this->countPaginate);

        return view('Admin.Audits.index',compact('audits'));

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return void
     */
    public function view(Request $request)
    {
        $audit = Audit::whereSlug($request->slug)->first();

        $user = User::whereId($audit->user_id)->first();

        $userAgent =  $this->getUserAgent($audit->user_agent);

        $created_at = $this->formatTypeDate($audit->created_at,'Y/m/d H:i:s');

        return view('Admin.Audits.viewModal',compact('audit','user' , 'userAgent','created_at'));
    }
}
