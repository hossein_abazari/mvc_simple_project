<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Traits\InvoiceTrait;
use App\Traits\CelebrationTrait;
use App\Traits\ToolTrait;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    use ToolTrait;
    use InvoiceTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::latest();

        $invoices = $this->search(Request(),$invoices);

        $invoices = $invoices->paginate($this->countPaginate);

        $autoCode = $this->autoCode('Invoice','invoice_code');

        return view('Admin.Invoices.index',compact('invoices','autoCode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $url = explode('?',\URL::previous());

        $this->validateRequest($request);

        $this->insertStore($request);

        flash(vl('flash','success'),'success');

        return json_encode([
            'status' => 100,
            'url' => $url[0]."?section=invoices"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoiceStore
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoiceStore)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoice  $invoiceStore
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoiceStore)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $invoiceStore
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$invoiceStore)
    {
        $invoiceStore = Invoice::whereSlug($invoiceStore)->first();

        $this->validateRequest($request,$invoiceStore);


        $invoiceStore->update($request->all());

        flash(vl('flash','edited'),'success');

        return json_encode([
            'status' => 100,
            'url' => $this->indexRoute()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $invoiceStore
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoiceStore)
    {
        //
    }
}
