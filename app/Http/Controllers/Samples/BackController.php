<?php

namespace App\Http\Controllers\Samples;

use App\Http\Controllers\Controller;
use App\Models\Guest;
use App\Models\Memory;
use App\Repositories\GuestRepository;
use App\Repositories\MemoryRepository;
use Illuminate\Http\Request;

class BackController extends Controller
{
    private $repository;
    private $repositoryMemory;

    public function __construct(GuestRepository $guestRepository , MemoryRepository $memoryRepository)
    {
        $this->repository = $guestRepository;
        $this->repositoryMemory = $memoryRepository;
    }


    public function invite()
    {
        $guests = $this->repository->getSearch(Request());
        $guestsSum = $this->repository->getSearch(Request(),'get')->sum('count_family');
        return view('Samples.Sample1.back.invite',compact('guests','guestsSum'));
    }

    public function inviteStore(Request $request)
    {
        $request->validate([
            'slug' =>'required|unique:guests',
            'name_fa' => 'required'
        ]);

        $this->repository->setCreate($request);

        flash(vl('flash','success'),'success');

        return json_encode([
            'status' => 100,
            'url' => back()->getRequest()->header()['referer'][0]
        ]);

    }

    public function editInvite(Request $request)
    {
        $guest = Guest::whereId($request->id)->first();
        $title= 'ویرایش '.$guest->name_fa;
        return view('Samples.Sample1.back._modal_invite',compact('title','guest'));
    }

    public function updateInvite(Request $request,Guest $guest)
    {
        $request->validate([
            'slug' =>'required|unique:guests,id,'.$guest->id.',id',
            'name_fa' => 'required'
        ]);

        $this->repository->setUpdate($request,$guest);

        flash(vl('flash','success'),'success');

        return json_encode([
            'status' => 100,
            'url' => back()->getRequest()->header()['referer'][0]
        ]);
    }

    public function deleteInvite(Guest $guest)
    {
        $guest->delete();

        flash('flash','deleted');
        return back();
    }

    public function memoriesInvited(Guest $guest)
    {
        return view('Samples.Sample1.back.memories_invite',compact('guest'));
    }

    public function logs(Guest $guest,Request $request)
    {
        $audits = $guest->audites()->latest()->paginate('20');
        return view('Samples.Sample1.back.logs',compact('audits','guest'));

    }

    public function getAllMessages()
    {
        $memories = Memory::latest()->orderBy('guest_id','asc')->get();

        return view('Samples.Sample1.back.all_messages',compact('memories'));
    }
}
