<?php

namespace App\Http\Requests;

use App\Models\CelebrateDetail;
use App\Repositories\CelebrateDetailRepository;
use Illuminate\Foundation\Http\FormRequest;

class AboutBrideGroomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $celebrateDetailRepository = new CelebrateDetailRepository(new CelebrateDetail());
//        $celebrateDetail = $celebrateDetailRepository->getFirst(\Request()->slug);
//        $aboutBrideGroom = $celebrateDetail->aboutBrideGroom;
//        if ($aboutBrideGroom){
//            return [
//                'img_groom' =>'mimes:jpeg,png,bmp,gif|file|min:2|max:1000|dimensions:ratio=2/2',
//                'img_bride' =>'mimes:jpeg,png,bmp,gif|file|min:2|max:1000|dimensions:ratio=2/2',
//            ];
//        }
        return [
            'img_groom' =>'mimes:jpeg,png,bmp,gif|file|min:2|max:1000|dimensions:ratio=2/2',
            'img_bride' =>'mimes:jpeg,png,bmp,gif|file|min:2|max:1000|dimensions:ratio=2/2',
            'name_groom' => 'required',
            'name_bride' => 'required',
        ];
    }
}
