<?php

namespace App\Http\Requests;

use App\Models\CelebrateDetail;
use App\Repositories\CelebrateDetailRepository;
use App\Rules\CountModelValidate;
use Illuminate\Foundation\Http\FormRequest;

class GalleryRequest extends FormRequest
{
    public $permittedCount = 4;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $celebrateDetail = (new CelebrateDetailRepository(new CelebrateDetail()))->getFirst(\Request()->slug);
        $countGalleries = $celebrateDetail->galleries()->count();
        if ((new CountModelValidate($countGalleries,$this->permittedCount))->passes('','')){
            return [
                'title' =>['required'],
                'file' =>'required|mimes:jpeg,png,bmp,gif|file|min:2|max:1000',
            ];
        }

        return [
            'title' =>[new CountModelValidate($countGalleries,$this->permittedCount)],
        ];

    }
}
