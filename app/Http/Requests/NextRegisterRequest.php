<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NextRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = [];
        if (\Request()->type == 1)
        {
            $request['mobile'] = 'required|regex:/(09)[0-9]{9}/|digits:11';

        }
        if (\Request()->type == 2)
        {
            $request['password'] = 'required|string|email|unique:users|max:255';
        }

        if (\Request()->type == 3 || !\Request()->type){
            return [
                'password' => 'required|string|min:6|confirmed',
                'mobile' => 'required|regex:/(09)[0-9]{9}/|digits:11|unique:users'
            ];
        }

        return $request;

    }
}
