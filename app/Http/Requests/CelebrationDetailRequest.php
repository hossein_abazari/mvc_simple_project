<?php

namespace App\Http\Requests;

use App\Models\CelebrateDetail;
use App\Rules\SlugExistsValidate;
use App\Rules\TypeStringValidate;
use Illuminate\Foundation\Http\FormRequest;

class CelebrationDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_celebrate' => ['required',new TypeStringValidate(CelebrateDetail::getArrayTypes())],
            'name' => 'required',
            'layout_id' => ['required',new SlugExistsValidate('Layout')],
            'number_invitation_card' => [new SlugExistsValidate('PackCard')],
            'link_name' => 'required|regex:/(^([a-zA-Z-_]+)(\d+)?$)/u|unique:celebrate_details',
        ];
    }
}
