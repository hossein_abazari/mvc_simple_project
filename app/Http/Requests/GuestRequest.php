<?php

namespace App\Http\Requests;

use App\Rules\CountBuyInvitationCardRule;
use App\Rules\EnglishCharacter;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class GuestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ((new CountBuyInvitationCardRule())->passes('','')){
            return [
            'slug_name' =>['required',
                'unique:guests,slug,null,id',
                'regex:/(^([a-zA-Z-_]+)(\d+)?$)/u'
                ],
                'name' => ['required'],
                'count_family' => ['required','numeric'],
            ];
        }

        return [
            'name' => [new CountBuyInvitationCardRule()],

        ];
    }
}
