<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CelebrateSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'background_img' =>'mimes:jpeg,png,bmp,gif|file|min:2|max:3000',
//            'name_logo' =>'regex:/(^[A-Za-z0-9 ]+$)+/',
            'date_ceremony' => 'required|date_format:Y/m/d',
            'time_ceremony' => 'required|date_format:H:i',
            'name' => 'required',
            'venue_ceremony' => 'required',
        ];
    }
}
