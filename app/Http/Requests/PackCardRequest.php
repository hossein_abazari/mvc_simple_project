<?php

namespace App\Http\Requests;

use App\Rules\SlugExistsValidate;
use Illuminate\Foundation\Http\FormRequest;

class PackCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
//            'slug' => ['required',new SlugExistsValidate('PackCard')],
            'number_card'=>'numeric',
            'amount'=>'nullable|regex:/^[0-9\.,]+$/',
        ];
    }
}
