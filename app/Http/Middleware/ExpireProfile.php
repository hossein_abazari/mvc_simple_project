<?php

namespace App\Http\Middleware;

use App\Models\CelebrationSetting;
use App\Traits\DeadlineTrait;
use Closure;
use Illuminate\Http\Request;

class ExpireProfile
{
    use DeadlineTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = \Auth::user();
        $storeSetting = CelebrationSetting::whereCelebrationId($user->celebration_id)->first();
        if ($storeSetting) {

            if ($this->getExpireDate($storeSetting)) {
                return $next($request);
            }
            else{
                return redirect(route('admin.celebration.tariff'));
            }

        }
        else{
            return redirect(route('admin.celebration.profile'));
        }
    }
}
