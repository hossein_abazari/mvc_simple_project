<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthCelebration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = \Auth::user();
        if ( $user->type == 'celebration') {
            return $next($request);
        }

        return abort(404);
    }
}
