<?php

namespace App\Http\Middleware;

use App\Http\Requests\CardCelebrateMiddlewareRequest;
use App\Repositories\CelebrateDetailRepositoryInterface;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class CardCelebrateMiddleware
{
    public $celebrateDetailRepository;

    public function __construct(CelebrateDetailRepositoryInterface $celebrateDetailRepository)
    {
        $this->celebrateDetailRepository = $celebrateDetailRepository;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $celebrateDetail = $this->celebrateDetailRepository->getFirst($request->slug);


        if($celebrateDetail){
            $totalAmount = $celebrateDetail->transactionDetail()->sum('total_amount');

            $celebrateDate = $celebrateDetail->whereSlug($request->slug)
                ->where('created_at','<=',Carbon::now()->subDays(1))
                ->get()
                ->isEmpty();

            if($totalAmount <= 0 || $celebrateDate)
            {
                return $next($request);
            }
            else
            {
                return redirect(route('celebration.payment',['slug'=>$request->slug]));
            }
        }

        abort('404');

    }
}
