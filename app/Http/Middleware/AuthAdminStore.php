<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthAdminStore
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        if (! $request->expectsJson()) {
//            return route('admin.celebration.show.login');
//        }

        $user = \Auth::user();
        if ( $user->type == 'celebrationAdmin') {
            return $next($request);
        }

        return abort(404);
    }
}
