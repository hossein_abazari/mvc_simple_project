<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;
use Illuminate\Http\Request;

class AuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        if (! $request->expectsJson()) {
//            return route('admin.show.login');
//        }
        $user = \Auth::user();

        if ($user->celebration_id == null && $user->type == 'admin') {
            return $next($request);
        }

        return abort(404);
    }
}
