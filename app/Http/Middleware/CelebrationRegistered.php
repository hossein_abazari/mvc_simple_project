<?php

namespace App\Http\Middleware;

use App\Models\CelebrateDetail;
use App\Repositories\CelebrateDetailRepository;
use App\Repositories\CelebrationRepository;
use Closure;
use Illuminate\Http\Request;

class CelebrationRegistered
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = \Auth::getUser();
        $celebrationDetailsRepository = new CelebrateDetailRepository(new CelebrateDetail());

        if ($user->celebration_id) {
            $celebrationDetail = $celebrationDetailsRepository->getWithCelebrationId($user->celebration_id);
            if ($celebrationDetail) {
                return $next($request);
            }
            return redirect(route('celebration.celebrate.details.create'));
        }

        return redirect(route('celebration.profile.register'));
    }
}
