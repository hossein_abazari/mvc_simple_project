<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;
use Illuminate\Http\Request;

class LanguageSetting
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $setting = Setting::latest()->first();

        \App::setLocale($setting ? $setting->locale : 'fa');

        \Artisan::call('cache:clear');

        return $next($request);
    }
}
