<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class HashOldPasswordValidate implements Rule
{
    public $authPassword;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->authPassword = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(\Hash::check($value,$this->authPassword)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return vl('general','incorrectPassword');
    }
}
