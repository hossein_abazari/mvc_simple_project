<?php

namespace App\Rules;

use App\Models\CodeDiscount;
use Illuminate\Contracts\Validation\Rule;

class ExistReferalCedeDiscountRule implements Rule
{
    protected $attributes;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attributes = $attribute;

        $codeDiscount = CodeDiscount::whereCodeDiscount($value)->first();

        if ($codeDiscount){
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return vl('flash',$this->attributes.'_validate');
    }
}
