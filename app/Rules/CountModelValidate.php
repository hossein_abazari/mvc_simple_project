<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CountModelValidate implements Rule
{
    public $count;
    public $permittedCount;

    /**
     * Create a new rule instance.
     *
     * @param $count
     * @param $permittedCount
     */
    public function __construct($count,$permittedCount)
    {
        $this->count = $count;
        $this->permittedCount = $permittedCount;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->count >= $this->permittedCount){
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return vl('general','notPermittedCount');
    }
}
