<?php

namespace App\Rules;

use App\Models\Deadline;
use Illuminate\Contracts\Validation\Rule;

class SlugExistsValidate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $model = '\App\Models\ '.$this->model;
        $model = str_replace(' ','',$model);
        $models = $model::whereSlug($value)->get();
        if ($models->isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return vl('general','slugRuleValidate');
    }
}
