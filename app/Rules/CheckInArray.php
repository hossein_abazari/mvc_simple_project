<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckInArray implements Rule
{
    private $array;

    private string $attribute;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $array)
    {
        $this->array = $array;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $bank_number = str_split($value, 6);

        return in_array($bank_number[0], $this->array);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->attribute.__(' is not valid.');
    }
}
