<?php

namespace App\Rules;

use App\Models\CelebrateDetail;
use App\Models\CountBuyInvitationCard;
use App\Repositories\CountBuyInvitationCardRepository;
use App\Repositories\CountBuyInvitationCardRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class CountBuyInvitationCardRule implements Rule
{

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $celebrateDetail = CelebrateDetail::whereSlug(\Request()->slug)->first();

        $countBuyInvitationCard = CountBuyInvitationCard::whereCelebrateDetailId($celebrateDetail->id);
        $count = $countBuyInvitationCard->sum('number_card');
        if ($countBuyInvitationCard->first()){
            if ($count <= 0){
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return vl('general','validMessageCard');
    }
}
