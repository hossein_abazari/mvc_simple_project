<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TypeStringValidate implements Rule
{
    public $arrayType;
    /**
     * Create a new rule instance.
     *
     * @param $arrayType
     */
    public function __construct($arrayType)
    {
        $this->arrayType = $arrayType;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (in_array($value,$this->arrayType)){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return vl('flash','typeString');
    }
}
