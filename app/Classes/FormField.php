<?php


namespace App\Classes;


class FormField
{
    /**
     * @param $name
     * @param $type
     * @param string $value
     * @param string $class
     * @param string $classInput
     * @param bool $placeholder
     * @param array $option [] add new attribute for input like ['data-id'=>'','label' =>true or false  ]
     * @param bool $label
     * @param bool $classForm
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * form for view
     * sample field => {{ \App\Classes\FormField::field('name','type','value','class','classInput','placeholder') }}
     */
    public static function field($name,$type,$value='',$class='',$classInput='form-control',$placeholder = false,$option =[],$label=true,$classForm=false)
    {
        $input = [
            $name,
            $type,
            'value'=>$value,
            'class'=>$class,
            'classInput'=>$classInput,
            'label'=>$label,
            'classForm'=>$classForm
        ];

        if ($placeholder){

            $input['placeholder'] = $placeholder;

        }

        $input = array_merge($input,$option);

        return view('Form.field',compact('input'));
    }

    /**
     * @param $name
     * @param array $value
     * @param string $class
     * @param string $classInput
     * @param bool $placeholder
     * @param array $option add new attribute for input like ['data-id'=>'','label' =>true or false  ]
     * @param bool $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * sample select =>  {{ \App\Classes\FormField::fieldSelect('name',['fa'=>'Persian','en'=>'English'],'class','classInput','placeholder',['multiple'=>'multiple']) }}
     */
    public static function fieldSelect($name,$value = [],$class='',$classInput='form-control',$placeholder = false,$option =[],$model = false)
    {
        $input = [$name,'value'=>$value,'class'=>$class,'classInput'=>$classInput];

        if ($placeholder){
            $input['placeholder'] = $placeholder;
        }

        $input = array_merge($input,$option);

        return view('Form.select',compact('input','model'));
    }

    /**
     * @param $titleButton
     * @param string $classInput
     * @param string $class
     * @param bool $ajax
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public static function fieldButton($titleButton, $classInput = 'btn btn-info', $class = 'col-sm-12', $ajax=true)
    {
        return view('Form.button',compact('titleButton','classInput','class','ajax'));
    }

    /**
     * @param $action
     * @param $method
     * @param string $class
     * @param string $id
     * @param bool $autocomplete
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public static function openForm($action,$method,$class='form-horizontal',$id='',$autocomplete = 'on' )
    {
        return view('Form.openForm',compact('action','method','class','id','autocomplete'));
    }

    public static function endForm()
    {
        return view('Form.endForm');
    }

    /**
     * @param $name
     * @param string $value
     * @param string $class
     * @param string $classInput
     * @param string $placeholder
     * @param array $option
     * @param bool $label
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * sample textarea =>   {{ \App\Classes\FormField::fieldTextarea('name','','col-md-6','form-control','localeSelectEnter',['rows'=>2]) }}
     */
    public static function fieldTextarea($name, $value='', $class='', $classInput='form-control', $placeholder = '', $option =[],$label=false)
    {
        $input = [$name,
            'value'=>$value,
            'class'=>$class,
            'classInput'=>$classInput,
            'placeholder'=>$placeholder,
            'label'=>$label
        ];

        $input = array_merge($input,$option);

        return view('Form.textarea',compact('input'));
    }

    /**
     * @param $href
     * @param $value
     * @param string $classLink
     * @param null $classIcon
     * @param null $formId
     * @param null $url
     * @param null $method
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public static function setLink($href, $value, $classLink='', $classIcon = null, $formId = null , $url=null , $method=null ){

        return view('Form.setLink',compact('href','value','classLink','classIcon','formId','url','method'));

    }

    /**
     * @param $name
     * @param string $value
     * @param string $class
     * @param string $classInput
     * @param array $option
     * @param string $label
     * @param bool $classForm
     * @param bool $checked
     * @param string $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public static function fieldCheckbox($name, $value='', $class='', $classInput='', $option =[], $label='', $classForm = false, $checked = false, $type='checkbox')
    {
        $input = [
            $name,
            'value'=>$value,
            'class'=>$class,
            'classInput'=>$classInput,
            'label'=>$label,
            'classForm'=>$classForm,
            'checked'=>$checked,
            'type'=>$type,
        ];


        $input = array_merge($input,$option);

        return view('Form.checkbox',compact('input'));
    }

    /**
     * @param $name
     * @param string $class
     * @param array $option
     * @param bool $classForm
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public static function fieldUploaderFile($name, $class='', $option =[], $classForm=false)
    {

        $input = [
            $name,
            'class'=>$class,
            'classForm'=>$classForm
        ];


        $input = array_merge($input,$option);

        return view('Form.uploader',compact('input'));

    }

    public static function fieldUploaderAvatar($name, $class='', $option =[], $classForm=false)
    {

        $input = [
            $name,
            'class'=>$class,
            'classForm'=>$classForm
        ];


        $input = array_merge($input,$option);

        return view('Form.avatar',compact('input'));

    }

}
