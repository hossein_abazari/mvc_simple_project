<?php


namespace App\Classes;


use App\Repositories\LayoutRepository;

class TemplateLayout
{
    protected $repository;

    /**
     * TemplateLayout constructor.
     * get repository
     *
     * @param LayoutRepository $repository
     */
    public function __construct()
    {
        $this->repository = new LayoutRepository();
    }

    /**
     * set box for layout
     *
     * @param null $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getSamples($type=null)
    {
        $layouts = $this->repository->getColumnTypeAll($type);

        return view('Classes.templateLayout.samples',compact('layouts'));
    }

    public function getTemplate()
    {

    }
}
