<?php


namespace App\Classes;


class GalleryTools
{
    /**
     * @param $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public static function getCardLeftBottom($model)
    {
        return view('Celebration.Cards.Galleries._left_card_button',compact('model'));
    }

    public static function getDirectoryImage($model)
    {
        $layoutId = $model->celebrateDetail->layouts()->first()->directory_id;

        $celebrateId = $model->celebrate_detail_id;

        return $layoutId.'/'.$celebrateId.'/';
    }
}
