<?php


namespace App\classes;

use App\Models\Guest;
use App\Models\Survey;
use App\Repositories\GuestRepository;
use App\Repositories\LayoutRepository;
use App\Traits\ToolTrait;
use Illuminate\Support\Facades\Route;

class Helper
{
    use ToolTrait;

    public static function getSelfGuest()
    {
        $guestRepository = new GuestRepository(new Guest());
        $guest = $guestRepository->getModel(request()->route('slug'))->GetCelebrateDetail(['link_name',request()->route('nameLink')])->first();

        return $guest;
    }

    public static function printName ()
    {
        return self::getSelfGuest()->name." ".self::getSelfGuest()->extra_name;
    }

    public static function printDateTime($date,$format='Y-m-d H:i:s')
    {
        $self = new self();

        return $self->formatTypeDate($date,$format);

    }

    public static function getCardRightBottom($model)
    {
        return view('Celebration.Cards.guests._right_card_button',compact('model'));
    }

    public static function getCardLeftBottom($model)
    {
        return view('Celebration.Cards.guests._left_card_button',compact('model'));
    }

    public static function guestTextAlert ($guest)
    {
       $survey = \App\models\Survey::whereGuestId($guest->id)->first();

        return view('Classes.helper.guestText',compact('survey'));
    }

    public static function layoutDirectoryId ($slug)
    {
        $layoutRepository = new LayoutRepository();
        return $layoutRepository->getFirst($slug)->directory_id;
    }

     /**
     * Is Card celebrateDetail
     *
     * @return bool
     */
    public static function isCard()
    {
        return in_array("card.celebrate", Route::getCurrentRoute()->getAction('middleware'));
    }

    /**
     * @param $stringMode
     * @return mixed
     */
    public static function setMenuCard ($stringMode = '')
    {
        $stringMode = ucwords($stringMode);
        $menu = 'getUserCelebration'.$stringMode;
        if (self::isCard()){
            $menu = 'getUserCelebrateDetail'.$stringMode;
        }
        return \App\Classes\Menu::{$menu}();
    }

}
