<?php


namespace App\Classes;


use App\Models\Setting;
use App\Models\TicketDetail;
use Illuminate\Support\Facades\Cookie;

class BladeCode
{
    /**
     * @param $name
     * @return string
     */
    public static function getTheme ($name)
    {
        $colors = Cookie::get('colors');

        $string = '';
        if ($colors == $name){
            $string = 'working';
        }
        if ($colors == null){
            if ($name == 'blue'){
                $string = 'working';
            }
        }

        return $string;
    }

    /**
     * @param $section
     * @return string
     */
    public static function countSidebar($section)
    {
        $count = 0;
        if ($section == 'tickets'){
           $count = TicketDetail::where('user_id','!=' , \Auth::id())->whereHas('user',function ($q){
                $q->where('type','!=','admin');
            })->whereView(false)->count();
        }

        $result = $count > 0 ? '<span class="label label-rounded label-success">'.$count.'</span>' : '';
        return $result;
    }

    /**
     * @return string
     */
    public static function getLanguageSetting()
    {
        $setting = Setting::latest()->first();

        $languages = json_decode($setting->language);

        $locale = $setting->locale;

        $data = "";

        foreach ($languages as $language){

            if ($locale != $language){

                $data .= FormField::setLink('#',
                    vl('general',$language),
                    'dropdown-item',
                    'flag-icon flag-icon-'.vl('variable','countryLanguage',$language),
                    'form-language' ,
                     route('admin.general.language',['locale'=>$language]),
                    'POST');
            }

        }

        return $data;

    }

    /**
     * @return array|bool|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public static function getSelectLanguage()
    {
        $setting = Setting::latest()->first();
        $locale = $setting->locale;

        return vl('variable','countryLanguage',$locale);

    }

    /**
     * @return int
     */
    public static function countLanguage()
    {
        $setting = Setting::latest()->first();

        $languages = json_decode($setting->language);


        return $languages ? count($languages) : 0;
    }

    public static function getStatisticArrayName()
    {
        return [
            'storesCount'=>['count',route('admin.celebration.index')],
            'usersCount'=>['count',route('admin.user.index')],
            'invoicesCount'=>['count',route('admin.invoice.index')],
            'invoicesSum'=>['sum',route('admin.invoice.index')],
            'transactionDebtSum'=>['sum'],
            'transactionDebtCount'=>['sum'],
            'transactionCredCount'=>['count'],
            ];
    }

    public static function buttonPayment()
    {
        return '1';
    }

}
