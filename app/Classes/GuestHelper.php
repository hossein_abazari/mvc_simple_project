<?php


namespace App\Classes;


use App\Builder\TemplateBuilder;
use App\Builder\TemplateBuilderInterface;

class GuestHelper
{
    public $templateBuilder;
    public $guest;
    public $directoryId;
    public $directory;

    public function __construct(TemplateBuilder $templateBuilder)
    {
        $this->templateBuilder = $templateBuilder;

        $this->guest = $templateBuilder->getGuestWithNamelink($templateBuilder->getRequestInput('nameLink'),
            $templateBuilder->getRequestInput('slug'));

        $this->directoryId = $templateBuilder->getDirectoryId($this->guest);
        $this->directory = templateDirectory($this->directoryId,'.');
    }

}
