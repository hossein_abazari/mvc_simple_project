<?php


namespace App\Classes;


use App\Models\CelebrateDetail;
use App\Repositories\CelebrateDetailRepository;
use Illuminate\Support\Facades\Route;

class Menu
{

    public static function getAdmin ()
    {
        return [
            'dashboard' => route('admin.dashboard'),
            'deadlines' => route('admin.deadline.index'),
            'pack_cards' => route('admin.pack.card.index'),
            'discounts' => route('admin.discount.index'),
            'plugins' => route('admin.plugin.index'),
            'layouts' => route('admin.layout.index'),
            'celebration_person' => route('admin.celebration.index'),
            'users' => route('admin.user.index'),
            'invoices' => route('admin.invoice.index'),
            'transaction' => route('admin.transaction.index'),
            'tickets' => route('admin.ticket.master.index'),
            'referral' => route('admin.code.discount.index'),
            'events' => [
                'logs' => route('admin.audit.logs'),
                'users_login' => route('admin.user.login.index'),
            ],
        ];
    }

    public static function getAdminIcon ()
    {
        return [
            'dashboard' => 'mdi mdi-gauge',
            'deadlines' => 'mdi mdi-alarm-check',
            'pack_cards' => 'mdi mdi-alarm-check',
            'discounts' => 'mdi mdi-alarm-check',
            'plugins' => 'mdi mdi-account-plus',
            'layouts' => 'mdi mdi-file',
            'celebration_person' => 'mdi mdi-shopping',
            'users' => 'fa fa-users',
            'invoices' => 'fa fa-file-text',
            'transaction' => 'fa fa-credit-card',
            'tickets' => 'fa fa-headphones',
            'referral' => 'fa fa-headphones',
            'events' => 'fa fa-bug',
        ];
    }

    public static function getAdminStore ()
    {
        return [
            'dashboard' => route('admin.celebration.dashboard'),
            'UIElements' => [
                'logs' => "#",
                'users_login' => '#',
            ],
            'FormElements' => '#',
            'Charts' => '#',
            'Tables' => '#',
            'Icons' => '#',
            'settings' => [
                'profile' => route('admin.celebration.profile')
            ],
        ];
    }

    public static function getAdminStoreIcon ()
    {
        return [
            'dashboard' => 'mdi mdi-gauge',
            'UIElements' => 'mdi mdi-cube-outline',
            'FormElements' => 'mdi mdi-chart-areaspline',
            'Charts' => 'mdi mdi-gauge',
            'Tables' => 'mdi mdi-grid',
            'Icons' => 'mdi mdi-emoticon',
            'settings' => 'mdi mdi-settings',
        ];
    }

    public static function getUserCelebration ()
    {
        return [
            'home' => route('celebration.celebrate.details.index'),
//            'tickets' => route('celebration.ticket.index'),
            // 'profile' => route('admin.celebration.dashboard'),
            // 'edit' => route('admin.celebration.dashboard'),
            // 'UIElements' => [
            //     'logs' => "#",
            //     'users_login' => '#',
            // ],

        ];
    }

    public static function getUserCelebrationIcon ()
    {
        return [
            'home' => 'fa fa-home',
//             'tickets' => 'fa fa-ticket',
            // 'profile' => 'fa fa-money',
            // 'edit' => 'fa fa-edit',
            // 'UIElements' => 'mdi mdi-cube-outline',
        ];
    }

     public static function getUserCelebrateDetail ()
    {
        $celebrateDetailRepository =new CelebrateDetailRepository(new CelebrateDetail());
        $insertPage = [
            'aboutGroomBride' => \route('about.bride.groom.index',['slug' => \Request()->slug]),
        ];
//        there_is_gallery
         $celebrateDetail = $celebrateDetailRepository->getFirstWithCelebrationId(\Request()->slug,\Auth::getUser()->celebration_id);

         if ($celebrateDetail && $celebrateDetail->there_is_gallery){
             $insertPage['galleries'] = \route('celebration.card.galleries.index',['slug' => \Request()->slug]);
         }

        return [
            'inviteCard' => route('celebration.celebrate.details.index'),
            'guests' => route('celebration.card.index',['slug' => Request()->slug, 'guest' => Request()->guest]),
            'allMessages' => route('messages.card',['slug' => Request()->slug]),
             'insertPage' => $insertPage,
            'settings' => \route('celebrate.setting.index',['slug' => \Request()->slug])


        ];
    }

    public static function getUserCelebrateDetailIcon ()
    {

        return [
            'inviteCard' => 'fa fa-id-card',
            'guests' => 'fa fa-person',
            'allMessages' => 'fa fa-message',
            'insertPage' => 'fa fa-file',
            'settings' => 'fa fa-gear',
        ];
    }

    public static function activeMenu($key)
    {
        if (Helper::isCard()) {
            $array = self::getUserCelebrateDetail();
        }
        else{
            $array = self::getUserCelebration();
        }

        if ($array[$key] == \Request()->url()){
            return 'active';
        }
        return '';
    }

}
