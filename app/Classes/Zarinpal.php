<?php


namespace App\Classes;


use phpDocumentor\Reflection\Types\False_;
use SoapClient;

class Zarinpal
{
    public $MerchantID; //Required
    public $Amount; //Amount will be based on Toman - Required
    public $Description; // Required
    public $Email; // Optional
    public $Mobile; // Optional
    public $CallbackURL; // Required
    public $authority;

    /**
     * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function verify()
    {
        $data =
            [
                'MerchantID' => $this->MerchantID,
                'Amount' => $this->Amount,
                'Description' => $this->Description,
                'Email' => $this->Email,
                'Mobile' => $this->Mobile,
                'CallbackURL' => $this->CallbackURL,
            ];

        $jsonData = json_encode($data);
        $ch = curl_init("https://www.zarinpal.com/pg/rest/WebGate/PaymentRequest.json");
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($jsonData)));

        $result = curl_exec($ch);
        $err 	= curl_error($ch);
        curl_close($ch);

        $result = json_decode($result, true);

        if ($result['Status'] == 100) {
            $this->authority = $result['Authority'];

            return redirect('https://www.zarinpal.com/pg/StartPay/' . $this->authority);

        } else {
            return false;
        }
    }

    /**
     * @param \Request $request
     * @return bool
     */
    public function checker($request)
    {
        if ($request->Status == 'OK') {

            $data = [
                    'MerchantID' => $this->MerchantID,
                    'Authority' => $this->authority,
                    'Amount' => $this->Amount,
                ];

            $jsonData = json_encode($data);
            $ch = curl_init("https://www.zarinpal.com/pg/rest/WebGate/PaymentVerification.json");
            curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($jsonData)));

            $result = curl_exec($ch);
            $err 	= curl_error($ch);
            curl_close($ch);

            $result = json_decode($result, true);
            if ($result['Status'] == 100) {
                return $result['RefID'];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
