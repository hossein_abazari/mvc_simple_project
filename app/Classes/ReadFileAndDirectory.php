<?php

namespace App\Classes;

class ReadFileAndDirectory
{
    public $folderName ='../resources/views/Samples'; // read folder default
    public $model;

    public function __construct($model)
    {
        if (substr($this->folderName, strlen($this->folderName) - 1) != "/") {
            $this->folderName .= '/';
        }

        $this->model = $model;
    }

    /**
     * Read name directory or file
     *
     * @param string $fileType
     * @return mixed
     */
    function getDirectoryList($fileType = "")
    {
        $folderName = $this->folderName;

        $file =[];
        foreach (glob($folderName . '*' . $fileType) as $filename) {

              $type = is_dir($filename) ? 'folder' : 'file';

              $file[]=str_replace('.php','', str_replace($folderName, '', $filename)) ;
        }

        return $file; // array Name directory or file
    }

    public function getReadFile($field = 'id')
    {

        $result =[];

        foreach ($this->getDirectoryList() as $value){
            $model = $this->model->where($field,$value)->first();
            if (!$model){
                $readFiles = file($this->folderName.$value.'/file.php');
                unset($readFiles[0]);
                foreach ($readFiles as $readFile) {
                    $strField = str_replace("\n", '', $readFile);
                    $arrayField = explode(':',$strField);
                    $result[$value][$arrayField[0]] = $arrayField[1];
                }
            }
        }

        return $result;

    }
}
