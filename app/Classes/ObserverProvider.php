<?php


namespace App\Classes;


use App\Models\AboutBrideGroom;
use App\Models\CelebrateDetail;
use App\Models\CodeDiscount;
use App\Models\Deadline;
use App\Models\Discount;
use App\Models\Gallery;
use App\Models\Guest;
use App\Models\InvoiceDetail;
use App\Models\Invoice;
use App\Models\Layout;
use App\Models\Log;
use App\Models\Memory;
use App\Models\PackBuy;
use App\Models\PackCard;
use App\Models\Plugin;
use App\Models\PortPayment;
use App\Models\Setting;
use App\Models\Store;
use App\Models\Survey;
use App\Models\TicketDetail;
use App\Models\TicketMaster;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use App\Models\UserLogin;
use App\Models\Celebration;
use App\Observers\AboutBrideGroomObserver;
use App\Observers\AuditObserver;
use App\Observers\CelebrateDetailObserver;
use App\Observers\CodeDiscountObserver;
use App\Observers\DeadlineObserver;
use App\Observers\DiscountObserver;
use App\Observers\GalleryObserver;
use App\Observers\GuestObserver;
use App\Observers\InvoiceDetailObserver;
use App\Observers\InvoiceObserver;
use App\Observers\LayoutObserver;
use App\Observers\LogObserver;
use App\Observers\MemoryObserver;
use App\Observers\PackBuyObserver;
use App\Observers\PackCardObserver;
use App\Observers\PluginObserver;
use App\Observers\PortPaymentObserver;
use App\Observers\SettingObserver;
use App\Observers\CelebrationObserver;
use App\Observers\SurveyObserver;
use App\Observers\TicketDetailObserver;
use App\Observers\TicketMasterObserver;
use App\Observers\TransactionDetailObserver;
use App\Observers\TransactionObserver;
use App\Observers\UserLoginObserver;
use App\Observers\UserObserver;
use OwenIt\Auditing\Models\Audit;

class ObserverProvider
{
    public static function main()
    {
        Invoice::observe(InvoiceObserver::class);
        Transaction::observe(TransactionObserver::class);
        Celebration::observe(CelebrationObserver::class);
        Audit::observe(AuditObserver::class);
        TicketMaster::observe(TicketMasterObserver::class);
        TicketDetail::observe(TicketDetailObserver::class);
        UserLogin::observe(UserLoginObserver::class);
        Setting::observe(SettingObserver::class);
        User::observe(UserObserver::class);
        Deadline::observe(DeadlineObserver::class);
        Plugin::observe(PluginObserver::class);
        InvoiceDetail::observe(InvoiceDetailObserver::class);
        PortPayment::observe(PortPaymentObserver::class);
        Discount::observe(DiscountObserver::class);
        PackCard::observe(PackCardObserver::class);
        CelebrateDetail::observe( CelebrateDetailObserver::class);
        Layout::observe( LayoutObserver::class);
        TransactionDetail::observe( TransactionDetailObserver::class);
        PackBuy::observe( PackBuyObserver::class);
        Survey::observe( SurveyObserver::class);
        AboutBrideGroom::observe( AboutBrideGroomObserver::class);
        Memory::observe( MemoryObserver::class);
        Guest::observe( GuestObserver::class);
        Log::observe( LogObserver::class);
        Gallery::observe( GalleryObserver::class);
        CodeDiscount::observe( CodeDiscountObserver::class);
    }
}
