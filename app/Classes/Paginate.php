<?php

namespace App\Classes;

class Paginate
{
    /**
     *
     */
    public function __construct()
    {

    }

     /**
     * @param $model
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render($model)
    {
        return view('Classes.Paginate.render',compact('model'));
    }

    /**
     * @param $model
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public static function renderS($model)
    {
        return view('Classes.Paginate.render',compact('model'));
    }
}
