<?php


namespace App\Classes;


use App\Enums\TransactionType;
use App\Repositories\PortPaymentRepositoryInterface;
use App\Repositories\TransactionRepositoryInterface;

class Payment
{
    /* merchant id for zarin pall */
    private $MerchantID = '73ba3492-3280-11e8-873b-005056a205be';
    public $Amount;
    public $transactionRepository;
    public $portPaymentRepository;
    public $zarinpall;

    /**
     * Payment constructor.
     * @param PortPaymentRepositoryInterface $portPaymentRepository
     * @param TransactionRepositoryInterface $transactionRepository
     */
    public function __construct(PortPaymentRepositoryInterface $portPaymentRepository,
                                TransactionRepositoryInterface $transactionRepository)
    {
        $zarinpal = new Zarinpal();
        $zarinpal->MerchantID = $this->MerchantID;
        $this->zarinpall = $zarinpal;

        $this->transactionRepository = $transactionRepository;

        $this->portPaymentRepository = $portPaymentRepository;
    }

    /**
     * @param $amount
     * @param $CallbackURL
     * @param $celebrateDetailId
     * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getPort($amount, $CallbackURL)
    {
        if ($amount != 0) {
            $zarinpal = $this->zarinpall;
            $zarinpal->Amount = $amount / 10;
            $zarinpal->Description = vl('general','buy');
            $zarinpal->CallbackURL = $CallbackURL;
            if ($verify = $zarinpal->verify()) {

                return $verify;
            }
            else{
                return false;
            }
        }

    }

    public function setPort($request)
    {
        $zarinpal = $this->zarinpall;
        $port = $this->portPaymentRepository->getAuthority($request->Authority);

        if ($port && $port->payment == 0) {
            $port->payment = 2;
            $port->update();

            /* zarin pull */
            $zarinpal->authority = $port->authority;
            $zarinpal->Amount = $port->amount / 10;
            if ($refId = $zarinpal->checker($request)) {
                $this->transactionRepository->setInsertMasterDetailWithoutInvoice($port->amount,TransactionType::TYPE_PORT,1,
                    \Auth::getUser()->celebration_id,$port->celebrate_detail_id);
                $port->payment = 1;
                $port->tracking_code = $refId;
                $port->update();

                return $refId;
            }
        }
        return false;

    }
}
