<?php


namespace App\Classes;


class TypeCelebration
{
    const TYPE_WEDDING = 'wedding';
    const TYPE_BIRTH = 'birth';
    const TYPE_OTHER = 'other';

}
