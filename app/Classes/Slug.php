<?php


namespace App\Classes;


class Slug
{
    protected $model;

    /**
     * Slug constructor.
     * @param $nameModel
     */
    public function __construct($nameModel){
       $this->model = $nameModel;
    }

    /**
     * @param $slug
     * @return mixed
     */
    private function setModel($slug){
        $model = $this->model;
        return $model::whereSlug($slug)->first();
    }

    /**
     * @param $slug
     * @return mixed
     */
    private function setModelWithId($id){
        $model = $this->model;
        return $model::whereId($id)->first();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function setId($slug)
    {
        return $this->setModel($slug)->id;
    }

    /**
     *
     *
     * @param $field Slug or Id
     * @param $index
     * @return mixed
     */
    public function setIndexId($field, $index)
    {
        if (is_string($field)){
            $model = $this->setModel($field);
            return $model->{$index};
        }

        $model = $this->setModelWithId($field);
        return $model->id;
    }


}
