<?php


namespace App\Classes;


use App\Repositories\PackCardRepository;

class TemplatePackCard
{
    protected $repository;

    /**
     * TemplatePackCard constructor.
     *
     * get repository
     *
     * @param LayoutRepository $repository
     */
    public function __construct()
    {
        $this->repository = new PackCardRepository();
    }

    /**
     * set box for layout
     *
     * @param null $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getSamples($type=null)
    {
        $packCards = $this->repository->getAll();

        return view('Classes.templatePackCard.samples',compact('packCards'));
    }

}
