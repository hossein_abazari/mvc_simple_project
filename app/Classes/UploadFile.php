<?php


namespace App\Classes;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Symfony\Component\Console\Input\Input;

class UploadFile
{
    public static $directory = '/uploads/';
    public $newDirectory = '';
    public $file;



    protected function uploadFile(){
        $imagePath = self::$directory.$this->newDirectory."/";
        $file = $this->file;

        $filename = $file->getClientOriginalName();

        if(file_exists(public_path($imagePath).$filename)){
            $filename= Carbon::now()->timestamp.$filename;
        }
        $file->move(public_path($imagePath),$filename);
        $urlfiles=$imagePath.$filename;
        return $filename;
    }

    public function uploadImageSubject()
    {
        $url= $this->uploadImageSize();

        return $url;
    }

    public function uploadImage()
    {
        $url= $this->uploadFile();

        return $url;
    }

    public function uploadAvatar(){

        $url= $this->uploadFile();

        return $url;
    }

    public function uploadImageSize($width = 800,$height=null)
    {
        $imagePath = self::$directory.$this->newDirectory."/";
        $file = $this->file;
        $filen = $file->getClientOriginalName();

        $filename = Carbon::now()->timestamp.$filen;
//        $file = $file->move(public_path($imagePath) , $filename);

//        $file = $this->resize($file->getRealPath() , [['width'=>$width, 'height'=>$height]] , $imagePath , $filename);
        $file = $this->resize($file->getRealPath() , $width, $height , $imagePath , $filename);
//        $url['thumb'] = $url['images'][$sizes[0]];
        return $file;
    }

    private function resize($path , $width, $height , $imagePath , $filename)
    {
//        $images['original'] = $imagePath . $filename;
            $images = $imagePath . $filename;
            Image::make($path)->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path($images));
        return $filename;
    }

//    private function resize($path , $sizes , $imagePath , $filename)
//    {
////        $images['original'] = $imagePath . $filename;
//        $images='';
//        foreach ($sizes as $size) {
//            $images = $imagePath . "{$size['width']}_" . $filename;
//            Image::make($path)->resize($size['width'], $size['height'], function ($constraint) {
//                $constraint->aspectRatio();
//            })->save(public_path($images));
//        }
//
//        return $images;
//    }
}
