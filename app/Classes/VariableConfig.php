<?php


namespace App\Classes;


class VariableConfig
{

    /**
     * @return array
     */
    public static function getVariable()
    {
        return [
            'countryLanguage' => [
                'en' => 'gb',
                'fa' => 'ir',
            ],

            'money' => 'rial',
            'countDayChartDashboard' => '20'
        ];
    }


}
