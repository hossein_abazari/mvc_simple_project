<?php

namespace App\Repositories\Elequents;


//use Your Model
use App\Models\CodeDiscount;
use App\Repositories\Interfaces\CodeDiscountRepositoryInterface;

/**
 * Class CodeDiscountRepository.
 */
class CodeDiscountRepository implements CodeDiscountRepositoryInterface
{
    public $model;
    public $paginate = 20;

    /**
     * CodeDiscountRepository constructor.
     * @param CodeDiscount $model
     */
    public function __construct(CodeDiscount $model)
    {
        $this->model = $model;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug){
        return $this->model->latest()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate(){
        return $this->model->latest()->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        return $this->model->latest()->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $data
     * @return CodeDiscount|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data){
        return $this->model->create($data);
    }

    /**
     * @param $data
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($data, $slug){
        return $this->model->whereSlug($slug)->first()->update($data);
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        return $this->model->whereSlug($slug)->delete();
    }

    /**
     * @param $code
     * @return int|mixed
     */
    public function getCodeDiscountIdWithCode($code)
    {
        $model = $this->model->whereCodeDiscount($code)->first();

        return $model->id;
    }

}
