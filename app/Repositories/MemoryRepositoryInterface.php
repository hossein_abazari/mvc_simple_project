<?php
namespace App\Repositories;

interface MemoryRepositoryInterface
{

    public function getFirst($slug);

    public function setCreate($data);
}
