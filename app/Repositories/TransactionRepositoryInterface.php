<?php
namespace App\Repositories;

use App\Enums\TransactionType;

interface TransactionRepositoryInterface{
    /**
     * @return string
     *  Return the model
     */
    public function model();

    public function getFirst($slug);

    public function getFirstWithId($id);

    public function getPaginate();

    public function getAll();

    public function setCreate($data);

    public function setUpdate($request, $slug);

    public function setDelete($slug);

    public function setInsertMasterDetailWithoutInvoice($amount, $type = TransactionType::TYPE_UNPAID, $payment=0,
                                                        $celebrationId = null, $celebrateDetailId=null, $invoiceId=null);

    public function setInsertMasterDetail($invoiceDetail,$type = TransactionType::TYPE_UNPAID);

}
