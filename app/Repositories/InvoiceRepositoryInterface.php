<?php
namespace App\Repositories;

interface InvoiceRepositoryInterface{
    /**
     * @return string
     *  Return the model
     */
    public function model();

    public function getFirst($slug);

    public function getPaginate();

    public function getAll();

    public function setCreate($data);

    public function setUpdate($request, $slug);

    public function setDelete($slug);

    public function setInsertMasterDetail($amount,$celebrateDetailId=null,$name=null);

}
