<?php

namespace App\Repositories;


use App\Models\Discount;

class DiscountRepository implements DiscountRepositoryInterface
{
    public $allData;

    public function getAll(){
        return Discount::latest()->get();
    }

    public function getPost($id){
        return Discount::find($id);
    }

    public function getPagination($limit)
    {
        return Discount::latest()->paginate($limit);
    }

    public function setCreateAll()
    {
        return Discount::create($this->allData);
    }

    public function setUpdateAll()
    {
        return Discount::create($this->allData);
    }

}
