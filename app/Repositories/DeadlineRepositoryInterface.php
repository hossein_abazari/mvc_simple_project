<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class PackCardRepositoryInterface.
 */
interface DeadlineRepositoryInterface
{
    public function model();

    public function getFirst($slug);

    public function getPaginate();

    public function getAll();

    public function setCreate($request);

    public function setUpdate($request, $slug);

    public function setDelete($slug);
}
