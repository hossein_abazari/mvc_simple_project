<?php

namespace App\Repositories;

interface AboutBrideGroomRepositoryInterface
{
    /**
     * @param $slug
     * @return mixed
     */
    public function getFirst($slug);

    /**
     * @return mixed
     */
    public function getPaginate();

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $slug
     * @return mixed
     */
    public function getModel($slug);

    /**
     * @param $request
     * @return mixed
     */
    public function setCreate($data);

    /**
     * @param $request
     * @param $slug
     * @return mixed
     */
    public function setUpdate($request , $slug);

    /**
     * @param $slug
     * @return mixed
     */
    public function setDelete($slug);

    /**
     * @param $celebrateId
     * @return mixed
     */
    public function getWithCelebrateId($celebrateDetailId);

}
