<?php

namespace App\Repositories;

use App\Models\Invoice;
use App\Models\InvoiceDetail;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class InvoiceDetailRepostory.
 */
class InvoiceDetailRepository implements InvoiceDetailRepositoryInterface
{
    public $paginate = 20;

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return InvoiceDetail::class;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug)
    {
        $model = $this->model();
        return $model::latest()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate()
    {
        $model = $this->model();
        return $model::latest()->paginate($this->paginate);
    }

    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll()
    {
        $model = $this->model();
        return $model::latest()->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $data
     * @return Layout|Model
     */
    public function setCreate($data)
    {
        $model = $this->model();
        return $model::create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->delete();
    }
}
