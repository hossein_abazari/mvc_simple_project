<?php

namespace App\Repositories;

use App\Models\Deadline;
use App\Models\PackCard;
//use Your Model

/**
 * Class PackCardRepository.
 */
class DeadlineRepository implements PackCardRepositoryInterface
{
    public $paginate = 20;
    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
       return Deadline::latest();
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug){
        return $this->model()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate(){
        return $this->model()->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        return $this->model()->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $request array request input
     *
     * @return PackCard|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($request){
        return $this->model()->create($request->all());
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug){
        return $this->model()->whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        return $this->model()->whereSlug($slug)->delete();
    }
}
