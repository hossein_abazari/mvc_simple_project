<?php

namespace App\Repositories;

use App\Models\Log;

class LogRepository implements LogRepositoryInterface
{
    protected $model;

    public function __construct(Log $log)
    {
        $this->model = $log;
    }

    public function getFirst($slug){
        return $this->model->whereSlug($slug)->latest()->first();
    }

    public function setCreate($data){
        return $this->model->create($data);
    }

}
