<?php

namespace App\Repositories;

use App\models\Survey;

//use Your Model

/**
 * Class SurveyRepository.
 */
class SurveyRepository implements SurveyRepositoryInterface
{
    public $model;

    /**
     * SurveyRepository constructor.
     * @param Survey $model
     */
    public function __construct(Survey $model)
    {
        $this->model = $model;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug){
        return $this->model->latest()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate(){
        return $this->model->latest()->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        return $this->model->latest()->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $data
     * @return Survey|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data){
        return $this->model->create($data);
    }

    /**
     * @param $data
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($data, $slug){
        return $this->model->whereSlug($slug)->first()->update($data);
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        return $this->model->whereSlug($slug)->delete();
    }

}
