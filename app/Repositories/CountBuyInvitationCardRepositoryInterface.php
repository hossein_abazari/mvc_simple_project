<?php

namespace App\Repositories;

interface CountBuyInvitationCardRepositoryInterface
{

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug);

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate();
    /**
     * Get model with Paginate
     *
     * @param $celebrationId
     * @return mixed
     */
    public function getCelebrateDetailIdPaginate($celebrationDetailId);
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll();
    /**
     * Get model with celebration id all
     *
     * @return mixed
     */
    public function getCelebrateDetailIdAll($celebrationDetailId);

    /**
     * @param $celebrationDetailId
     * @return mixed
     */
    public function getCelebrateDetailIdFirst($celebrationDetailId);

    /**
     * insert Data CelebrateDetail
     *
     * @param $data
     * @return CelebrateDetail|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data);

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug);

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug);

}
