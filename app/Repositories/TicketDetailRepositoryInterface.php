<?php

namespace App\Repositories;

//use Your Model

/**
 * Class CelebrateDetailRepositoryInterface.
 */
interface TicketDetailRepositoryInterface
{

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug);

    /**
     * @param $slug
     * @param $ticketMasterId
     * @return mixed
     */
    public function getFirstWithTicketMasterId($slug, $ticketMasterId);

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate();
    /**
     * Get model with Paginate
     *
     * @param $celebrationId
     * @return mixed
     */
    public function getTicketMasterIdPaginate($ticketMasterId);
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll();

    /**
     * Get model with celebration id all
     *
     * @return mixed
     */
    public function getTicketMasterIdAll($ticketMasterId);

    /**
     * insert Data CelebrateDetail
     *
     * @param $data
     * @return CelebrateDetail|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data);

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug);

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug);




}
