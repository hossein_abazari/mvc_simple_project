<?php

namespace App\Repositories;

use App\Models\PackCard;
//use Your Model

/**
 * Class PackCardRepository.
 */
class PackCardRepository implements PackCardRepositoryInterface
{
    public $paginate = 20;
    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
       return PackCard::class;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug){
        $model = $this->model();
        return $model::whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate(){
        $model = $this->model();
        return $model::latest()->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        $model = $this->model();
        return $model::latest()->get();
    }

    /**
     * Get model with all
     *
     * @param $data
     * @param $field
     * @return mixed
     */
    public function getArray($data,$field)
    {
        $model = $this->model();

        return $model::latest()
            ->whereIn($field, $data)
            ->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $request array request input
     *
     * @return PackCard|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data){
        $model = $this->model();
        return $model::create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug){
        $model = $this->model();
        return $model::whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->delete();
    }
}
