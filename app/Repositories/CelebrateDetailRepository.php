<?php

namespace App\Repositories;

//use Your Model
use App\Classes\Slug;
use App\Models\CelebrateDetail;
use App\Models\Layout;

/**
 * Class CelebrateDetailRepository.
 */
class CelebrateDetailRepository implements CelebrateDetailRepositoryInterface
{
    public $paginate = 20;

    public $model;

    public function __construct(CelebrateDetail $model)
    {
        $this->model = $model;
    }

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return CelebrateDetail::class;
    }
    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug)
    {
        return $this->model->whereSlug($slug)->latest()->first();
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @param $celebrationId
     * @return mixed
     */
    public function getFirstWithCelebrationId($slug,$celebrationId)
    {
        return $this->model->whereCelebrationId($celebrationId)->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate()
    {
        return $this->model->latest()->paginate($this->paginate);
    }

    /**
     * Get model with Paginate
     *
     * @param $celebrationId
     * @return mixed
     */
    public function getCelebrationIdPaginate($celebrationId)
    {
        return $this->model->whereCelebrationId($celebrationId)->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        return $this->model->get();
    }
    /**
     * Get model with celebration id all
     *
     * @return mixed
     */
    public function getCelebrationIdAll($celebrationId){
        return $this->model->whereCelebrationId($celebrationId)->All();
    }

    /**
     * insert Data CelebrateDetail
     *
     * @param $data
     * @return CelebrateDetail|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data){
        return $this->model->create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug){
        return $this->model->whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        return $this->model->whereSlug($slug)->delete();
    }

    /**
     * Get data with column name_link
     *
     * @param $nameLink
     * @return mixed
     */
    public function getWithNameLink($nameLink)
    {
      return $this->model->whereLinkName($nameLink)->first();
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getWithCelebrationId($id){
        return $this->model->celebrationId($id)->latest()->first();
    }

    /**
     * @param $id
     * @param $slug
     * @return mixed|void
     */
    public function syncLayout($id,$slug)
    {

        return $this->model
            ->latest()
            ->whereId($id)
            ->first()
            ->layouts()
            ->sync([(new Slug(Layout::class))
            ->setId($slug)]);
    }
}
