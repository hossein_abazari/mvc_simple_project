<?php

namespace App\Repositories;

//use Your Model
use App\Classes\Slug;
use App\Models\CelebrateDetail;
use App\Models\Layout;
use App\Models\TicketMaster;

/**
 * Class CelebrateDetailRepository.
 */
class TicketDetailRepository implements TicketDetailRepositoryInterface
{
    public $paginate = 20;

    public $model;

    public function __construct(TicketMaster $model)
    {
        $this->model = $model;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug)
    {
        return $this->model->whereSlug($slug)->latest()->first();
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @param $celebrationId
     * @return mixed
     */
    public function getFirstWithTicketMasterId($slug,$ticketMasterId)
    {
        return $this->model->whereTicketMasterId($ticketMasterId)->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate()
    {
        return $this->model->latest()->paginate($this->paginate);
    }

    /**
     * Get model with Paginate
     *
     * @param $celebrationId
     * @return mixed
     */
    public function getTicketMasterIdPaginate($ticketMasterId)
    {
        return $this->model->whereTicketMasterId($ticketMasterId)->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        return $this->model->get();
    }
    /**
     * Get model with celebration id all
     *
     * @return mixed
     */
    public function getTicketMasterIdAll($ticketMasterId)
    {
        return $this->model->whereTicketMasterId($ticketMasterId)->All();
    }

    /**
     * insert Data CelebrateDetail
     *
     * @param $data
     * @return CelebrateDetail|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data){
        return $this->model->create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug){
        return $this->model->whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        return $this->model->whereSlug($slug)->delete();
    }

}
