<?php

namespace App\Repositories;
use App\Models\PortPayment;


//use Your Model

/**
 * Class InvoiceDetailRepostory.
 */
class PortPaymentRepository implements PortPaymentRepositoryInterface
{
    protected $packCardRepository;

    private $expireDay = 1;

    public function __construct()
    {
        $this->packCardRepository = new PackCardRepository();
    }

    public $paginate = 20;

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return PortPayment::class;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug)
    {
        $model = $this->model();
        return $model::latest()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate()
    {
        $model = $this->model();
        return $model::latest()->paginate($this->paginate);
    }

    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll()
    {
        $model = $this->model();
        return $model::latest()->get();
    }

    /**
     * Get model with all
     *
     * @param $data
     * @param $field
     * @return mixed
     */
    public function getArray($data,$field)
    {
        $model = $this->model();
        return $model::latest()
            ->whereIn($field, $data)
            ->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $data
     * @return Layout|Model
     */
    public function setCreate($data)
    {
        $model = $this->model();
        return $model::create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->delete();
    }

    /**
     * Get Authority for port payment
     *
     * @param $authority
     */
    public function getAuthority ($authority)
    {
        $model = $this->model();
        return $model::whereAuthority($authority)->wherePayment(0)->first();
    }
}
