<?php

namespace App\Repositories;

use App\Models\AboutBrideGroom;
use App\Models\Guest;

class AboutBrideGroomRepository implements AboutBrideGroomRepositoryInterface
{
    public $pageination = 20;
    public $model;
    public function __construct(AboutBrideGroom $model)
    {
        $this->model = $model;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getFirst($slug){
        return $this->model->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate(){
        return $this->model->latest()->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        return $this->model->latest()->get();
    }

    /**
     * @param $data
     * @return Guest|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data){
        return $this->model->create($data);
    }

    /**
     * @param $data
     * @param $slug
     * @return int
     */
    public function setUpdate($data , $slug){
        return $this->model->whereSlug($slug)->update($data);
    }

    /**
     * @param $slug
     * @return \App\Guest|\Illuminate\Database\Eloquent\Builder
     */
    public function getModel($slug)
    {
        return $this->model->latest()->whereSlug($slug);
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        return $this->model->whereSlug($slug)->delete();
    }

    /**
     * @param $celebrateId
     * @return mixed|void
     */
    public function getWithCelebrateId($celebrateDetailId){
        return $this->model->whereCelebrateDetailId($celebrateDetailId)->first();
    }


}
