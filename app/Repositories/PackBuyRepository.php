<?php

namespace App\Repositories;

use App\Enums\TransactionType;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\PackBuy;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class InvoiceDetailRepostory.
 */
class PackBuyRepository implements PackBuyRepositoryInterface
{
    protected $packCardRepository;

    private $expireDay = 1;

    public function __construct()
    {
        $this->packCardRepository = new PackCardRepository();
    }

    public $paginate = 20;

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return PackBuy::class;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug)
    {
        $model = $this->model();
        return $model::latest()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate()
    {
        $model = $this->model();
        return $model::latest()->paginate($this->paginate);
    }

    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll()
    {
        $model = $this->model();
        return $model::latest()->get();
    }

    /**
     * Get model with all
     *
     * @param $data
     * @param $field
     * @return mixed
     */
    public function getArray($data,$field)
    {
        $model = $this->model();
        return $model::latest()
            ->whereIn($field, $data)
            ->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $data
     * @return Layout|Model
     */
    public function setCreate($data)
    {
        $model = $this->model();
        return $model::create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->delete();
    }

    public function setCreateArray($data, $celebrateDetailId)
    {
        $result = [];
        if ($data) {
            $models = $this->packCardRepository->getArray($data, 'slug')->toArray();
            foreach ($models as $model) {

                $result = array_merge($model, [
                    'pack_card_id' => $model['id'],
                    'user_id' => \Auth::getUser()->id,
                    'celebrate_detail_id' => $celebrateDetailId,
                ]);

                $result = unsetAllArray($result,['id','slug','deleted_at','created_at','updated_at']);

                $this->setCreate($result);
            }

        }
        return $result;

    }
}
