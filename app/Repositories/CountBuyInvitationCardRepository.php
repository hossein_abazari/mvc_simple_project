<?php

namespace App\Repositories;

use App\Models\CountBuyInvitationCard;

class CountBuyInvitationCardRepository implements CountBuyInvitationCardRepositoryInterface
{
    public $paginate = 20;

    public $model;

    public function __construct(CountBuyInvitationCard $model)
    {
        $this->model = $model;
    }

    /**
     * Get First in object
     *
     * @param $id string slug input
     *
     * @return mixed
     */
    public function getFirst($id)
    {
        return $this->model->whereId($id)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate()
    {
        return $this->model->latest()->paginate($this->paginate);
    }

    /**
     * Get model with Paginate
     *
     * @param $celebrationId
     * @return mixed
     */
    public function getCelebrateDetailIdPaginate($celebrationDetailId)
    {
        return $this->model->whereCelebrateDetailId($celebrationDetailId)->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        return $this->model->get();
    }
    /**
     * Get model with celebration id all
     *
     * @return mixed
     */
    public function getCelebrateDetailIdAll($celebrationDetailId)
    {
        return $this->model->whereCelebrateDetailId($celebrationDetailId)->All();
    }

    /**
     * Get model with celebration id all
     *
     * @return mixed
     */
    public function getCelebrateDetailIdFirst($celebrationDetailId)
    {
        return $this->model->whereCelebrateDetailId($celebrationDetailId)->first();
    }

    /**
     * insert Data CelebrateDetail
     *
     * @param $data
     * @return CelebrateDetail|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data){
        return $this->model->create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug){
        return $this->model->whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        return $this->model->whereSlug($slug)->delete();
    }

}
