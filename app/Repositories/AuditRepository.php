<?php

namespace App\Repositories;


use App\Audit;
use App\Classes\Helper;
use App\Guest;

class AuditRepository implements AuditRepositoryInterface
{
    public function setCreate(){
        return Audit::create([
            'guest_id' => Helper::getSelfGuest()->id,
            'url' => url()->current(),
            'ip_address' => \Request()->ip()
        ]);
    }
}
