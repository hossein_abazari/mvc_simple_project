<?php
namespace App\Repositories;

use App\Enums\TransactionType;

interface PortPaymentRepositoryInterface{
    /**
     * @return string
     *  Return the model
     */
    public function model();

    public function getFirst($slug);

    public function getPaginate();

    public function getAll();

    public function getArray($data,$field);

    public function setCreate($data);

    public function setUpdate($request, $slug);

    public function setDelete($slug);

    public function getAuthority ($authority);


}
