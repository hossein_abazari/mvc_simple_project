<?php
namespace App\Repositories;

interface GuestRepositoryInterface{

    public function getFirst($slug);

    public function getAll();

    public function getSearch($request);

    public function getModel($slug);

    public function setCreate($request);

    public function setUpdate($data , $slug);

    public function setDelete($slug);

}
