<?php

namespace App\Repositories;

//use Your Model

/**
 * Class CelebrationRepositoryInterface.
 */
interface CelebrationRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function model();

    public function getFirst($slug);

    public function getPaginate();

    public function getAll();

    public function setCreate($request);

    public function setUpdate($request, $slug);

    public function setDelete($slug);
}
