<?php

namespace App\Repositories;

//use Your Model

/**
 * Class PackCardRepositoryInterface.
 */
interface PackCardRepositoryInterface
{
    public function model();

    public function getFirst($slug);

    public function getPaginate();

    public function getAll();

    public function getArray($data,$field);

    public function setCreate($request);

    public function setUpdate($request, $slug);

    public function setDelete($slug);
}
