<?php

namespace App\Repositories;

use App\Classes\ReadFileAndDirectory;
use App\Models\Layout;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LayoutRepository.
 */
class LayoutRepository implements LayoutRepositoryInterface
{
    public $paginate = 20;
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Layout::class;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug){
        $model = $this->model();
        return $model::latest()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate(){
        $model = $this->model();
        return $model::latest()->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        $model = $this->model();
        return $model::latest()->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $data
     * @return Layout|Model
     */
    public function setCreate($data){
        $model = $this->model();
        return $model::create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug){
        $model = $this->model();
        return $model::whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->delete();
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getColumnTypeAll($type)
    {
        $model = $this->model();
        $model = $model::latest();
        if ($type){
            $model = $model->whereType($type);
        }

        return $model->get();
    }

    public function setCreteWithFile()
    {
        $readFile = new ReadFileAndDirectory(new Layout());
        $results = $readFile->getReadFile('directory_id');
        foreach ($results as $key => $value){
            $typeFile = isset($value['type_file']) ? $value['type_file'] : 'jpg';
            $value['directory_id'] = $key;
            $value['img_temp'] = $key.'.'.$typeFile;

            $this->setCreate($value);
        }
        return true;
    }
}
