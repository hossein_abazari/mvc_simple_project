<?php

namespace App\Repositories;

//use Your Model
use App\Models\User;
use App\Traits\Login;

/**
 * Class UserRepository.
 */
class UserRepository implements UserRepositoryInterface
{
    use Login;
    public $paginate = 20;
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug){
        $model = $this->model();
        return $model::latest()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate(){
        $model = $this->model();
        return $model::latest()->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        $model = $this->model();
        return $model::latest()->get();
    }

    /**
     * insert Data User
     *
     * @param $request array request input
     *
     * @return Layout|Model
     */
    public function setCreate($request){
        $model = $this->model();
        return $model::create($request->all());
    }

    /**
     * @param $requestArray
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($requestArray, $slug){
        $model = $this->model();
        return $model::whereSlug($slug)->first()->update($requestArray);
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->delete();
    }

    /**
     * insert Data User
     *
     * @param $request array request input
     *
     * @return bool
     */
    public function setRegister($request){
        $this->setCreate($request);
        $result = false;
        if ($this->attemptLoginUserCelebration($request)) {

            if ($this->sendLoginResponse($request)){
                $result = true;
            }
        }

        return $result;

    }
}
