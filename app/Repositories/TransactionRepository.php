<?php

namespace App\Repositories;

use App\Enums\TransactionType;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use Carbon\Carbon;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class InvoiceDetailRepostory.
 */
class TransactionRepository implements TransactionRepositoryInterface
{
    protected $detailRepository;

    public function __construct()
    {
        $this->detailRepository = new TransactionDetailRepository();
    }

    public $paginate = 20;

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Transaction::class;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug)
    {
        $model = $this->model();
        return $model::latest()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get First in object
     *
     * @param $id
     * @return mixed
     */
    public function getFirstWithId($id)
    {
        $model = $this->model();
        return $model::latest()->whereId($id)->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate()
    {
        $model = $this->model();
        return $model::latest()->paginate($this->paginate);
    }

    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll()
    {
        $model = $this->model();
        return $model::latest()->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $data
     * @return Layout|Model
     */
    public function setCreate($data)
    {
        $model = $this->model();
        return $model::create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->delete();
    }

    /**
     * @param $amount
     * @param string $type
     * @param int $payment
     * @param null $celebrationId
     * @param null $celebrateDetailId
     * @param null $invoiceId
     */
    public function setInsertMasterDetailWithoutInvoice($amount, $type = TransactionType::TYPE_UNPAID, $payment=0,
                                                        $celebrationId = null, $celebrateDetailId=null, $invoiceId=null)
    {
        $arrayMaster =[
            'amount' => $amount,
            'payment_date' => Carbon::now()->format('Y-m-d'),
            'type' => $type,
            'payment' => $payment,
        ];

        $master = $this->setCreate($arrayMaster);

        $arrayDetail = [
            'invoice_id' => $invoiceId,
            'celebration_id' => $celebrationId,
            'transaction_id' => $master->id,
            'celebrate_detail_id' => $celebrateDetailId,
            'total_amount' => $master->amount,
        ];

        $this->detailRepository->setCreate($arrayDetail);

    }

    /**
     * @param $invoiceDetail
     * @param string $type
     */
    public function setInsertMasterDetail($invoiceDetail, $type = TransactionType::TYPE_UNPAID)
    {
        $payment = 0;
        if ($invoiceDetail->total_amount < 0) {
            $payment = 1;
        }

        $this->setInsertMasterDetailWithoutInvoice($invoiceDetail->total_amount,$type,$payment,
            $invoiceDetail->invoice->celebration_id,$invoiceDetail->celebrate_detail_id,$invoiceDetail->invoice_id);

    }
}
