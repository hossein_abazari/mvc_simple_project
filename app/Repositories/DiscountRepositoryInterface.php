<?php
namespace App\Repositories;

interface DiscountRepositoryInterface{

    public function getAll();

    public function getPost($id);

    public function getPagination($limit);

    public function setCreateAll();

    public function setUpdateAll();
}
