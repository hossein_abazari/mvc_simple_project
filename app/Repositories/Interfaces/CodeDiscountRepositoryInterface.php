<?php

namespace App\Repositories\Interfaces;

interface CodeDiscountRepositoryInterface
{
    public function getFirst($slug);

    public function getPaginate();

    public function getAll();

    public function setCreate($data);

    public function setUpdate($request, $slug);

    public function setDelete($slug);

    /**
     * @param $code
     * @return mixed
     */
    public function getCodeDiscountIdWithCode($code);

}
