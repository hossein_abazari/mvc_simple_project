<?php

namespace App\Repositories;

interface GalleryRepositoryInterface
{
    /**
     * @param $slug
     * @return mixed
     */
    public function getFirst($slug);

    /**
     * @return mixed
     */
    public function getPaginate();

        /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $slug
     * @return mixed
     */
    public function getModel($slug);

    /**
     * @param $request
     * @return mixed
     */
    public function setCreate($request);

    /**
     * @param $request
     * @param $slug
     * @return mixed
     */
    public function setUpdate($request , $slug);

    /**
     * @param $slug
     * @return mixed
     */
    public function setDelete($slug);

}
