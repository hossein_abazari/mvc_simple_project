<?php

namespace App\Repositories;

//use Your Model

/**
 * Class CelebrateDetailRepositoryInterface.
 */
interface TicketRepositoryInterface
{

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug);

    /**
     * @param $slug
     * @param $celebrationId
     * @return mixed
     */
    public function getFirstWithCelebrationId($slug, $celebrationId);

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate();
    /**
     * Get model with Paginate
     *
     * @param $celebrationId
     * @return mixed
     */
    public function getCelebrationIdPaginate($celebrationId);
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll();
    /**
     * Get model with celebration id all
     *
     * @return mixed
     */
    public function getCelebrationIdAll($celebrationId);

    /**
     * insert Data CelebrateDetail
     *
     * @param $data
     * @return CelebrateDetail|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data);

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug);

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug);


    /**
     * @param $id
     * @return mixed
     */
    public function getWithCelebrationId($id);


}
