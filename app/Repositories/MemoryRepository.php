<?php

namespace App\Repositories;


use App\models\Memory;

class MemoryRepository implements MemoryRepositoryInterface
{
    protected $model;

    public function __construct(Memory $memory)
    {
        $this->model = $memory;
    }

    public function getFirst($slug){
        return $this->model->whereSlug($slug)->latest()->first();
    }

    public function setCreate($data){
        return $this->model->create($data);
    }

}
