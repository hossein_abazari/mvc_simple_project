<?php

namespace App\Repositories;

interface LogRepositoryInterface
{
    public function getFirst($slug);

    public function setCreate($data);

}
