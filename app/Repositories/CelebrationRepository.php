<?php

namespace App\Repositories;

use App\Models\Celebration;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class CelebrationRepository.
 */
class CelebrationRepository implements CelebrationRepositoryInterface
{
    public $paginate = 20;
    public $model;

    /**
     * CelebrationRepository constructor.
     * @param Celebration $model
     */
    public function __construct(Celebration $model)
    {
        $this->model = $model;
    }
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Celebration::class;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug){
        $model = $this->model();
        return $model::latest()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate(){
        $model = $this->model();
        return $model::latest()->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        $model = $this->model();
        return $model::latest()->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $request array request input
     *
     * @return Layout|Model
     */
    public function setCreate($data){
        $model = $this->model();
        return $model::create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug){
        $model = $this->model();
        return $model::whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->delete();
    }

}
