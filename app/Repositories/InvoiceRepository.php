<?php

namespace App\Repositories;


use App\Models\Invoice;
use App\Traits\ToolTrait;
use Carbon\Carbon;

class InvoiceRepository implements InvoiceRepositoryInterface
{
    use ToolTrait;
    public $paginate = 20;
    protected $detailRepository;

    public function __construct()
    {
        $this->detailRepository = new InvoiceDetailRepository();
    }

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Invoice::class;
    }

    /**
     * Get First in object
     *
     * @param $slug string slug input
     *
     * @return mixed
     */
    public function getFirst($slug){
        $model = $this->model();
        return $model::latest()->whereSlug($slug)->latest()->first();
    }

    /**
     * Get model with Paginate
     *
     * @return mixed
     */
    public function getPaginate(){
        $model = $this->model();
        return $model::latest()->paginate($this->paginate);
    }
    /**
     * Get model with all
     *
     * @return mixed
     */
    public function getAll(){
        $model = $this->model();
        return $model::latest()->get();
    }

    /**
     * insert Data PackCard
     *
     * @param $data
     * @return Layout|Model
     */
    public function setCreate($data){
        $model = $this->model();
        return $model::create($data);
    }

    /**
     * @param $request array request input
     * @param $slug string random
     * @return mixed
     */
    public function setUpdate($request, $slug){
        $model = $this->model();
        return $model::whereSlug($slug)->first()->update($request->all());
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        $model = $this->model();
        return $model::whereSlug($slug)->delete();
    }

    public function setInsertMasterDetail($amount,$celebrateDetailId=null,$name=null)
    {
        $arrayMaster =[
            'amount' => $amount,
            'celebration_id' => \Auth::getUser()->celebration_id,
            'invoice_code' => $this->autoCode('invoice','invoice_code'),
            'invoice_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'name' => $name,
            'type' => 'celebrationRecord'
        ];

        $master = $this->setCreate($arrayMaster);

        $arrayDetail = [
            'invoice_id' => $master->id,
            'celebrate_detail_id' => $celebrateDetailId,
            'amount' => $amount,
            'product_name' => $name,
        ];

        $this->detailRepository->setCreate($arrayDetail);

    }

}
