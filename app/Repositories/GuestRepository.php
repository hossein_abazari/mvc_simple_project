<?php

namespace App\Repositories;


use App\models\Guest;
use Carbon\Carbon;

class GuestRepository implements GuestRepositoryInterface
{
   public $pageination = 20;
   public $model;
    public function __construct(Guest $model)
    {
        $this->model = $model;
    }

    public function getFirst($slug){
        return $this->model->whereSlug($slug)->latest()->first();
    }
    public function getAll(){
        return $this->model->latest()->paginate($this->pageination);
    }

    /**
     * @param $data
     * @return Guest|\Illuminate\Database\Eloquent\Model
     */
    public function setCreate($data){
        return $this->model->create($data);
    }

    /**
     * @param $data
     * @param $slug
     * @return int
     */
    public function setUpdate($data , $slug){
        return $this->model->whereSlug($slug)->first()->update($data);
    }

    /**
     * @param $slug
     * @return \App\Guest|\Illuminate\Database\Eloquent\Builder
     */
    public function getModel($slug)
    {
        return $this->model->latest()->whereSlug($slug);
    }

    /**
     * Delete this model
     *
     * @param $slug string random
     *
     * @return mixed
     */
    public function setDelete($slug)
    {
        return $this->model->whereSlug($slug)->delete();
    }
    /**
     * show model with search
     *
     * @param $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getSearch($request,$mode='pageinate')
    {
        $guests = Guest::latest();
        $request_search = "";
        $request_search_two = "";
//        if (isset($request->btn_search)){

            if (isset($request->name_fa_search)) {

                $guests = $guests->where('name_fa', 'like', '%'.$request->name_fa_search.'%');
                $request_search = 'name_fa_search';

            }
            if (isset($request->family_search)) {

                $guests = $guests->where('family', 'like', '%'.$request->family_search.'%');

                $request_search_two = 'family_search';

            }

//        }
        if (isset($request->comment_search) && $request->comment_search == 1) {
            $guests = $guests->whereHas('memories');
            $request_search = 'comment_search';
        }
        if ($mode == 'get'){
            $guests=$guests->get();
        }
        else{
            $guests=$guests->paginate($this->pageination);
            if ($request_search) {
                $guests->appends([$request_search=>$request->{$request_search}]);
            }
            if ($request_search_two = 'family_search') {
                $guests->appends(['family_search'=>$request->family_search]);
            }
        }


            return $guests;
    }
}
