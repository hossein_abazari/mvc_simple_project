<?php

namespace App\Models;

use App\Traits\UserLoginTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserLogin
 *
 * @property int $id
 * @property string|null $slug
 * @property int|null $user_id
 * @property string $login
 * @property string|null $key
 * @property string|null $ip_address
 * @property string|null $user_agent
 * @property string|null $country
 * @property string|null $city
 * @property string|null $os
 * @property string|null $browser
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereBrowser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereOs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereUserId($value)
 * @mixin \Eloquent
 * @property string $action
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereAction($value)
 */
class UserLogin extends Model
{
    use HasFactory;
    use UserLoginTrait;

    protected $connection = 'mysql_log';
    protected $fillable = ['slug','user_id','login','key','ip_address','user_agent','country','city','os','browser','action'];
}
