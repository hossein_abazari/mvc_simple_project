<?php

namespace App\Models;

use App\Traits\PackCardTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\PackCard
 *
 * @property int $id
 * @property string|null $slug
 * @property string|null $name
 * @property int $number_card
 * @property int $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PackCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PackCard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PackCard query()
 * @method static \Illuminate\Database\Eloquent\Builder|PackCard whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackCard whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackCard whereNumberCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackCard whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackCard whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Query\Builder|PackCard onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|PackCard withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PackCard withoutTrashed()
 */
class PackCard extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;
    use PackCardTrait;


    protected $fillable =['slug','name','number_card','amount'];
}
