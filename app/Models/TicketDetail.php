<?php

namespace App\Models;

use App\Traits\ToolTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\TicketDetail
 *
 * @property int $id
 * @property string|null $slug
 * @property int $user_id
 * @property int $ticket_master_id
 * @property string $message
 * @property int $view if is true mean is viewed
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail newQuery()
 * @method static \Illuminate\Database\Query\Builder|TicketDetail onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail whereTicketMasterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketDetail whereView($value)
 * @method static \Illuminate\Database\Query\Builder|TicketDetail withTrashed()
 * @method static \Illuminate\Database\Query\Builder|TicketDetail withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \App\Models\TicketMaster $ticketMaster
 * @property-read \App\Models\User $user
 */
class TicketDetail extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;
    use ToolTrait;

    public $slugMaster;

    protected $fillable = ['slug','ticket_master_id','user_id','message','view'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function ticketMaster()
    {
        return $this->belongsTo(TicketMaster::class,'ticket_master_id');
    }


}
