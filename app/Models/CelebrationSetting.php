<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;



/**
 * App\Models\CelebrationSetting
 *
 * @property int $celebration_id
 * @property string|null $from_date
 * @property string|null $to_date
 * @property string|null $name_link
 * @property int $status
 * @property string|null $address
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \App\Models\Celebration|null $celebration
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting newQuery()
 * @method static \Illuminate\Database\Query\Builder|CelebrationSetting onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting whereCelebrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting whereFromDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting whereNameLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting whereToDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrationSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|CelebrationSetting withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CelebrationSetting withoutTrashed()
 * @mixin \Eloquent
 */
class CelebrationSetting extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'celebration_id';
    protected $fillable = ['celebration_id','from_date','to_date','name_link','status','address'];

    public function celebration()
    {
        return $this->hasOne(Celebration::class,'celebration_id');
    }
}
