<?php

namespace App\Models;

use App\Traits\ToolTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Memory
 *
 * @property int $id
 * @property string|null $slug
 * @property int $guest_id
 * @property string|null $message
 * @property string $answer
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Memory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Memory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Memory query()
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Memory whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Memory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Memory whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Memory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Memory whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Memory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Memory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $user_id
 * @property-read \App\models\Guest $guest
 * @method static \Illuminate\Database\Eloquent\Builder|Memory whereUserId($value)
 */
class Memory extends Model
{
    public $guestName;
    use ToolTrait;
    protected $fillable = ['slug','user_id','guest_id','celebrate_detail_id','message','answer','status'];

    public function guest()
    {
        return $this->belongsTo(Guest::class,'guest_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function celebrateDetail()
    {
        return $this->belongsTo(CelebrateDetail::class,'celebrate_detail_id');
    }


}
