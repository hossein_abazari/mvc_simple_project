<?php

namespace App\Models;

use App\Traits\CelebrationTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;




/**
 * App\Models\Celebration
 *
 * @property int $id
 * @property string|null $slug
 * @property string|null $code_discount_id
 * @property string|null $name
 * @property string|null $amount_paid
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invoice[] $invoices
 * @property-read int|null $invoices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Plugin[] $plugins
 * @property-read int|null $plugins_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PortPayment[] $portPayments
 * @property-read int|null $port_payments_count
 * @property-read \App\Models\CelebrationSetting|null $storeSetting
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TicketMaster[] $ticketMasters
 * @property-read int|null $ticket_masters_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TransactionDetail[] $transactionDetail
 * @property-read int|null $transaction_detail_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Celebration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Celebration newQuery()
 * @method static \Illuminate\Database\Query\Builder|Celebration onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Celebration query()
 * @method static \Illuminate\Database\Eloquent\Builder|Celebration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Celebration whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Celebration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Celebration whereCodeDiscountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Celebration whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Celebration whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Celebration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Celebration withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Celebration withoutTrashed()
 * @mixin \Eloquent
 * @property-read \App\Models\CelebrationSetting|null $celebrationSetting
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CelebrateDetail[] $celebrateDetails
 * @property-read int|null $celebrate_details_count
 */
class Celebration extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use CelebrationTrait;
    use SoftDeletes;
    public $amount_paid_operation;

    protected $fillable = ['slug','code_discount_id','name'];

    protected $appends = ['amount_paid'];

    public function invoices()
    {
        return $this->hasMany(Invoice::class,'celebration_id');
    }

    public function plugins()
    {
        return $this->belongsToMany(Plugin::class);
    }


    public function portPayments()
    {
        return $this->hasMany(PortPayment::class,'celebration_id');
    }

    public function celebrationSetting()
    {
        return $this->hasOne(CelebrationSetting::class,'celebration_id');
    }

    public function transactionDetail()
    {
        return $this->hasMany(TransactionDetail::class,'celebration_id');
    }

    public function users()
    {
        return $this->hasMany(User::class,'celebration_id');
    }

    public function ticketMasters()
    {
        return $this->hasMany(TicketMaster::class,'celebration_id');
    }

    public function celebrateDetails()
    {
        return $this->hasMany(CelebrateDetail::class,'celebration_id');
    }

    public function getAmountPaidAttribute()
    {
        return $this->amount_paid_operation;
    }
}
