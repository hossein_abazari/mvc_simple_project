<?php

namespace App\Models;

use App\Traits\TicketMasterTrait;
use App\Traits\ToolTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\TicketMaster
 *
 * @property int $id
 * @property string|null $slug
 * @property int $celebration_id
 * @property int $user_id
 * @property string|null $title
 * @property string|null $message
 * @property int $close if is true mean closed
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster newQuery()
 * @method static \Illuminate\Database\Query\Builder|TicketMaster onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster query()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster whereClose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster whereCelebrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMaster whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|TicketMaster withTrashed()
 * @method static \Illuminate\Database\Query\Builder|TicketMaster withoutTrashed()
 * @mixin \Eloquent
 * @property int $wedding_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TicketDetail[] $ticketDetails
 * @property-read int|null $ticket_details_count
 * @property-read \App\Models\User $user
 * @property-read \App\Models\Celebration $celebration
 */
class TicketMaster extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;
    use ToolTrait;
    use TicketMasterTrait;

    public $store_name;
    public $user_name;
    public $closed;
    protected $appends = ['message'];

    protected $fillable = ['slug','celebration_id','user_id','title','close'];


    public function ticketDetails()
    {
        return $this->hasMany(TicketDetail::class,'ticket_master_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function celebration()
    {
        return $this->belongsTo(Celebration::class,'celebration_id');
    }

}
