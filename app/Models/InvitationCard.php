<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;

/**
 * App\Models\InvitationCard
 *
 * @method static \Illuminate\Database\Eloquent\Builder|InvitationCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvitationCard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvitationCard query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $celebrate_detail_id
 * @property int $number_invitation_card
 * @property int $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|InvitationCard whereCelebrateDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvitationCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvitationCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvitationCard whereNumberInvitationCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvitationCard wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvitationCard whereUpdatedAt($value)
 */
class InvitationCard extends Model
{
    use HasFactory;
    use Auditable;
    protected $fillable = ['number_invitation_card','price'];
}
