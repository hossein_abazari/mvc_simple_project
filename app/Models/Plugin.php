<?php

namespace App\Models;

use App\Traits\PluginTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Plugin
 *
 * @property int $id
 * @property string|null $slug
 * @property string|null $name
 * @property int $amount
 * @property int $status
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin query()
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Plugin extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use PluginTrait;

    protected $fillable = ['slug','name','amount','status'];

    public function stores()
    {
        return $this->belongsToMany(Store::class);
    }
}
