<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


/**
 * App\Models\PackBuy
 *
 * @property int $id
 * @property string|null $slug
 * @property int|null $user_id
 * @property int|null $pack_card_id
 * @property int|null $celebrate_detail_id
 * @property string|null $name
 * @property int $number_card
 * @property int $amount
 * @property string|null $expire
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy newQuery()
 * @method static \Illuminate\Database\Query\Builder|PackBuy onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy query()
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereCelebrateDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereExpire($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereNumberCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy wherePackCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackBuy whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|PackBuy withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PackBuy withoutTrashed()
 * @mixin \Eloquent
 */
class PackBuy extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;

    protected $fillable =
        [
        'slug',
        'user_id',
        'pack_card_id',
        'celebrate_detail_id',
        'name',
        'number_card',
        'amount',
        'expire'
    ];

    /**
     * Relation belongs to User model
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * Relation belongs to PackCard model
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function packCard()
    {
        return $this->belongsTo(PackCard::class,'pack_card_id');
    }

    /**
     * Relation belongs to PackCard model
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function celebrateDetail()
    {
        return $this->belongsTo(CelebrateDetail::class,'celebrate_detail_id');
    }
}
