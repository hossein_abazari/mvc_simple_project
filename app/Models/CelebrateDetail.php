<?php

namespace App\Models;

use App\Casts\SurveyCast;
use App\Casts\TextFooterCast;
use App\Casts\TextInviteCast;
use App\Repositories\TransactionDetailRepositoryInterface;
use App\Traits\CelebrateDetailTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\CelebrateDetail
 *
 * @property int $id
 * @property string|null $slug
 * @property int|null $user_id
 * @property string $type_celebrate
 * @property string|null $name
 * @property string|null $Date_ceremony
 * @property string|null $text_invite
 * @property int $survey
 * @property string|null $venue_ceremony
 * @property string|null $address
 * @property string|null $location
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereDateCeremony($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereSurvey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereTextInvite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereTypeCelebrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereVenueCeremony($value)
 * @mixin \Eloquent
 * @property int|null $celebration_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \App\Models\Celebration|null $celebration
 * @method static \Illuminate\Database\Query\Builder|CelebrateDetail onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereCelebrationId($value)
 * @method static \Illuminate\Database\Query\Builder|CelebrateDetail withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CelebrateDetail withoutTrashed()
 * @property string|null $text_footer
 * @property string|null $text_survey
 * @property int $memories
 * @property string|null $expire
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail celebrationId($arg)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereExpire($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereMemories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereTextFooter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereTextSurvey($value)
 * @property string|null $name_groom
 * @property string|null $name_bride
 * @property string|null $link_name
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereLinkName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereNameBride($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereNameGroom($value)
 * @property string|null $date_ceremony
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InvoiceDetail[] $invoiceDetail
 * @property-read int|null $invoice_detail_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Layout[] $layouts
 * @property-read int|null $layouts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PackBuy[] $packBuy
 * @property-read int|null $pack_buy_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TransactionDetail[] $transactionDetail
 * @property-read int|null $transaction_detail_count
 * @property string|null $background_img
 * @property int $there_is_gallery
 * @property int $free
 * @property-read \App\Models\AboutBrideGroom|null $aboutBrideGroom
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Gallery[] $galleries
 * @property-read int|null $galleries_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\Guest[] $guests
 * @property-read int|null $guests_count
 * @property-read int|null $memories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Survey[] $surveys
 * @property-read int|null $surveys_count
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereBackgroundImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CelebrateDetail whereThereIsGallery($value)
 */
class CelebrateDetail extends Model implements Auditable
{
    use CelebrateDetailTrait;
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;
    public $text_invite_show;
    public $text_footer_show;

    protected $fillable =
        [
        'slug',
        'celebration_id',
        'type_celebrate',
        'name',
        'name_logo',
        'date_ceremony',
        'text_invite',
        'text_footer',
        'survey',
        'text_survey',
        'link_name',
        'memories',
        'venue_ceremony',
        'address',
        'location',
        'expire',
        'background_img',
        'there_is_gallery',
        'free'
        ];

//        protected $appends = ['text_invite_show','text_footer_show'];

        protected $casts = [
            'text_invite_show' => TextInviteCast::class,
            'text_footer_show' => TextFooterCast::class,
        ];

    public function scopeCelebrationId($query,$arg)
    {
        return $query->where('celebration_id',$arg);
    }

    public function celebration()
    {
        return $this->belongsTo(Celebration::class,'celebration_id');
    }

    public function invoiceDetail()
    {
        return $this->hasMany(InvoiceDetail::class,'celebrate_detail_id');
    }

    public function packBuy()
    {
        return $this->hasMany(PackBuy::class,'celebrate_detail_id');
    }

    public function transactionDetail()
    {
        return $this->hasMany(TransactionDetail::class,'celebrate_detail_id');
    }

    public function layouts()
    {
        return $this->belongsToMany(Layout::class);
    }

    public function guests()
    {
        return $this->hasMany(Guest::class,'celebrate_detail_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function surveys()
    {
        return $this->hasMany(Survey::class,'celebrate_detail_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function memories()
    {
        return $this->hasMany(Memory::class,'celebrate_detail_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function galleries()
    {
        return $this->hasMany(Gallery::class,'celebrate_detail_id');
    }

    public function aboutBrideGroom()
    {
        return $this->hasOne(AboutBrideGroom::class,'celebrate_detail_id');
    }


}
