<?php

namespace App\Models;

use App\Traits\TransactionDetailTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\TransactionDetail
 *
 * @property int $transaction_id
 * @property int $store_id
 * @property int|null $invoice_store_id
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereInvoiceStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereCelebrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $wedding_id
 * @property int|null $invoice_id
 * @property-read \App\Models\Invoice|null $invoice
 * @property-read \App\Models\Transaction|null $transaction
 * @property-read \App\Models\Celebration $celebration
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereInvoiceId($value)
 * @property int $celebration_id
 */
class TransactionDetail extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use TransactionDetailTrait;

    protected $primaryKey = 'transaction_id';
//    public $incrementing = false;

    protected $fillable = ['transaction_id','celebration_id','invoice_id','celebrate_detail_id','total_amount'];

//    protected static function boot()
//    {
//        parent::boot();
//
//        static::saving(function ($model) {
//
//        });
//
//
//    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class,'transaction_id');
    }

    public function celebration()
    {
        return $this->belongsTo(Celebration::class,'celebration_id');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class,'invoice_id');
    }

    public function celebrateDetail()
    {
        return $this->belongsTo(CelebrateDetail::class,'celebrate_detail_id');
    }
}
