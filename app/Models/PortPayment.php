<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\PortPayment
 *
 * @property int $id
 * @property string|null $slug
 * @property int $store_id
 * @property int|null $invoice_store_id
 * @property int|null $invoice_customer_id
 * @property string|null $authority
 * @property int $amount
 * @property int $discount
 * @property int $discount_cent
 * @property string|null $description
 * @property int $payment
 * @property string|null $tracking_code_error
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereAuthority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereDiscountCent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereInvoiceCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereInvoiceStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment wherePayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereCelebrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereTrackingCodeError($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\InvoiceCustomer|null $invoiceCustomer
 * @property-read \App\Models\Invoice|null $invoiceStore
 * @property-read \App\Models\Celebration $store
 * @property string|null $tracking_code
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereTrackingCode($value)
 * @property int $wedding_id
 * @property int|null $invoice_id
 * @property-read \App\Models\Invoice|null $invoice
 * @property-read \App\Models\Celebration $celebration
 * @method static \Illuminate\Database\Eloquent\Builder|PortPayment whereInvoiceId($value)
 * @property int $celebration_id
 */
class PortPayment extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;

    public $amount_str;

    protected $fillable = ['slug','celebration_id','invoice_id','celebrate_detail_id','authority','tracking_code'
        ,'amount','discount','discount_cent','description','payment','tracking_code_error'];

    public function celebration()
    {
        return $this->belongsTo(Celebration::class,'celebration_id');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class , 'invoice_id');
    }

    public function celebrationDetail()
    {
        return $this->belongsTo(CelebrateDetail::class , 'celebrate_detail_id');
    }

}
