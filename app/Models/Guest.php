<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Guest
 *
 * @property int $id
 * @property string|null $slug
 * @property string|null $name_fa
 * @property string|null $name_en
 * @property string|null $extra_name
 * @property string|null $family
 * @property string|null $count_family
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest query()
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest whereExtraName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest whereNameFa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest whereFamily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest whereCountFamily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|App\Models\Guest whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $user_id
 * @property string|null $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\Memory[] $memories
 * @property-read int|null $memories_count
 * @property-read \App\Models\Survey|null $survey
 * @method static \Illuminate\Database\Eloquent\Builder|Guest whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Guest whereUserId($value)
 */
class Guest extends Model
{
    public $survey_description;

    protected $fillable = [
        'slug',
        'user_id',
        'celebrate_detail_id',
        'celebration_id',
        'name',
        'extra_name',
        'family',
        'count_family',
        'status'
    ];

    public function scopeGetCelebrateDetail($query,$arg)
    {
        return $query->whereHas('celebrateDetail',function ($query) use($arg) {
            return $query->where($arg[0],$arg[1]);
        });
    }

    public function scopeDirectoryId($query)
    {
        return $query->first()->celebrateDetail->layouts()->first()->directory_id;
    }

    public function survey()
    {
        return $this->hasOne(Survey::class,'guest_id');
    }

    public function memories()
    {
        return $this->hasMany(Memory::class,'guest_id');
    }

    public function celebrateDetail()
    {
        return $this->belongsTo(CelebrateDetail::class,'celebrate_detail_id','id');
    }

    public function celebration()
    {
        return $this->belongsTo(Celebration::class,'celebration_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function aboutBrideGroom()
    {
        return $this->hasOne(AboutBrideGroom::class,'guest_id');
    }

    public function logs()
    {
        return $this->hasMany(Log::class,'guest_id');
    }

}
