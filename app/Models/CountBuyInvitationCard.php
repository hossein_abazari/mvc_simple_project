<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CountBuyInvitationCard
 *
 * @property int $id
 * @property int|null $celebrate_detail_id
 * @property int|null $user_id
 * @property int $number_card
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\CelebrateDetail|null $celebrateDetail
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|CountBuyInvitationCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CountBuyInvitationCard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CountBuyInvitationCard query()
 * @method static \Illuminate\Database\Eloquent\Builder|CountBuyInvitationCard whereCelebrateDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountBuyInvitationCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountBuyInvitationCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountBuyInvitationCard whereNumberCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountBuyInvitationCard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountBuyInvitationCard whereUserId($value)
 * @mixin \Eloquent
 */
class CountBuyInvitationCard extends Model
{
    use HasFactory;

    protected $fillable = ['celebrate_detail_id','user_id', 'number_card'];

    /**
     * Relation CelebrateDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function celebrateDetail()
    {
        return $this->belongsTo(CelebrateDetail::class,'celebrate_detail_id');
    }

    /**
     * Relation CelebrateDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'celebrate_detail_id');
    }
}
