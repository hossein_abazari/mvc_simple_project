<?php

namespace App\Models;

use App\Casts\TypeTransactionCast;
use App\Enums\TransactionType;
use App\Traits\TransactionTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;




/**
 * App\Models\Transaction
 *
 * @property int $id
 * @property string|null $slug
 * @property string $type
 * @property string|null $payment_date
 * @property int $amount if 0 >= is Creditor and 0 <= is Debtor
 * @property string|null $description
 * @property string|null $tracking_code
 * @property int $payment if payment is true record for pay
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \App\Models\TransactionDetail|null $transactionDetail
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction newQuery()
 * @method static \Illuminate\Database\Query\Builder|Transaction onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction wherePayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction wherePaymentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereTrackingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Transaction withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Transaction withoutTrashed()
 * @mixin \Eloquent
 */
class Transaction extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;
    use TransactionTrait;

    public $store_name;
    public $invoice_code;
    public $type_payment;
    public $amount_str;
    public $date_locale;
    public $payment_str;
    public $date;

    protected $fillable = ['slug','type','payment_date','amount','description','tracking_code','payment'];

    protected $casts = [
        'type' => TypeTransactionCast::class,
    ];

    public function transactionDetail()
    {
        return $this->hasOne(TransactionDetail::class,'transaction_id');
    }

}
