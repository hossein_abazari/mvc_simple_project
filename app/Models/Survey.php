<?php

namespace App\Models;

use App\Casts\ActionSurveyCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Survey
 *
 * @property int $id
 * @property string|null $slug
 * @property int $guest_id
 * @property string $action
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $user_id
 * @property-read \App\models\Guest $guest
 * @method static \Illuminate\Database\Eloquent\Builder|Survey whereUserId($value)
 * @property int|null $celebrate_detail_id
 * @property string|null $deleted_at
 * @property-read \App\Models\CelebrateDetail|null $celebrateDetail
 * @method static \Illuminate\Database\Eloquent\Builder|Survey whereCelebrateDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Survey whereDeletedAt($value)
 */
class Survey extends Model
{
    public $action_description;
    use SoftDeletes;

    protected $fillable = ['slug','guest_id','celebrate_detail_id','action'];

    protected $casts = [
        'action_description' => ActionSurveyCast::class
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function guest()
    {
        return $this->belongsTo(Guest::class,'guest_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function celebrateDetail()
    {
        return $this->belongsTo(CelebrateDetail::class,'celebrate_detail_id ');
    }


}
