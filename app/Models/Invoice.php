<?php

namespace App\Models;

use App\Traits\InvoiceTrait;
use App\Traits\ToolTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


/**
 * App\Models\Invoice
 *
 * @property int $id
 * @property string|null $slug
 * @property int $store_id
 * @property string|null $invoice_code code manual for invoice
 * @property string|null $invoice_date date manual for invoice
 * @property string|null $name
 * @property string|null $description
 * @property int $amount
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InvoiceDetail[] $invoiceDetails
 * @property-read int|null $invoice_details_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PortPayment[] $portPayments
 * @property-read int|null $port_payments_count
 * @property-read \App\Models\Celebration $celebration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TransactionDetail[] $transactionStore
 * @property-read int|null $transaction_store_count
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice newQuery()
 * @method static \Illuminate\Database\Query\Builder|Invoice onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereInvoiceCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereInvoiceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereCelebrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Invoice withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Invoice withoutTrashed()
 * @mixin \Eloquent
 * @property int $wedding_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TransactionDetail[] $transactionDetail
 * @property-read int|null $transaction_detail_count
 * @property int $celebration_id
 */
class Invoice extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;
    use InvoiceTrait;
    Use ToolTrait;

    public $celebration_name;
    public $type_invoice;
    public $amount_str;

    protected $fillable = ['slug','invoice_code','invoice_date','celebration_id','name','description','amount','type'];


    public function invoiceDetails()
    {
        return $this->hasMany(InvoiceDetail::class , 'invoice_id');
    }

    public function celebration()
    {
        return $this->belongsTo(Celebration::class,'celebration_id');
    }

    public function portPayments()
    {
        return $this->hasMany(PortPayment::class,'invoice_id');
    }

    public function transactionDetail()
    {
        return $this->hasMany(TransactionDetail::class,'invoice_id');
    }
}
