<?php

namespace App\Models;

use App\Traits\DeadlineTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\DeadlineTrait
 *
 * @property int $id
 * @property string|null $slug
 * @property string|null $name
 * @property int $expire_date
 * @property int $amount
 * @property int $popular
 * @property int $status
 * @property int $free
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline query()
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline whereExpireDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline whereFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline wherePopular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deadline whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|Deadline onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|Deadline withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Deadline withoutTrashed()
 */
class Deadline extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;
    use DeadlineTrait;

    protected $fillable = ['slug','name','expire_date','amount','popular','status','free'];


}
