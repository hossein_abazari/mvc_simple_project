<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Layout
 *
 * @property int $id
 * @property string|null $slug
 * @property int $directory_id
 * @property string $name
 * @property string|null $description
 * @property string|null $directory
 * @property string|null $img_temp
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Celebration[] $celebrations
 * @property-read int|null $celebrations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Layout newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Layout newQuery()
 * @method static \Illuminate\Database\Query\Builder|Layout onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Layout query()
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereDirectory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereDirectoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereImgTemp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Layout withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Layout withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereType($value)
 * @property int $number_invitation_card
 * @property int $price
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CelebrateDetail[] $celebrateDetails
 * @property-read int|null $celebrate_details_count
 * @method static \Illuminate\Database\Eloquent\Builder|Layout whereNumberInvitationCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Layout wherePrice($value)
 */
class Layout extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    public $priceStr;

    protected $fillable =
        [
        'slug',
        'directory_id',
        'type',
        'name',
        'number_invitation_card',
        'description',
        'directory',
        'img_temp',
        'price',
        'free', // enum y n default n
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function celebrateDetails()
    {
        return $this->belongsToMany(CelebrateDetail::class);
    }
}
