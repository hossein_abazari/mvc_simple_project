<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


/**
 * App\Models\AboutBrideGroom
 *
 * @property int $id
 * @property int|null $celebrate_detail_id
 * @property int $guest_id
 * @property string $name_groom
 * @property string $name_bride
 * @property string|null $description_groom
 * @property string|null $description_bride
 * @property string|null $img_groom
 * @property string|null $img_bride
 * @property array|null $introduction
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \App\Models\CelebrateDetail|null $celebrateDetail
 * @property-read \App\models\Guest $guest
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom newQuery()
 * @method static \Illuminate\Database\Query\Builder|AboutBrideGroom onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom query()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereCelebrateDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereDescriptionBride($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereDescriptionGroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereImgBride($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereImgGroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereIntroduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereNameBride($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereNameGroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|AboutBrideGroom withTrashed()
 * @method static \Illuminate\Database\Query\Builder|AboutBrideGroom withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $slug
 * @method static \Illuminate\Database\Eloquent\Builder|AboutBrideGroom whereSlug($value)
 */
class AboutBrideGroom extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;
    public $arr = [
        'date' => '',
        'title' => '',
        'body' => '',
        'class_right' => 'timeline-inverted',
        'class_right_2' => 'bg-color1',
    ];

    protected $fillable = [
        'slug',
        'celebrate_detail_id',
        'name_groom',
        'name_bride',
        'description_groom',
        'description_bride',
        'img_groom',
        'img_bride',
        'introduction'
    ];


    /**
     * Introduction is json convert to array
     *
     * @var string[]
     */
    protected $casts = [
        'introduction' => 'array',
    ];


    /**
     * relation CelebrateDetail
     *
     * @return void
     */
    public function celebrateDetail()
    {
        return $this->belongsTo(CelebrateDetail::class,'celebrate_detail_id');
    }

    /**
     * relation Guest
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function guest()
    {
        return $this->hasOne(Guest::class,'guest_id');
    }
}
