<?php

namespace App\Models;

use App\Traits\InvoiceDetailTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\InvoiceDetail
 *
 * @property int $id
 * @property int|null $invoice_customer_id
 * @property int|null $invoice_store_id
 * @property int|null $product_id
 * @property int $number
 * @property int $amount
 * @property int $total_amount
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereInvoiceCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereInvoiceStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereTotalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $invoice_id
 * @property-read \App\Models\Invoice|null $invoice
 * @method static \Illuminate\Database\Query\Builder|InvoiceDetail onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereInvoiceId($value)
 * @method static \Illuminate\Database\Query\Builder|InvoiceDetail withTrashed()
 * @method static \Illuminate\Database\Query\Builder|InvoiceDetail withoutTrashed()
 * @property int|null $celebrate_detail_id
 * @property-read \App\Models\CelebrateDetail|null $celebrateDetail
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceDetail whereCelebrateDetailId($value)
 */
class InvoiceDetail extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;
    use InvoiceDetailTrait;

    protected $fillable = ['invoice_id','celebrate_detail_id','number','amount','total_amount'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class , 'invoice_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function celebrateDetail()
    {
        return $this->belongsTo(CelebrateDetail::class , 'celebrate_detail_id ');
    }
}
