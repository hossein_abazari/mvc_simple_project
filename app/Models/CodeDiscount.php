<?php

namespace App\Models;

use App\Casts\AmountDecimalCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\CodeDiscount
 *
 * @property int $id
 * @property string|null $slug
 * @property int|null $user_id
 * @property string|null $name
 * @property string|null $code_discount
 * @property int|null $cent_discount
 * @property int|null $amount_discount
 * @property int|null $cent_inviter
 * @property int|null $cent_amount_inviter
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount query()
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereAmountDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereCentDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereCentInviter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereCentAmountInviter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereCodeDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeDiscount whereUserId($value)
 * @mixin \Eloquent
 */
class CodeDiscount extends Model implements Auditable
{
    public $amount_invitees;
    public $number_invitees;

    use \OwenIt\Auditing\Auditable;
    use HasFactory;


    protected $fillable = [
        'slug',
        'user_id',
        'name',
        'code_discount',
        'cent_discount',
        'amount_discount',
        'cent_amount_inviter',
        'cent_inviter',
        'status',
    ];

    protected $casts = [
        'amount_discount' => AmountDecimalCast::class,
        'cent_amount_inviter' => AmountDecimalCast::class,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function celebrations()
    {
        return $this->hasMany(Celebration::class,'code_discount_id');
    }
}
