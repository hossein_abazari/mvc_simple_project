<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CelebrateDetailRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\CelebrateDetailRepositoryInterface',
            'App\Repositories\CelebrateDetailRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
