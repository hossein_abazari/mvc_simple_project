<?php

namespace App\Providers;

use App\Repositories\AboutBrideGroomRepository;
use App\Repositories\AboutBrideGroomRepositoryInterface;
use App\Repositories\CountBuyInvitationCardRepository;
use App\Repositories\CountBuyInvitationCardRepositoryInterface;
use App\Repositories\Elequents\CodeDiscountRepository;
use App\Repositories\GuestRepository;
use App\Repositories\GuestRepositoryInterface;
use App\Repositories\Interfaces\CodeDiscountRepositoryInterface;
use App\Repositories\InvoiceRepository;
use App\Repositories\InvoiceRepositoryInterface;
use App\Repositories\LayoutRepository;
use App\Repositories\LayoutRepositoryInterface;
use App\Repositories\LogRepository;
use App\Repositories\LogRepositoryInterface;
use App\Repositories\PackBuyRepository;
use App\Repositories\PackBuyRepositoryInterface;
use App\Repositories\SurveyRepository;
use App\Repositories\SurveyRepositoryInterface;
use App\Repositories\GalleryRepository;
use App\Repositories\GalleryRepositoryInterface;


use App\Repositories\TicketDetailRepository;
use App\Repositories\TicketDetailRepositoryInterface;
use App\Repositories\TicketRepository;
use App\Repositories\TicketRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryBindServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            GuestRepositoryInterface::class,
            GuestRepository::class
        );
        $this->app->bind(
            SurveyRepositoryInterface::class,
            SurveyRepository::class
        );
        $this->app->bind(
            CountBuyInvitationCardRepositoryInterface::class,
            CountBuyInvitationCardRepository::class
        );

        $this->app->bind(CountBuyInvitationCardRepositoryInterface::class, CountBuyInvitationCardRepository::class);

         $this->app->bind(
            GalleryRepositoryInterface::class,
            GalleryRepository::class
        );

        $this->app->bind(LogRepositoryInterface::class, LogRepository::class);

        $this->app->bind(LayoutRepositoryInterface::class, LayoutRepository::class);
        $this->app->bind(InvoiceRepositoryInterface::class, InvoiceRepository::class);
        $this->app->bind(PackBuyRepositoryInterface::class, PackBuyRepository::class);

        $this->app->bind(
            AboutBrideGroomRepositoryInterface::class,
            AboutBrideGroomRepository::class
        );

        $this->app->bind(
            TicketRepositoryInterface::class,
            TicketRepository::class
        );

        $this->app->bind(
            TicketDetailRepositoryInterface::class,
            TicketDetailRepository::class
        );
        $this->app->bind(
            CodeDiscountRepositoryInterface::class,
            CodeDiscountRepository::class
        );

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
