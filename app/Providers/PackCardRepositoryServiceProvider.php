<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PackCardRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\PackCardRepositoryInterface',
            'App\Repositories\PackCardRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
