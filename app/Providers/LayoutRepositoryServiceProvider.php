<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LayoutRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\LayoutRepositoryInterface',
            'App\Repositories\LayoutRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
