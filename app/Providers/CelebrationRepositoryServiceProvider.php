<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CelebrationRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\CelebrationRepositoryInterface',
            'App\Repositories\CelebrationRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
