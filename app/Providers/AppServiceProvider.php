<?php

namespace App\Providers;

use App\Builder\TemplateBuilder;
use App\Builder\TemplateBuilderInterface;
use App\Classes\ObserverProvider;
use App\Models\Deadline;
use App\Models\Invoice;
use Faker\Provider\UserAgent;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use OwenIt\Auditing\Models\Audit;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        ServiceProvider::class => BuilderBindServiceProvider::class,
    ];
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Builder\PaymentBuilderInterface',
            'App\Builder\PaymentBuilder'
        );
        $this->app->bind(
            'App\Repositories\DiscountRepositoryInterface',
            'App\Repositories\DiscountRepository'
        );
        $this->app->bind(
            'App\Repositories\GuestRepositoryInterface',
            'App\Repositories\GuestRepository'
        );
        $this->app->bind(
            'App\Repositories\MemoryRepositoryInterface',
            'App\Repositories\MemoryRepository'
        );
        $this->app->bind(
            'App\Repositories\AuditRepositoryInterface',
            'App\Repositories\AuditRepository'
        );
        $this->app->bind(
            'App\Repositories\PortPaymentRepositoryInterface',
            'App\Repositories\PortPaymentRepository'
        );
        $this->app->bind(
            'App\Repositories\CelebrateDetailRepositoryInterface',
            'App\Repositories\CelebrateDetailRepository'
        );
        $this->app->bind(
            'App\Repositories\TransactionRepositoryInterface',
            'App\Repositories\TransactionRepository'
        );
        $this->app->bind(
            TemplateBuilderInterface::class,
            TemplateBuilder::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultstringLength(191);
        /* observe for code project */
        ObserverProvider::main();
    }
}
