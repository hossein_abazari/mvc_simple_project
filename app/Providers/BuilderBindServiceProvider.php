<?php

namespace App\Providers;

use App\Builder\AboutBrideGroomBuilder;
use App\Builder\AboutBrideGroomBuilderInterface;
use App\Builder\Admin\CodeDiscountBuilder;
use App\Builder\Admin\CodeDiscountBuilderInterface;
use App\Builder\CelebrateSettingBuilder;
use App\Builder\CelebrateSettingBuilderInterface;
use App\Builder\GuestBuilder;
use App\Builder\GuestBuilderInterface;
use App\Builder\MemoryBuilder;
use App\Builder\MemoryBuilderInterface;
use App\Builder\TemplateBuilder;
use App\Builder\TemplateBuilderInterface;
use App\Builder\GalleryBuilder;
use App\Builder\GalleryBuilderInterface;
use App\Builder\CelebrateDetailBuilder;
use App\Builder\CelebrateDetailBuilderInterface;
use App\Builder\TicketBuilder;
use App\Builder\TicketBuilderInterface;
use Illuminate\Support\ServiceProvider;

class BuilderBindServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            TemplateBuilderInterface::class,
            TemplateBuilder::class
        );
        $this->app->bind(
            GuestBuilderInterface::class,
            GuestBuilder::class
        );

         $this->app->bind(
            GalleryBuilderInterface::class,
            GalleryBuilder::class
        );

        $this->app->bind(
            CelebrateDetailBuilderInterface::class,
            CelebrateDetailBuilder::class
        );

        $this->app->bind(
            MemoryBuilderInterface::class,
            MemoryBuilder::class
        );

        $this->app->bind(
            AboutBrideGroomBuilderInterface::class,
            AboutBrideGroomBuilder::class
        );
        $this->app->bind(
            CelebrateSettingBuilderInterface::class,
            CelebrateSettingBuilder::class
        );
        $this->app->bind(
            TicketBuilderInterface::class,
            TicketBuilder::class
        );
        $this->app->bind(
            CodeDiscountBuilderInterface::class,
            CodeDiscountBuilder::class
        );

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
