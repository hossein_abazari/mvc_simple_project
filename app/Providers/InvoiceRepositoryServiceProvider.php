<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class InvoiceRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\InvoiceRepositoryInterface',
            'App\Repositories\InvoiceRepositoryRepository'
        );
        $this->app->bind(
            'App\Repositories\InvoiceDetailRepositoryInterface',
            'App\Repositories\InvoiceDetailRepositoryRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
