<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

/**
 * Set Enum Const
 */
final class TransactionType extends Enum
{
    const TYPE_UNPAID = 'unpaid';
    const TYPE_CASH = 'cash';
    const TYPE_CARD = 'card';
    const TYPE_PORT = 'port';
}
