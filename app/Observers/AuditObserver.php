<?php

namespace App\Observers;


use App\Models\UserLogin;
use App\Traits\ToolTrait;
use http\Client\Curl\User;
use OwenIt\Auditing\Models\Audit;

class AuditObserver
{
    use ToolTrait;
    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param Audit $audit
     * @return void
     */
    public function creating(Audit $audit)
    {
        $userLogin = UserLogin::latest()->whereUserId(\Auth::id())->first();

        $audit->slug = str_slug_random(8);

        if ($userLogin)
             $audit->user_login_id = $userLogin->id;
    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param Audit $audit
     * @return void
     */
    public function retrieved(Audit $audit)
    {
        $user = \App\Models\User::whereId($audit->user_id)->first();
//        $audit->user_id = $user->id." - ".$user->email;
        $audit->event = vl('admin',$audit->event);

    }
}
