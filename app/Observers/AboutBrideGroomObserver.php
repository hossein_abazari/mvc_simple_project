<?php

namespace App\Observers;

use App\Models\AboutBrideGroom;

class AboutBrideGroomObserver
{
    /**
     * Handle the AboutBrideGroom "created" event.
     *
     * @param  \App\Models\AboutBrideGroom  $aboutBrideGroom
     * @return void
     */
    public function created(AboutBrideGroom $aboutBrideGroom)
    {
        //
    }

    /**
     * @param $request
     * @param $fields
     * @return array
     */
    public function mergeDate($request, $fields)
    {
        $result = [];
        foreach ($fields as $field){
            foreach ($request->{$field} as $key => $value){
                $result[$key][$field] = $value;
            }
        }

        $i=0;
        while ($i < count($result)){
            $result[$i]['class_right'] = 'timeline-inverted';
            $result[$i]['class_right_2'] = 'bg-color1';
            $i = $i+2;
        }

        return $result;

    }

    /**
     * Handle the Transaction "force creating" event.
     *
     * @param AboutBrideGroom $aboutBrideGroom
     * @return void
     */
    public function creating(AboutBrideGroom $aboutBrideGroom)
    {
        $array = ['date','title','body'];

        $aboutBrideGroom->slug = str_slug_random(8);
//        dd(serialize(['introduction' => $this->mergeDate(\Request(),$array)]));
        $aboutBrideGroom->introduction = $this->mergeDate(\Request(),$array);
    }

    /**
     * Handle the AboutBrideGroom "updated" event.
     *
     * @param  \App\Models\AboutBrideGroom  $aboutBrideGroom
     * @return void
     */
    public function updated(AboutBrideGroom $aboutBrideGroom)
    {
        //
    }

    /**
     * Handle the AboutBrideGroom "deleted" event.
     *
     * @param  \App\Models\AboutBrideGroom  $aboutBrideGroom
     * @return void
     */
    public function deleted(AboutBrideGroom $aboutBrideGroom)
    {
        //
    }

    /**
     * Handle the AboutBrideGroom "restored" event.
     *
     * @param  \App\Models\AboutBrideGroom  $aboutBrideGroom
     * @return void
     */
    public function restored(AboutBrideGroom $aboutBrideGroom)
    {
        //
    }

    /**
     * Handle the AboutBrideGroom "force deleted" event.
     *
     * @param  \App\Models\AboutBrideGroom  $aboutBrideGroom
     * @return void
     */
    public function forceDeleted(AboutBrideGroom $aboutBrideGroom)
    {
        //
    }
}
