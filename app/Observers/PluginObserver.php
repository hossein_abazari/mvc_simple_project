<?php

namespace App\Observers;

use App\Models\Deadline;
use App\Models\Invoice;
use App\Models\Plugin;
use App\Models\Transaction;
use App\Models\Store;
use App\Traits\ToolTrait;
use Carbon\Carbon;

class PluginObserver
{
    use ToolTrait;

    /**
     * @param Plugin $plugin
     */
    public function creating(Plugin $plugin)
    {
        $plugin->slug = str_slug_random(8);

    }


    /**
     * @param Plugin $plugin
     */
    public function saving(Plugin $plugin)
    {

        if ($plugin->amount) {
            $plugin->amount = str_replace(',', '', $plugin->amount);
        }
        else{
            $plugin->amount = 0;
        }

    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param Plugin $plugin
     * @return void
     */
    public function retrieved(Plugin $plugin)
    {
        $plugin->amount_str = number_format($plugin->amount);
    }


}
