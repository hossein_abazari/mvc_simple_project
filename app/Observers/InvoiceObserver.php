<?php

namespace App\Observers;

use App\Models\Celebration;
use App\Models\Invoice;
use App\Traits\ToolTrait;

class InvoiceObserver
{
    use ToolTrait;

    /**
     * Handle the Invoice "force seving" event.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return void
     */

    public function creating(Invoice $invoice)
    {
        $invoice->slug = str_slug_random(8);
    }

    /**
     * Handle the Invoice "force seving" event.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return void
     */
    public function saving(Invoice $invoice)
    {
        if ($invoice->amount) {
            $invoice->amount = str_replace(',', '', $invoice->amount);
        }
        else{
            $invoice->amount = 0;
        }

        if (is_string($invoice->celebration_id)){
            $celebration = Celebration::whereSlug($invoice->celebration_id)->first();
            $invoice->celebration_id = $celebration->id;
        }


    }

    /**
     * Handle the Invoice "force retrieved" event.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return void
     */
    public function retrieved(Invoice $invoice)
    {
        $invoice->amount_str = number_format($invoice->amount);

        $invoice->celebration_name = Celebration::whereId($invoice->celebration_id)->first()->name;

        $invoice->type_invoice = vl('admin',$invoice->type);

    }
}
