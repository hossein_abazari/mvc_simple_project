<?php

namespace App\Observers;

use App\Models\PortPayment;
use App\Traits\ToolTrait;

class PortPaymentObserver
{
    use ToolTrait;

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param PortPayment $portPayment
     * @return void
     */
    public function creating(PortPayment $portPayment)
    {
        $portPayment->slug = str_slug_random(8);

    }


    /**
     * @param PortPayment $portPayment
     */
    public function saving(PortPayment $portPayment)
    {
        if ($portPayment->amount) {
            $portPayment->amount = str_replace(',', '', $portPayment->amount);
        }
        else{
            $portPayment->amount = 0;
        }

    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param PortPayment $portPayment
     * @return void
     */
    public function retrieved(PortPayment $portPayment)
    {
        $portPayment->amount_str = number_format($portPayment->amount);
    }
}
