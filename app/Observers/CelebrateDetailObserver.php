<?php

namespace App\Observers;

use App\Models\CelebrateDetail;
use App\Traits\ToolTrait;

class CelebrateDetailObserver
{
    /* count day */
    private $expireDay = 60;
    use ToolTrait;
    /**
     * Handle the CelebrateDetail "created" event.
     *
     * @param  \App\Models\CelebrateDetail  $celebrateDetail
     * @return void
     */
    public function created(CelebrateDetail $celebrateDetail)
    {
        //
    }

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param CelebrateDetail $celebrateDetail
     * @return void
     */
    public function creating(CelebrateDetail $celebrateDetail)
    {
        $celebrateDetail->slug = str_slug_random(8);

        $celebrateDetail->expire = timeExpire($this->expireDay);
    }

    /**
     * Handle the CelebrateDetail "updated" event.
     *
     * @param  \App\Models\CelebrateDetail  $celebrateDetail
     * @return void
     */
    public function updated(CelebrateDetail $celebrateDetail)
    {
        //
    }

    /**
     * Handle the CelebrateDetail "deleted" event.
     *
     * @param  \App\Models\CelebrateDetail  $celebrateDetail
     * @return void
     */
    public function deleted(CelebrateDetail $celebrateDetail)
    {
        //
    }

    /**
     * Handle the CelebrateDetail "restored" event.
     *
     * @param  \App\Models\CelebrateDetail  $celebrateDetail
     * @return void
     */
    public function restored(CelebrateDetail $celebrateDetail)
    {
        //
    }

    /**
     * Handle the CelebrateDetail "force deleted" event.
     *
     * @param  \App\Models\CelebrateDetail  $celebrateDetail
     * @return void
     */
    public function forceDeleted(CelebrateDetail $celebrateDetail)
    {
        //
    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param CelebrateDetail $celebrateDetail
     * @return void
     */
    public function retrieved(CelebrateDetail $celebrateDetail)
    {
        $celebrateDetail->image_layout = $celebrateDetail->layouts()->first() ?
            $celebrateDetail->layouts()->first()->img_temp  :
            null;

        $celebrateDetail->number_invitation_card = $celebrateDetail->layouts()->sum('number_invitation_card') +
                $celebrateDetail->packBuy()->sum('number_card');

        $celebrateDetail->type_celebrate_name = vl('general',$celebrateDetail->type_celebrate);

        $celebrateDetail->expire_date_convert = $this->formatTypeDate($celebrateDetail->date_ceremony,'Y/m/d');

        $celebrateDetail->expire_date = $this->formatTypeDate($celebrateDetail->expire,'Y/m/d');

        $celebrateDetail->expire_time = $this->formatTypeDate($celebrateDetail->expire,'H:i:s');

        $celebrateDetail->total_amount = number_format($celebrateDetail->layouts()->sum('price') +
        $celebrateDetail->packBuy()->sum('amount'));

        $celebrateDetail->text_invite_show = $celebrateDetail->text_invite;
        $celebrateDetail->text_footer_show = $celebrateDetail->text_footer;

        if (isset(vl('sample_name')[$celebrateDetail->text_invite])) {
            $celebrateDetail->text_invite_show = vl('sample_name')[$celebrateDetail->text_invite];
        }

        if (isset(vl('text_footer')[$celebrateDetail->text_footer])) {
            $celebrateDetail->text_footer_show = vl('text_footer')[$celebrateDetail->text_footer];
        }


    }
}
