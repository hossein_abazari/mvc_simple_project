<?php

namespace App\Observers;

use App\Models\Celebration;
use App\Models\TicketDetail;
use App\Models\TicketMaster;
use App\Models\User;

class TicketDetailObserver
{

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param TicketDetail $ticketDetail
     * @return void
     */
    public function crating(TicketDetail $ticketDetail)
    {
        $ticketDetail->slug = str_slug_random(8);
    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param TicketDetail $ticketDetail
     * @return void
     */
    public function retrieved(TicketDetail $ticketDetail)
    {
        $ticketDetail->slugMaster = $ticketDetail->ticketMaster->slug;
    }
}
