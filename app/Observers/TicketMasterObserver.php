<?php

namespace App\Observers;

use App\Models\Celebration;
use App\Models\Store;
use App\Models\TicketMaster;
use App\Models\User;
use App\Repositories\TicketDetailRepositoryInterface;

class TicketMasterObserver
{
    public $ticketDetailRepository;

    public function __construct(TicketDetailRepositoryInterface $ticketDetailRepository)
    {
        $this->ticketDetailRepository = $ticketDetailRepository;
    }

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param TicketMaster $ticketMaster
     * @return void
     */
    public function creating(TicketMaster $ticketMaster)
    {
        $ticketMaster->slug = str_slug_random(8);
    }

    /**
     * @param TicketMaster $ticketMaster
     * @return void
     */
    public function saving(TicketMaster $ticketMaster)
    {
        if (isset($ticketMaster->message)){
            $data = [
                'user_id' => \Auth::getUser()->id,
                'ticket_master_id' => $ticketMaster->id,
                'message' => $ticketMaster->message
            ];

            $this->ticketDetailRepository->setCreate($data);
        }
    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param TicketMaster $ticketMaster
     * @return void
     */
    public function retrieved(TicketMaster $ticketMaster)
    {
        $ticketMaster->celebrate_name = Celebration::find($ticketMaster->celebration_id)->name; // todo fix name store_name

        $ticketMaster->user_name = User::find($ticketMaster->user_id)->name;

        $ticketMaster->closed = $ticketMaster->close ? vl('admin','yes') : vl('admin','no');
    }
}
