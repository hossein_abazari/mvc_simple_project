<?php

namespace App\Observers;

use App\Models\Transaction;
use App\Models\Celebration;

class CelebrationObserver
{
    /**
     * Handle the CelebrateDetail "created" event.
     *
     * @param Celebration $celebration
     * @return void
     */
    public function created(Celebration $celebration)
    {
    }

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param Celebration $celebration
     * @return void
     */
    public function creating(Celebration $celebration)
    {
        $celebration->slug = str_slug_random(8);
    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param Celebration $celebration
     * @return void
     */
    public function retrieved(Celebration $celebration)
    {
        $transaction = Transaction::leftJoin('transaction_details', function ($join) {
            $join->on('transaction_details.transaction_id', '=', 'transactions.id');
        })->where('transaction_details.celebration_id', $celebration->id);

        $totalAmount = $celebration->transactionDetail()->where('total_amount','<',0)->sum('total_amount');

        $celebration->amount_debt = toDebtCredit($transaction->sum('amount'));

        $celebration->amount_paid_operation = (int)str_replace('-','',$totalAmount);

    }
}
