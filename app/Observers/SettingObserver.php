<?php

namespace App\Observers;

use App\Models\Setting;
use App\Traits\ToolTrait;

class SettingObserver
{
    use ToolTrait;

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param Setting $setting
     * @return void
     */
    public function creating(Setting $setting)
    {
        $setting->slug = str_slug_random(8);
    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param Setting $setting
     * @return void
     */
    public function saving(Setting $setting)
    {
        if($setting->isDirty('language')){
            $setting->language = json_encode($setting->language);
        }
    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param Setting $setting
     * @return void
     */
    public function retrieved(Setting $setting)
    {



    }
}
