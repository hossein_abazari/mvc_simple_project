<?php

namespace App\Observers;

use App\Classes\Helper;
use App\models\Guest;

class GuestObserver
{
    /**
     * Handle the Guest "created" event.
     *
     * @param  \App\Models\Guest  $guest
     * @return void
     */
    public function created(Guest $guest)
    {
        //
    }

    /**
     * Handle the Transaction "force creating" event.
     *
     * @param Guest $guest
     * @return void
     */
    public function creating(Guest $guest)
    {
//        $guest->slug = str_slug_random(8);
    }


    /**
     * Handle the Guest "updated" event.
     *
     * @param  \App\Models\Guest  $guest
     * @return void
     */
    public function updated(Guest $guest)
    {
        //
    }

    /**
     * Handle the Guest "deleted" event.
     *
     * @param  \App\Models\Guest  $guest
     * @return void
     */
    public function deleted(Guest $guest)
    {
        //
    }

    /**
     * Handle the Guest "restored" event.
     *
     * @param  \App\Models\Guest  $guest
     * @return void
     */
    public function restored(Guest $guest)
    {
        //
    }

    /**
     * Handle the Guest "force deleted" event.
     *
     * @param  \App\Models\Guest  $guest
     * @return void
     */
    public function forceDeleted(Guest $guest)
    {
        //
    }

    public function retrieved(Guest $guest)
    {
        $guest->survey_description = Helper::guestTextAlert($guest);
    }
}
