<?php

namespace App\Observers;

use App\Models\Celebration;
use App\Models\Store;
use App\Models\User;
use App\Models\UserLogin;
use App\Traits\ToolTrait;

class UserObserver
{
    use ToolTrait;

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param User $user
     * @return void
     */
    public function creating(User $user)
    {
        $user->slug = str_slug_random(8);

    }


    /**
     * Handle the User Login "force retrieved" event.
     *
     * @param User $user
     * @return void
     */
    public function saving(User $user)
    {
        if($user->isDirty('password')) {
            $user->password = bcrypt($user->password);
        }
        if ($user->isDirty('celebration_id') && is_string($user->celebration_id)){
            $celebration = Celebration::whereSlug($user->celebration_id)->first();
            $user->celebration_id = $celebration->id;
        }
    }
}
