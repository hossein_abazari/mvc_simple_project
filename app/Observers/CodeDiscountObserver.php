<?php

namespace App\Observers;

use App\Models\CodeDiscount;
use App\Traits\ToolTrait;

class CodeDiscountObserver
{
    use ToolTrait;

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param CodeDiscount $codeDiscount
     * @return void
     */
    public function creating(CodeDiscount $codeDiscount)
    {
        $codeDiscount->slug = str_slug_random(8);

    }


    /**
     * @param CodeDiscount $codeDiscount
     */
    public function saving(CodeDiscount $codeDiscount)
    {


    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param CodeDiscount $codeDiscount
     * @return void
     */
    public function retrieved(CodeDiscount $codeDiscount)
    {
        $celebrations = $codeDiscount->celebrations()->get();
        $numberInvite = 0;
        if ($celebrations->isNotEmpty()){
            $numberInvite = $celebrations->count();
        }
        $codeDiscount->number_invitees = $numberInvite;

        $amountPaid = $codeDiscount->celebrations()->get()->sum(function($item) {
            return $item->amount_paid;
        });

         $amountInviter = $amountPaid * ($codeDiscount->cent_inviter /100);

        $codeDiscount->amount_invitees = number_format($amountInviter);
    }
}
