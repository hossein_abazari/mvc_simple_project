<?php

namespace App\Observers;

use App\Models\Celebration;
use App\Models\Invoice;
use App\Models\Transaction;
use App\Models\Store;
use App\Traits\ToolTrait;
use Carbon\Carbon;

class TransactionObserver
{
    use ToolTrait;

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return void
     */
    public function creating(Transaction $transaction)
    {
        $transaction->slug = str_slug_random(8);

        $transaction->tracking_code = mt_rand(1000, 99999).str_slug_random(4);

    }

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return void
     */
    public function saving(Transaction $transaction)
    {

        if ($transaction->amount) {
            $transaction->amount = $transaction->payment ? -str_replace(',', '', $transaction->amount) : str_replace(',', '', $transaction->amount);
        }
        else{
            $transaction->amount = 0;
        }


    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return void
     */
    public function retrieved(Transaction $transaction)
    {
//        $transaction->amount_str = number_format($transaction->amount);
//
//        $transaction->store_name = Celebration::whereId($transaction->transactionDetail->celebration_id)->first()->name;
//
//        $transaction->type_payment = vl('admin',$transaction->type);
//
//        $transaction->date_locale = $this->formatTypeDate($transaction->payment_date,'Y/m/d');
//
//        $transaction->invoice_code = $transaction -> transactionDetail ->invoice_id ?
//            Invoice::whereId($transaction->transactionDetail->invoice_id)->first()->invoice_code : '--';
//
//        $transaction->payment_str = $transaction->payment == true ? vl('admin','creditor') : vl('admin','debtor');

    }
}
