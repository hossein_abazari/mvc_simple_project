<?php

namespace App\Observers;

use App\Models\Deadline;
use App\Models\Invoice;
use App\Models\Transaction;
use App\Models\Store;
use App\Traits\ToolTrait;
use Carbon\Carbon;

class DeadlineObserver
{
    use ToolTrait;

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param Deadline $deadline
     * @return void
     */
    public function creating(Deadline $deadline)
    {
        $deadline->slug = str_slug_random(8);

    }


    /**
     * @param Deadline $deadline
     */
    public function saving(Deadline $deadline)
    {
        if ($deadline->amount) {
            $deadline->amount = str_replace(',', '', $deadline->amount);
        }
        else{
            $deadline->amount = 0;
        }

        $deadline->free = $deadline->free && $deadline->free != 1 ? true : false;

    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param Deadline $deadline
     * @return void
     */
    public function retrieved(Deadline $deadline)
    {
        $deadline->amount_str = number_format($deadline->amount);
    }


}
