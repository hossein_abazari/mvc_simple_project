<?php

namespace App\Observers;

use App\Models\Deadline;
use App\Models\InvoiceDetail;
use App\Models\Invoice;
use App\Models\Plugin;
use App\Models\Transaction;
use App\Models\Celebration;
use App\Repositories\TransactionRepository;
use App\Traits\ToolTrait;
use Carbon\Carbon;

class InvoiceDetailObserver
{
    use ToolTrait;

    protected $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * @param InvoiceDetail $invoiceDetail
     */
    public function saving(InvoiceDetail $invoiceDetail)
    {
        $invoiceDetail->number = $invoiceDetail->number ?? 1;

        if ($invoiceDetail->amount) {
            $invoiceDetail->amount = str_replace(',', '', $invoiceDetail->amount);
            $invoiceDetail->total_amount = str_replace(',', '', $invoiceDetail->amount) * $invoiceDetail->number;
        }
        else{
            $invoiceDetail->amount = 0;
            $invoiceDetail->total_amount = 0;
        }

    }

    /**
     * Handle the TransactionDetail "created" event.
     *
     * @param InvoiceDetail $invoiceDetail
     * @return void
     */
    public function created(InvoiceDetail $invoiceDetail)
    {
        $this->transactionRepository->setInsertMasterDetail($invoiceDetail);
    }

    /**
     * Handle the Invoice "force retrieved" event.
     *
     * @param InvoiceDetail $invoiceDetail
     * @return void
     */
    public function retrieved(InvoiceDetail $invoiceDetail)
    {
        $invoiceDetail->amount_str = number_format($invoiceDetail->amount);
        $invoiceDetail->total_amount_str = number_format($invoiceDetail->total_amount);

    }


}
