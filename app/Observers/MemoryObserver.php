<?php

namespace App\Observers;

use App\Models\Memory;

class MemoryObserver
{
    /**
     * Handle the Memory "created" event.
     *
     * @param  \App\Models\Memory  $memory
     * @return void
     */
    public function created(Memory $memory)
    {
        //
    }

    /**
     * @param Memory $memory
     */
    public function creating(Memory $memory)
    {
        $memory->slug = str_slug_random(8);
    }

    /**
     * Handle the Memory "updated" event.
     *
     * @param  \App\Models\Memory  $memory
     * @return void
     */
    public function updated(Memory $memory)
    {
        //
    }

    /**
     * Handle the Memory "deleted" event.
     *
     * @param  \App\Models\Memory  $memory
     * @return void
     */
    public function deleted(Memory $memory)
    {
        //
    }

    /**
     * Handle the Memory "restored" event.
     *
     * @param  \App\Models\Memory  $memory
     * @return void
     */
    public function restored(Memory $memory)
    {
        //
    }

    /**
     * Handle the Memory "force deleted" event.
     *
     * @param  \App\Models\Memory  $memory
     * @return void
     */
    public function forceDeleted(Memory $memory)
    {
        //
    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param Memory $memory
     * @return void
     */
    public function retrieved(Memory $memory)
    {
        $memory->guestName = $memory->guest->name;
    }
}
