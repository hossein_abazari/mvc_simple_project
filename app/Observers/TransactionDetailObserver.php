<?php

namespace App\Observers;

use App\Models\Celebration;
use App\Models\TransactionDetail;
use App\Repositories\TransactionRepository;

class TransactionDetailObserver
{
    public $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Handle the TransactionDetail "saving" event.
     *
     * @param  \App\Models\TransactionDetail  $transactionDetail
     * @return void
     */
    public function saving(TransactionDetail $transactionDetail)
    {
        if (is_string($transactionDetail->celebration_id)){
            $celebration = Celebration::whereSlug($transactionDetail->celebration_id)->first();
            $transactionDetail->celebration_id = $celebration->id;
        }
    }

    /**
     * Handle the TransactionDetail "created" event.
     *
     * @param  \App\Models\TransactionDetail  $transactionDetail
     * @return void
     */
    public function created(TransactionDetail $transactionDetail)
    {

    }

    /**
     * Handle the TransactionDetail "created" event.
     *
     * @param  \App\Models\TransactionDetail  $transactionDetail
     * @return void
     */
    public function saved(TransactionDetail $transactionDetail)
    {

    }

    /**
     * Handle the TransactionDetail "updated" event.
     *
     * @param  \App\Models\TransactionDetail  $transactionDetail
     * @return void
     */
    public function updated(TransactionDetail $transactionDetail)
    {
        //
    }

    /**
     * Handle the TransactionDetail "deleted" event.
     *
     * @param  \App\Models\TransactionDetail  $transactionDetail
     * @return void
     */
    public function deleted(TransactionDetail $transactionDetail)
    {
        //
    }

    /**
     * Handle the TransactionDetail "restored" event.
     *
     * @param  \App\Models\TransactionDetail  $transactionDetail
     * @return void
     */
    public function restored(TransactionDetail $transactionDetail)
    {
        //
    }

    /**
     * Handle the TransactionDetail "force deleted" event.
     *
     * @param  \App\Models\TransactionDetail  $transactionDetail
     * @return void
     */
    public function forceDeleted(TransactionDetail $transactionDetail)
    {
        //
    }

    public function retrieved(TransactionDetail $transactionDetail)
    {

//        $transactionDetail->total_amount = number_format($transactionDetail->total_amount);
    }
}
