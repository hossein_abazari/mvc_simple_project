<?php

namespace App\Observers;

use App\Models\PackCard;
use App\Traits\ToolTrait;

class PackCardObserver
{
    use ToolTrait;

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param PackCard $packCard
     * @return void
     */
    public function creating(PackCard $packCard)
    {
        $packCard->slug = str_slug_random(8);

    }


    /**
     * @param PackCard $packCard
     */
    public function saving(PackCard $packCard)
    {
        if ($packCard->amount) {
            $packCard->amount = str_replace(',', '', $packCard->amount);
        }
        else{
            $packCard->amount = 0;
        }

    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param PackCard $packCard
     * @return void
     */
    public function retrieved(PackCard $packCard)
    {
        $packCard->amount_str = number_format($packCard->amount);
    }
}
