<?php

namespace App\Observers;

use App\Classes\Slug;
use App\Models\CelebrateDetail;
use App\Models\PackBuy;
use App\Models\PackCard;
use App\Models\User;
use App\Repositories\CountBuyInvitationCardRepositoryInterface;
use App\Repositories\InvoiceRepository;

class PackBuyObserver
{
    /* count day */
    private $expireDay = 30;

    protected $invoiceRepository;

    protected $countBuyInvitationCardRepository;

    private $indexModel =
    [
        'pack_card_id' => PackCard::class ,
        'user_id' => User::class ,
        'celebrate_detail_id' => CelebrateDetail::class
    ];

    public function __construct(InvoiceRepository $invoiceRepository,
                                CountBuyInvitationCardRepositoryInterface $countBuyInvitationCardRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
        $this->countBuyInvitationCardRepository = $countBuyInvitationCardRepository;
    }
    /**
     * Handle the PackBuy "created" event.
     *
     * @param  \App\Models\PackBuy  $packBuy
     * @return void
     */
    public function creating(PackBuy $packBuy)
    {
        $packBuy->slug = str_slug_random(8);
        /* index id */
        if (is_string($packBuy->pack_card_id)) {
            foreach ($this->indexModel as $index => $model) {
                $slug = new Slug($model);
                $packBuy->pack_card_id = $slug->setIndexId($packBuy->pack_card_id, $index);
            }
        }

        $packBuy->expire = timeExpire($this->expireDay);

    }
    /**
     * Handle the PackBuy "created" event.
     *
     * @param  \App\Models\PackBuy  $packBuy
     * @return void
     */
    public function created(PackBuy $packBuy)
    {
        $this->invoiceRepository->setInsertMasterDetail($packBuy->amount,$packBuy->celebrate_detail_id,
            vl('general','packCard'.$packBuy->number_card));

        $data = [
            'celebration_detal_id' => $packBuy->celebrate_detail_id,
            'user_id' => \Auth::getUser()->id,
        ];

        $data['number_card'] = $packBuy->number_card;
        $this->countBuyInvitationCardRepository->setCreate($data);

    }

    /**
     * Handle the PackBuy "updated" event.
     *
     * @param  \App\Models\PackBuy  $packBuy
     * @return void
     */
    public function updated(PackBuy $packBuy)
    {
        //
    }

    /**
     * Handle the PackBuy "deleted" event.
     *
     * @param  \App\Models\PackBuy  $packBuy
     * @return void
     */
    public function deleted(PackBuy $packBuy)
    {
        //
    }

    /**
     * Handle the PackBuy "restored" event.
     *
     * @param  \App\Models\PackBuy  $packBuy
     * @return void
     */
    public function restored(PackBuy $packBuy)
    {
        //
    }

    /**
     * Handle the PackBuy "force deleted" event.
     *
     * @param  \App\Models\PackBuy  $packBuy
     * @return void
     */
    public function forceDeleted(PackBuy $packBuy)
    {
        //
    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param PackBuy $packBuy
     * @return void
     */
    public function retrieved(PackBuy $packBuy)
    {

    }
}
