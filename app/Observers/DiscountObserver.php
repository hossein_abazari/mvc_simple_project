<?php

namespace App\Observers;

use App\Models\Discount;
use App\Traits\ToolTrait;
use Carbon\Carbon;

class DiscountObserver
{
    use ToolTrait;

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param Discount $discount
     * @return void
     */
    public function creating(Discount $discount)
    {
        $discount->slug = str_slug_random(8);
        $time = time() + (60 * 60 * 24 * $discount->expire_date);

        $expire_date = Carbon::parse($time)->format('Y-m-d H:i:s');
        $discount->expire_date = $expire_date;

    }


    /**
     * @param Discount $discount
     */
    public function saving(Discount $discount)
    {

    }

    /**
     * Handle the Transaction "force retrieved" event.
     *
     * @param Discount $discount
     * @return void
     */
    public function retrieved(Discount $discount)
    {
    }
}
