<?php

namespace App\Observers;

use App\Models\Log;
use Hekmatinasser\Verta\Verta;

class LogObserver
{
    /**
     * Handle the Log "created" event.
     *
     * @param  \App\Models\Log  $log
     * @return void
     */
    public function created(Log $log)
    {
        //
    }

    /**
     * Handle the Log "creating" event.
     *
     * @param  \App\Models\Log  $log
     * @return void
     */
    public function creating(Log $log)
    {
        $log->slug = str_slug_random(8);
    }

    /**
     * Handle the Log "updated" event.
     *
     * @param  \App\Models\Log  $log
     * @return void
     */
    public function updated(Log $log)
    {
        //
    }

    /**
     * Handle the Log "deleted" event.
     *
     * @param  \App\Models\Log  $log
     * @return void
     */
    public function deleted(Log $log)
    {
        //
    }

    /**
     * Handle the Log "restored" event.
     *
     * @param  \App\Models\Log  $log
     * @return void
     */
    public function restored(Log $log)
    {
        //
    }

    /**
     * Handle the Log "force deleted" event.
     *
     * @param  \App\Models\Log  $log
     * @return void
     */
    public function forceDeleted(Log $log)
    {
        //
    }

    /**
     * Handle the Invoice "force retrieved" event.
     *
     * @param Log $log
     * @return void
     */
    public function retrieved(Log $log)
    {
        $v = new Verta($log->created_at);

        $log->date_created_at = $v->format('Y/m/d');
        $log->time_created_at = $v->format('H:i:s');
    }
}
