<?php

namespace App\Observers;

use App\Models\UserLogin;
use App\Traits\ToolTrait;

class UserLoginObserver
{
    use ToolTrait;

    /**
     * Handle the Transaction "force seving" event.
     *
     * @param UserLogin $userLogin
     * @return void
     */
    public function creating(UserLogin $userLogin)
    {
        $userLogin->slug = str_slug_random(6);
        $userLogin->key = str_slug_random(18);

    }


    /**
     * Handle the User Login "force retrieved" event.
     *
     * @param UserLogin $userLogin
     * @return void
     */
    public function retrieved(UserLogin $userLogin)
    {
        $userLogin->login_date = $this->formatTypeDate($userLogin->login,'Y/m/d');
        $userLogin->login_time = $this->formatTypeDate($userLogin->login,'H:i a');
    }
}
