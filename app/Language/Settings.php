<?php


namespace App\Language;



use App\Http\Controllers\Controller;
use App\Models\Setting;

class Settings extends Controller
{

    /* language in project */
    public static function getLocale()
    {
        return 'fa';
    }

    /**
     * @return string
     * time zone project
     */
    public static function getTimeZone()
    {
        return 'Asia/Tehran'; // UTC
    }
}
