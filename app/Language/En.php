<?php


namespace App\Language;


class En
{
    /* global text for all function */
    private static $textGlobal = [
        'tickets' => 'Ticket',
        'more' => 'More',
        'payments' => 'Payments',
        'dashboard' => 'dashboard',
        'store' => 'Store',
        'profile' => 'Profile',
        'settings' => 'Settings',
        'countDay' => 'Day Count',
        'buy' => 'Buy',
        'nameTariff' => 'Tariff Name',
        'amountPayable' => 'The amount payable',
        'discount' => 'Discount',
        'discounts' => 'Discounts',
        'discount_cent' => 'Discount Cent',
        'number' => 'Number',
        'increaseInvitationCards' => 'You can increase your invitation cards number',
        'selectFileAndDrag' => 'Select a file or drag here',
        'selectAFile' => 'Select a file',
        'selectAnImage' => 'Please select an image',
    ];

    /* english string All project message */
    public static function getGeneral()
    {
        return array_merge(self::$textGlobal,[
            'nothing' => 'There is No Data Show',
            'alertDelete' => 'Are you sure you want to delete this?',
            'search' => 'Search',
            'messages' => 'Messages',
            'message' => 'Message',
            'writeMessage' => 'Write your message here',
            'daysAgo' => 'Days Ago',
            'hoursAgo' => 'Hours Ago',
            'minutesAgo' => 'Minutes Ago',
            'days' => 'Days',
            'hours' => 'Hours',
            'minutes' => 'Minutes',
            'second' => 'Second',
            'logout' => 'Logout',
            'update' => 'Update',
            'persian' => 'Persian',
            'english' => 'English',
            'change_password' => 'Change Password',
            'searchFor' => 'Search for ...',
            'enterToSystem' => 'Enter To System',
            'forgot' => 'Forgot password?',
            'dontHaveAccount' => "Don't have an account?",
            'create' => 'Create',
            'loginWithGoogle' => 'Login with Google',
            'alreadyHaveAccount' => 'Already have an account?',
            'login' => 'Login',
            'register' => 'Register',
            'registerInStore' => 'Register In Store',
            'profileCheck' => 'Your details are not complete. Please complete the details of the store.',
            'popular' => 'Popular',

            'slugRuleValidate' => ':attribute Must be entered correctly.',
            'incorrectPassword' => ':attribute The previous password is incorrect.',

            /* language */
            'fa' => 'Persian',
            'en' => 'English',

            /* money */
            'rial' => 'Rial',
            'toman' => 'Toman',
            'dollar' => '$',
            'euro' => '€',

            /* description */
            'buyDescription' => 'Click the Buy button and enter the payment gateway.'

        ]);
    }

    /* english string flash message */
    public static function getFlash ()
    {
        return[
            'success' => 'Successfully registered.',
            'deleted' => 'Successfully removed.',
            'edited' => 'Successfully edited.',
            'changed' => 'Successfully changed.',
            'passwordChanged' => 'Successfully Changed Password.',
            'problem' => 'Problem created try again.',
            'errorsCeilingCent' =>'The discount ceiling is above the limit.',
            'validateRequest' =>'Your request is not valid.',
            'validateDisabled' =>'item is disable.',
            'validateNotIn'=>'This account is not owed!',
            'errorTemporary'=>'Not Expired tariff module can not be changed.',
            'errorVerify'=>'Error sending information.',
            'errorMerchant'=>'No port is available. Please contact support.',
            'cancelPort'=>'You have unsubscribed from the purchase.',
            'errorsPayment'=>'Support your payment to post the information created to track the tracking code. Tracking Code:',
            'successPayment'=>'Your payment has been successfully recorded. Tracking Code:',
        ];
    }

    /* english string panel Admin */
    public static function getAdmin ()
    {
        return array_merge(self::$textGlobal,[
            'footer' => 'All rights reserved for H3N.',
            'deadlines' => 'Deadlines',
            'add' => 'Add',
            'submit' => 'Submit',
            'name' => 'Name',
            'expire_date' => 'Expire Date (day Count)',
            'amount' => 'Amount',
            'free' => 'Free',
            'cancel' => 'Cancel',
            'operation' => 'Operation',
            "edit" => 'Edit',
            "plugins" => 'Plugins',
            "nameEnter" => 'Enter Name',
            "amountEnter" => 'Enter Amount',
            "home" => 'Home',
            "stores" => 'Store',
            "type" => 'Type',
            "deadlineSelectEnter" => 'Select a Deadline',
            "typeSelectEnter" => 'Select a Type',
            'learning'=>'Learning',
            'files'=>'Files',
            'sellProduct'=> 'Sell Product',
            'name_link'=>'Name link',
            'address'=>'Address',
            'addressEnter'=>'Enter address',
            'always'=>'Is the store permanent?',
            'passwordEnter' => 'Enter Password',
            'emailEnter' => 'Enter Email',
            'password' => 'Password',
            'email' => 'Email',
            'pluginSelectEnter' => 'Select a Plugin',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
            'status' => 'Status',
            'fromDateEnter' => 'Enter From Date',
            'toDateEnter' => 'Enter To Date',
            'users' => 'Users',
            'user' => 'User',
            'password_confirmation' => 'Password Confirmation',
            'passwordConfirmationEnter' => 'Enter Password Confirmation',
            'celebration_id' => 'Celebration',
            'storeIdSelectEnter' => 'Select A Store',
            'invoices' => 'Invoices',
            'store_name' => 'Store Name',
            'invoice_code' => 'Invoice Code',
            'invoiceCodeEnter' => 'Enter Invoice Code',
            'description' => 'Description',
            'descriptionEnter' => 'Enter Description',
            'invoice_date' => 'Invoice Date',
            'invoiceDateEnter' => 'Enter Invoice Date',
            'storeRecord' => 'Store Record',
            'pluginRecord' => 'Plugin Record',
            'type_invoice' => 'Type Invoice',
            'manualRecord' => 'Manual Record',
            'other' => 'Other',
            'transaction' => 'Transaction',
            'cash' => 'Cash',
            'card' => 'Card',
            'port' => 'Port',
            'unpaid' => 'Unpaid',
            'tracking_code' => 'Tracking Code',
            'payment_date' => 'Payment Date',
            'fromAmountEnter' => 'Enter From Amount',
            'toAmountEnter' => 'Enter To Amount',
            'amount_str' => 'Amount',
            'date_locale' => 'Date',
            'debtor' => 'Debtor',
            'creditor' => 'Creditor',
            'paymentDateEnter' => 'Enter Payment Date',
            'payment_str' => 'Payment',
            'amount_debt' => 'Amount Debtor',
            'logs' => 'Log',
            'user_id' => 'User Id',
            'event' => 'Event',
            'ip_address' => 'Ip Address',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
            'restored' => 'Restored',
            'created_at' => 'Created At',
            'models' => 'Models',
            'pages' => 'Pages',
            'table_id' => 'Table ID',
            'new_values' => 'New values',
            'old_values' => 'Old values',
            'values' => 'Values',
            'before_data' => 'Before Data',
            'new_data' => 'New Data',
            'slug' => 'slug',
            'id' => 'ID',
            'operating_system' => 'Operating System',
            'browser' => 'Browser',
            'other_user_agent' => 'Other User Agent',
            'user_agent' => 'User Agent',
            'details' => 'Details',
            'audit' => 'Audit',
            'eventEnter' => 'Enter Event',
            'UserIdEnter' => 'Enter User ID',
            'eventSelectEnter' => 'Select a Event',
            'user_name' => 'User Name',
            'title' => 'Title',
            'closed' => 'Closed',
            'yes' => 'Yes',
            'no' => 'No',
            'storeNameEnter' => 'Enter Store Name',
            'events' => 'Events',
            'users_login' => 'Users Login',
            'time_of_online' => 'Time Of Online',
            'login_date' => 'Login date',
            'login_time' => 'Login time',
            'country' => 'Country',
            'city' => 'City',
            'os' => 'Operating System',
            'shops_settings' => 'Shops settings',
            'localeEnter' => 'Enter Locale',
            'locale' => 'Locale',
            'languageSelectEnter' => 'select Language',
            'localeSelectEnter' => 'Select a Locale',
            'language' => 'Language',
            'old_password' => 'Old Password',
            'oldPasswordEnter' => 'Enter Old Password',
            'newPasswordEnter' => 'Enter New Password',
            'viewProfile' => 'View Profile',
            'myProfile' => 'My Profile',
            'myIncome' => 'My Income',
            'inbox' => 'Inbox',
            'accountSettings' => 'Account Settings',
            'exit' => 'Exit',
            'settingsPanel' => 'Settings Panel',
            'enter' => 'Enter',
            'storesCount' => 'Stores Count',
            'usersCount' => 'Users Count',
            'invoicesCount' => 'Invoices Count',
            'invoicesSum' => 'Invoices Sum',
            'transactionDebtSum' => 'Transaction Sum Debtor',
            'transactionDebtCount' => 'Transaction Count Debtor',
            'transactionCredCount' => 'Transaction Count Creditor',
            'today' => 'Today',
            'yesterday' => 'Yesterday',
            'notRead' => 'Not Read',
            'ticketReceiveCount' => 'Ticket Receive Count',
            'ticketSendCount' => 'Ticket Send Count',
            'paymentsCount' => 'Payments Count',
            'storeSearch' => 'Store Search',
            'expireDateEnter' => 'Enter Expire Date',
            'DiscountCentEnter' => 'Enter Discount Cent',
            'numberEnter' => 'Enter Number',
            'celebration_person' => 'Celebration',
            'pack_cards' => 'Packs',

        ]);
    }

    /* english string panel Admin Store */
    public static function getCelebration ()
    {
        return[

        ];
    }

    /* persian string Link */
    public static function getLink ()
    {
        return [
            'select-picker' => 'js/admin/defaults-en_US.js'
        ];
    }

    /**
     * text ready for sample
     *
     * @return array
     */
    public static function getSampleName ()
    {
        return [
            's1' => ''
        ];
    }

    public static function getTextFooter ()
    {
        return [];
    }

    public static function getMethodBoolean()
    {
        return [];
    }
}
