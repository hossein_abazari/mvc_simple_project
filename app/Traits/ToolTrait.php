<?php


namespace App\Traits;


use App\Language\Settings;
use App\Models\Invoice;
use App\Models\Setting;
use App\Models\Store;
use App\Models\Transaction;
use App\Models\UserLogin;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use OwenIt\Auditing\Models\Audit;

trait ToolTrait
{
    public $countPaginate = 15;

    public function formatTypeDate($date,$format='Y-m-d H:i:s')
    {
        $fromDate= "-";
        if ($date) {
            if (\App::getLocale() == 'fa') {
                $v = new Verta($date);
                $fromDate = $v->format($format);
            } else {
                $carbon = new Carbon($date);
                $fromDate = $carbon->format($format);
            }
        }

        return $fromDate;
    }

    public static function formatTypeDateNow($date,$format='Y-m-d H:i:s')
    {
        $fromDate= "-";
        if ($date) {
            if (\App::getLocale() == 'fa') {
                $v = new Verta($date);
                $fromDate = $v->format($format);

            } else {
                $carbon = new Carbon($date);
                $fromDate = $carbon->format($format);
            }
        }

        return $fromDate;
    }

    public function getCreateFromFormat($date,$format='Y-m-d H:i:s')
    {
        $createFromFormat ='';

        if (\App::getLocale() == 'fa') {
            $createFromFormat = Verta::createFromFormat($format,$date);
        }
        else{
            $createFromFormat = Carbon::createFromFormat($format,$date);
        }

        return $createFromFormat;
    }

    public function setDateTable($date , $mode = '',$time=null) /* change date 0000/00/00 */
    {
        $date = explode('/', $date);

        if (\App::getLocale() == 'fa') {
            $date = Verta::getGregorian($date[0], $date[1], $date[2]);
        }

        $date = Carbon::create($date[0],
            $date[1],
            $date[2],
            Carbon::now()->hour
        )->startOfDay()->toDateString();

        $hour = '00:00:00';

        if ($mode == 'now'){
            $hour = Carbon::now()->format('H:i:s');
        }

        if ($time){
            $hour = $time;
        }

        return $date." ".$hour;
    }

    /**
     * @param $model
     * @return mixed|string
     * model for dynamic with string
     */
    public function modelString($model)
    {
        $model = '\App\Models\ '.$model;
        $model = str_replace(' ','',$model);

        return $model;
    }


    /**
     * @param $model
     * @param $field
     * @return int
     */
    public function autoCode($model, $field)
    {
        $model = $this->modelString($model);
        $data = $model::latest();

        $dataFirst = $data->first();

        $lastAutoCode = 1;
        if ($dataFirst) {
            $dataFile = $dataFirst->$field;
            $dataFile ++;
            while (true) {
                $data_count = $data->where($field, $dataFile)->count();

                if ($data_count == 0) {
                    $lastAutoCode = $dataFile;
                    break;
                } else {
                    $dataFile ++;
                }
            }
        }
        return $lastAutoCode;
    }

    /**
     * @param $data
     * @param $keySelect
     * @return string
     */
    public function selectStore($data, $keySelect)
    {
        $store = Store::whereId($data->celebration_id)->first();

        return $keySelect == $store->slug ? 'selected' : '';

    }

    /**
     * @return Setting|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    protected function getSetting()
    {
        $setting = Setting::latest()->first();

        return $setting;
    }

    /**
     * @param $from
     * @param $to
     * @return string
     */
    public function dateDifferent($from, $to)
    {

        $minutes = $from->diffInMinutes($to);
        $hours = $from->diffInHours($to);
        $days = $from->diffInDays($to);
        $seconds = $from->diffInSeconds($to);

        $date =$days > 0 ? $days." ".vl('general','days') :($hours >0 ? $hours." "
            .vl('general','hours') : ($minutes > 0 ? $minutes." ".vl('general','minutes') :
            $seconds." ".vl('general','second')
        ));

        return $date;
    }

    /**
     * @param $data
     * @return array
     */
    public function getUserTimeOnline($data)
    {
        $userLogin = UserLogin::whereUserId($data->user_id)->where('login','>',$data->login)->first();

        $result = [];

        if ($userLogin && $userLogin->action == 'logout'){

            $from = Carbon::createFromFormat('Y-m-d H:i:s',$data->login);
            $to = Carbon::createFromFormat('Y-m-d H:i:s',$userLogin->login);

            $result= ['number'=> $this->dateDifferent($from,$to) ,'mode'=>'logout'];
        }
        else{
            $audit = Audit::latest()->whereUserLoginId($data->id)->whereUserId($data->user_id)->where('created_at','>',$data->login)->first();

            if ($audit) {
                $from = Carbon::createFromFormat('Y-m-d H:i:s', $data->login);
                $to = Carbon::createFromFormat('Y-m-d H:i:s', $audit->created_at);

                $result = ['number' => $this->dateDifferent($from,$to) ,'mode'=>'online'];

            }

        }

        return $result;

    }

    /**
     * @param $model
     * @return bool|void
     */
    public function checkSlugModel($model)
    {
        if (!$model) {
            return abort('404');
        }

        return true;
    }

    /**
     * expire date
     *
     * @param $date
     * @return bool
     */
    public function getExpireDate($date)
    {
        $result = false;

        if ($date && Carbon::parse($date)->getTimestamp() > time()) {
            $result = true;
        }

        return $result;
    }

}
