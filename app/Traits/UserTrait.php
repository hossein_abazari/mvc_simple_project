<?php


namespace App\Traits;
use App\Models\Deadline;
use App\Models\Store;
use App\Rules\SlugExistsValidate;
use Illuminate\Http\Request;


trait UserTrait
{
    public function indexRoute ()
    {
        return route('admin.user.index');
    }

    public function validateRequest(Request $request,$data=null)
    {
        $validateArray = [
            'celebration_id' => ['required',new SlugExistsValidate('Celebration')],
            'name' => 'required',
            'email' => 'required|email|unique:users,email,null,id',
            'password' => 'required|string|min:6|confirmed|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'mobile' => 'nullable|regex:/(09)[0-9]{9}/||digits:11',
        ];

        if ($data){
            $validateArray['email'] = 'required|email|unique:users,email,'.$data->id.',id';
            $validateArray['password'] = 'nullable|string|min:6|confirmed';
        }

        $request->validate($validateArray);
    }

    public function dataStoreId($data)
    {
        $data = $data->store;

        return view('Trait.Admin.Stores.gridName',compact('data'));

    }



}
