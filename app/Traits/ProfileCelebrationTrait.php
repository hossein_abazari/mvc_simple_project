<?php

namespace App\Traits;

trait ProfileCelebrationTrait
{
    /**
     * @return string
     * redirect route
     */
    public function indexRouteCelebrationDetails()
    {
        return route('celebration.celebrate.details.index');
    }

}
