<?php


namespace App\Traits;
use App\Models\Plugin;
use App\Models\TicketDetail;
use App\Models\TicketMaster;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Rules\SlugExistsValidate;
use App\Rules\StoreAmountValidate;
use Illuminate\Http\Request;


trait TicketMasterTrait
{
    public function indexRoute ()
    {
        return route('admin.ticket.master.index');
    }

    /**
     * @param Request $request
     * @param $ticketMasters
     * @return mixed
     * search transaction in grid
     */
    public function search(Request $request, $ticketMasters)
    {
        if (isset($request->btn_search)){

            if (isset($request->store_name_search)) {

                $ticketMasters = $ticketMasters->whereHas('store',function ($q) use($request){
                    $q->where('name','like','%'.$request->store_name_search.'%');
                });

            }
        }

        return $ticketMasters;
    }

    /**
     * @param Request $request
     * validation insert
     */
    public function validateRequest(Request $request)
    {
        $request->validate([
            'message' => 'required',
        ]);
    }


    /**
     * @param $ticketMaster
     * @param Request $request
     * @return TicketDetail[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     *
     * insert details for show
     */
    public function insertDetails($ticketMaster, Request $request)
    {
        $ticketMaster = TicketMaster::whereSlug($ticketMaster)->first();

       TicketDetail::create(array_merge($request->all(),[
            'ticket_master_id' => $ticketMaster->id,
            'user_id' => \Auth::id(),
        ]));

       $ticketDetails = TicketDetail::whereTicketMasterId($ticketMaster->id)->get();

       return $ticketDetails;
    }

    /**
     * @param $data
     * @return string
     * set html for grid
     * set count tickets
     */
    public function dataNameLabel($data)
    {
        $ticketDetailCount = TicketDetail::whereTicketMasterId($data->id)
            ->where('user_id','!=',\Auth::id())
            ->whereHas('user',function ($q){
                $q->where('type','!=','admin');
            })
            ->whereView(0)
            ->count();

        $count = $ticketDetailCount > 0 ? "<span class='label label-rounded label-info ml-1'>".$ticketDetailCount."</span>" : '';

        $result = "<a href='".route('admin.ticket.master.show',[$data->slug])."'>".$count.$data->store_name."</a>";

        return $result;
    }


    /**
     * @param $ticketMaster
     * change view ticket details
     */
    public function viewTicket($ticketMaster)
    {
         TicketDetail::whereTicketMasterId($ticketMaster->id)->where('user_id','!=',\Auth::id())->whereHas('user',function ($q){
                $q->where('type','!=','admin');
            })->update([
            'view' => 1
        ]);
    }



}
