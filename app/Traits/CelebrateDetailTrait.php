<?php

namespace App\Traits;

use App\Classes\BladeCode;

trait CelebrateDetailTrait
{
    public $image_layout;
    public $number_invitation_card;
    public $type_celebrate_name;
    public $expire_date_convert;
    public $expire_date;
    public $expire_time;
    public $total_amount;

    public $field = [
        'link_name'=>false,
        'expire_date'=>false,
        'date_ceremony'=>'expire_date_convert',
        'number_invitation_card'=>false,
        'total_amount'=>false,
        'payment' => 'content'
    ];
   public $image = 'image_layout';
   public $title = 'name';
    /**
     * get array for select type value
     *
     * @return array
     */
    public static function getSelectType()
    {
       return [
            'wedding' => vl('general','wedding'),
            'birth' => vl('general','birth'),
            'other' => vl('general','other')
        ];
    }

    public static function getTypes()
    {
        return json_encode(json_encode(array_keys(self::getSelectType())));
    }

    public static function getArrayTypes()
    {
        return array_keys(self::getSelectType());
    }

    public function indexRoot()
    {
        return route('celebration.celebrate.details.index');
    }

    public function content($name,$model)
    {
        return view('Trait.content',compact('name','model'));
    }
}
