<?php


namespace App\Traits;
use App\Models\Plugin;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Rules\SlugExistsValidate;
use App\Rules\StoreAmountValidate;
use Illuminate\Http\Request;


trait TransactionTrait
{
    public function indexRoute ()
    {
        return route('admin.transaction.index');
    }

    /**
     * @param Request $request
     * @param $transaction
     * @return mixed
     * search transaction in grid
     */
    public function search(Request $request, $transaction)
    {
        if (isset($request->btn_search)){

            if (isset($request->invoice_code_search)) {

                $transaction = $transaction->whereHas('transactionDetail',function ($q) use($request) {

                  $q->whereHas('invoice',function ($q) use ($request) {

                     $q->where('invoice_code', 'like', $request->invoice_code_search);

                  });
                });

            }

            if (isset($request->from_amount_search)) {

                $transaction = $transaction->where('amount', '>=', str_replace(',','',$request->from_amount_search));

            }

            if (isset($request->to_amount_search)) {

                $transaction = $transaction->where('amount', '<=', str_replace(',','',$request->to_amount_search));

            }

            if (isset($request->from_date_search)) {

                $transaction = $transaction->where('created_at', '>=', $this->setDateTable($request->from_date_search));

            }

            if (isset($request->to_date_search)) {

                $transaction = $transaction->where('created_at', '<=', $this->setDateTable($request->to_date_search,'now'));

            }
        }

        return $transaction;
    }

    /**
     * @param Request $request
     * validation insert
     */
    public function validateRequest(Request $request)
    {
        $request->validate([
            'celebration_id' => ['required',new SlugExistsValidate('Celebration')],
            'type' => 'required',
            'amount'=>'required|regex:/^[0-9\.,]+$/',
        ]);
    }

    /**
     * @param Request $request
     * function insert store
     */
    public function insertStore(Request $request)
    {
        $transaction = Transaction::create(array_merge($request->all(),[
            'payment' => true,
            'payment_date'=> $this->setDateTable($request->payment_date)
        ]));

        TransactionDetail::create(array_merge($request->all(),[
            'transaction_id' => $transaction->id,
        ]));
    }

    public function trClass($data)
    {
        $class = 'text-danger';

        if ($data->payment){
            $class = 'text-success';
        }

        return $class;
    }


}
