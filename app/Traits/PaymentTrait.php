<?php

namespace App\Traits;

use App\Repositories\TransactionRepository;

trait PaymentTrait
{
    public static function setTransaction($transactionId)
    {
        $transactionRepository = new TransactionRepository();

        return $transactionRepository->getFirstWithId($transactionId);
    }
}
