<?php


namespace App\Traits;
use App\Models\Plugin;
use Illuminate\Http\Request;


trait PluginTrait
{
    public $amount_str;

    public function indexRoute ()
    {
        return route('admin.plugin.index');
    }

    public function validateRequest(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'amount'=>'required|regex:/^[0-9\.,]+$/',
        ]);
    }

    public static function arrayDataPlugin()
    {
        return Plugin::latest()->pluck('name','slug');
    }

}
