<?php


namespace App\Traits;


use App\Models\Invoice;
use App\Models\TicketDetail;
use App\Models\Transaction;
use App\Models\Celebration;

trait AdminDashboardLastData
{
    public $limit = 5;

    /**
     * @return array
     * last data for view in dashboard
     */
    public function lastData()
    {
        $ticketDetails = TicketDetail::latest()->where('user_id','!=',\Auth::id())
            ->whereHas('user',function ($q){
                $q->where('type','!=','admin');
            })
            ->whereView(0)
            ->limit($this->limit)->get();

        $transactions = Transaction::leftJoin('transaction_details', function ($join) {
            $join->on('transaction_details.transaction_id', '=', 'transactions.id');
        })->limit($this->limit)->get();

        $celebrations = Celebration::latest()->limit($this->limit)->get();

        $invoices = Invoice::latest()->limit($this->limit)->get();

        return [
            'ticketDetails' => $ticketDetails,
            'transactions' => $transactions,
            'stores' => $celebrations,
            'invoices' => $invoices,
        ];
    }
}
