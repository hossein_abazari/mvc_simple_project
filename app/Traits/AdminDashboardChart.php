<?php


namespace App\Traits;


use App\Models\Invoice;
use App\Models\Store;
use App\Models\TicketDetail;
use App\Models\Transaction;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;

trait AdminDashboardChart
{
    /**
     * @param $data
     * @param string $operation
     * @return mixed
     */
    private function collectionData($data, $operation='count')
    {
        $result = $data
            ->whereDate('created_at','>=', Carbon::now()->subDays(vl('variable','countDayChartDashboard')))
            ->get()
            ->groupBy(function($data) {
            return $this->formatTypeDate($data->created_at,'Y-m-d');
        });

        $result = $result->map(function ($row) use($operation) {
            $data = '';

            if ($operation == 'sum') {
                $data = (int)str_replace('-', '', $row->sum('amount'));
            }
            elseif ($operation == 'count') {
                $data = $row->count();
            }

            return $data;
        });

        return $result;
    }

    /**
     * @return array
     * chart statistic in dashboard
     */
    public function statisticChart()
    {

        $transactions = Transaction::latest()->wherePayment(true);

        $stores = Store::latest();

        $arraySumPayments = $this->collectionData($transactions,'sum');

        $arrayCountStores = $this->collectionData($stores);

        return [
            'payments' => $arraySumPayments,
            'stores' => $arrayCountStores,
        ];
    }
}
