<?php

namespace App\Traits;

trait GuestTrait
{
    public $field = [
        'nameGuest'=>'name',
        'extra_name'=>false,
        'count_family'=>false,
        'survey_description' => false,
    ];
}
