<?php


namespace App\Traits;
use App\Builder\InvoiceStoreCreateBuilder;
use App\Models\Deadline;
use App\Models\Invoice;
use App\Models\Plugin;
use App\Models\Store;
use App\Models\CelebrationSetting;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;


trait InvoiceTrait
{
    public function indexRoute ()
    {
        return route('admin.invoice.index');
    }

    public function validateRequest(Request $request,$data = null)
    {
        $arrayValidate = [
            'celebration_id' => 'required',
            'amount'=>'required|regex:/^[0-9\.,]+$/',
            'invoice_code' => 'required|unique:invoice_stores',
        ];

        if ($data != null){
            $arrayValidate['invoice_code'] = 'required|unique:invoice_stores,invoice_code,'.$data->id.',id';
        }

        $request->validate($arrayValidate);
    }

    public function search(Request $request, $invoiceStores)
    {
        if (isset($request->btn_search)){

            if (isset($request->invoice_code_search)) {

                $invoiceStores = $invoiceStores->where('invoice_code', 'like', $request->invoice_code_search);

            }

            if (isset($request->from_amount_search)) {

                $invoiceStores = $invoiceStores->where('amount', '>=', str_replace(',','',$request->from_amount_search));

            }

            if (isset($request->to_amount_search)) {

                $invoiceStores = $invoiceStores->where('amount', '<=', str_replace(',','',$request->to_amount_search));

            }

            if (isset($request->from_date_search)) {

                $invoiceStores = $invoiceStores->where('created_at', '>=', $this->setDateTable($request->from_date_search));

            }

            if (isset($request->to_date_search)) {

                $invoiceStores = $invoiceStores->where('created_at', '<=', $this->setDateTable($request->to_date_search,'now'));

            }
        }

        return $invoiceStores;
    }


    /**
     * @param Request $request
     */
    public function insertStore(Request $request)
    {
        $allData = array_merge($request->all(),[
            'type' =>'manualRecord',
            'invoice_date' =>$this->setDateTable($request->invoice_date)
        ]);

        $invoiceStoreCreateBuilder = new InvoiceStoreCreateBuilder($allData);


        $invoiceStoreCreateBuilder->paymentTransactionDate = Carbon::now()->format('Y-m-d H:i:s');

        $invoiceStoreCreateBuilder->setTransaction();

    }

    /**
     * @param $data
     * @return mixed
     * set data field update
     */
    public function dataInvoiceDate($data)
    {
        return $this->formatTypeDate($data->invoice_date,'Y/m/d');
    }

    /**
     * @param $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function dataStoreId($data)
    {
        $data = $data->store;

        return view('Trait.Admin.Stores.gridName',compact('data'));

    }


}
