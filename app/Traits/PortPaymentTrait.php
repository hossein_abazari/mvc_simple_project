<?php


namespace App\Traits;


use App\ApartmentPort;
use App\Builder\InvoiceStoreCreateBuilder;
use App\Classes\Zarinpal;
use App\Models\Deadline;
use App\Models\PortPayment;
use App\Models\CelebrationSetting;
use Carbon\Carbon;

trait PortPaymentTrait
{
    /*
     * zarinpall port
     * */
    private $MerchantID = '73ba3492-3280-11e8-873b-005056a205be';
    public $Amount;

    /**
     * @param \Request $request
     */
    public function validateRequestPort(\Request $request)
    {
        $request->validate([
            'amount'=>"required|regex:/^[0-9\.,]+$/|not_in:0",
            'slug'=>'required',
        ]);
    }

    private function getZarinpal()
    {
        $zarinpal = new Zarinpal();
        $zarinpal->MerchantID = $this->MerchantID;

        return $zarinpal;
    }

    /**
     * @param $deadline
     * @param $invoice
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getVerify($deadline,$invoice)
    {
        if ($deadline->amount != 0) {
            $zarinpal = $this->getZarinpal();
            $zarinpal->Amount = $deadline->amount / 10;
            $zarinpal->Description = vl('general','buy');
            $zarinpal->CallbackURL = route('admin.celebration.tariff.port.checker',[$deadline->slug]);

            if ($verify = $zarinpal->verify()) {
                PortPayment::create([
                    'celebration_id' => \Auth::getUser()->celebration_id,
                    'amount' => $deadline->amount,
                    'invoice_id' => $invoice->invoice_id,
                    'authority' => $zarinpal->authority
                ]);
                return $verify;
            }
            else{
                return back();
            };
        }

    }

    /**
     * @param $slugDeadline
     * @param \Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getChecker($slugDeadline,$request)
    {
        $zarinpal = $this->getZarinpal();
        $port = PortPayment::whereAuthority($request->Authority)->wherePayment(0)->first();

        if ($port) {
            $port->payment = 2;
            $port->update();

            /* zarin pull */
            $zarinpal->authority = $port->authority;
            $zarinpal->Amount = $port->amount / 10;

            if ($refId = $zarinpal->checker($request)) {
                $port->payment = 1;
                $port->tracking_code = $refId;
                $port->update();

                $this->setStoreSettingDeadline($slugDeadline, $port->amount, $port->invoice_id);

                return $refId;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * @param $deadlineId
     * @param $amount
     * @param $invoiceId
     * Tariff extensions
     */
    public function setStoreSettingDeadline($deadlineSlug, $amount, $invoiceId)
    {
        $deadline = Deadline::whereSlug($deadlineSlug)->first();
        $nowCarbon = Carbon::now();
        $storeId =\Auth::user()->celebration_id;
        $storeSetting = CelebrationSetting::whereCelebrationId($storeId)->first();

        $date1 = Carbon::createFromFormat('Y-m-d H:i:s', $storeSetting->to_date);
        $date2 = Carbon::createFromFormat('Y-m-d H:i:s', $nowCarbon->toDateTimeString());

        $resultDate = $date1->gt($date2);

        if($resultDate){
            $fromDate = $storeSetting->from_date;
            $toDate = $date1->addDays($deadline->expire_date)->toDateTimeString();
        }
        else {
            $fromDate = $nowCarbon->toDateTimeString();
            $toDate = $nowCarbon->addDays($deadline->expire_date)->toDateTimeString();
        }

        $storeSetting->update([
            'from_date' => $fromDate,
            'to_date' => $toDate,
        ]);

        if ($amount != 0) {

            $allData = [
                'invoice_id'=>$invoiceId,
            ];

            $invoiceStoreCreateBuilder = new InvoiceStoreCreateBuilder($allData);

            $invoiceStoreCreateBuilder->paymentTransactionDate = Carbon::now()->format('Y-m-d H:i:s');

            $invoiceStoreCreateBuilder->setTransaction('-');

        }
    }

}
