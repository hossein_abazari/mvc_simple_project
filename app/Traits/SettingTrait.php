<?php


namespace App\Traits;
use App\Models\Plugin;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Rules\HashOldPasswordValidate;
use App\Rules\SlugExistsValidate;
use App\Rules\StoreAmountValidate;
use Illuminate\Http\Request;


trait SettingTrait
{
    use ToolTrait;
    /**
     * @return string
     */
    public function indexRoute ()
    {
        return route('admin.setting.index');
    }


    public static function getLocaleSetting ()
    {
        $setting = Setting::latest()->first();

        return $setting ? $setting->locale : false;

    }

    public function validateRequest(Request $request)
    {
        $request->validate([
            'locale' => 'required',
            'language'=>'required',
        ]);
    }

    public function insertUpdateSetting(Request $request)
    {

        $setting = $this->getSetting();

        if ($setting){
            $setting->update($request->all());
        }
        else{
            Setting::create($request->all());
        }

    }

    public static function getLanguageSetting ()
    {
        $setting = Setting::latest()->first();


        return $setting ? json_decode($setting->language) : false;

    }

    /**
     * @param Request $request
     */
    public function validateRequestProfile(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'mobile' => ["nullable","regex:/(09)[0-9]{9}/","digits:11"],
        ]);
    }

    /**
     * @param Request $request
     */
    public function updateProfile(Request $request)
    {
        $user = \Auth::getUser();
        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->update();
    }

    /**
     * @param Request $request
     */
    public function validateRequestChangePassword(Request $request)
    {
        $request->validate([
            'old_password' => ['required','string','min:6',
                new HashOldPasswordValidate(\Auth::getUser()->getAuthPassword())],
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function getChangePassword(Request $request)
    {
        $user = \Auth::getUser();

        $old = $request->old_password;
        $pass = $user->getAuthPassword();

        if(\Hash::check($old,$pass)) {
            $user->update([
                'password' => $request->password
            ]);
        }

    }


}
