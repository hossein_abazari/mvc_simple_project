<?php


namespace App\Traits;


use App\Models\Celebration;
use App\Models\TicketDetail;
use App\Models\Transaction;
use Carbon\Carbon;

trait AdminDashboard
{
    use ToolTrait;
    use AdminDashboardLastData;
    use AdminDashboardChart;

    /**
     * @param $day
     * @param $model
     * @param string $operation
     * @param null $fieldSum
     * @param null $status
     * @return string
     * count and sum all models
     */
    private function getDataCountSum($day, $model, $operation = 'count', $fieldSum = null,$status =null)
    {
        $model = $this->modelString($model);

        $result = $model::oldest();
        /* debtor */
        if ($status== 'debt'){
            $result = $result->where('amount','>',0);
        }
        /* creditor */
        elseif ($status == 'cred'){
            $result = $result->where('amount','<=',0);
        }

        if ($day == 'today') {
            $result = $result->where('created_at', '>=', Carbon::today());
        }
        elseif ($day == 'yesterday') {

            $result = $result->where('created_at', '>=', Carbon::now()->subDays(2)->format('Y-m-d H:i:s'))
                ->where('created_at', '<=', Carbon::now()->subDays(1)->format('Y-m-d H:i:s'));

        }
        elseif ($day == 'all') {
//            $result = $result;
        }
        else{
            /* count day == 2 example */
            $result = $result->where('created_at', '>=', Carbon::today()->subDays($day));

        }

        if ($operation == 'sum'){
//            $result = number_format($result->sum($fieldSum));
            $result = $result->sum($fieldSum);
        }
        else{
            $result = $result->count();
        }

        return $result;
    }

    /**
     * @param bool $read
     * @param string $transfer
     * @return int
     * ticket all and read count
     */
    private function getTicketCount($read=false, $transfer = 'receive')
    {
        $ticketDetails = TicketDetail::latest();

        if ($transfer == 'receive'){
            $ticketDetails = $ticketDetails->where('user_id','!=',\Auth::id())
                ->whereHas('user',function ($q){
                    $q->where('type','!=','admin');
                });
        }
        else{

            $ticketDetails = $ticketDetails->where('user_id',\Auth::id())
                ->whereHas('user',function ($q){
                    $q->where('type','admin');
                });
        }

        if ($read){
            $ticketDetails = $ticketDetails->whereView(0);
        }

        return $ticketDetails->count();

    }

    /**
     * @return array
     * payment count
     */
    private function getPayment()
    {
        $transactionPort = Transaction::whereType('port');

        $transactionAll = Transaction::whereIn('type',array('cash','card','port'));

        $transactionCash = Transaction::whereType('cash');

        $transactionCard = Transaction::whereType('card');

       $result = [
           'all' => $transactionAll->count(),
           'port' => $transactionPort->count(),
           'cash' => $transactionCash->count(),
           'card' => $transactionCard->count(),
       ];

        return $result;
    }

    private function getPercent($A,$B){
        return $A == 0 ? 0 : number_format((($B - $A) / abs($A)) * 100,1);
    }


    /**
     * @return array
     */
    public function topStatistics()
    {
        $result = [];

        $models = ['storesCount' => 'Celebration','usersCount' => 'User','invoicesCount' => 'Invoice','invoicesSum' => 'Invoice',];

        foreach ($models as $key=>$model) {
            if (strpos($key, 'Sum')){
                $operation = 'sum';
                $fieldSum = 'amount';
            }
            else{
                $operation = 'count';
                $fieldSum = null;
            }

            /* statistics all models */
            $result[$key] = ['all' => $this->getDataCountSum('all', $model,$operation,$fieldSum),
                'today' => $this->getDataCountSum('today', $model,$operation,$fieldSum),
                'yesterday' => $this->getDataCountSum('yesterday', $model,$operation,$fieldSum),
                'percent' => $this->getPercent($this->getDataCountSum('today', $model,$operation,$fieldSum) ,
                        $this->getDataCountSum('yesterday', $model,$operation,$fieldSum) )

            ];

        }

        /* transaction debtor or creditor */
        $transactionCalculates = ['transactionDebtSum' => 'sum','transactionDebtCount' => 'count','transactionCredSum' => 'sum','transactionCredCount' => 'count',];

        foreach ($transactionCalculates as $key => $value) {

            if (strpos($key, 'Debt')){
                $status = 'debt';
            }
            else{
                $status = 'cred';
            }

            $result[$key] = ['all' => $this->getDataCountSum('all', 'Transaction', $value, 'amount', $status),
                'today' => $this->getDataCountSum('today', 'Transaction', $value, 'amount', $status),
                'yesterday' => $this->getDataCountSum('yesterday', 'Transaction', $value, 'amount', $status),
                'percent' => $this->getPercent($this->getDataCountSum('today', 'Transaction', $value, 'amount', $status) ,
                    $this->getDataCountSum('yesterday', 'Transaction', $value, 'amount', $status) )
                ];

        }

        $ticketTransfers = ['ticketReceiveCount'=>'receive','ticketSendCount'=>'send'];

        foreach ($ticketTransfers as $key => $value) {
            $result[$key] = [
                'all' => $this->getTicketCount(false,$value),
                'notRead' => $this->getTicketCount(true,$value)
            ];
        }

        $result['paymentCount'] = $this->getPayment();

        return $result;
    }
}
