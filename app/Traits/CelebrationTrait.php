<?php


namespace App\Traits;
use App\Builder\InvoiceStoreCreateBuilder;
use App\Language\Settings;
use App\Models\Deadline;
use App\Models\InvoiceDetail;
use App\Models\Invoice;
use App\Models\Plugin;
use App\Models\CelebrationSetting;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use App\Models\Celebration;
use App\Rules\SlugExistsValidate;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;


trait CelebrationTrait
{
    use ToolTrait;

    public $amount_debt;

    public $formatDate = 'Y/m/d - H:i:s';

    /**
     * @return string
     * redirect route
     */
    public function indexRoute ()
    {
        return route('admin.celebration.index');
    }

    /**
     * @param Request $request
     * validation insert
     */
    public function validateRequest(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'name_link' => 'required|unique:celebration_settings,name_link,null,id|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'email' => 'required|email|unique:users,email,null,id',
            'password' => 'required|string|min:6|confirmed|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'amount'=>'required|regex:/^[0-9\.,]+$/',
            'mobile' => 'nullable|regex:/(09)[0-9]{9}/||digits:11',
        ]);
    }

    /**
     * @param Request $request
     * validation update
     */
    public function validateUpdateRequest(Request $request,$celebration)
    {
//        $celebration = Celebration::whereSlug($celebration)->first();

        $request->validate([
            'name' => 'required',
            'type' => 'required',
//            'name_link' => 'required|unique:celebration_settings,name_link,'.$celebration->celebrationSetting->name_link,
            'name_link' => 'required',
        ]);
    }

    /**
     * @param Request $request
     * function insert celebration
     */
    public function insertCelebration(Request $request)
    {

        $celebration = Celebration::create($request->all());

        $deadline = Deadline::whereSlug($request->deadlines)->first();
        $nowCarbon = Carbon::now();

        $fromDate = $nowCarbon->toDateTimeString();
        $toDate = $nowCarbon->addDays($deadline->expire_date)->toDateTimeString();

        $always =false;
        if (isset($request->always)){
            $always = true;
        }

       CelebrationSetting::create(array_merge($request->all(),[
            'celebration_id' => $celebration->id,
            'from_date' => $fromDate,
            'to_date' => $toDate,
            'always' => $always,
        ]));

       if ($request->plugins) {
           $plugins = Plugin::whereIn('slug', $request->plugins)->get();

           if ($plugins->isNotEmpty()) {
               $celebration->plugins()->sync($plugins->pluck('id')->toArray());
           }
       }

        User::create(array_merge($request->all(),[
            'celebration_id'=>$celebration->id,
            'type' => 'celebrationAdmin'
        ]));

       if ($request->amount != 0) {

           $allData = [
               'celebration_id'=>$celebration->id,
               'invoice_code' => $this->autoCode('Invoice','invoice_code'),
               'invoice_date' => Carbon::now()->format('Y-m-d H:i:s'),
               'amount' => $request->amount,
               'description' => Vl('admin','storeRecord'), /* after with trans set name */
               'type' =>'celebrationRecord'
           ];

           $invoiceStoreCreateBuilder = new InvoiceStoreCreateBuilder($allData);

           $invoiceStoreCreateBuilder->paymentTransactionDate = Carbon::now()->format('Y-m-d H:i:s');

           $invoiceStoreCreateBuilder->setTransaction();

       }

       if (isset($request->plugins)){

           $plugins = Plugin::whereIn('slug',$request->plugins)->pluck('amount')->toArray();

           foreach ($plugins as $pluginAmount){

               $allData = [
                   'celebration_id'=>$celebration->id,
                   'invoice_code' => $this->autoCode('Invoice','invoice_code'),
                   'invoice_date' => Carbon::now()->format('Y-m-d H:i:s'),
                   'amount' => $pluginAmount,
                   'description' => Vl('admin','pluginRecord'), /* after with trans set name */
                   'type' =>'celebrationRecord'
               ];

               $invoiceStoreCreateBuilder = new InvoiceStoreCreateBuilder($allData);

               $invoiceStoreCreateBuilder->paymentTransactionDate = Carbon::now()->format('Y-m-d H:i:s');

               $invoiceStoreCreateBuilder->setTransaction();

           }

       }


    }

    /**
     * @param Request $request
     * @param $celebration
     * function update celebration
     */
    public function updateCelebration(Request $request, $celebration)
    {
        $celebration = Celebration::whereSlug($celebration)->first();

        $storeSetting = CelebrationSetting::whereCelebrationId($celebration->id)->first();


        $celebration->update($request->all());

        $always =false;
        if (isset($request->always)){
            $always = true;
        }

        $status =false;
        if (isset($request->status)){
            $status = true;
        }

        $storeSetting->update(array_merge($request->all(), [
            'always' => $always,
            'status' => $status,
            ]));

       if ($request->plugins) {

           $plugins = Plugin::whereIn('slug', $request->plugins)->get();

           if ($plugins->isNotEmpty()) {
               $celebration->plugins()->sync($plugins->pluck('id')->toArray());
           }
       }
       else{

           $celebration->plugins()->detach($celebration->plugins()->pluck('id')->toArray());

       }


    }

    /**
     * @param $data
     * @return string
     * @throws \Exception
     * data from_date for table in component
     */
    public function dataFromDate($data)
    {
        return $data->celebrationSetting ? $this->formatTypeDate($data->celebrationSetting->from_date,$this->formatDate): '-';
    }

    /**
     * @param $data
     * @return string
     * @throws \Exception
     * data to_date for table in component
     */
    public function dataToDate($data)
    {
        $toDate = '-';
        if ($data->celebrationSetting && $data->celebrationSetting->to_date) {
            if (\App::getLocale() == 'fa') {
                $v = new Verta($data->celebrationSetting->to_date);
                $toDate = $v->format($this->formatDate);
            } else {
                $carbon = new Carbon($data->celebrationSetting->to_date);
                $toDate = $carbon->format($this->formatDate);
            }
        }

        return $toDate;

    }

    /**
     * @param $data
     * @return array|bool|\Illuminate\Contracts\Translation\Translator|string|null
     * data type for table in component
     */
    public function dataType($data)
    {
        if ($data->celebrationSetting) {
            return vl('admin', $data->celebrationSetting->type);
        }
        return vl('admin', 'learning');
    }

    /**
     * @param $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * view data name for grid
     */
    public function dataName($data)
    {
        return view('Trait.Admin.Stores.gridName',compact('data'));
    }

    /**
     * @param $celebration
     * @throws \Exception
     * function delete celebration
     */
    public function deleteCelebration($celebration)
    {
        $celebration = Celebration::whereSlug($celebration)->first();

        $celebration->delete();

        $storeSetting = CelebrationSetting::whereCelebrationId($celebration->id)->first();

        $storeSetting->delete();

    }

    /**
     * @param Request $request
     * @return string
     * view amount for select ajax
     */
    public function showAmountDeadline(Request $request)
    {
        $request->validate([
            'slug' => ['required',new SlugExistsValidate('Deadline')]
        ]);

        $deadline = Deadline::whereSlug($request->slug)->first();

        return number_format($deadline->amount);
    }

    public function search(Request $request, $celebrations)
    {
        if (isset($request->btn_search)){
            $celebrations = $celebrations->where('name', 'like', '%'.$request->name_search.'%');
        }

        return $celebrations;
    }

    public static function arrayDataCelebration()
    {
        return Celebration::latest()->pluck('name','slug');
    }

    /**
     * @param Request $request
     */
    public function getSection (Request $request)
    {
        $section = $request->section;

        $arraySection = [
            'transaction' => '',
            'invoices' => '',
            'users' => '',
        ];

        if ($section == 'transaction'){
            $arraySection['transaction'] = 'active';
        }
        elseif ($section == 'invoices'){
            $arraySection['invoices'] = 'active';
        }
        else{
            $arraySection['users'] = 'active';
        }

        return $arraySection;
    }

}
