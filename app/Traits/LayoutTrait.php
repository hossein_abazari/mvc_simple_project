<?php

namespace App\Traits;

trait LayoutTrait
{
    public function indexRoute(){
        return route('admin.layout.index');
    }
}
