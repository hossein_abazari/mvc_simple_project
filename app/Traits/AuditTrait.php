<?php


namespace App\Traits;
use App\Models\Plugin;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Rules\SlugExistsValidate;
use App\Rules\StoreAmountValidate;
use Illuminate\Http\Request;


trait AuditTrait
{
    /**
     * @return string
     */
    public function indexRoute ()
    {
        return route('admin.transaction.index');
    }

    /**
     * @param Request $request
     * @param $transaction
     * @return mixed
     * search transaction in grid
     */
    public function search(Request $request, $audits)
    {
        if (isset($request->btn_search)){

            if (isset($request->user_id_search)) {

                $audits = $audits->whereUserId($request->user_id_search);

            }

            if (isset($request->event_search)) {

                $audits = $audits->whereEvent($request->event_search);

            }

            if (isset($request->from_date_search)) {

                $audits = $audits->where('created_at', '>=', $this->setDateTable($request->from_date_search));

            }

            if (isset($request->to_date_search)) {

                $audits = $audits->where('created_at', '<=', $this->setDateTable($request->to_date_search,'now'));

            }
        }

        return $audits;
    }

    public function getUserAgent($userAgent)
    {
        $agent = explode(')',$userAgent);
        $userAgent = array(
            'browser' => $agent[2],
            'operating_system' => explode('(',$agent[0])[1],
            'other_user_agent' => $agent[1].")"
        );

        return $userAgent;
    }

    public static function getSelectEvent()
    {
        $array = array(
            'created' => vl('admin','created'),
            'updated' => vl('admin','updated'),
            'deleted' => vl('admin','deleted'),
            'restored' => vl('admin','restored'),
        );

        return $array;
    }


}
