<?php


namespace App\Traits;
use App\Models\Plugin;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\UserLogin;
use App\Rules\SlugExistsValidate;
use App\Rules\StoreAmountValidate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use OwenIt\Auditing\Models\Audit;


trait UserLoginTrait
{
    use ToolTrait;
    public $login_date;
    public $login_time;
    /**
     * @return string
     */
    public function indexRoute ()
    {
        return route('admin.transaction.index');
    }

    /**
     * @param Request $request
     * @param $userLogins
     * @return mixed
     * search transaction in grid
     */
    public function search(Request $request, $userLogins)
    {
        if (isset($request->btn_search)){

            if (isset($request->user_id_search)) {

                $userLogins = $userLogins->whereUserId($request->user_id_search);

            }


            if (isset($request->from_date_search)) {

                $userLogins = $userLogins->where('login', '>=', $this->setDateTable($request->from_date_search));

            }

            if (isset($request->to_date_search)) {

                $userLogins = $userLogins->where('login', '<=', $this->setDateTable($request->to_date_search,'now'));

            }
        }

        return $userLogins;
    }



    /**
     * @param $data
     * @return string
     */
    public function dataTimeOfOnline($data)
    {

        $online = $this->getUserTimeOnline($data);
        $result = '--';

        if (!empty($online)) {
            $result = "<span class='badge badge-primary' title='" . vl('general', $online['mode']) . "'>" . $online['number'] . "</span>";
        }

        return $result;

    }

}
