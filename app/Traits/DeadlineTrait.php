<?php


namespace App\Traits;
use App\Models\Deadline;
use App\Models\Invoice;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Builder\InvoiceStoreCreateBuilder;
use Carbon\Carbon;
use Illuminate\Http\Request;


trait DeadlineTrait
{
    use ToolTrait;
    public $amount_str;
    private $builderInterface;

    /**
     * @return string
     */
    public function indexRoute ()
    {
        return route('admin.deadline.index');
    }

    /**
     * @param Request $request
     */
    public function validateRequest(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'amount'=>'nullable|regex:/^[0-9\.,]+$/',
            'expire_date'=>'required|numeric|not_in:0'
        ]);
    }

    /**
     * @param $data
     * @return string
     */
    public function dataAmount($data)
    {
        return number_format($data->amount);
    }

    /**
     * @param $data
     * @return string
     */
    public function dataFree($data)
    {
        $free = "<span class='badge badge-warning'>".vl('admin',"isn't")."</span>";
        if ($data->free == true){
            $free = "<span class='badge badge-info'>".vl('admin',"is")."</span>";
        }

        return $free;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function arrayDataDeadline()
    {
        return Deadline::latest()->pluck('name','slug');
    }

    public function getExpireDate($storeSetting)
    {
        $result = false;

        if ($storeSetting->to_date && Carbon::parse($storeSetting->to_date)->getTimestamp() > time()) {
            $result = true;
        }

        return $result;
    }

    public function getDeadline($paginate)
    {
        return Deadline::whereStatus(true)->paginate($paginate);
    }

    public function loadModel($slug)
    {
        $deadline = Deadline::whereSlug($slug)->first();

        return $deadline;
    }

    public function setInvoice($deadline)
    {
        $allData = [
            'amount' => $deadline->amount,
            'celebration_id' => \Auth::user()->celebration_id,
            'invoice_code' => $this->autoCode('Invoice','invoice_code'),
            'invoice_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'description' => Vl('admin','storeRecord'), /* after with trans set name */
            'type' =>'storeRecord'
        ];

        $invoiceStoreCreateBuilder = new InvoiceStoreCreateBuilder($allData);

        $invoiceStoreCreateBuilder->paymentTransactionDate = Carbon::now()->format('Y-m-d H:i:s');

        return $invoiceStoreCreateBuilder->setTransaction();
    }
}
