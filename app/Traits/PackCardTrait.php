<?php

namespace App\Traits;

trait PackCardTrait
{
    public $amount_str;
    /**
     * @param $data
     * @return string
     */
    public function dataAmount($data)
    {
        return number_format($data->amount);
    }
    /**
     * @return string
     * redirect route
     */
    public function indexRoute ()
    {
        return route('admin.pack.card.index');
    }
}
