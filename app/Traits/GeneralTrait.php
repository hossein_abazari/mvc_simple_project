<?php


namespace App\Traits;
use App\Models\Plugin;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Rules\HashOldPasswordValidate;
use App\Rules\SlugExistsValidate;
use App\Rules\StoreAmountValidate;
use Illuminate\Http\Request;


trait GeneralTrait
{
    use ToolTrait;

    public function getDataModal(Request $request,$modal)
    {
        $data = null;
        if (isset($modal['update']['model'])){

            $model = $this->modelString($modal['update']['model']);

            if (isset($modal['update']['relation'])){
                $data = $model::with($modal['update']['relation'])->whereSlug($request->slug)->first();
            }
            else{
                $data = $model::whereSlug($request->slug)->first();
            }
        }

        return $data;
    }

    /**
     * @param Request $request
     */
    public function updateLanguage(Request $request)
    {
        $setting = $this->getSetting();

        $setting->update([
            'locale' => $request->locale
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectSearchStoreIndex(Request $request)
    {
        return redirect(route('admin.celebration.index').'?name_search='.$request->search_store.'&btn_search=search');
    }

    public function redirectSearchStoreDetail($stores)
    {
        $store = $stores->first();

        return redirect(route('admin.celebration.show',[$store->slug]));

    }


}
