<?php


namespace App\Traits;
use App\Models\Deadline;
use App\Models\Invoice;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Builder\InvoiceStoreCreateBuilder;
use Carbon\Carbon;
use Illuminate\Http\Request;


trait DiscountTrait
{
    use ToolTrait;

    /**
     * @return string
     */
    public function indexRoute ()
    {
        return route('admin.discount.index');
    }

    /**
     * @param Request $request
     */
    public function validateRequest(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'number' => 'nullable|numeric',
            'discount_cent' => 'nullable|numeric',
            'expire_date'=>'required|numeric|not_in:0'
        ]);
    }

}
