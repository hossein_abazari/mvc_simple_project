@if($survey)
    @if ($survey->action == 'Y')
        <span class="badge bg-Main-color ">{{ vl('general','notPresent') }}</span>
    @else
         <span class="badge bg-Main-color-light text-main-color ">{{ vl('general','isPresent') }}</span>
    @endif
@else
    <span class="badge bg-grey">
        {{ vl('general','notParticipate') }}
    </span>
@endif
