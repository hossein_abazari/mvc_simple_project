<div class="row mb-5">

    <div class="alert alert-main-color-light">{{ vl('general','increaseInvitationCards') }}</div>

    @foreach($packCards as $packCard)
                <div class="card-image-item col-xs-3 col-sm-4" >
                    <div class="box-card-design">

                        <div class="picture-legend">
                            <div class="row">
                                <div class="col-8 name-card">
                                    <p class="mt-2"><b>{{ vl('general','name_pack') }} : {{ $packCard->name }}</b></p>
                                    <p>{{ vl('general','price') }} : {{ number_format($packCard->amount) }}</p>
                                    <p>{{ vl('general','number_card') }} : {{  $packCard->number_card }}</p>
                                </div>
                                <div class="col-4 select-radio-card text-left">
                                    {{ \App\Classes\FormField::fieldCheckbox('number_invitation_card[]',/* name */
                                      $packCard->slug, /* value */
                                      'col-md-12', /* first div class */
                                       'input-checkbox-check', /* input class */
                                      [], /* set other option like id, data-id */
                                      false, /* set label*/
                                      'radio-form', /* second class for form group */
                                      false /* is checked */
                                      ) }}
                                </div>
                            </div>
                        </div>
                        <div class="mask"></div>
                    </div>
                </div>

    @endforeach

</div>

