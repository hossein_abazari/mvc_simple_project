<div class="row card-grid">
    <div class="title-layouts">
         {{ vl('general','template') }} {{ vl('general',$layouts->first()->type) }}
    </div>
    @foreach($layouts as $layout)
         @if($layout->img_temp)
              <div class="card-image-item col-xs-3 col-sm-4" >
                    <div class="box-card-design">
                        <div class="content-card">
                            <a href="{{ $layout->directory }}" target="_blank">
                                <img class="img-fluid" src="{{ asset(\App\Classes\UploadFile::$directory.'templates/'.$layout->img_temp) }}">
                            </a>
                            <div class="picture-legend">
                                <div class="row">
                                    <div class="col-8 name-card">
                                        <p class="mt-2"><b>{{ vl('general','name_template') }} : {{ $layout->name }}</b></p>
                                        <p>{{ vl('general','price_main') }} : {{ number_format($layout->price_main) }}</p>
                                        <p>{{ vl('general','price_discount') }} : {{ number_format($layout->price) }}</p>
                                        <p>{{ vl('general','number_card') }} : {{  $layout->number_invitation_card }}</p>
                                    </div>
                                    <div class="col-4 select-radio-card text-left">
                                        {{ \App\Classes\FormField::fieldCheckbox('layout_id',/* name */
                                          $layout->slug, /* value */
                                          'col-md-12', /* first div class */
                                           'input-radio-check', /* input class */
                                          [], /* set other option like id, data-id */
                                          false, /* set label*/
                                          'radio-form', /* second class for form group */
                                          false, /* is checked */
                                          'radio' /* type radio or checkbox */
                                          ) }}
                                    </div>
                                </div>
                            </div>
                            <div class="mask"></div>
                        </div>
                    </div>
                </div>
          @endif
    @endforeach

</div>
<style>
    .card-grid .name-card p {
        margin-right: 15px;
    }
</style>
