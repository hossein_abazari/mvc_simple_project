@php
    $result = $model;
@endphp

@if($result->lastPage() != 1)
    <div class="text-left footable-loaded footable mt-3">
        <ul class="pagination">
            @if($result->previousPageUrl())
                <li class="footable-page-arrow disabled"><a href="{{ $result->previousPageUrl() }}">‹</a></li>
            @endif

            @for($i=1; $i <= $result->lastPage(); $i++)
                @if ($result->currentPage() == $i)
                    <li class="footable-page active"><a href="#">{{ $i }}</a></li>
                @else
                    <li class="footable-page"><a href="{{ $result->url($i) }}">{{ $i }}</a></li>
                @endif

            @endfor

            @if($result->nextPageUrl())
                <li class="footable-page-arrow disabled"><a href="{{ $result->nextPageUrl() }}" rel="next">›</a></li>
            @endif
        </ul>
    </div>
@endif
