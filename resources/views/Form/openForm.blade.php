<form class="{{ isset($class) ? $class : '' }}" method="{{ strtolower($method) == 'patch' ? 'POST' :$method }}" autocomplete="{{$autocomplete}}"
      action="{{ $action }}" id="{{ $id }}" >

    @if(strtolower($method) == 'post')
        @csrf
    @elseif(strtolower($method) == 'patch')
       @method('PATCH')
       @csrf
    @endif

