<div class="{{ isset($input['class']) ? $input['class'] : 'col-md-12 m-b-20' }}">
    <div class="{{ $input['classForm'] ? $input['classForm'] : 'form-group' }}">
        <div class="avatar-upload">
            @if (isset($input['label']) && $input['label'])
                <label class="{{$input['labelClass']}}">{{ $input['label'] }}</label>
            @endif
            <div class="avatar-edit">
                <input type='file' id="imageUpload_{{ $input[0] }}" name="{{ $input[0] }}" class="upload-avatar" accept=".png, .jpg, .jpeg" />
                <label for="imageUpload_{{ $input[0] }}"></label>
            </div>
            <div class="avatar-preview">
                <div id="imagePreview_{{ $input[0] }}" style="background-image: url({{ isset($input['url']) && $input['url'] ? $input['url'] :
                'http://i.pravatar.cc/500?img=7' }});">
                </div>
            </div>
        </div>
    </div>
</div>

    @section('cssFileAvatar')
        <style>

            .avatar-upload {
                position: relative;
                max-width: 205px;
                margin: 50px auto;
            }
            .avatar-upload .avatar-edit {
                position: absolute;
                right: 12px;
                z-index: 1;
                top: 10px;
            }
            .avatar-upload .avatar-edit input {
                display: none;
            }
            .avatar-upload .avatar-edit input + label {
                display: inline-block;
                width: 34px;
                height: 34px;
                margin-bottom: 0;
                border-radius: 100%;
                background: #ffffff;
                border: 1px solid transparent;
                box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
                cursor: pointer;
                font-weight: normal;
                transition: all 0.2s ease-in-out;
            }
            .avatar-upload .avatar-edit input + label:hover {
                background: #f1f1f1;
                border-color: #d6d6d6;
            }
            .avatar-upload .avatar-edit input + label:after {
                content: "\f040";
                font-family: "FontAwesome";
                color: #757575;
                position: absolute;
                top: 10px;
                left: 0;
                right: 0;
                text-align: center;
                margin: auto;
            }
            .avatar-upload .avatar-preview {
                width: 192px;
                height: 192px;
                position: relative;
                border-radius: 100%;
                border: 6px solid #f8f8f8;
                box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
            }
            .avatar-upload .avatar-preview > div {
                width: 100%;
                height: 100%;
                border-radius: 100%;
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
            }


        </style>
        @endsection

    @section('scriptFileAvatar')

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#imagePreview_{{ $input[0] }}').css('background-image', 'url('+e.target.result +')');
                        $('#imagePreview_{{ $input[0] }}').hide();
                        $('#imagePreview_{{ $input[0] }}').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#imageUpload_{{ $input[0] }}").change(function() {
                readURL(this);
            });
        </script>

        @endsection

@section('scriptFileAvatarImgBride')

        <script>
            function readURLImg(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#imagePreview_img_bride').css('background-image', 'url('+e.target.result +')');
                        $('#imagePreview_img_bride').hide();
                        $('#imagePreview_img_bride').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#imageUpload_img_bride").change(function() {
                readURLImg(this);
            });
        </script>

        @endsection
