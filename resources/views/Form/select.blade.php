<div class="{{ isset($input['class']) ? $input['class'] : 'col-md-12 m-b-20' }}">

    <div class="form-group">

        @if(!isset($input['label']) || $input['label'])

            <label class="mb-1">{{ vl('admin',$input[0]) }}</label>

        @endif

        <select name="{{ isset($input['multiple']) ? $input[0]."[]" : $input[0] }}"

                class="{{ isset($input['classInput']) ? $input['classInput'] : 'form-control' }}"

        {{-- set attributes for input --}}
        @foreach ($input as $k=>$v)
            @if($k != 0 && $k != 'classInput' && $k != 'placeholder' && $k != 'class'&& $k != 'value' && $k != 'relation' && $k != 'label' && $k != 'selectManual')
                {{$k}}="{{ $v }}"
            @endif
        @endforeach

        >

        @if(!empty($input['value']))

            {{-- placeholder select --}}
            @if(isset($input['placeholder']))
                <option class="select-place-holder" value="">{{ vl('admin',$input['placeholder']) }}</option>
            @endif

            {{-- data for select --}}
            @foreach($input['value'] as $k=>$val)

                    <option
                    {{--if select is multiple--}}
                    @if (isset($input['multiple']))

{{--                    {{isset($mode) && $mode=='update' ? /* if modal for update */--}}

{{--                     (isset($input['relation']) ?--}}

{{--                        (in_array(trim($k) , $data->{$input['relation']}->pluck('slug')->toArray()) ? 'selected' : '' )--}}

{{--                     : ($data->$input[0] == $k ? 'selected' : '') )--}}

{{--                     :''}}--}}

                    @if ($model != false)

                        @if (is_object($model))

                            {{ in_array(trim($k) , $model->pluck('slug')->toArray()) ? 'selected' : '' }}

                        @else
                            {{ in_array(trim($k) , $model) ? 'selected' : '' }}

                        @endif

                    @endif
                    {{-- if select is normal --}}
                    @else

                       @if ($model != false)

                            @if (!is_string($model))
                                {{ $model->slug == $k }}

                            @else

                                {{ $model == $k ? 'selected' : '' }}

                            @endif

                       @endif

                    @endif

                    value="{{ $k }}">

                    {{ $val }}

                </option>
            @endforeach

        @else
            <option>{{ vl('admin','noData') }}</option>
            @endif
            </select>
    </div>
</div>

@section('cssSelect')
    <link href="{{ asset('css/admin/bootstrap-select.min.css') }}" rel="stylesheet">
@endsection

@section('scriptSelect')
    <script src="{{ asset('js/admin/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset(vl('link','select-picker')) }}"></script>
    <script>
        $('.selectpicker').selectpicker('refresh');
    </script>
@endsection
