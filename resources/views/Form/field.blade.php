<div class="{{ isset($input['class']) ? $input['class'] : 'col-md-12 m-b-20' }}">
    <div class="{{ $input['classForm'] ? $input['classForm'] : 'form-group' }}">
        @if(!isset($input['label']) || $input['label'])
            <label class="mb-1">{{ vl('admin',$input[0]) }}</label>
        @endif

        <input name="{{ $input[0] == 'amount_str' ? 'amount' : $input[0] }}" type="{{ $input[1] }}"

               class="form-control {{ isset($input['classInput']) ? $input['classInput'] : '' }}"
               {{-- if modal for update and is checkbox --}}
               @if (isset($input['relation']))
               {{ isset($mode) && $mode=='update' && $input[1] == 'checkbox'?

                    ($data->$relationUpdate->$input[0] == true) ? 'checked' : ''

               : '' }}
               @else

               {{ isset($mode) && $mode=='update' && $input[1] == 'checkbox'?

                ($data->$input[0] == true) ? 'checked' : ''

               : '' }}

               @endif

               @if ($input[1] != 'checkbox' && $input[1] != 'password')

               @if (isset($fieldSearch) && $fieldSearch)

               value="{{ isset(Request()->$input[0]) ? Request()->$input[0] : '' }}"

               @else
               value="{{ isset($mode) && $mode=='update' ? /* if modal is update */

                                                           (isset($input['relation']) ?  /* if input have relation */

                                                           ($data->{$relationUpdate}->$input[0])

                                                           : ($data->$input[0])) /* if input is decimal for amount 200,000 */

                                                           :(isset($input['value']) ? $input['value'] : '') }}"


               @endif


               @endif

               @if(isset($decimalName[$input[0]])) onkeyup="javascript:this.value=itpro(this.value)" @endif {{-- java script for decimal in amount 200,000  --}}

               placeholder="{{ isset($input['placeholder']) ? vl('admin',$input['placeholder']) : '' }}"

        {{-- set attributes for input --}}
        @foreach ($input as $k=>$v)

            @if($k != 0 && $k != 1 && $k != 'classInput' && $k != 'placeholder' && $k != 'class' && $k != 'value' && $k != 'relation' && $k != 'label' )
                {{$k}}="{{ $v }}"
            @endif

        @endforeach >

    </div>
</div>

