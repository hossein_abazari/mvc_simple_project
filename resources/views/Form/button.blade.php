<div class="{{ $class }}">
    <button type="submit" class="btn-submit {{ $classInput }}">{{ $titleButton }}</button>
</div>
@if($ajax)
@section('scriptButton')
        <script src="{{ asset('js/script.ajax.js') }}"></script>
        <script>
            $(document).on('click','.btn-submit',function (e) {
                e.preventDefault();

                if($(this).closest('form').attr('enctype') === 'multipart/form-data'){
                    upload_image($(this));

                }
                else{
                    ajaxJquery($(this));
                }

            });
        </script>
    @endsection
@endif
