<div class="{{ isset($input['class']) ? $input['class'] : 'col-md-12 m-b-20' }}">
    <div class="{{ $input['classForm'] ? $input['classForm'] : 'form-group' }}">
        <div class="form-check">
            <input name="{{ $input[0] }}" class="form-check-input {{ $input['classInput'] }}" type="{{ $input['type'] }}" value="{{ $input['value'] }}"
                   {!! $input['checked'] ? 'checked' : '' !!}

                {{-- set attributes for input --}}
                @foreach ($input as $k=>$v)

                    @if($k != 0 && $k != 'class' && $k != 'value' && $k != 'label' && $k != 'checked' )
                        {{$k}}="{{ $v }}"
                    @endif

                @endforeach

                    />
            <label class="form-check-label">{{ $input['label'] }}</label>
        </div>
    </div>
</div>
