<div class="{{ isset($input['class']) ? $input['class'] : 'col-md-12 m-b-20' }}" xmlns="http://www.w3.org/1999/html">
    <div class="form-group">
        @if(!isset($input['label']) || $input['label'])
            <label class="mb-1">{{ vl('admin',$input[0]) }}</label>
        @endif
        @php
            $valueTextarea = isset($mode) && $mode=='update' ? /* if modal is update */

                                                         (isset($input['relation']) ?  /* if input have relation */

                                                         ($data->{$relationUpdate}->$input[0])

                                                         : ($data->$input[0])) /* if input is decimal for amount 200,000 */

                                                         :(isset($input['value']) ? $input['value'] : '')
            @endphp

        <textarea name="{{ $input[0] == 'amount_str' ? 'amount' : $input[0] }}"

               class="{{ isset($input['classInput']) ? $input['classInput'] : 'form-control' }}"


               placeholder="{{ isset($input['placeholder']) ? vl('admin',$input['placeholder']) : '' }}"

        {{-- set attributes for input --}}
        @foreach ($input as $k=>$v)

            @if($k != 0 && $k != 'classInput' && $k != 'placeholder' && $k != 'class' && $k != 'value' && $k != 'relation' && $k != 'label' )
                {{$k}}="{{ $v }}"
            @endif

        @endforeach >{{$valueTextarea}}</textarea>

    </div>
</div>

