<a class="{{ $classLink }}" href="{{ $href }}"
   @if(isset($formId) && $formId)
       onclick="event.preventDefault();document.getElementById('{{  $formId }}').submit();"
   @endif
>
    @if(isset($classIcon) && $classIcon)
        <i class="{{ $classIcon }}"></i>
    @endif
    {{ $value }}
    @if(isset($formId) && $formId)
        <form id="{{ $formId }}" action="{{ $url }}" method="{{ $method }}" style="display: none;">
            @csrf
        </form>
    @endif
</a>
