
@php
       $ip =  Request()->ip() == '::1' ? '185.159.152.0' : Request()->ip() ;
       /*$data = Location::get($ip);*/
       $lat = isset($lat) ? $lat : 29.935711;
       $lng = isset($lng) ? $lng : 52.887702;

@endphp
@section('lat'){{ $lat }}@endsection
@section('lng'){{ $lng }}@endsection
<div class="row">
    <div class="col-md-12">
        <div id="map"></div>
    </div>
</div>
<div class="row justify-content-center mt-3">
    <div class="col-md-2">
        <div class="form-group">
            {{ \App\Classes\FormField::field('type','hidden',$lat,'','','',[
                  'value' =>''
                  ],false) }}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {{ \App\Classes\FormField::field('type','hidden',$lng,'','','',[
                  'value' =>''
                  ],false) }}
        </div>
    </div>
</div>
{{--@section('scriptMap')--}}
{{--<script--}}
{{--    src="http://maps.googleapis.com/maps/api/js?key=AIzaSyB41DRUbKWJHPxaFjMAwdrzWzbVKartNGg&callback=initMap&v=weekly"--}}
{{--    defer--}}
{{--></script>--}}
{{--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3QPDkZDiPMBIlG7pZpKD_cdgcjgFC6gk&callback=initMap"></script>--}}

{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>--}}

<script>
    let map;
    let marker;
    let geocoder;
    let responseDiv;
    let response;
    function initMap() {
        map = new google.maps.Map(document.getElementById("map"), {
            zoom: 8,
            center: { lat: -34.397, lng: 150.644 },
            mapTypeControl: false,
        });
        geocoder = new google.maps.Geocoder();

        const inputText = document.createElement("input");

        inputText.type = "text";
        inputText.placeholder = "Enter a location";

        const submitButton = document.createElement("input");

        submitButton.type = "button";
        submitButton.value = "Geocode";
        submitButton.classList.add("button", "button-primary");

        const clearButton = document.createElement("input");

        clearButton.type = "button";
        clearButton.value = "Clear";
        clearButton.classList.add("button", "button-secondary");
        response = document.createElement("pre");
        response.id = "response";
        response.innerText = "";
        responseDiv = document.createElement("div");
        responseDiv.id = "response-container";
        responseDiv.appendChild(response);

        const instructionsElement = document.createElement("p");

        instructionsElement.id = "instructions";
        instructionsElement.innerHTML =
            "<strong>Instructions</strong>: Enter an address in the textbox to geocode or click on the map to reverse geocode.";
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(inputText);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(submitButton);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(clearButton);
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(instructionsElement);
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(responseDiv);
        marker = new google.maps.Marker({
            map,
        });
        map.addListener("click", (e) => {
            geocode({ location: e.latLng });
        });
        submitButton.addEventListener("click", () =>
            geocode({ address: inputText.value })
        );
        clearButton.addEventListener("click", () => {
            clear();
        });
        clear();
    }

    function clear() {
        marker.setMap(null);
        responseDiv.style.display = "none";
    }

    function geocode(request) {
        clear();
        geocoder
            .geocode(request)
            .then((result) => {
                const { results } = result;

                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                marker.setMap(map);
                responseDiv.style.display = "block";
                response.innerText = JSON.stringify(result, null, 2);
                return results;
            })
            .catch((e) => {
                alert("Geocode was not successful for the following reason: " + e);
            });
    }

    window.initMap = initMap;
</script>
{{-- @endsection--}}

