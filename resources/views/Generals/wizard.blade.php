    <form method="POST" id="signup-form" class="signup-form" data-value = '0' action="{{ $route }}" enctype="multipart/form-data">
    @csrf

    @php
        $validForm =[];
    @endphp

    @foreach($data as $key => $value)
        <h3>
            {!! $value['step_name'] !!}
        </h3>
        <fieldset data-type="{{ $key }}">
            <div class="{{ isset($classForm) && $classForm ? $classForm : '' }}">
                <h2 class="{{ isset($value['titleClass']) ? $value['titleClass'] : '' }}">{!! $value['title'] !!}</h2>
                @if(isset($value['input']) && $value['input'])

                    @if (!empty($value['validate_form']))
                        @php
                            $validForm[$key] = $value['validate_form']
                        @endphp
                    @endif

                    {{ \App\Classes\FormField::field($key,$value['input']['type'],'','col-12','form-control login-field field-'.$key,$key,[
                    'value' =>''
                    ],false) }}

                @endif

                @if(isset($value['inputs']) && $value['inputs'])
                    <div class="row">
                    {{-- set inputs array --}}
                    @foreach($value['inputs'] as $keyInput => $valueInput)

                        @if (!empty($valueInput['validate_form']))
                            @php
                                $validForm[$keyInput] = $valueInput['validate_form']
                            @endphp
                        @endif
                        {{-- text area section for wizard --}}
                        @if($valueInput['type'] == 'textarea')

                                {{ \App\Classes\FormField::fieldTextarea($keyInput,
                            '',
                            isset($valueInput['class']) && $valueInput['class'] ? $valueInput['class'] : 'col-md-12',
                            isset($valueInput['classInput']) && $valueInput['classInput'] ? $valueInput['classInput'] : 'form-control',
                            vl('general',$valueInput['label']),
                           isset($valueInput['option']) ? $valueInput['option'] : []) }}
                          {{--  section for wizard --}}
                        @elseif($valueInput['type'] == 'dynamic')
                                <div class="{{ isset($valueInput['classContent']) ? $valueInput['classContent'] : '' }}"> {!! $valueInput['content'] !!} </div>
                        @elseif($valueInput['type'] == 'checkbox')

                                {{ \App\Classes\FormField::fieldCheckbox($keyInput,
                            '1',
                            isset($valueInput['class']) && $valueInput['class'] ? $valueInput['class'] : 'col-md-12',
                            isset($valueInput['classInput']) && $valueInput['classInput'] ? $valueInput['classInput'] : '',
                           isset($valueInput['option']) ? $valueInput['option'] : [],
                            vl('general',$valueInput['label'])
                            ) }}

                        @elseif($valueInput['type'] == 'select')
                            {{ \App\Classes\FormField::fieldSelect(
                            $keyInput,
                            $valueInput['value'],
                            'col-md-6',
                            'form-control',
                            $keyInput,
                            isset($valueInput['option']) ? $valueInput['option'] : [],
                            false
                            ) }}
                          @else
                        {{ \App\Classes\FormField::field($keyInput,
                        $valueInput['type'],
                        '',
                        isset($valueInput['class']) && $valueInput['class'] ? $valueInput['class'] : 'col-md-12' ,
                        isset($valueInput['classInput']) && $valueInput['classInput'] ? $valueInput['classInput'] : '',
                        isset($valueInput['option']['text_manual']) ? $valueInput['option']['text_manual'] : $keyInput,
                       isset($valueInput['option']) ? $valueInput['option'] : [],
                        false) }}
                            @endif
                    @endforeach

                    </div>

                @endif
                {{-- validate layout for end section --}}
                @if(isset($value['content']) && $value['content'])
                    {!! $value['content'] !!}
                    @if (!empty($value['validate_finish']))
                        <div class="form-group mb-5">
                            @foreach($value['validate_finish'] as $key => $value)
                                <span class="email" name="{{$value}}"></span>
                            @endforeach
                        </div>
                    @endif
                @endif
            </div>
        </fieldset>
    @endforeach

        {{ \App\Classes\FormField::field('type','hidden','','col-12','form-control field-type','',[
                  'value' =>''
                  ],false) }}
    </form>
@section('wizardScript')
    <script src="{{ asset('js/celebration/jquery.steps.js') }}"></script>
    <script src="{{ asset('js/stepsJS/main.js') }}"></script>
    <script src="{{ asset('js/celebration/pwt-date.js') }}"></script>
    <script src="{{ asset('js/celebration/pwt-datepicker.js') }}"></script>
{{--    <script src="{{ asset('js/admin/jquery.md.bootstrap.datetimepicker.js') }}"></script>--}}
    <script type="text/javascript">
        var dp;
        $(document).ready(function() {
            var options = {
                format : "YYYY/MM/DD",
                formatter : function(unix) {
                    var pdate = new persianDate(unix);
                    pdate.formatPersian = false;
                    return pdate.format("YYYY/MM/DD");
                    //return new persinDate(unix).format("YYYY/MM/DD");
                },
                daysTitleFormat : "YYYY MMMM",
                observer : true,
                sendOption : "p",
                //position : [2, 2],
                autoclose : true,
                toolbox : true,
                altField : "#alternateField",
                altFormat : "u",
                altFieldFormatter : function(unix) {
                    var pdate = new persianDate(unix);
                    pdate.formatPersian
                    pdate.formatPersian = false;
                    return pdate.format("YYYY MM DD");
                },
                onShow : function() {
                    //console.log("user config onShow event ")
                },
                onHide : function() {
                    //console.log("user config onHide event ")
                },
                onSelect : function(unix) {
                    //console.log("user config onSelect event as : "+unix)

                }
            };
            $(".datePicker").persianDatepicker(options);
            dp = $(".datePicker").data("datepicker");
        });
    </script>
    @if($validate[0])
        <script src="{{ asset('js/celebration/login/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('js/celebration/login/additional-methods.min.js') }}"></script>
        <script>
            var validForm = $.parseJSON({!! json_encode(json_encode($validForm)) !!});
            var labels = $.parseJSON({!! json_encode(json_encode($button_labels)) !!});
            wizardForm(validForm, labels);

        </script>
    @endif

@endsection

@section('wizardCss')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/stepsCSS/wizard_'.$nameCSS.'.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/celebration/pwt-datepicker.css') }}">

@endsection
