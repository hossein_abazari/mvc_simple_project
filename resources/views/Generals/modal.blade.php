<!-- Modal -->
<div class="modal fade font3 mt-3" style="{{ vl('general','direction') }}" id="{!! $nameModal !!}" role="dialog">
    <div class="modal-dialog {{ isset($sizeModal) ? $sizeModal : '' }}">
        <!-- Modal content-->
        <form class="form" action="{!! $route !!}"
              @php
                $classButtonImg= 'btn-click-modal';
              @endphp
              @if(isset($option['enctype']) && $option['enctype'])
              @php
                  $classButtonImg= 'btn-click-modal-img';
              @endphp
              enctype="multipart/form-data"
              @endif
              method="post"
              >
            @csrf
        <div class="modal-content">
            <div class="modal-header modal-header--sticky">
                <h4 class="modal-title">{{ $title }}</h4>
                <button type="button" class="btn-close float-left" data-bs-dismiss="modal"></button>
            </div>
            <div class="modal-body modal-body--sticky">
                {!! $content !!}
            </div>
            <div class="modal-footer modal-footer--sticky">
                <button type="submit" class="btn {{ $classButtonImg }} {{ isset($buttonClass)? $buttonClass : '' }}">
                    {{ isset($buttonName) ? $buttonName :''}}</button>
{{--                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
            </div>
        </div>
        </form>

    </div>
</div>

@section('scriptModal')
    <script src="{{ asset('js/script.ajax.js') }}"></script>

    <script>
        $(document).on('click','.btn-click-modal-img',function (e) {
            e.preventDefault();

            upload_image($(this))
        });

        $(document).on('click','.btn-click-modal',function (e) {
            e.preventDefault();

            ajaxJquery($(this))
        });
    </script>
    @endsection
