@php
    $date ='';
@endphp
@if ($modelDetails->isNotEmpty())

    @foreach($modelDetails as $modelDetail)
    @if ($date != $modelDetail->formatTypeDate($modelDetail->created_at,'Y-m-d'))
        @php
            $date = $modelDetail->formatTypeDate($modelDetail->created_at,'Y-m-d')
        @endphp

            <div class="col-12 position-absolute text-center date-message-label">
                {{ $modelDetail->formatTypeDate($modelDetail->created_at,'Y/m/d') }}
            </div>
    @endif
    @if($modelDetail->user_id != Auth::id())
        <li class="box-card-message bg-light-success">
            <div class="chat-img "><img src="{{ asset('images/users/default_user.png') }}" alt="user" /></div>
            <div class="chat-content">
                <h5><a @if(isset($nameRoute)) href="{{ route($nameRoute,[$modelDetail->slugMaster]) }}" @endif>{{ $modelDetail->user->name }}</a></h5>
                <div class="box">{{ $modelDetail->message }}</div>
            </div>
            <div class="chat-time">{{ $modelDetail->formatTypeDate($modelDetail->created_at,'H:i a') }}</div>
        </li>
    @else
        <li class="reverse box-card-message bg-light-inverse">
            <div class="chat-time">{{ $modelDetail->formatTypeDate($modelDetail->created_at,'H:i a') }}</div>
            <div class="chat-content">
                <h5>{{ $modelDetail->user->name }}</h5>
                <div class="box">{{ $modelDetail->message }}</div>
            </div>
            <div class="chat-img"><img src="{{ asset('images/users/default_user.png') }}" alt="user" /></div>
        </li>
    @endif

@endforeach

    @else
    <li class="">
        {{ vl('general' , 'nothing') }}
    </li>
@endif
