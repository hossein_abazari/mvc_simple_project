<div class="row">

    <div class="col-12">
        <div class="card m-b-0">
            <!-- .chat-row -->
            <div class="chat-main-box">

                <!-- .chat-left-panel -->
                <!-- .chat-right-panel -->
                <div class="col-12">
                    <div class="chat-main-header">
                        <div class="p-20 b-b">
                            <h3 class="box-title">{{ vl('general','tickets') }}</h3>
                        </div>
                    </div>
                    @if(!isset($messages['sendMessage']) || $messages['sendMessage'])
                      <a class="btn btn-warning waves-effect waves-light mt-4" href="#message"><span class="btn-label"><i class="fa fa-envelope-o"></i></span> {{ vl('general','message') }} </a>
                    @endif
                    <div class="chat-rbox">
                        <ul class="chat-list p-20">
                            @component('Generals.listMessage')
                                @slot('modelDetails',$messages['modelDetails'])

                                @if(isset($messages['nameRoute']))
                                    @slot('nameRoute',$messages['nameRoute'])
                                @endif

                            @endcomponent
                        </ul>
                    </div>

                    @if(!isset($messages['sendMessage']) || $messages['sendMessage'])
                        <div class="card-body b-t border-top">
                        <form action="{{ route($messages['routeFormSubmit'],[$messages['modelMaster']->slug]) }}" method="post">
                            <div class="row">
                                @csrf
                                <div class="col-8">
                                    <textarea id="message" name="message" placeholder="{{ vl('general','writeMessage') }}" class="form-control b-0"></textarea>
                                </div>
                                <div class="col-4 text-right">
                                    <button type="button" class="btn btn-info btn-circle btn-lg btn-send"><i class="fa fa-paper-plane-o"></i> </button>
                                </div>
                            </div>
                        </form>
                    </div>
                     @endif
                </div>
                <!-- .chat-right-panel -->
            </div>
            <!-- /.chat-row -->
        </div>
    </div>
</div>

@section('scriptView')
    <script src="{{ asset('js/script.ajax.js') }}"></script>
    <script>
        $(document).on('click','.btn-send',function (e) {
            e.preventDefault();
            ajaxJquery($(this),'.chat-list');
            $('#message').val("")
        });
    </script>
@endsection

@section('cssView')
@endsection
