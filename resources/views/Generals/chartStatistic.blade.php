<div class="row">
    <!-- Column -->
    <div class="col-12">
        <div class="card">
            <div class="row">
                <div class="col-lg-12 b-l">
                    <div class="card-body">
                        <h4 class="font-medium text-inverse">{{ vl('admin',$name) }}</h4>

                        <div class="chart-statistic-{{ $name }}" style="height: 350px;"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>

@section('cssChart')
    <link href="{{ asset('css/apexchart/apexcharts.css') }}" rel="stylesheet">
@endsection

@section('scriptChart')

    <script src="{{ asset('js/apexchart/apexcharts.min.js') }}"></script>

    <script>
        function chartResult(data,categories,name) {
            var options = {
                series: [{
                    name: 'series1',
                    data: data
                }],
                chart: {
                    height: 350,
                    type: 'area'
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    curve: 'smooth'
                },
                xaxis: {

                    categories: categories,
                    tickPlacement: 'on'
                },
                tooltip: {
                    x: {
                        format: 'dd/MM/yyyy'
                    },
                },
            };

            var chart = new ApexCharts(document.querySelector(".chart-statistic-"+name), options);
            chart.render();

        }
    </script>


@endsection

@section('script'.ucfirst($name))
    <script>

        function chartData(strMode) {
            $(document).ready(function (e) {
                var url = `{{ route('admin.dashboard.chart') }}`;

                $.get(url,{'strData':strMode},function (data) {
                    var response = JSON.parse(data);

                    chartResult(response.result,response.date,strMode);
                });
            });
        }

        chartData('{{ $name }}')
    </script>
@endsection


