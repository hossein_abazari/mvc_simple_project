<div class="col-md-3 col-xs-12 col-sm-6 no-padding">
    <div class="pricing-box @if(isset($popular) && $popular) featured-plan @endif">
        <div class="pricing-body">
            <div class="pricing-header">
                @if(isset($popular) && $popular)
                  <h4 class="price-lable text-white bg-warning">{{ vl('general','popular') }}</h4>
                @endif
                <h4 class="text-center">{{ $title }}</h4>
                <h2 class="text-center"><span class="price-sign">{{ labelMoney() }}</span>{{ number_format($amount) }}</h2>
{{--                <p class="uppercase">{{ vl('general','days') }}</p>--}}
            </div>
            <div class="price-table-content">
                <div class="price-row"><i class="icon-user"></i> {{ vl('general','countDay') }} : {{ $expireDate }}</div>
                <div class="price-row">
                    <a class="btn @if(isset($popular) && $popular) btn-lg btn-info @else btn-success @endif waves-effect waves-light m-t-20" href="{{ $url }}">{{ vl('general','buy') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
