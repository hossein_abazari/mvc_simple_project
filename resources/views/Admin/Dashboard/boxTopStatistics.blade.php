<div class="row">

    @foreach(\App\Classes\BladeCode::getStatisticArrayName() as $key => $value)
        @component('Admin.Generals.boxStatistic')
            @slot('title',$key)
            @if(isset($value[1]))
                @slot('link',$value[1])
            @endif
            @slot($value[0] == 'sum' ?'money' :'number',$topStatistics[$key]['all'])
            @slot('percent',$topStatistics[$key]['percent'])
            @slot('today',$topStatistics[$key]['today'])
            @slot('yesterday',$topStatistics[$key]['yesterday'])
        @endcomponent
    @endforeach

    @component('Admin.Generals.boxStatistic')
        @slot('title','paymentsCount')
        @slot('money',$topStatistics['ticketSendCount']['all'])
        @slot('textMuted',[
            'total'=> $topStatistics['paymentCount']['all'],
            'port'=> $topStatistics['paymentCount']['port'],
            'cash'=> $topStatistics['paymentCount']['cash'] + $topStatistics['paymentCount']['card'],
        ]),
        @slot('link',route('admin.transaction.index'))
    @endcomponent
    {{-- ticket Statistic --}}
    @component('Admin.Generals.boxStatistic')
        @slot('title','ticketReceiveCount')
        @slot('number',$topStatistics['ticketReceiveCount']['all'])
        @slot('notRead',$topStatistics['ticketReceiveCount']['notRead']),
        @slot('link',route('admin.ticket.master.index'))
    @endcomponent

     @component('Admin.Generals.boxStatistic')
        @slot('title','ticketSendCount')
        @slot('number',$topStatistics['ticketSendCount']['all'])
        @slot('notRead',$topStatistics['ticketSendCount']['notRead'])
        @slot('link',route('admin.ticket.master.index'))
     @endcomponent
</div>
