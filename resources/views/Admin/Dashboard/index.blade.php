@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','dashboard') }}
@endsection

@section('content')
    {{-- statistics box top dashboard --}}
    @component('Admin.Dashboard.boxTopStatistics')
        @slot('topStatistics',$topStatistics)
    @endcomponent

    {{-- last data for view --}}
    @component('Admin.Dashboard.boxLastData')
        @slot('lastData',$lastData)
    @endcomponent

    {{-- chart statistics --}}
    @component('Admin.Dashboard.boxStatisticChart')
{{--        @slot('lastData',$lastData)--}}
    @endcomponent

@endsection

@section('script')
    @yield('scriptPayments')
    @yield('scriptStores')
@endsection

