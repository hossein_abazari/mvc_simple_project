<div class="row">
    <div class="col-6">
        @component('Admin.Generals.view')
            @slot('table',[
                'setting' => ['grid'=>true,'title'=>'stores','more_route'=>route('admin.celebration.index')],
                'model' => $lastData['stores'],
                'pagination' => false,
                 'field' => ['name'=>'dataName','from_date'=>'dataFromDate','to_date'=>'dataToDate','amount_debt'=>false]
            ])
        @endcomponent
    </div>

    <div class="col-6">
        @component('Admin.Generals.view')
            @slot('table',[
                'setting' => ['grid'=>true,'title'=>'transaction','more_route'=>route('admin.transaction.index')],
                'model' => $lastData['transactions'],
                'tr_class' => true, // trClass()
                'pagination' => false,
                'field' => ['store_name'=>false,'invoice_code'=>false,'type_payment'=>false,
                'amount_str'=>false,'payment_str'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            ])
        @endcomponent
    </div>
</div>

<div class="row">
    <div class="col-6">
        @component('Generals.message')
            @slot('messages',[
                'modelDetails' => $lastData['ticketDetails'],
                'routeFormSubmit' => 'admin.ticket.master.show.store',
                'sendMessage' => false,
                'nameRoute' => 'admin.ticket.master.show'
            ])
        @endcomponent
    </div>

    <div class="col-6">
        @component('Admin.Generals.view')
            @slot('table',[
                'setting' => ['grid'=>true,'title'=>'invoices','more_route'=>route('admin.invoice.index')],
                'model' => $lastData['invoices'],
                'pagination' => false,
                'field' => ['store_name'=>false,'invoice_code'=>false,'amount_str'=>false,'type_invoice'=>false]
            ])
        @endcomponent
    </div>
</div>

