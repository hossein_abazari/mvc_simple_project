<div class="card-body">
    {{ \App\Classes\FormField::openForm(route('admin.setting.change.password'),'POST') }}
    <div class="row">
        {{ \App\Classes\FormField::field('old_password','password','','col-md-4','form-control','oldPasswordEnter') }}

        {{ \App\Classes\FormField::field('password','password','','col-md-4','form-control','newPasswordEnter') }}

        {{ \App\Classes\FormField::field('password_confirmation','password','','col-md-4','form-control','passwordConfirmationEnter') }}

        {{ \App\Classes\FormField::fieldButton(vl('general','update')) }}
    </div>
    {{ \App\Classes\FormField::endForm() }}
</div>
