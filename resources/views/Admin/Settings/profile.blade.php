<div class="card-body">
    {{ \App\Classes\FormField::openForm(route('admin.setting.profile'),'POST') }}
    <div class="row">
        {{ \App\Classes\FormField::field('email','text',$user->email,'col-md-4','form-control',false,['readonly' => 'readonly']) }}

        {{ \App\Classes\FormField::field('name','text',$user->name,'col-md-4','form-control','nameEnter') }}

        {{ \App\Classes\FormField::field('mobile','text',$user->mobile,'col-md-4','form-control','mobileEnter') }}

        {{ \App\Classes\FormField::fieldButton(vl('general','update')) }}
    </div>
    {{ \App\Classes\FormField::endForm() }}
</div>
