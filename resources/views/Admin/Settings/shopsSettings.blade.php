
<div class="card-body">
    {{ \App\Classes\FormField::openForm(route('admin.setting.store'),'POST') }}
        <div class="row">
            {{ \App\Classes\FormField::fieldSelect('locale',['fa'=>'FA','en'=>'EN'],'col-md-6','form-control selectpicker','localeSelectEnter',[],\App\Traits\SettingTrait::getLocaleSetting()) }}

            {{ \App\Classes\FormField::fieldSelect('language',['fa'=>vl('general','persian'),'en'=>vl('general','english')],'col-md-6','form-control selectpicker',false
                                ,['multiple'=>'multiple'],\App\Traits\SettingTrait::getLanguageSetting()) }}

            {{ \App\Classes\FormField::fieldButton(vl('general','update')) }}
        </div>
   {{ \App\Classes\FormField::endForm() }}
</div>
