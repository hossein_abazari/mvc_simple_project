@extends('Admin.layout.main')

@section('title')
    {{ vl('general','settings') }}
@endsection

@section('content')

    <div class="row">


        <div class="col-lg-12">
            <div class="card">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs profile-tab" role="tablist">
                    <li class="nav-item"> <a class="nav-link link-menu active" data-toggle="tab" href="#shops_settings" role="tab">{{ vl('admin','shops_settings') }}</a> </li>
                    <li class="nav-item"> <a class="nav-link link-menu" data-toggle="tab" href="#profile" role="tab">{{ vl('general','profile') }}</a> </li>
                    <li class="nav-item"> <a class="nav-link link-menu" data-toggle="tab" href="#change_password" role="tab">{{ vl('general','change_password') }}</a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!--second tab-->
                    <div class="tab-pane active" id="shops_settings" role="tabpanel">

                       @component('Admin.Settings.shopsSettings')
                        @endcomponent

                    </div>
                    <div class="tab-pane" id="profile" role="tabpanel">

                        @component('Admin.Settings.profile')
                            @slot('user',Auth::user())
                        @endcomponent

                    </div>
                    <div class="tab-pane" id="change_password" role="tabpanel">
                        @component('Admin.Settings.changePassword')
                            @slot('user',Auth::user())
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>


@endsection

@include('Admin.Settings.script')
