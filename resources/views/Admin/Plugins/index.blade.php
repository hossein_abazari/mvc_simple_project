@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','plugins') }}
@endsection

@section('content')

    @component('Admin.Generals.view')
        @slot('modal',[
            'setting' => ['modalForm'=>true,'sizeModal'=>'','targetModal' => 'deadline-modal'],
            'input' => [
                'name'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'nameEnter'],
                'amount_str'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'amountEnter'],
/*                'user_name'=>['select','value'=>['a'=>'A','b'=>'B'],'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'userEnter'],*/
            ],
            'form' => [
                'route' => route('admin.plugin.store'),
                'method' => 'post',
                'class' => '',
                'csrf' => csrf_field(),
            ],
            'update' => [
                'model'=>'Plugin',
                'route'=>'admin.plugin'
            ],

        ])

        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $plugins,
            'field' => ['name'=>false,'amount_str'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['delete'=>'admin.plugin','edit'=>true]
        ])
    @endcomponent

@endsection

@include('Admin.Deadlines.css')
@include('Admin.Deadlines.script')
