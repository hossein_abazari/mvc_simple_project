@section('script')
    <script src="{{ asset('js/admin/footable.all.min.js') }}"></script>

    <script>
        $(document).on('change','.deadline',function () {
            var url = `{{ route('admin.amount.deadline') }}`;
            var slug = $(this).val();

            $.get(url,{slug:slug},function (data) {

                $('#amount-store').val(data)

            })

        })
    </script>
@endsection

