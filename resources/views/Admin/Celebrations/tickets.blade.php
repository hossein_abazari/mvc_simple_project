
<div class="card-body">
    @component('Admin.Generals.view')
        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $ticketMasters,
            'field' => ['store_name'=>'dataNameLabel','user_name'=>false,'title'=>false,'closed'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['show'=>'admin.ticket.master'],
            'pagination' => false,
        ])
    @endcomponent
</div>

