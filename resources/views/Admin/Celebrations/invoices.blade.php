
<div class="card-body">
    @component('Admin.Generals.view')
        @slot('modal',[
         'setting' => ['modalForm'=>true,'sizeModal'=>'','targetModal' => 'deadline-modal'],
         'input' => [
             'celebration_id'=>['hidden','value'=>$store->slug,'class'=>'','classInput'=>'form-control','placeholder'=>'nameEnter','label'=>false],
             'invoice_code'=>['text','value'=>$autoCode,'class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'invoiceCodeEnter'],
             'invoice_date'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control date','placeholder'=>'invoiceDateEnter','autocomplete' => "off"],
             'amount_str'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'amountEnter'],
             'description'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'descriptionEnter'],

         ],
         'form' => [
             'route' => route('admin.invoice.store'),
             'method' => 'post',
             'class' => '',
         ],

     ])
        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $invoiceStores,
            'field' => ['invoice_code'=>false,'description'=>false,'amount_str'=>false,'type_invoice'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
             'pagination' => false,
        ])
    @endcomponent


</div>

