
<div class="card-body">
    @component('Admin.Generals.view')
        @slot('modal',[
         'setting' => ['modalForm'=>true,'sizeModal'=>'','targetModal' => 'transaction-modal','date' => true],
         'input' => [
              'celebration_id'=>['hidden','value'=>$store->slug,'class'=>'','classInput'=>'form-control','placeholder'=>'nameEnter','label'=>false],
              'type'=>['select','value'=>['cash'=>vl('admin','cash'), 'card'=>vl('admin','card')],'class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'typeSelectEnter'],
              'amount_str'=>['text','value'=>number_format($transactions->sum('amount') > 0 ? $transactions->sum('amount') : 0 ),'class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'amountEnter'],
              'payment_date'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control date','placeholder'=>'paymentDateEnter','autocomplete' => 'off'],
              'description'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'descriptionEnter'],
             ],
         'form' => [
             'route' => route('admin.transaction.store'),
             'method' => 'post',
             'class' => '',
         ]

        ])

        @slot('table',[
            'setting' => ['grid'=>true],
            'model' => $transactions,
            'tr_class' => true, // trClass()
            'pagination' => false,
            'field' => ['invoice_code'=>false,'type_payment'=>false,'amount_str'=>false,
            'tracking_code'=>false,'date_locale'=>false,'payment_str'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
        ])
    @endcomponent

</div>

