@extends('Admin.layout.main')

@section('title')
    {{ vl('general','profile') }}
@endsection

@section('content')
<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30"> <img src="{{ asset('images/users/default_user.png') }}" class="img-circle" width="150">
                    <h4 class="card-title m-t-10">{{ $store->name }}</h4>
                    <h6 class="card-subtitle">{{ $storeSetting->name_link }}</h6>
                    <div class="row text-center justify-content-md-center">
                        <div class="col-6"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-light">{{ $users->count() }}</font></a></div>
                        <div class="col-6"><a href="javascript:void(0)" class="link"><font class="font-14">{!! toDebtCredit($transactions->sum('amount')) !!}</font></a></div>
                    </div>
                </center>
            </div>
            <div>
                <hr> </div>
            <div class="card-body"> <small class="text-muted">{{ vl('admin','type') }}</small>
                <h6>{{ vl('admin',$storeSetting->type) }}</h6> <small class="text-muted p-t-30 db">{{ vl('admin','always') }}</small>
                <h6>{{ $storeSetting->always ? vl('admin','yes') : vl('admin','no') }}</h6>
                @if($storeSetting->address )
                    <small class="text-muted p-t-30 db">{{ vl('admin','address') }}</small>
                    <h6>{{ $storeSetting->address }}</h6>
                    <div class="map-box">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d103687.07656767916!2d51.38071067551615!3d35.696175314662945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e00491ff3dcd9%3A0xf0b3697c567024bc!2sTehran%2C+Tehran+Province!5e0!3m2!1sen!2sir!4v1507058825528" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                    </div>
                @endif
{{--                 <small class="text-muted p-t-30 db">پروفایل های اجتماعی</small>--}}
{{--                <br>--}}
{{--                <button class="btn btn-circle btn-secondary"><i class="fa fa-facebook"></i></button>--}}
{{--                <button class="btn btn-circle btn-secondary"><i class="fa fa-twitter"></i></button>--}}
{{--                <button class="btn btn-circle btn-secondary"><i class="fa fa-youtube"></i></button>--}}
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> <a class="nav-link {{ $active['users'] }}" data-toggle="tab" href="#users" role="tab">{{ vl('admin','users') }}</a> </li>
                <li class="nav-item"> <a class="nav-link {{ $active['transaction'] }}" data-toggle="tab" href="#transaction" role="tab">{{ vl('admin','transaction') }}</a> </li>
                <li class="nav-item"> <a class="nav-link {{ $active['invoices'] }}" data-toggle="tab" href="#invoices" role="tab">{{ vl('admin','invoices') }}</a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tickets" role="tab">{{ vl('admin','tickets') }}</a> </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane {{ $active['users'] }}" id="users" role="tabpanel">
                    @component('Admin.Stores.users')
                        @slot('users',$users)
                        @slot('store',$store)
                    @endcomponent
                </div>
                <!--second tab-->
                <div class="tab-pane {{ $active['transaction'] }}" id="transaction" role="tabpanel">
                    @component('Admin.Stores.transactions')
                        @slot('transactions',$transactions)
                        @slot('store',$store)
                    @endcomponent
                </div>
                <div class="tab-pane {{ $active['invoices'] }}" id="invoices" role="tabpanel">
                    @component('Admin.Stores.invoices')
                        @slot('Invoices',$invoiceStores)
                        @slot('store',$store)
                        @slot('autoCode',$autoCode)
                    @endcomponent
                </div>
                <div class="tab-pane" id="tickets" role="tabpanel">
                    @component('Admin.Stores.tickets')
                        @slot('ticketMasters',$ticketMasters)
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
@endsection
