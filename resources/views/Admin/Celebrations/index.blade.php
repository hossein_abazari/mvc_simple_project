@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','celebration_person') }}
@endsection

@section('content')

    @component('Admin.Generals.view')
        @slot('modal',[
            'setting' => ['modalForm'=>true,'sizeModal'=>'modal-xl','targetModal' => 'store-modal'],
            'input' => [
                 'name'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'nameEnter'],
                 'deadlines'=>['select','value'=>\App\Traits\DeadlineTrait::arrayDataDeadline(),'class'=>'col-md-4 m-b-20','classInput'=>'form-control deadline','placeholder'=>'deadlineSelectEnter'],
                 'amount'=>['text','value'=>'0','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'amountEnter','id'=>'amount-store'],
                 'name_link'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'nameLinkEnter'],
                 'email'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'emailEnter'],
                 'address'=>['text','value'=>'','class'=>'col-md-8 m-b-20','classInput'=>'form-control','placeholder'=>'addressEnter'],
                 'mobile'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'mobileEnter'],
                 'password'=>['password','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'passwordEnter'],
                 'password_confirmation'=>['password','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'passwordConfirmationEnter'],

                ],
            'form' => [
                'route' => route('admin.celebration.store'),
                'method' => 'post',
                'class' => '',
                'csrf' => csrf_field(),
            ],
            'update' => [
                'model'=>'Store',
                'route'=>'admin.celebration',
                'relation'=>'celebrationSetting',
                'input'=>[
                    'name'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'nameEnter'],
                    'from_date'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'fromDateEnter','relation'=>true],
                    'to_date'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'toDateEnter','relation'=>true],
                    'type'=>['select','value'=>['learning'=>vl('admin','learning'), 'files'=>vl('admin','files'), 'sellProduct'=> vl('admin','sellProduct')],
                                            'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'typeSelectEnter','relation'=>true,],

/*                    'name_link'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'nameLinkEnter','relation'=>true],
*/                    'always'=>['checkbox','class'=>'col-md-2 m-b-20','classInput'=>'form-control','relation'=>true],
                    'status'=>['checkbox','value'=>'1','class'=>'col-md-2 m-b-20','classInput'=>'form-control','relation'=>true],
                    'plugins'=>['select','value'=>\App\Traits\PluginTrait::arrayDataPlugin(),'class'=>'col-md-4 m-b-20','classInput'=>'form-control selectpicker','placeholder'=>'pluginSelectEnter','multiple'=>'multiple','relation'=>'plugins'],
                ]
            ],

        ])

        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $celebrations,
            'relation' => 'celebrationSetting',
            'field' => ['name'=>'dataName','from_date'=>'dataFromDate','to_date'=>'dataToDate',/*'name_link'=>'relation',*/'amount_debt'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['delete'=>false,'edit'=>true],
            'search' => [
                'name_search' => ['text','value'=>'','class'=>'col-md-3','classInput'=>'form-control','placeholder'=>'nameEnter','label' => false]
            ]
        ])
    @endcomponent

@endsection

@include('Admin.Celebrations.css')
@include('Admin.Celebrations.script')
