<div class="card-body">
    @component('Admin.Generals.view')
        @slot('modal',[
            'setting' => ['modalForm'=>true,'sizeModal'=>'','targetModal' => 'user-modal'],
            'input' => [
                'celebration_id'=>['hidden','value'=>$store->slug,'class'=>'','classInput'=>'form-control','placeholder'=>'nameEnter','label'=>false],
                'name'=>['text','value'=>'','class'=>'col-md-12 m-b-20','classInput'=>'form-control','placeholder'=>'nameEnter'],
                'email'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'emailEnter'],
                'mobile'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'mobileEnter'],
                'password'=>['password','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'passwordEnter'],
                'password_confirmation'=>['password','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'passwordConfirmationEnter'],
            ],
            'form' => [
                'route' => route('admin.user.store'),
                'method' => 'post',
                'class' => '',
            ],
        ])

    @endcomponent

    <div class="profiletimeline">
    @foreach($users as $user)
            <div class="sl-item">
                <div class="sl-left"> <img src="{{ asset('images/users/default_user.png') }}" alt="user" class="img-circle"> </div>
                <div class="sl-right">
                    <div><a href="#" class="link">{{ $user->name }}</a> <span class="sl-date">online</span>
                        <p>{{ vl('admin','email') }}:<a href="#"> {{$user->email}}</a></p>

                        <p>{{ vl('admin','mobile') }}:<span> {{$user->mobile}}</span></p>

                    </div>
                </div>
            </div>
            <hr>
        @endforeach
    </div>
</div>

