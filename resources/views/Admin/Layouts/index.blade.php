@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','layouts') }}
@endsection

@section('content')

    @component('Admin.Generals.view')
        @slot('modal',[
            'setting' => ['modalForm'=>true,'sizeModal'=>'modal-sm','targetModal' => 'pack-card-modal'],
            'input' => [
            ],
            'form' => [
                'route' => route('admin.layout.store'),
                'method' => 'post',
                'class' => '',
            ],
            'update' => [
                'model'=>'Layout',
                'route'=>'admin.layout',
                'input'=>[
                    'name'=>['text','value'=>'','class'=>'col-md-12 m-b-20','classInput'=>'form-control','placeholder'=>'nameEnter'],
                    ]
            ],

        ])

        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $layouts,
            'field' => ['name'=>false,'directory_id'=>false,'number_invitation_card'=>false,'priceStr'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['delete'=>'admin.layout','edit'=>true]
        ])
    @endcomponent

@endsection

@include('Admin.Layouts.css')
@include('Admin.Layouts.script')
