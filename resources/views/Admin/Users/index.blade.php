@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','users') }}
@endsection

@section('content')

    @component('Admin.Generals.view')
        @slot('modal',[
            'setting' => ['modalForm'=>true,'sizeModal'=>'','targetModal' => 'deadline-modal'],
            'input' => [
                'celebration_id'=>['select','value'=>\App\Traits\CelebrationTrait::arrayDataCelebration(),'class'=>'col-md-6 m-b-20',
                            'classInput'=>'form-control selectpicker','placeholder'=>'storeIdSelectEnter','data-live-search'=>true,'selectManual' => 'selectStore'],
                'name'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'nameEnter'],
                'email'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'emailEnter'],
                'mobile'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'mobileEnter'],
                'password'=>['password','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'passwordEnter'],
                'password_confirmation'=>['password','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'passwordConfirmationEnter'],
            ],
            'form' => [
                'route' => route('admin.user.store'),
                'method' => 'post',
                'class' => '',
            ],
            'update' => [
                'model'=>'User',
                'route'=>'admin.user',

            ],

        ])

        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $users,
            'field' => ['celebration_id'=>'dataStoreId','name'=>false,'email'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['delete'=>'admin.user','edit'=>true]
        ])
    @endcomponent

@endsection

@include('Admin.Deadlines.css')
@include('Admin.Deadlines.script')
