@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','pack_cards') }}
@endsection

@section('content')

    @component('Admin.Generals.view')
        @slot('modal',[
            'setting' => ['modalForm'=>true,'sizeModal'=>'modal-lg','targetModal' => 'pack-card-modal'],
            'input' => [
                'name'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'nameEnter'],
                'amount_str'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'amountEnter'],
                'number_card'=>['text','value'=>1,'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'userEnter'],
            ],
            'form' => [
                'route' => route('admin.pack.card.store'),
                'method' => 'post',
                'class' => '',
            ],
            'update' => [
                'model'=>'PackCard',
                'route'=>'admin.pack.card'
            ],

        ])

        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $packCards,
            'field' => ['name'=>false,'amount'=>'dataAmount','number_card'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['delete'=>'admin.pack.card','edit'=>true]
        ])
    @endcomponent

@endsection

@include('Admin.PackCards.css')
@include('Admin.PackCards.script')
