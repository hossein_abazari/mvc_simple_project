@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','transaction') }}
@endsection


@section('content')

    @component('Admin.Generals.view')

        @slot('modal',[
         'setting' => ['modalForm'=>true,'sizeModal'=>'','targetModal' => 'transaction-modal','date' => true],
         'input' => [
              'celebration_id'=>['select','value'=>\App\Traits\CelebrationTrait::arrayDataCelebration(),'class'=>'col-md-6 m-b-20',
                            'classInput'=>'form-control selectpicker store-id','placeholder'=>'storeIdSelectEnter','data-live-search'=>true,'selectManual' => 'selectStore'],
              'type'=>['select','value'=>['cash'=>vl('admin','cash'), 'card'=>vl('admin','card')],'class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'typeSelectEnter'],
              'amount_str'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'amountEnter'],
              'payment_date'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control date','placeholder'=>'paymentDateEnter','autocomplete' => 'off'],
               'description'=>['text','value'=>'','class'=>'col-md-12 m-b-20','classInput'=>'form-control','placeholder'=>'descriptionEnter'],


             ],
         'form' => [
             'route' => route('admin.transaction.store'),
             'method' => 'post',
             'class' => '',
         ],
         'update' => [
             'model'=>'Store',
             'route'=>'admin.transaction',
         ],

     ])

        @slot('table',[
            'setting' => ['grid'=>true],
            'model' => $transactions,
            'tr_class' => true, // trClass()
            'field' => ['store_name'=>false,'invoice_code'=>false,'type_payment'=>false,'amount_str'=>false,
            'tracking_code'=>false,'date_locale'=>false,'payment_str'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            'search' => [
                'invoice_code_search' => ['text','value'=>'','class'=>'col-md-2','classInput'=>'form-control','placeholder'=>'invoiceCodeEnter','label' => false],
                'from_amount_search' => ['text','value'=>'0','class'=>'col-md-2','classInput'=>'form-control','placeholder'=>'fromAmountEnter','label' => false,'onkeyup'=>"javascript:this.value=itpro(this.value)"],
                'to_amount_search' => ['text','value'=>'0','class'=>'col-md-2','classInput'=>'form-control','placeholder'=>'toAmountEnter','label' => false,'onkeyup'=>"javascript:this.value=itpro(this.value)"],
                'from_date_search' => ['text','value'=>'0','class'=>'col-md-2','classInput'=>'form-control from-date','placeholder'=>'fromDateEnter','label' => false,'autocomplete' => "off"],
                'to_date_search' => ['text','value'=>'0','class'=>'col-md-2','classInput'=>'form-control to-date','placeholder'=>'toDateEnter','label' => false,'autocomplete' => "off"],
            ]
        ])
    @endcomponent

@endsection

@include('Admin.Transaction.css')
@include('Admin.Transaction.script')
