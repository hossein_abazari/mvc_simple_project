<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/admin/bootstrap.min.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ asset('js/admin/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset('js/admin/waves.js') }}"></script>
<!--Menu sidebar -->
<script src="{{ asset('js/admin/sidebarmenu.js') }}"></script>
<!--stickey kit -->
<script src="{{ asset('js/admin/sticky-kit.min.js') }}"></script>
<!--Custom JavaScript -->
<script src="{{ asset('js/admin/custom.min.js') }}"></script>

<script src="{{ asset('js/bootstrap3-typeahead.min.js') }}"></script>


<script src="{{ asset('js/admin/jQuery.style.switcher.js') }}"></script>

<script>

    function itpro(Number)
    {
        Number+= '';
        Number= Number.replace(',', ''); Number= Number.replace(',', ''); Number= Number.replace(',', '');
        Number= Number.replace(',', ''); Number= Number.replace(',', ''); Number= Number.replace(',', '');
        x = Number.split('.');
        y = x[0];
        z= x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(y))
            y= y.replace(rgx, '$1' + ',' + '$2');
        return y+ z;
    }


    function settingDate (nameDate ){
        var data = {
            targetTextSelector: nameDate,
            fromDate: true,
            enableTimePicker: false,
            groupId: 'rangeSelector1',
            englishNumber: true,
            dateFormat: 'yyyy/MM/dd',
            textFormat: 'yyyy/MM/dd',
        };

        var dataLanguage = {isGregorian: false,};

        @if(App::getLocale() == 'en')

            dataLanguage = {
            isGregorian: true,
        };

            @endif

        var result = Object.assign(data, dataLanguage);


        return result;
    }

    var path = "{{ route('general.autocomplete') }}";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
            if (query.length > 2) {
                return $.get(path, {query: query}, function (data) {
                    return process(data);
                });
            }
        }
    });

</script>

@yield('scriptChart')
@yield('scriptView')
@yield('scriptSelect')
@yield('scriptButton')
@yield('script')
@yield('scriptDate')

