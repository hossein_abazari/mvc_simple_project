<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/images/favicon.png') }}">
    <title>@yield('title')</title>
    @include('Admin.layout.css')
</head>

<body class="fix-header fix-sidebar card-no-border">

{{--<div class="preloader">--}}
{{--    <svg class="circular" viewBox="25 25 50 50">--}}
{{--        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>--}}
{{--</div>--}}

<div id="main-wrapper">

    @include('Admin.layout.header')

    @include('Admin.layout.sidebar')

    <div class="page-wrapper">

        <div class="container-fluid">

           @include('Admin.layout.headTitle')

            <div class="row">
                <div class="col-12">
                    @include('partial.flash')
                </div>
            </div>

            @yield('content')

            @include('Admin.layout.right-sidebar')

        </div>

        <footer class="footer">
            {{ vl('admin','footer') }}
        </footer>

    </div>

</div>

@include('Admin.layout.script')
</body>

</html>
