<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> <img src="{{ asset('images/users/default_user.png') }}" alt="user" /> </div>
            <!-- User profile text-->
            <div class="profile-text"> <a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">{{ Auth::getUser()->name }} <span class="caret"></span></a>
                <div class="dropdown-menu animated flipInY">
                    <a href="#" class="dropdown-item"><i class="ti-user"></i> {{ vl('admin','myProfile') }}</a>
                    <a href="#" class="dropdown-item"><i class="ti-wallet"></i> {{ vl('admin','myIncome') }}</a>
                    <a href="#" class="dropdown-item"><i class="ti-email"></i> {{ vl('admin','inbox') }}</a>
                    <div class="dropdown-divider"></div> <a href="{{ route('admin.setting.index') }}" class="dropdown-item"><i class="ti-settings"></i> {{ vl('admin','accountSettings') }}</a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('admin.logout') }}"  onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"  class="dropdown-item"><i class="fa fa-power-off"></i> {{ vl('admin','exit') }}
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </a>
                </div>
            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">

                @php
                    $arrayLabel = ['tickets'=>true]
                @endphp

                @foreach(\App\Classes\Menu::getAdmin() as $key => $value)
                <li>
                    <a class="{{ is_array($value) ? 'has-arrow' : '' }}" href="{{ !is_array($value) ? $value : '#' }}" aria-expanded="false"><i class="{{ \App\Classes\Menu::getAdminIcon()[$key] }}"></i><span class="hide-menu">{{ vl('admin',$key) ? vl('admin',$key) : $key }}
                        {!!  !empty($arrayLabel[$key]) ? \App\Classes\BladeCode::countSidebar($key) : '' !!}
                        </span></a>
                   @if(is_array($value))
                        <ul aria-expanded="false" class="collapse">
                            @foreach($value as $k=>$v)
                                 <li><a href="{{ $v }}">{{ vl('admin',$k) ? vl('admin',$k) : $k }}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </li>
                @endforeach
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer">
        <!-- item-->
        <a href="{{ route('admin.setting.index') }}" class="link" data-toggle="tooltip" title="{{ vl('general','settings') }}"><i class="ti-settings"></i></a>
        <!-- item-->
        <a href="" class="link" data-toggle="tooltip" title="ایمیل"><i class="mdi mdi-gmail"></i></a>
        <!-- item-->
        <a href="{{ route('admin.logout') }}"  onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"  class="link" data-toggle="tooltip" title="خروج از سامانه"><i class="mdi mdi-power"></i>
            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </a>
    </div>
    <!-- End Bottom points-->
</aside>
