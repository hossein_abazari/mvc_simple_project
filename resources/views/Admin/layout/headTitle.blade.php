<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">@yield('title')</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">{{ vl('admin','home') }}</a></li>
            <li class="breadcrumb-item active">@yield('title')</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm side-theme m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
