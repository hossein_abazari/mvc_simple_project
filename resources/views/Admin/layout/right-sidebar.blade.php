<div class="right-sidebar">
    <div class="slimscrollright">
        <div class="rpanel-title"> {{ vl('admin','settingsPanel') }} <span><i class="ti-close right-side-toggle"></i></span> </div>
        <div class="r-panel-body">

            <ul id="themecolors" class="m-t-20">
{{--                <li><b>همراه با نوار کناری روشن</b></li>--}}
                <li><b></b></li> {{-- insted top --}}
                <li><a href="javascript:void(0)" data-theme="default" class="default-theme {{ \App\Classes\BladeCode::getTheme('default') }}">1</a></li>
                <li><a href="javascript:void(0)" data-theme="green" class="green-theme {{ \App\Classes\BladeCode::getTheme('green') }}">2</a></li>
                <li><a href="javascript:void(0)" data-theme="red" class="red-theme {{ \App\Classes\BladeCode::getTheme('red') }}">3</a></li>
                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme {{ \App\Classes\BladeCode::getTheme('blue') }}">4</a></li>
                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme {{ \App\Classes\BladeCode::getTheme('purple') }}">5</a></li>
                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme {{ \App\Classes\BladeCode::getTheme('megna') }}">6</a></li>
{{--                <li class="d-block m-t-30"><b>همراه با نوار کناری تیره</b></li>--}}
                <li><b></b></li> {{-- insted top --}}
                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme {{ \App\Classes\BladeCode::getTheme('default-dark') }}">7</a></li>
                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme {{ \App\Classes\BladeCode::getTheme('green-dark') }}">8</a></li>
                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme {{ \App\Classes\BladeCode::getTheme('red-dark') }}">9</a></li>
                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme {{ \App\Classes\BladeCode::getTheme('blue-dark') }}">10</a></li>
                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme {{ \App\Classes\BladeCode::getTheme('purple-dark') }}">11</a></li>
                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme {{ \App\Classes\BladeCode::getTheme('megna-dark') }}">12</a></li>
            </ul>
        </div>
    </div>
</div>
