<!-- Bootstrap Core CSS -->
<link href="{{ asset('css/admin/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/custom-bootstrap-rtl.css') }}" rel="stylesheet">
<!-- chartist CSS -->


{{-- icons --}}
<link href="{{ asset('css/admin/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/simple-line-icons.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/weather-icons.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/linea.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/themify-icons.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/flag-icon.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/materialdesignicons.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/spinners.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/animate.css') }}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{ asset('css/admin/style.css') }}" rel="stylesheet">

@if(App::getLocale() == 'en')
<link href="{{ asset('css/admin/style-ltr.css') }}" rel="stylesheet">
@endif

<link href="{{ asset('css/admin/jquery.md.bootstrap.datetimepicker.style.css') }}" rel="stylesheet">
@yield('css')
@yield('cssView')
@yield('cssSelect')
@yield('cssChart')

<!-- You can change the theme colors from here -->
@php
    $colors = Cookie::get('colors');
    $color = $colors ? $colors : 'blue';
@endphp
<link href="{{ asset('css/admin/colors/'.$color.'.css') }}" id="theme" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
