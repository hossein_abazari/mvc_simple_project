<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header left-nav">
            <a class="navbar-brand" href="index.html">
                <!-- Logo icon -->
                <b>
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <img src="{{ asset('images/logo-icon.png') }}" alt="homepage" class="dark-logo" />
                    <!-- Light Logo icon -->
                    <img src="{{ asset('images/logo-light-icon.png') }}" alt="homepage" class="light-logo" />
                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span>
                         <!-- dark Logo text -->
                         <img src="{{ asset('images/logo-text.png') }}" alt="homepage" class="dark-logo" />
                    <!-- Light Logo text -->
                         <img src="{{ asset('images/logo-light-text.png') }}" class="light-logo" alt="homepage" /></span> </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav side-auto mt-md-0 ">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                <!-- ============================================================== -->
                <!-- Comment -->
                <!-- ============================================================== -->
{{--                <li class="nav-item dropdown">--}}
{{--                    <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="index.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>--}}
{{--                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>--}}
{{--                    </a>--}}
{{--                    <div class="dropdown-menu mailbox animated bounceInDown">--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <div class="drop-title">شما 4 پیام جدید دارید</div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <div class="message-center">--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>نیما لعل زاد</h5> <span class="mail-desc">لورم ایپسوم متن ساختگی با تولید !</span> <span class="time">9:30 صبح</span> </div>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>نیما لعل زاد</h5> <span class="mail-desc">لورم ایپسوم متن ساختگی با تولید </span> <span class="time">9:10 صبح</span> </div>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>نیما لعل زاد</h5> <span class="mail-desc">لورم ایپسوم متن ساختگی با تولید </span> <span class="time">9:08 صبح</span> </div>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>نیما لعل زاد</h5> <span class="mail-desc">لورم ایپسوم متن ساختگی با تولید !</span> <span class="time">9:02 صبح</span> </div>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a class="nav-link text-center" href="javascript:void(0);"> <strong>دیدن همه ایمیل ها </strong> <i class="fa fa-angle-left"></i> </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
                <!-- ============================================================== -->
                <!-- End Comment -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Messages -->
                <!-- ============================================================== -->
{{--                <li class="nav-item dropdown">--}}
{{--                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="index.html" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-email"></i>--}}
{{--                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>--}}
{{--                    </a>--}}
{{--                    <div class="dropdown-menu mailbox animated bounceInDown" aria-labelledby="2">--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <div class="drop-title">اعلان ها</div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <div class="message-center">--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="user-img"> <img src="{{ asset('images/users/default_user.png') }}" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>نیما لعل زاد</h5> <span class="mail-desc">لورم ایپسوم متن ساختگی با تولید!</span> <span class="time">9:30 صبح</span> </div>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="user-img"> <img src="{{ asset('images/users/2.jpg') }}" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>نیما لعل زاد</h5> <span class="mail-desc">لورم ایپسوم متن ساختگی با تولید</span> <span class="time">9:10 صبح</span> </div>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="user-img"> <img src="{{ asset('images/users/3.jpg') }}" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>نیما لعل زاد</h5> <span class="mail-desc">لورم ایپسوم متن ساختگی با تولید!</span> <span class="time">9:08 صبح</span> </div>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="user-img"> <img src="{{ asset('images/users/4.jpg') }}" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>نیما لعل زاد</h5> <span class="mail-desc">لورم ایپسوم متن ساختگی با تولید!</span> <span class="time">9:02 صبح</span> </div>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a class="nav-link text-center" href="javascript:void(0);"> <strong>دیدن همه اعلان ها</strong> <i class="fa fa-angle-left"></i> </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}

{{--                <li class="nav-item dropdown mega-dropdown"> <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="index.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-view-grid"></i></a>--}}
{{--                    <div class="dropdown-menu animated bounceInDown">--}}
{{--                        <ul class="mega-dropdown-menu row">--}}
{{--                            <li class="col-lg-3 col-xlg-2 m-b-30">--}}
{{--                                <h4 class="m-b-20">اسلایدر</h4>--}}
{{--                                <!-- CAROUSEL -->--}}
{{--                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">--}}
{{--                                    <div class="carousel-inner" role="listbox">--}}
{{--                                        <div class="carousel-item active">--}}
{{--                                            <div class="container"> <img class="d-block img-fluid" src="../assets/images/big/img1.jpg" alt="First slide"></div>--}}
{{--                                        </div>--}}
{{--                                        <div class="carousel-item">--}}
{{--                                            <div class="container"><img class="d-block img-fluid" src="../assets/images/big/img2.jpg" alt="Second slide"></div>--}}
{{--                                        </div>--}}
{{--                                        <div class="carousel-item">--}}
{{--                                            <div class="container"><img class="d-block img-fluid" src="../assets/images/big/img3.jpg" alt="Third slide"></div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">قبلی</span> </a>--}}
{{--                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">بعدی</span> </a>--}}
{{--                                </div>--}}
{{--                                <!-- End CAROUSEL -->--}}
{{--                            </li>--}}
{{--                            <li class="col-lg-3 m-b-30">--}}
{{--                                <h4 class="m-b-20">سوالات متداول</h4>--}}
{{--                                <!-- Accordian -->--}}
{{--                                <div id="accordion" class="nav-accordion" role="tablist" aria-multiselectable="true">--}}
{{--                                    <div class="card">--}}
{{--                                        <div class="card-header" role="tab" id="headingOne">--}}
{{--                                            <h5 class="mb-0">--}}
{{--                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">--}}
{{--                                                    سوالات متداول قسمت #1--}}
{{--                                                </a>--}}
{{--                                            </h5> </div>--}}
{{--                                        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">--}}
{{--                                            <div class="card-body"> لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک. </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="card">--}}
{{--                                        <div class="card-header" role="tab" id="headingTwo">--}}
{{--                                            <h5 class="mb-0">--}}
{{--                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">--}}
{{--                                                    سوالات متداول قسمت #2--}}
{{--                                                </a>--}}
{{--                                            </h5> </div>--}}
{{--                                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">--}}
{{--                                            <div class="card-body"> لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک. </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="card">--}}
{{--                                        <div class="card-header" role="tab" id="headingThree">--}}
{{--                                            <h5 class="mb-0">--}}
{{--                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">--}}
{{--                                                    سوالات متداول قسمت #3--}}
{{--                                                </a>--}}
{{--                                            </h5> </div>--}}
{{--                                        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">--}}
{{--                                            <div class="card-body"> لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک. </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="col-lg-3  m-b-30">--}}
{{--                                <h4 class="m-b-20">تماس با ما</h4>--}}
{{--                                <!-- Contact -->--}}
{{--                                <form>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <input type="text" class="form-control" id="exampleInputname1" placeholder="نام را وارد کنید"> </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <input type="email" class="form-control" placeholder="ایمیل را وارد کنید"> </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <textarea class="form-control" id="exampleTextarea" rows="3" placeholder="پیغام"></textarea>--}}
{{--                                    </div>--}}
{{--                                    <button type="submit" class="btn btn-info">ثبت</button>--}}
{{--                                </form>--}}
{{--                            </li>--}}
{{--                            <li class="col-lg-3 col-xlg-4 m-b-30">--}}
{{--                                <h4 class="m-b-20">لیست استایل دار</h4>--}}
{{--                                <!-- List style -->--}}
{{--                                <ul class="list-style-none">--}}
{{--                                    <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> لورم ایپسوم متن ساختگی با تولید سادگی</a></li>--}}
{{--                                    <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> لورم ایپسوم متن ساختگی با تولید سادگی</a></li>--}}
{{--                                    <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> لورم ایپسوم متن ساختگی با تولید سادگی</a></li>--}}
{{--                                    <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> لورم ایپسوم متن ساختگی با تولید سادگی</a></li>--}}
{{--                                    <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> لورم ایپسوم متن ساختگی با تولید سادگی</a></li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
                <li class="nav-item hidden-sm-down">
                    {{ \App\Classes\FormField::openForm(route('admin.general.store.search'),'POST','app-search','search-store-form') }}

                        {{ \App\Classes\FormField::field('search_store','text','','','form-control typeahead','storeSearch',["autocomplete"=>"off"],false) }}

                        <a class="srh-btn" onclick="event.preventDefault();
                    document.getElementById('search-store-form').submit();"><i class="ti-search"></i></a>

                    {{ \App\Classes\FormField::endForm() }}
                </li>
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav right-nav my-lg-0">
               {{-- search before --}}
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('images/users/default_user.png') }}" alt="user" class="profile-pic" /></a>
                    <div class="dropdown-menu menu-side-language animated flipInY">

                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="{{ asset('images/users/default_user.png') }}" alt="user"></div>
                                    <div class="u-text">
                                        <h4>{{ Auth::getUser()->name }}</h4>
                                        <p class="text-muted">{{ Auth::getUser()->email }}</p><a href="{{ route('admin.setting.index') }}" class="btn btn-rounded btn-danger btn-sm">{{ vl('admin','viewProfile') }}</a></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-user"></i> {{ vl('admin','myProfile') }}</a></li>
                            <li><a href="#"><i class="ti-wallet"></i> {{ vl('admin','myIncome') }}</a></li>
                            <li><a href="#"><i class="ti-email"></i> {{ vl('admin','inbox') }}</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('admin.setting.index') }}"><i class="ti-settings"></i> {{ vl('admin','accountSettings') }}</a></li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="{{ route('admin.logout') }}"  onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" ><i class="fa fa-power-off"></i> {{ vl('admin','exit') }}
                                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flag-icon flag-icon-{{ \App\Classes\BladeCode::getSelectLanguage() }}"></i></a>
                    @if(\App\Classes\BladeCode::countLanguage() > 1)
                        <div class="dropdown-menu menu-side-language animated bounceInDown">
                            {!! \App\Classes\BladeCode::getLanguageSetting() !!}
                        </div>
                    @endif
                </li>
            </ul>
        </div>
    </nav>
</header>
