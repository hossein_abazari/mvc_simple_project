@extends('Admin.Auth.main')

@section('title')
    {{vl('general','enterToSystem')}}
@endsection

@section('content')
     <form class="form-horizontal form-material" method="post" action="{{route("admin.login")}}">
       @csrf
        <a href="javascript:void(0)" class="text-center db"><img src="{{ asset('images/logo-icon.png') }}" alt="Home" /><br/><img src="{{ asset('images/logo-text.png') }}" alt="Home" /></a>

        <div class="form-group m-t-40">
            <div class="col-xs-12">
                <input class="form-control" type="text" name="email" required="" placeholder="{{ vl('admin','email') }}">
                @if ($errors->has('email'))
                    <span class="text-danger" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <input class="form-control" type="password" name="password" required="" placeholder="{{ vl('admin','password') }}">
                @if ($errors->has('password'))
                    <span class="text-danger" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
            </div>
        </div>
        <div class="form-group">
{{--            <div class="col-md-12">--}}
{{--                <div class="checkbox checkbox-primary pull-left p-t-0">--}}
{{--                    <input id="checkbox-signup" type="checkbox">--}}
{{--                    <label for="checkbox-signup"> من را به خاطر بسپار </label>--}}
{{--                </div>--}}
{{--                <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> فراموشی رمز؟</a> </div>--}}
        </div>
        <div class="form-group text-center m-t-20">
            <div class="col-xs-12">
                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{ vl('admin','enter') }}</button>
            </div>
        </div>
{{--        <div class="row">--}}
{{--            <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">--}}
{{--                <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="ورود با فیس بوک"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="ورود با گوگل"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="form-group m-b-0">--}}
{{--            <div class="col-sm-12 text-center">--}}
{{--                <p>حساب کاربری ندارید؟ <a href="https://nimadeveloper.ir" class="text-primary m-l-5"><b>ثبت نام</b></a></p>--}}
{{--            </div>--}}
{{--        </div>--}}
    </form>
    <form class="form-horizontal" id="recoverform" action="index.html">
        <div class="form-group ">
            <div class="col-xs-12">
                <h3>بازیابی رمز</h3>
                <p class="text-muted">ایمیل خود را وارد کنید و دستورالعمل ها به شما ارسال می شود! </p>
            </div>
        </div>
        <div class="form-group ">
            <div class="col-xs-12">
                <input class="form-control" type="text" required="" placeholder="ایمیل">
            </div>
        </div>
        <div class="form-group text-center m-t-20">
            <div class="col-xs-12">
                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">بازیابی</button>
            </div>
        </div>
    </form>
@endsection


