@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','invoices') }}
@endsection


@section('content')

    @component('Admin.Generals.view')
        @slot('modal',[
            'setting' => ['modalForm'=>true,'sizeModal'=>'','targetModal' => 'deadline-modal','date'=>true],
            'input' => [
                'celebration_id'=>['select','value'=>\App\Traits\CelebrationTrait::arrayDataCelebration(),'class'=>'col-md-6 m-b-20',
                            'classInput'=>'form-control selectpicker','placeholder'=>'storeIdSelectEnter','data-live-search'=>true,'selectManual' => 'selectStore'],
                'invoice_code'=>['text','value'=>$autoCode,'class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'invoiceCodeEnter'],
                'invoice_date'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control date','placeholder'=>'invoiceDateEnter','autocomplete' => "off",'manualValue' => 'dataInvoiceDate'],
                'amount_str'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'amountEnter'],
                 'description'=>['text','value'=>'','class'=>'col-md-12 m-b-20','classInput'=>'form-control','placeholder'=>'descriptionEnter'],

            ],
            'form' => [
                'route' => route('admin.invoice.store'),
                'method' => 'post',
                'class' => '',
                'csrf' => csrf_field(),
            ],
            'update' => [
                'model'=>'Invoice',
                'route'=>'admin.invoice',
            ],

        ])

        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $invoices,
            'field' => ['store_name'=>'dataStoreId','invoice_code'=>false,'description'=>false,'amount_str'=>false,'type_invoice'=>false,'invoice_date'=>'dataInvoiceDate'], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['delete'=>false,'edit'=>true],
             'search' => [
                'invoice_code_search' => ['text','value'=>'','class'=>'col-md-2','classInput'=>'form-control','placeholder'=>'invoiceCodeEnter','label' => false],
                'from_amount_search' => ['text','value'=>'0','class'=>'col-md-2','classInput'=>'form-control','placeholder'=>'fromAmountEnter','label' => false,'onkeyup'=>"javascript:this.value=itpro(this.value)"],
                'to_amount_search' => ['text','value'=>'0','class'=>'col-md-2','classInput'=>'form-control','placeholder'=>'toAmountEnter','label' => false,'onkeyup'=>"javascript:this.value=itpro(this.value)"],
                'from_date_search' => ['text','value'=>'0','class'=>'col-md-2','classInput'=>'form-control from-date','placeholder'=>'fromDateEnter','label' => false,'autocomplete' => "off"],
                'to_date_search' => ['text','value'=>'0','class'=>'col-md-2','classInput'=>'form-control to-date','placeholder'=>'toDateEnter','label' => false,'autocomplete' => "off"],
            ]
        ])
    @endcomponent

@endsection

@include('Admin.Invoices.css')
@include('Admin.Invoices.script')
