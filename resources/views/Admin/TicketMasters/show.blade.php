@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','tickets') }}
@endsection

@section('content')

@component('Generals.message')
    @slot('messages',[
        'modelDetails' => $ticketDetails,
        'modelMaster' => $ticketMaster,
        'routeFormSubmit' => 'admin.ticket.master.show.store',
    ])
@endcomponent

@endsection


