@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','tickets') }}
@endsection

@section('content')

    @component('Admin.Generals.view')
        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $ticketMasters,
            'field' => ['store_name'=>'dataNameLabel','user_name'=>false,'title'=>false,'closed'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['show'=>'admin.ticket.master'],
            'search' => [
                'store_name_search' => ['text','value'=>'','class'=>'col-md-3','classInput'=>'form-control','placeholder'=>'storeNameEnter','label' => false],
            ]
        ])
    @endcomponent

@endsection

@include('Admin.TicketMasters.css')
@include('Admin.TicketMasters.script')
