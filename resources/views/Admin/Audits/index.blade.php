@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','logs') }}
@endsection


@section('content')

    @component('Admin.Generals.view')

        @slot('table',[
            'setting' => ['grid'=>true],
            'model' => $audits,
            'field' => ['user_id'=>false,'event'=>false,'ip_address'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            'search' => [
                'user_id_search' => ['text','value'=>'','class'=>'col-md-2','classInput'=>'form-control','placeholder'=>'UserIdEnter','label' => false],
                'event_search' => ['text','value'=>'0','class'=>'col-md-2','classInput'=>'form-control','placeholder'=>'eventEnter','label' => false],
                'event_search'=>['select','value'=>\App\Traits\AuditTrait::getSelectEvent(),'class'=>'col-md-2','classInput'=>'form-control','placeholder'=>'eventSelectEnter','label' => false],
                'from_date_search' => ['text','value'=>'0','class'=>'col-md-3','classInput'=>'form-control from-date','placeholder'=>'fromDateEnter','label' => false,'autocomplete' => "off"],
                'to_date_search' => ['text','value'=>'0','class'=>'col-md-3','classInput'=>'form-control to-date','placeholder'=>'toDateEnter','label' => false,'autocomplete' => "off"],
            ],
             'operation' => ['view'=>'admin.audit.view','view']

        ])
    @endcomponent

@endsection

@include('Admin.Audits.css')
@include('Admin.Audits.script')
