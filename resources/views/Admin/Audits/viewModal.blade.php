@php
    /* @var $user \App\Models\User */
    /* @var $audit \OwenIt\Auditing\Models\Audit */
@endphp
<div id="view-data"

     class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    {{ vl('admin','details')}}
                </h4>
            </div>
            <div class="container-fluid mb-2">
                <h4>{{ vl('admin','user') }}</h4>
                @if($user)
                    <div class="border header-data mb-4">
                        <div class="row mb-4 ">
                            <div class="col-4">
                                <label class="label-form text-white bg-secondary">{{ vl('admin','name')}} </label>{{ $user->name }}
                            </div>
                            <div class="col-4">
                                <label class="label-form text-white bg-secondary">{{ vl('admin','store_name')}} </label>{{ $user->celebration_id ? $user->celebration->name : '--' }}
                            </div>

                            <div class="col-4">
                                <label class="label-form text-white bg-secondary">{{ vl('admin','mobile')}} </label>{{ $user->mobile ? $user->mobile : '--' }}
                            </div>

                        </div>

                        <div class="row mt-3">
                            <div class="col-4 mt-2">
                                <label class="label-form text-white bg-secondary">{{ vl('admin','type')}} </label>{{ $user->type }}
                            </div>

                            <div class="col-8 mt-2">
                                <label class="label-form text-white bg-secondary">{{ vl('admin','email')}} </label>{{ $user->email }}
                            </div>
                        </div>

                    </div>
                @endif

                <div class="table-responsive mt-2">
                    <h4>{{ vl('admin','audit') }}</h4>
                    <table class="table full-color-table full-inverse-table hover-table color-bordered-table">
                        <tr>
                            <th><b>{{ vl('admin','models') }}:</b></th>
                            <td>{{ $audit->auditable_type  }}</td>
                        </tr>

                        <tr>
                            <th><b>{{ vl('admin','pages') }}:</b></th>
                            <td> {{ $audit->url  }}</td>
                        </tr>

                        <tr>
                            <th><b>{{ vl('admin','table_id') }}:</b></th>
                            <td>{{ $audit->auditable_id  }}</td>
                        </tr>

                        <tr>
                            <th><b>{{ vl('admin','event') }}:</b></th>
                            <td>{{ $audit->event  }}</td>
                        </tr>


                        @foreach($userAgent as $key => $value)
                            <tr>
                                <th><b>{{ vl('admin',$key) }}:</b></th>
                                <td>{{ $value }}</td>
                            </tr>
                        @endforeach

                        <tr>
                            <th><b>{{ vl('admin','created_at') }}:</b></th>
                            <td>{{ $created_at }}</td>
                        </tr>

                    </table>
                </div>

                <h4><b>{{ vl('admin','values') }}</b></h4>
                <div class="row">
                    <div class="col-6">
                        <div class="table-responsive mt-2">
                            <table class="table full-color-table full-inverse-table hover-table">
                                <thead>
                                <th colspan="2" class="text-center" ><b>{{ vl('admin','before_data') }}</b></th>
                                </thead>
                                @if(!empty($audit->old_values))
                                    @foreach($audit->old_values as $key => $old_values)
                                        <tr>
                                            <th><b>{{ vl('admin',$key) }}:</b></th>
                                            <td>{{ $key == 'type' ? vl('admin',$old_values) :$old_values }}</td>

                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="2">{{ vl('general','nothing') }}</td>
                                    </tr>
                                @endif

                            </table>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="table-responsive mt-2">
                            <table class="table full-color-table full-inverse-table hover-table">
                                <thead>
                                <th colspan="2" class="text-center" ><b>{{ vl('admin','new_data') }}</b></th>
                                </thead>
                                @if(!empty($audit->new_values))
                                    @foreach($audit->new_values as $key => $new_values)
                                        <tr>
                                            <th><b>{{ vl('admin',$key) }}:</b></th>
                                            <td>{{ $key == 'type' ? vl('admin',$new_values) :$new_values }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="2">{{ vl('general','nothing') }}</td>
                                    </tr>
                                @endif

                            </table>
                        </div>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{ vl('admin','cancel') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

