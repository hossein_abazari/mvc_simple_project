@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','users_login') }}
@endsection


@section('content')

    @component('Admin.Generals.view')

        @slot('table',[
            'setting' => ['grid'=>true],
            'model' => $userLogins,
            'field' => ['user_id'=>false,'login_date'=>false,'login_time'=>false,'ip_address'=>false,'country'=>false,'city'=>false,'os'=>false,'browser'=>false,'time_of_online'=>'dataTimeOfOnline'], /* if value is false print field name , if exists value send to name function in model into use trait */
            'search' => [
                'user_id_search' => ['text','value'=>'','class'=>'col-md-2','classInput'=>'form-control','placeholder'=>'UserIdEnter','label' => false],
                'from_date_search' => ['text','value'=>'0','class'=>'col-md-3','classInput'=>'form-control from-date','placeholder'=>'fromDateEnter','label' => false,'autocomplete' => "off"],
                'to_date_search' => ['text','value'=>'0','class'=>'col-md-3','classInput'=>'form-control to-date','placeholder'=>'toDateEnter','label' => false,'autocomplete' => "off"],
            ],

        ])
    @endcomponent

@endsection

@include('Admin.UserLogins.css')
@include('Admin.UserLogins.script')
