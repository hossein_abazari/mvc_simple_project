@section('script')
    <script src="{{ asset('js/admin/footable.all.min.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.md.bootstrap.datetimepicker.js') }}"></script>

    <script>

        $('.from-date').MdPersianDateTimePicker(settingDate('.from-date'));

        $('.to-date').MdPersianDateTimePicker(settingDate('.to-date'));

    </script>
@endsection

