@php
    $buttonName = isset($mode) && $mode=='update' ? 'edit' : 'submit';

    $inputs = isset($mode) && $mode=='update' ? (!empty($modal['update']['input']) ? $modal['update']['input'] : $modal['input'] ) : $modal['input'];
@endphp

<div id="{{ isset($nameModal)?$nameModal:(isset($modal['setting']['targetModal']) ?

     $modal['setting']['targetModal'] : 'viewModal')  }}"

     class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog {{isset($modal['setting']['sizeModal']) ? $modal['setting']['sizeModal'] : ''}}">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">

                  {{ isset($mode) && $mode=='update' ? vl('admin','edit') :

                 (isset($headerModal) ? $headerModal : vl('admin','add')) }}
                </h4>
            </div>

            <form class="form-horizontal {{ isset($modal['form']['class']) ? $modal['form']['class'] : '' }}"

                  method="{{ isset($mode) && $mode=='update' ? 'POST' : $modal['form']['method'] }}"

                  action="{{isset($mode) && $mode=='update' ? route($modal['update']['route'].'.update',[$data->slug]) : ($modal['form']['route'])}}" >

                @if(isset($mode) && $mode=='update') {{-- madal is update and set methode patch --}}
                     @method('PATCH')
                @endif
                @csrf

                <div class="modal-body">
                    <div class="form-group">

                        @component('Admin.Generals.inputs')
                            @slot('inputs',$inputs)
                            @if(isset($modal['update']['relation']))
                                 @slot('relationUpdate',$modal['update']['relation'])
                            @endif
                            @if(isset($mode) && $mode=='update')
                                 @slot('data',$data)
                                 @slot('mode',$mode)
                            @endif
                        @endcomponent

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info submit-form-view">{{ vl('admin',$buttonName) }}</button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{ vl('admin','cancel') }}</button>
                </div>
            </form>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
