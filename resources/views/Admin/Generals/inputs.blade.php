@php
    $decimalName = [
        'amount'=>false,
        'amount_str'=>false,
    ];
@endphp
<div class="row">
    @if(!empty($inputs))

        @foreach($inputs as $key=>$value)

            @if($value[0] == 'select')

                <div class="{{ isset($value['class']) ? $value['class'] : 'col-md-12 m-b-20' }}">

                    <div class="form-group">

                        @if(!isset($value['label']) || $value['label'])

                        <label class="mb-1">{{ vl('admin',$key) }}</label>

                        @endif

                        <select name="{{ isset($value['multiple']) ? $key."[]" : $key }}"

                                class="{{ isset($value['classInput']) ? $value['classInput'] : 'form-control' }}"

                        {{-- set attributes for input --}}
                        @foreach ($value as $k=>$v)
                            @if($k != 0 && $k != 'classInput' && $k != 'placeholder' && $k != 'class'&& $k != 'value' && $k != 'relation' && $k != 'label' && $k != 'selectManual')
                                {{$k}}="{{ $v }}"
                            @endif
                        @endforeach

                        >
                        @if(!empty($value['value']))

                            {{-- placeholder select --}}
                            @if(isset($value['placeholder']))
                                <option value="">{{ vl('admin',$value['placeholder']) }}</option>
                            @endif

                            {{-- data for select --}}
                            @foreach($value['value'] as $k=>$val)
                                <option
                                    {{--if select is multiple--}}
                                    @if (isset($value['multiple']))

                                    {{isset($mode) && $mode=='update' ? /* if modal for update */

                                     (isset($value['relation']) ?

                                        (in_array(trim($k) , $data->{$value['relation']}->pluck('slug')->toArray()) ? 'selected' : '' )

                                     : ($data->$key == $k ? 'selected' : '') )

                                     :''}}
                                    {{-- if select is normal --}}
                                    @else

                                    @if (isset($mode) && $mode=='update' && !empty($value['selectManual']))

                                        {{ $data->{$value['selectManual']}($data,$k) }}

                                        @else

                                    {{isset($mode) && $mode=='update' ?  /* if modal for update */

                                     ((isset($value['relation'])  ?

                                      $data->{$relationUpdate}->$key

                                      :$data->$key) == $k ? 'selected' :'' ) :''}}

                                    @endif



                                    @endif

                                    value="{{ $k }}">

                                    {{ $val }}

                                </option>
                            @endforeach

                        @else
                            <option>{{ vl('admin','noData') }}</option>
                            @endif
                            </select>
                    </div>
                </div>

            @else {{-- if input is normal text and password and etc --}}
            <div class="{{ isset($value['class']) ? $value['class'] : 'col-md-12 m-b-20' }}">
                <div class="form-group">
                    @if(!isset($value['label']) || $value['label'])
                    <label class="mb-1">{{ vl('admin',$key) }}</label>
                    @endif

                    <input name="{{ $key == 'amount_str' ? 'amount' : $key }}" type="{{ $value[0] }}"

                           class="{{ isset($value['classInput']) ? $value['classInput'] : 'form-control' }}"
                           {{-- if modal for update and is checkbox --}}
                            @if (isset($value['relation']))
                           {{ isset($mode) && $mode=='update' && $value[0] == 'checkbox'?

                                ($data->$relationUpdate->$key == true) ? 'checked' : ''

                           : '' }}
                               @else

                           {{ isset($mode) && $mode=='update' && $value[0] == 'checkbox'?

                            ($data->$key == true) ? 'checked' : ''

                           : '' }}

                            @endif

                            @if ($value[0] != 'checkbox' && $value[0] != 'password')

                               @if (isset($fieldSearch) && $fieldSearch)

                               value="{{ isset(Request()->$key) ? Request()->$key : '' }}"

                               @else
                               value="{{ isset($mode) && $mode=='update' ? /* if modal is update */

                                                           (isset($value['relation']) ?  /* if input have relation */

                                                           ($data->{$relationUpdate}->$key)

                                                           : (

                                                           isset($value['manualValue']) && $value['manualValue'] ? $data->{$value['manualValue']}($data) :$data->$key /* manual function update in trait */

                                                           )) /* if input is decimal for amount 200,000 */

                                                           :(isset($value['value']) ? $value['value'] : '') }}"


                               @endif


                            @endif


                           @if(isset($decimalName[$key])) onkeyup="javascript:this.value=itpro(this.value)" @endif {{-- java script for decimal in amount 200,000  --}}

                           placeholder="{{ isset($value['placeholder']) ? vl('admin',$value['placeholder']) : '' }}"

                    {{-- set attributes for input --}}
                    @foreach ($value as $k=>$v)

                        @if($k != 0 && $k != 'classInput' && $k != 'placeholder' && $k != 'class' && $k != 'value' && $k != 'relation' && $k != 'label' )
                            {{$k}}="{{ $v }}"
                        @endif

                    @endforeach >

                </div>
            </div>
            @endif
        @endforeach
    @endif

    @if(!empty($btnSearch))
            <div class="form-group">
                <button type="submit" name="btn_search" value="search" class="btn btn-info">{{ vl('general','search') }}</button>
            </div>
    @endif

</div>
