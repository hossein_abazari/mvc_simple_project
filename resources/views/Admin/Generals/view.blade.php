<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">
                    @if(isset($modal['setting']['modalForm']) && $modal['setting']['modalForm'])

                         <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#{{ isset($modal['setting']['targetModal']) ? $modal['setting']['targetModal'] : 'viewModal' }}">{!!  !empty($modal['setting']['buttonName']) ?
                          $modal['setting']['buttonName']
                          : vl('admin','add') !!}</button>

                    @elseif(isset($table['setting']['title']) && $table['setting']['title'])

                        {{ vl('admin',$table['setting']['title']) }}

                    @endif

                </h4>
                @if(isset($table['setting']['grid']) && $table['setting']['grid'])

                @if(!empty($table['search']))

                    <h4 class="mt-4 mb-2">{{ vl('general','search') }}</h4>
                    <form action="{{ url()->current() }}" method="GET" >

                        @component('Admin.Generals.inputs')
                                @slot('inputs',$table['search'])
                                @slot('btnSearch',true)
                                @slot('fieldSearch',true)
                        @endcomponent

                    </form>


                 @endif

                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-striped table-bordered table-hover contact-list" data-page-size="10">
                        <thead>
                        <tr>
                            @foreach($table['field'] as $key => $value)
                                <th>{{ vl('admin',$key) }}</th>
                            @endforeach
                            @if (!empty($table['operation']))
                                <th>{{ vl('admin','operation') }}</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $countField = count($table['field']);
                            $existOperation = (int)!empty($table['operation']) ? 1 : 0;
                            $colspan = $countField + $existOperation;
                        @endphp

                        @if($table['model']->isNotEmpty())
                            @foreach($table['model'] as $data )
                                <tr class="{{ !empty($table['tr_class']) ? $data->trClass($data) : '' }}">
                                    @foreach($table['field'] as $key=>$value)
                                     <td>

                                         @if($value == false)
                                         {!!  $data->$key !!}
                                             @else
                                             {!! isset($table['relation']) && $value=='relation' ? $data->{$table['relation']}->$key : $data->{$value}($data) !!}
                                         @endif
                                     </td>
                                    @endforeach
                                    @if (!empty($table['operation']))
                                        <td>
                                            @if(isset($table['operation']['edit']) && $table['operation']['edit'])
                                                  <button class='btn btn-info btn-sm ml-1 edit-view' data-slug="{{ $data->slug }}"><i class='fa fa-edit'></i></button>
                                            @endif

                                            @if(isset($table['operation']['delete']) && $table['operation']['delete'])
                                            <button class='btn btn-danger btn-sm delete-view' data-csrf="{{ csrf_token() }}" data-url="{{ route($table['operation']['delete'].'.destroy',[$data->slug]) }}"><i class='fa fa-trash'></i></button>
                                            @endif

                                             @if(isset($table['operation']['view']) && $table['operation']['view'])
                                            <button class='btn btn-primary btn-sm view-btn-click' data-slug="{{ $data->slug }}" data-url="{{ route($table['operation']['view']) }}"><i class='fa fa-eye'></i></button>
                                            @endif

                                            @if(isset($table['operation']['show']) && $table['operation']['show'])
                                            <a class='btn btn-success btn-sm' data-slug="{{ $data->slug }}" href="{{ route($table['operation']['show'].'.show',[$data->slug]) }}"><i class='fa fa-eye'></i></a>
                                            @endif
                                        </td>
                                    @endif
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="{{ $colspan }}">{{ vl('general','nothing') }}</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                    @if(isset($table['setting']['more_route']) && $table['setting']['more_route'])
                        <div class="text-center">
                            <a href="{{ $table['setting']['more_route'] }}" class="btn btn-success">{{ vl('admin','more') }}</a>
                        </div>
                    @endif
                </div>

                @php
                    $result = $table['model'];
                @endphp

                    @if(!isset($table['pagination']) ||  $table['pagination'])

                        @if($result->lastPage() != 1)
                         <div class="text-left footable-loaded footable">
                            <ul class="pagination">
                                @if($result->previousPageUrl())
                                     <li class="footable-page-arrow disabled"><a href="{{ $result->previousPageUrl() }}">‹</a></li>
                                @endif

                                @for($i=1; $i <= $result->lastPage(); $i++)
                                    @if ($result->currentPage() == $i)
                                         <li class="footable-page active"><a href="#">{{ $i }}</a></li>
                                        @else
                                            <li class="footable-page"><a href="{{ $result->url($i) }}">{{ $i }}</a></li>
                                     @endif

                                 @endfor

                                 @if($result->nextPageUrl())
                                     <li class="footable-page-arrow disabled"><a href="{{ $result->nextPageUrl() }}" rel="next">›</a></li>
                                 @endif
                            </ul>


                    </div>
                    @endif

                    @endif


                @endif

                {{ isset($content) ? $content : '' }}
            </div>
        </div>
    </div>
</div>
@if(isset($modal['setting']['modalForm']) && $modal['setting']['modalForm'])
    @component('Admin.Generals.modal')
        @slot('modal',$modal)
    @endcomponent
@endif

@if (!empty($table['operation']) && isset($table['operation']['edit']) && $table['operation']['edit'])
    <div id="update-form">

    </div>
@endif

<div class="content-view">

</div>

@section('scriptView')
    <script src="{{ asset('js/admin/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset(vl('link','select-picker')) }}"></script>
    <script src="{{ asset('js/script.ajax.js') }}"></script>
    <script>

        $(document).on('click','.submit-form-view',function (e) {
            e.preventDefault();
            ajaxJquery($(this))
        });

        $(document).on('click','.delete-view',function () {
            var url = $(this).data('url');
            var csrf = $(this).data('csrf');
            if(confirm("{{ vl('general','alertDelete') }}")){
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    data:{'_token': csrf},
                    success: function(result) {
                        var res = JSON.parse(result);
                        if (res.status === 1){
                            window.location = res.url
                        }
                    }
                });
            }
            else{
                return false;
            }

        });
        @if (!empty($table['operation']) && isset($table['operation']['edit']) && $table['operation']['edit'])
            $(document).on('click','.edit-view',function () {
                var slug = $(this).data('slug');
                var url = `{{ route('admin.general.edit.modal') }}`;
                var modal = `@json($modal)`;
                $.get(url,{'slug':slug,'modal':modal},function (data) {
                    $('#update-form').html(data);
                    $('#update-modal').modal('show');
                    $('.selectpicker').selectpicker('refresh');
                })
            });
        @endif


        $(document).on('click','.view-btn-click',function (e) {

            e.preventDefault();
            var slug = $(this).data('slug');
            var url = $(this).data('url');

            $.get(url,{'slug':slug} , function (data) {
                $('.content-view').html(data);

                $('#view-data').modal('show')
            })

        })

    </script>

@endsection

@section('cssView')
    <link href="{{ asset('css/admin/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/persian-datepicker.css') }}" rel="stylesheet">
@endsection

@if(isset($modal['setting']['date']) && $modal['setting']['date'])
    @section('scriptDate')
        <script src="{{ asset('js/admin/jquery.md.bootstrap.datetimepicker.js') }}"></script>

        <script>

            $('.date').MdPersianDateTimePicker(settingDate('.date'));

            $('.from-date').MdPersianDateTimePicker(settingDate('.from-date'));

            $('.to-date').MdPersianDateTimePicker(settingDate('.to-date'));

        </script>

    @endsection
@endif

