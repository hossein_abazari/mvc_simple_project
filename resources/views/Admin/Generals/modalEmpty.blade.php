<div id="{{ isset($setting['targetModal']) ? $setting['targetModal'] : 'modal-view'  }}"

     class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog {{isset($setting['sizeModal']) ? $setting['sizeModal'] : ''}}">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">

                  {{ isset($setting['title']) && $setting['title'] ? $setting['title'] : vl('admin','view')}}
                </h4>
            </div>
            <div class="mb-2 mt-2">

                {{ $content }}

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{ vl('admin','cancel') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
