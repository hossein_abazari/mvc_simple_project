<div class="{{ isset($class) ? $class : 'col-lg-3 col-md-6' }}">
    <div class="card">
        <a @if(isset($link)) href="{{ $link }}" @endif>
        <div class="card-body">
            <h4 class="card-title">{{ vl('admin' , $title) }}</h4>
            <div class="text-side-opposing">
                <h2 class="font-light m-b-0"><i class="ti-arrow-up text-primary"></i>{{ isset($money) ? toMoney($money) : $number }}</h2>
{{--                <span class="text-muted">{{ vl('admin',$underTitle) }}</span>--}}

            </div>


            @if(!empty($textMuted))
                @foreach($textMuted as $key => $value)
                    <div class="text-side-opposing text-top-box">
                        <span class="text-muted">{{ vl('admin',$key).": ".$value }} </span>
                    </div>
                @endforeach
            @endif

            @if(isset($today))
                <div class="text-side-opposing text-top-box">
                   <span class="text-muted">{{ vl('admin','today').": ".number_format($today) }} </span>
                </div>
            @endif

            @if(isset($yesterday))
                <div class="text-side-opposing text-top-box">
                   <span class="text-muted">{{ vl('admin','yesterday').": ".number_format($yesterday) }} </span>
                </div>
            @endif

            @if(isset($notRead))
                <div class="text-side-opposing text-top-box">
                   <span class="text-muted">{{ vl('admin','notRead').": ".number_format($notRead) }} </span>
                </div>
            @endif

            @if (isset($percent))
                <span class="text-primary">{{ $percent }}%</span>
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: {{$percent}}%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            @endif


        </div>
        </a>
    </div>
</div>
