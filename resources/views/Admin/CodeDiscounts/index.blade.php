@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','referral') }}
@endsection

@section('content')

    @component('Admin.Generals.view')

        @slot('modal',[
            'setting' => ['modalForm'=>true,'sizeModal'=>'modal-lg','targetModal' => 'count-discount-modal'],
            'input' => [
                'name'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'nameReagentEnter'],
                'code_discount'=>['text','value'=> $randomCode ,'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'codeDiscountEnter'],
                'cent_discount'=>['text','value'=>0,'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'centDiscountEnter'],
                'amount_discount'=>['text','value'=>0,'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'amountDiscountEnter'],
                'cent_inviter'=>['text','value'=>0,'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'centInviterEnter'],
                'cent_amount_inviter'=>['text','value'=>0,'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'centAmountInviterEnter'],
            ],
            'form' => [
                'route' => route('admin.code.discount.store'),
                'method' => 'post',
                'class' => '',
            ],
            'update' => [
                'model'=>'CodeDiscount',
                'route'=>'admin.code.discount'
            ],

        ])

        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $codeDiscounts,
            'field' => [
            'name'=>false,
            'code_discount'=>false,
            'cent_discount'=>false,
            'amount_discount'=>false,
            'cent_inviter'=>false,
            'cent_amount_inviter'=>false,
            'number_invitees'=>false,
            'amount_invitees'=>false,
            ], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['delete'=>'admin.code.discount','edit'=>true]
        ])
    @endcomponent

@endsection

{{--@include('Admin.PackCards.css')--}}
{{--@include('Admin.PackCards.script')--}}
