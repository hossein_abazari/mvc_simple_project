@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','deadlines') }}
@endsection

@section('content')

    @component('Admin.Generals.view')
        @slot('modal',[
            'setting' => ['modalForm'=>true,'sizeModal'=>'modal-lg','targetModal' => 'deadline-modal'],
            'input' => [
                'name'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'nameEnter'],
                'expire_date'=>['number','value'=>30,'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'expireDateEnter'],
                'amount_str'=>['text','value'=>'','class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'amountEnter'],
/*                'user_name'=>['select','value'=>['a'=>'A','b'=>'B'],'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'userEnter'],*/
                    'free'=>['checkbox','value'=>'1','class'=>'col-md-4 m-b-20','classInput'=>'form-control'],
            ],
            'form' => [
                'route' => route('admin.deadline.store'),
                'method' => 'post',
                'class' => '',
            ],
            'update' => [
                'model'=>'Deadline',
                'route'=>'admin.deadline'
            ],

        ])

        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $deadlines,
            'field' => ['name'=>false,'expire_date'=>false,'amount'=>'dataAmount','free'=>'dataFree'], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['delete'=>'admin.deadline','edit'=>true]
        ])
    @endcomponent

@endsection

@include('Admin.Deadlines.css')
@include('Admin.Deadlines.script')
