@extends('Admin.layout.main')

@section('title')
    {{ vl('admin','discounts') }}
@endsection

@section('content')

    @component('Admin.Generals.view')
        @slot('modal',[
            'setting' => ['modalForm'=>true,'sizeModal'=>'modal-lg','targetModal' => 'discount-modal','date'=>true],
            'input' => [
                'name'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'nameEnter'],
                'number'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'numberEnter'],
                'discount_cent'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'DiscountCentEnter'],
                'expire_date'=>['text','value'=>'','class'=>'col-md-6 m-b-20','classInput'=>'form-control','placeholder'=>'expireDateEnter'],
/*                'user_name'=>['select','value'=>['a'=>'A','b'=>'B'],'class'=>'col-md-4 m-b-20','classInput'=>'form-control','placeholder'=>'userEnter'],*/
            ],
            'form' => [
                'route' => route('admin.discount.store'),
                'method' => 'post',
                'class' => '',
            ],
            'update' => [
                'model'=>'Discount',
                'route'=>'admin.discount'
            ],

        ])

        @slot('table',[
            'setting' => ['grid'=>true,],
            'model' => $discounts,
            'field' => ['name'=>false,'expire_date'=>false,'discount_cent'=>false,'number'=>false], /* if value is false print field name , if exists value send to name function in model into use trait */
            'operation' => ['delete'=>'admin.discount','edit'=>true]
        ])
    @endcomponent

@endsection

@include('Admin.Deadlines.css')
@include('Admin.Deadlines.script')
