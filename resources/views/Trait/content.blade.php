@if ($model->transactionDetail()->sum('total_amount') > 0)
    <a class="badge bg-Main-color-light btn-pay " href="{{ route('celebration.payment',[$model->slug]) }}" >{{ vl('general',$name) }}</a>
    @else
    <span class="badge bg-Main-color"><i class="fa fa-check-to-slot"></i> {{ vl('general','paid') }}</span>
@endif


