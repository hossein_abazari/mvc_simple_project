@if(session()->has('flash_message'))
    <div class="alert alert-{{session('flash_message_level')}} knob-animate"> <i class="fa fa-check-circle"></i>
        {{session('flash_message')}}
{{--        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>--}}
    </div>
@endif

<div class="alert-ajax">

</div>
