    @php
        $validForm =[];
    @endphp

    @component('Generals.wizard')
        @slot('data' , $data)
        @slot('validate' , [
        true
        ])
        @slot('route' , $route),
        @slot('button_labels', $button_labels)
        @slot('nameCSS', 'login')

    @endcomponent


@section('scriptGeneral')
    <script src="{{ asset('js/script.ajax.js') }}"></script>

    <script>

        $(document).on('click','.next',function () {

            var url = `{{$routeValidate}}`;
            var value = $(this).data('value');
            var type = $('.field-type').val();
            var csrf_token = `{{ csrf_token() }}`;
            var serialize = {
                'email': $('.field-email').val(),
                'password': $('.field-password').val(),
                '_token': csrf_token,
                'type': type,
            };
            // submitGeneralAjax(url,serialize);

        })
        $(document).on('click','.finish',function (e) {
            e.preventDefault();
            ajaxJquery($(this))
        });

    </script>
@endsection

