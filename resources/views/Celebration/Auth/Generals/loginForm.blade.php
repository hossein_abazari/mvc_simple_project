<div class="wrap-login100">
    <form class="login100-form validate-form" method="POST" action="{{ $route }}">
        @csrf
					<span class="login100-form-logo">
						<img class="logo-width" src="{{ asset('Root/img/logo_'.Config::get('celebration.design.color').'.png') }}">
					</span>

        <span class="login100-form-title p-b-34 p-t-27">
						{{ vl('general',$namePage) }}
					</span>
        @foreach($inputs as $key => $value)
               <div class="wrap-input100">
                    <input class="input100" type="{{ isset($value['type']) ? $value['type'] : 'text'  }}" name="{{ $key }}" placeholder="{{ vl('general', $key) }}">
                    <span class="focus-input100" data-placeholder="{!!  $value['codeIcon'] !!} "></span>
                </div>
        @endforeach


        {{--            <div class="contact100-form-checkbox">--}}
        {{--                <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">--}}
        {{--                <label class="label-checkbox100" for="ckb1">--}}
        {{--                    Remember me--}}
        {{--                </label>--}}
        {{--            </div>--}}

        <div class="container-login100-form-btn">
            <button type="submit" class="login100-form-btn btn-save">
                {{ vl('general',$namePage) }}

            </button>
        </div>

        <div class="text-center p-t-90">
            <a class="txt1" href="{{ $link['url'] }}">
                {{ vl('general',$link['name']) }}
            </a>
        </div>
    </form>
</div>
@section('script')
    <script src="{{ asset('js/script.ajax.js') }}"></script>

    <script>

        $(document).on('click','.btn-save',function (e) {
            e.preventDefault();
            ajaxJquery($(this),false,1,'wrap-input100')
        });

    </script>
@endsection
