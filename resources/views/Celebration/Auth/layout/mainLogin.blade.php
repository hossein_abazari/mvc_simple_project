<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{ asset('Root/img/logo_'.Config::get('celebration.design.color').'.png') }}"/>

    <link href="{{ asset('css/celebration/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/celebration/custom-bootstrap-rtl.css') }}" rel="stylesheet">
    <!--===============================================================================================-->
    <link href="{{ asset('css/celebration/fontAW.css') }}" rel="stylesheet">
    @include('Generals.css_main')
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/celebration/login/material-design-iconic-font.min.css') }}">
    <!--===============================================================================================-->
    <link href="{{ asset('css/admin/animate.css') }}" rel="stylesheet">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/celebration/login/hamburgers.min.css') }}">

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/celebration/login/animsition.min.css') }}">

    <!--===============================================================================================-->
    <!--===============================================================================================-->
    <!--===============================================================================================-->

    <link rel="stylesheet" type="text/css" href="{{ asset('css/celebration/login/util.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/celebration/login/main.css') }}">

<!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100" style="background-image: url('/images/celebration/img-01.jpg');">
        @yield('content')
    </div>
</div>


<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('/js/celebration/login/animsition.min.js') }}"></script>

<!--===============================================================================================-->
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/celebration/bootstrap.bundle.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('/js/celebration/login/countdowntime.js') }}"></script>

<!--===============================================================================================-->
<script src="{{ asset('/js/celebration/login/main.js') }}"></script>

@yield('script')

</body>
</html>
