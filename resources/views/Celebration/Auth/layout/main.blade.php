<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    @include('Celebration.Auth.layout.css')

    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login" style="background-image: url('/images/celebration/img-01.jpg');">
        <div class="container">
           @yield('content')
            <div class="row col-md-12">
                <div class="text-center name-link">
                    @yield('link')
                </div>
            </div>
        </div>

    </div>
</div>




@include('Celebration.Auth.layout.script')

</body>
</html>
