@extends('Celebration.Auth.layout.mainLogin')

{{--@section('link')--}}
{{--    <a href="{{ route('celebration.register.index') }}" class="text-white">{{ vl('general','register') }}</a>--}}
{{--    @endsection--}}

@section('title')
    {{ vl('general' , 'login') }}
@endsection

@section('content')

    @component('Celebration.Auth.Generals.loginForm')
        @slot('route',route('celebration.register.store'))
        @slot('namePage','register')
        @slot('link',[
            'url' => route('celebration.show.login'),
            'name' => 'login',
        ])
        @slot('inputs',[
            'mobile' => [
                'codeIcon' => '&#xf207;'
            ] ,
            'password' => [
               'codeIcon' => '&#xf191;',
               'type' => 'password',
            ],
             'password_confirmation' => [
               'codeIcon' => '&#xf191;',
               'type' => 'password',
            ],
        ])
    @endcomponent

@endsection



