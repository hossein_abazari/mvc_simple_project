<div class="row">

    {{ \App\Classes\FormField::field('title','text',isset($ticket) ? $ticket->title: '' ,'col-md-12 mt-3','form-control','enterType',[]) }}

    {{ \App\Classes\FormField::fieldTextarea('message',isset($ticket) ? $ticket->message: '' ,'col-md-12 mt-3','form-control','enterType',['rows'=>4]) }}


</div>
