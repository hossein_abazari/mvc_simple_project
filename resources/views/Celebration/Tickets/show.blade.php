@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','ticketDetails') }}
@endsection

@section('headerTop')
    <a class="btn btn-link pull-left text-main-color none-text-decoration" data-bs-toggle="modal" data-bs-target="#insert-ticket">
        {{ vl('general','addTicket') }}
        <i class="fa fa-plus"></i>
    </a>
@endsection

@section('content')
    @component('Generals.message')
        @slot('messages',[
            'modelDetails' => $ticketDetails,
            'modelMaster' => $ticket,
            'routeFormSubmit' => 'celebration.tickets.show.store',
        ])
    @endcomponent
@endsection
