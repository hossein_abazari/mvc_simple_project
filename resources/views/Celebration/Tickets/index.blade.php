@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','tickets') }}
@endsection

@section('headerTop')
    <a class="btn btn-link pull-left text-main-color none-text-decoration" data-bs-toggle="modal" data-bs-target="#insert-ticket">
        {{ vl('general','addTicket') }}
        <i class="fa fa-plus"></i>
    </a>
@endsection

@section('content')
    <div class="page-content">
        @include('partial.flash')

        <div class="container font3" style="{{ vl('general','direction') }}">
            <div class="row">
                <div class="col-md-12">
                    @component('Celebration.Generals.table')
                        @slot('models' , $tickets )
                        @slot('inputs',[
                            [
                                'name' => 'title',
                                'label' => vl('general','title'),
                            ],
                            [
                                'name' => 'close',
                                'label' => vl('general','close'),
                            ]
                        ])
                    @endcomponent

                </div>
            </div>

        </div>
    </div>

    @component('Generals.modal')
        @slot('route') {!! route('celebration.ticket.store')!!}  @endslot
        @slot('content')
            @component('Celebration.Tickets._form')
            @endcomponent
        @endslot
        @slot('title') {!! vl('general','addTicket') !!}  @endslot
        @slot('nameModal') {!! 'insert-ticket' !!}  @endslot
        @slot('buttonName') {!! vl('general','create') !!}  @endslot
        @slot('buttonClass') {{ 'btn-main-color' }}  @endslot
    @endcomponent

    <div class="content-ticket-modal">

    </div>

@endsection
