@yield('topHtml')
<div class="page-wrapper">
    <div class="container mt-5 mb-5">
        <div class="card card-header header-custom">
            <h3 class="h3 title-size mt-2"><i class="fa fa-@yield('nameIcon')" style="font-size: 15px"></i>@yield('title') @yield('headerTop')</h3>

        </div>
        <div class="card card-body border-top-0 radius-0">
            <div class="container margin-container">
                @yield('content')
            </div>
        </div>
    </div>
</div>
