<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/celebration/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/celebration/fontawesome.min.js') }}"></script>

<script src="{{ asset('js/celebration/fontawesome/fontawesome.min.js') }}"></script>
<script src="{{ asset('js/celebration/fontawesome/all.min.js') }}"></script>
<script src="{{ asset('js/celebration/fontawesome/brands.min.js') }}"></script>
<script src="{{ asset('js/celebration/fontawesome/regular.min.js') }}"></script>
<script src="{{ asset('js/celebration/fontawesome/solid.min.js') }}"></script>
<script src="{{ asset('js/celebration/fontawesome/svg-with-js.min.js') }}"></script>
<script src="{{ asset('js/celebration/fontawesome/v4-shims.min.js') }}"></script>
<script src="{{ asset('/js/date/persianDatepicker.min.js') }}"></script>

@yield('wizardScript')

<script>
    $(document).ready(function(){
        // Input Focus
        $('.form-holder').delegate("input", "focus", function(){
            $('.form-holder').removeClass("active");
            $(this).parent().addClass("active");
        });

        $('.knob-animate').delay(3000).slideUp(500);
    });

</script>
@yield('scriptButton')

