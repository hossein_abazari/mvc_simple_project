<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/images/favicon.png') }}">
{{--    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>--}}
    <title>@yield('title')</title>
    @include('Celebration.layout.css')
</head>

<body class="body-main" dir="rtl">

{{--<div class="preloader">--}}
{{--    <svg class="circular" viewBox="25 25 50 50">--}}
{{--        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>--}}
{{--</div>--}}

@include('Celebration.layout.header')

@include('Celebration.layout.body')

@include('Celebration.layout.footer')

@include('Celebration.layout.script')

</body>

</html>
