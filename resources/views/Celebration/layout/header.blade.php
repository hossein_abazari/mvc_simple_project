
<header class="topbar">
    <nav class="navbar navbar-expand-lg nav-border-bottom navbar-light bg-header navbar-dark shadow">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><img class="logo-celebrate" src="{{ asset('images/celebration/logo.png') }}"></a>
            <button class="navbar-toggler collapsed text-white mobile-button" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-content">
                <div class="hamburger-toggle">
                    <div class="hamburger">
                        <span style="background: white;"></span>
                        <span style="background: white;"></span>
                        <span style="background: white;"></span>
                    </div>
                </div>
            </button>

            <div class="collapse navbar-collapse" id="navbar-content">
                <ul class="navbar-nav nav-header-bar mr-auto mb-2 mb-lg-0">
                    @foreach(\App\Classes\Helper::setMenuCard() as $key => $value)
                        @php $active = '' @endphp
                        @php $active = \App\Classes\Menu::activeMenu($key) @endphp
                        @if(is_array($value))
                            <li class="nav-item dropdown {{$active}}">
                                <a class="nav-link dropdown-toggle {{$active}}" href="#" data-bs-toggle="dropdown" data-bs-auto-close="outside">
                                    {{ vl('admin',$key) ? vl('admin',$key) : $key }}
                                    <i class="{{ \App\Classes\Helper::setMenuCard('icon')[$key] }}"></i>
                                </a>
                                <ul class="dropdown-menu shadow">
                                    @foreach($value as $k=>$v)
                                        <li><a class="dropdown-item" href="{{ $v }}">{{ vl('admin',$k) ? vl('admin',$k) : $k }}</a></li>
                                    @endforeach
                                    {{--                                    <li class="dropstart">--}}
                                    {{--                                        <a href="#" class="dropdown-item dropdown-toggle" data-bs-toggle="dropdown">Submenu Left</a>--}}
                                    {{--                                        <ul class="dropdown-menu shadow">--}}
                                    {{--                                            <li><a class="dropdown-item" href=""> Third level 1</a></li>--}}
                                    {{--                                            <li><a class="dropdown-item" href=""> Third level 2</a></li>--}}
                                    {{--                                            <li><a class="dropdown-item" href=""> Third level 3</a></li>--}}
                                    {{--                                            <li><a class="dropdown-item" href=""> Third level 4</a></li>--}}
                                    {{--                                            <li><a class="dropdown-item" href=""> Third level 5</a></li>--}}
                                    {{--                                        </ul>--}}
                                    {{--                                    </li>--}}
                                    {{--                                    <li class="dropend">--}}
                                    {{--                                        <a href="#" class="dropdown-item dropdown-toggle" data-bs-toggle="dropdown" data-bs-auto-close="outside">Submenu Right</a>--}}
                                    {{--                                        <ul class="dropdown-menu shadow">--}}
                                    {{--                                            <li><a class="dropdown-item" href=""> Second level 1</a></li>--}}
                                    {{--                                            <li><a class="dropdown-item" href=""> Second level 2</a></li>--}}
                                    {{--                                            <li><a class="dropdown-item" href=""> Second level 3</a></li>--}}
                                    {{--                                            <li class="dropend">--}}
                                    {{--                                                <a href="#" class="dropdown-item dropdown-toggle" data-bs-toggle="dropdown">Let's go deeper!</a>--}}
                                    {{--                                                <ul class="dropdown-menu dropdown-submenu shadow">--}}
                                    {{--                                                    <li><a class="dropdown-item" href=""> Third level 1</a></li>--}}
                                    {{--                                                    <li><a class="dropdown-item" href=""> Third level 2</a></li>--}}
                                    {{--                                                    <li><a class="dropdown-item" href=""> Third level 3</a></li>--}}
                                    {{--                                                    <li><a class="dropdown-item" href=""> Third level 4</a></li>--}}
                                    {{--                                                    <li><a class="dropdown-item" href=""> Third level 5</a></li>--}}
                                    {{--                                                </ul>--}}
                                    {{--                                            </li>--}}
                                    {{--                                            <li><a class="dropdown-item" href=""> Third level 5</a></li>--}}
                                    {{--                                        </ul>--}}
                                    {{--                                    </li>--}}
                                </ul>
                            </li>
                        @else
                            <li class="nav-item {{$active}}">
                                <a class="nav-link {{$active}}" aria-current="page" href="{{ !is_array($value) ? $value : '#' }}">
                                    {{ vl('admin',$key) ? vl('admin',$key) : $key }}
                                    <i class="{{ \App\Classes\Helper::setMenuCard('icon')[$key] }}"></i></a>
                            </li>
                        @endif
                    @endforeach
                        <li class="nav-item display-exit">
                            <a href="{{ route('celebration.logout') }}" class="nav-link light-drop text-white" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();" ><i class="fa fa-power-off"></i> {{ vl('admin','exit') }}
                                <form id="logout-form" action="{{ route('celebration.logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </a>
                        </li>
                    {{--                    <li class="nav-item dropdown dropdown-mega position-static">--}}
                    {{--                        <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown" data-bs-auto-close="outside">Megamenu</a>--}}
                    {{--                        <div class="dropdown-menu shadow">--}}
                    {{--                            <div class="mega-content px-4">--}}
                    {{--                                <div class="container-fluid">--}}
                    {{--                                    <div class="row">--}}
                    {{--                                        <div class="col-12 col-sm-4 col-md-3 py-4">--}}
                    {{--                                            <h5>Pages</h5>--}}
                    {{--                                            <div class="list-group">--}}
                    {{--                                                <a class="list-group-item" href="#">Accomodations</a>--}}
                    {{--                                                <a class="list-group-item" href="#">Terms & Conditions</a>--}}
                    {{--                                                <a class="list-group-item" href="#">Privacy</a>--}}
                    {{--                                            </div>--}}
                    {{--                                        </div>--}}
                    {{--                                        <div class="col-12 col-sm-4 col-md-3 py-4">--}}
                    {{--                                            <h5>Card</h5>--}}
                    {{--                                            <div class="card">--}}
                    {{--                                                <img src="img/banner-image.jpg" class="img-fluid" alt="image">--}}
                    {{--                                                <div class="card-body">--}}
                    {{--                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
                    {{--                                                </div>--}}
                    {{--                                            </div>--}}
                    {{--                                        </div>--}}
                    {{--                                        <div class="col-12 col-sm-4 col-md-3 py-4">--}}
                    {{--                                            <h5>About CodeHim</h5>--}}
                    {{--                                            <p><b>CodeHim</b> is one of the BEST developer websites that provide web designers and developers with a simple way to preview and download a variety of free code & scripts.</p>--}}
                    {{--                                        </div>--}}
                    {{--                                        <div class="col-12 col-sm-12 col-md-3 py-4">--}}
                    {{--                                            <h5>Damn, so many</h5>--}}
                    {{--                                            <div class="list-group">--}}
                    {{--                                                <a class="list-group-item" href="#">Accomodations</a>--}}
                    {{--                                                <a class="list-group-item" href="#">Terms & Conditions</a>--}}
                    {{--                                                <a class="list-group-item" href="#">Privacy</a>--}}
                    {{--                                            </div>--}}
                    {{--                                        </div>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </li>--}}

                </ul>
                {{--                <form class="d-flex ms-auto">--}}
                {{--                    <div class="input-group">--}}
                {{--                        <input class="form-control border-0 mr-2" type="search" placeholder="Search" aria-label="Search">--}}
                {{--                        <button class="btn btn-primary border-0" type="submit">Search</button>--}}
                {{--                    </div>--}}
                {{--                </form>--}}


            </div>

            <div class="navbar-collapse flex-grow-0 nav-mobile">
                <ul class="navbar-nav right-nav my-lg-0 left">
                    {{-- search before --}}
                    <li class=" dropdown">
                        <a class="dropdown-toggle text-white waves-effect waves-dark" href="#" id="drop-down" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="{{ asset('images/users/default_user.png') }}" alt="user" class="profile-pic" /></a>
                        <div class="dropdown-menu menu-side-language animated flipInY" aria-labelledby="drop-down">
                            <ul class="dropdown-user">
                                @if(Auth::getUser()->celebration)
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-text">
                                                <h4>{{ Auth::getUser()->celebration->name }}</h4>
                                                <p class="text-muted">{{ Auth::getUser()->mobile }}</p>

                                                <button href="#" class="btn btn-main-color btn-sm mt-2">{{ vl('admin','viewProfile') }}</button>

                                            </div>
                                        </div>
                                    </li>
                                @endif
                                {{--                                <li role="separator" class="divider"></li>--}}
                                {{--                                <li><a href="#" class="light-drop"><i class="ti-user"></i> {{ vl('admin','myProfile') }}</a></li>--}}
                                {{--                                <li><a href="#" class="light-drop"><i class="ti-wallet"></i> {{ vl('admin','myIncome') }}</a></li>--}}
                                {{--                                <li><a href="#" class="light-drop"><i class="ti-email"></i> {{ vl('admin','inbox') }}</a></li>--}}
                                {{--                                <li role="separator" class="divider"></li>--}}
                                {{--                                <li><a href="{{ route('admin.setting.index') }}" class="light-drop"><i class="ti-settings"></i> {{ vl('admin','accountSettings') }}</a></li>--}}
                                {{--                                <li role="separator" class="divider"></li>--}}
                                <li>
                                    <a href="{{ route('celebration.logout') }}" class="light-drop" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();" ><i class="fa fa-power-off"></i> {{ vl('admin','exit') }}
                                        <form id="logout-form" action="{{ route('celebration.logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

