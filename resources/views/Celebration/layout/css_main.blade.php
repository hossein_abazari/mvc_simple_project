<link href="{{ asset('css/celebration/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/celebration/custom-bootstrap-rtl.css') }}" rel="stylesheet">
<link href="{{ asset('css/celebration/fontAW.css') }}" rel="stylesheet">
@include('Generals.css_main')
<link href="{{ asset('css/celebration/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.md.bootstrap.datetimepicker.style.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/date/persianDatepicker-default.css') }}">

@yield('wizardCss')

