@include('Celebration.layout.script_main')

<script src="{{ asset('js/celebration/custom.js') }}"></script>

@yield('script')
@yield('scriptComponent')
@yield('scriptMap')
@yield('scriptLayout')
@yield('scriptModal')
@yield('scriptFile')
@yield('scriptFileAvatar')
@yield('scriptFileAvatarImgBride')


