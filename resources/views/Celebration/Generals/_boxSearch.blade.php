<div class="row">
    <div class="col-sm-12">
        <form action="{{ $url }}" method="get">
            <div class="search-box mb-4">
{{--                <div class="input-field first-wrap border-right-none">--}}
{{--                    <div class="icon-wrap text-main-color">--}}
{{--                        <i class="fa fa-file"></i>--}}
{{--                    </div>--}}
{{--                    <input id="search" style="" name="name_search"--}}
{{--                           value="{{ isset(Request()->name_search) ? Request()->name_search : '' }}"--}}
{{--                           class="form-control radius-right" type="text" placeholder="{{ vl('general','nameGuest') }}">--}}
{{--                </div>--}}
{{--                <div class="input-field second-wrap">--}}
{{--                    <div class="icon-wrap text-main-color">--}}
{{--                        <i class="fa fa-file"></i>--}}
{{--                    </div>--}}
{{--                    <select name="family_search" class="form-control">--}}
{{--                        <option selected  value="">{{ vl('general','allFamily') }}</option>--}}
{{--                        <option {{ isset(Request()->family_search) && Request()->family_search == 'groom' ? 'selected' : '' }} value="groom">{{ vl('general','groom') }}</option>--}}
{{--                        <option {{ isset(Request()->family_search) && Request()->family_search == 'bride' ? 'selected' : '' }} value="bride">{{ vl('general','bride') }}</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--                <div class="input-field second-wrap">--}}
{{--                    <div class="icon-wrap text-main-color">--}}
{{--                        <i class="fa fa-file"></i>--}}
{{--                    </div>--}}
{{--                    <select name="message_search" class="form-control">--}}
{{--                        <option selected  value="">{{ vl('general','all') }}</option>--}}
{{--                        <option {{ isset(Request()->message_search) && Request()->message_search == 'allMessage' ? 'selected' : '' }} value="allMessage">{{ vl('general','allMessage') }}</option>--}}
{{--                        <option {{ isset(Request()->message_search) && Request()->message_search == 'openMessage' ? 'selected' : '' }} value="openMessage">{{ vl('general','openMessage') }}</option>--}}
{{--                        <option {{ isset(Request()->message_search) && Request()->message_search == 'newMessage' ? 'selected' : '' }} value="newMessage">{{ vl('general','newMessage') }}</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--                @php--}}
{{--                    $array =[--}}
{{--                           [--}}
{{--                                'name' => 'name_search',--}}
{{--                                'type' => 'text',--}}
{{--                                'label' => false,--}}
{{--                                'placeholder' => vl('general','nameGuest'),--}}
{{--                            ],--}}
{{--                            [--}}
{{--                                'name' => 'family_search',--}}
{{--                                'type' => 'select',--}}
{{--                                'label' => false,--}}
{{--                                'value' => [--}}
{{--                                        'groom' => vl('general','groom'),--}}
{{--                                        'bride' => vl('general','bride'),--}}
{{--                                        ],--}}
{{--                                'placeholder' => vl('general','allFamily'),--}}
{{--                            ],--}}
{{--                            [--}}
{{--                                'name' => 'message_search',--}}
{{--                                'type' => 'select',--}}
{{--                                'label' => false,--}}
{{--                                'value' => [--}}
{{--                                        'allMessage' => vl('general','allMessage'),--}}
{{--                                        'openMessage' => vl('general','openMessage'),--}}
{{--                                        'newMessage' => vl('general','newMessage'),--}}
{{--                                        ],--}}
{{--                                'placeholder' => vl('general','all'),--}}
{{--                            ],--}}
{{--                                                                   --}}
{{--                        ]--}}
{{--                @endphp--}}

                @foreach($inputs as $key => $input)
                   @php
                       $classHolder = $key == 0 ? 'first-wrap border-right-none' : 'second-wrap';
                       $classRadius = $key == 0 ? 'radius-right' : '';
                   @endphp

                    @if($input['type'] == 'select')
                        <div class="input-field {{ $classHolder }}">
                            <div class="icon-wrap text-main-color">
                                <i class="fa fa-file"></i>
                            </div>
                            <select name="{{ $input['name'] }}" class="form-control {{ $classRadius }} {{ $input['class'] ?? '' }}">
                                @if(isset($input['placeholder']))
                                    <option selected  value="">{{ $input['placeholder'] }}</option>
                                @endif
                                @foreach($input['value'] as $key_v => $value_v)
                                        <option {{ isset(Request()->{ $input['name'] }) && Request()->{$input['name']} == $key_v ? 'selected' : '' }} value="{{ $key_v }}">{{ $value_v }}</option>
                                    @endforeach
                            </select>
                        </div>
                    @else
                        <div class="input-field {{ $classHolder }}">
                            <div class="icon-wrap text-main-color">
                                <i class="fa fa-file"></i>
                            </div>
                            <input id="search_{{ $key }}" style="" name="{{ $input['name'] }}"
                                   value="{{ isset(Request()->{$input['name']}) ? Request()->{$input['name']} : '' }}"
                                   class="form-control {{ $classRadius }} {{ $input['class'] ?? '' }}" type="{{ $input['type'] }}" placeholder="{{ $input['placeholder'] }}">
                        </div>
                    @endif
                @endforeach

                <div class="input-field fifth-wrap">
                    <button class="btn btn-search radius-left" style="" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
