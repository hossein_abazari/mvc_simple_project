<div class="card-grid">
    @if(!isset($data['topButton']) || $data['topButton'])
        <div class="mb-lg-4">
            <a href="{{ route('celebration.celebrate.details.create') }}" class="btn btn-main-color">{{ vl('general','create') }}</a>
        </div>
    @endif

    @if(isset($data['search']) && $data['search'])
            @component('Celebration.Generals._boxSearch')
                @slot('url',$data['search']['url'])
                @slot('inputs', $data['search']['inputs'])
            @endcomponent

        @endif
    <div class="row {{ isset($data['classBeforeData']) ? $data['classBeforeData'] : '' }}">
     @if($data['model']->isNotEmpty())
         @foreach($data['model'] as $model)
        <div class="mt-4 card-item {{ isset($data['classAfterData'])?$data['classAfterData'] : 'col-md-4' }}" style="z-index: 0">
            <div class="card-image-item" >
                <div class="box-card-design">
                        @if(isset($data['nameMessage']))
                        <div class="card-celebrate-label badge">
                        {{ $model->{$data['nameMessage']} }}
                        </div>
                            @elseif(isset($data['type']))
                        <div class="card-celebrate-label badge">
                            {{vl('general','card_'.$model->{$data['type']})}}
                        </div>
                            @endif
                    <div class="content-card">
                        @component('Celebration.Generals._card')
                            @slot('model',$model)
                            @if(isset($data['image']) && $data['image'])
                                 @slot('image',$data['image'])
                            @endif
                            @slot('title',$data['title'])
                            @if(isset($data['field']))
                                  @slot('field',$data['field'])
                            @endif

                            @if(isset($data['rightBottom']) && $data['rightBottom'])
                               @slot('rightBottom',$data['rightBottom'])
                            @endif
                            @if(isset($data['leftBottom']) && $data['leftBottom'])
                                @slot('leftBottom',$data['leftBottom'])
                            @endif
                            @if(isset($data['rightBottomEmpty']) && $data['rightBottomEmpty'])
                               @slot('rightBottomEmpty',$data['rightBottomEmpty'])
                            @endif

                            @if(isset($data['leftBottomEmpty']) && $data['leftBottomEmpty'])
                                @slot('leftBottomEmpty',$data['leftBottomEmpty'])
                            @endif

                            @if(isset($data['contentClass']) && $data['contentClass'])
                                @slot('contentClass',$data['contentClass'])
                            @endif

                         @endcomponent
                    </div>
                </div>
            </div>
        </div>
    @endforeach
         @else
         <div class="col-sm-12">
             <div class="alert alert-secondary">
                 {{ vl('general','nothing') }}
             </div>
         </div>
      @endif
    </div>
        {!! \App\Classes\Paginate::renderS($data['model']) !!}

</div>

@section('scriptComponent')
    <script>
        $(document).on('click','.btn-click-detail',function (e) {
            e.preventDefault();
            fetchContent($(this));
        });

        $(document).on('click','.btn-click-card',function (e) {
            e.preventDefault();
            fetchContent($(this));
        });

        function fetchContent(elem) {
            var url = elem.attr('href');
            var content = elem.closest('.content-card');
            $.get(url,function (data) {
                content.html(data)
            })
        }
    </script>
    @endsection
