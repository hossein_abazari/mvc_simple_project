@if(isset($image) && $image)
    <div class="text-center mt-3">
        @if(is_string($image))
        <img src="{{ asset(\App\Classes\UploadFile::$directory.$model->{$image}) }}">
            @else
            <img style="{{ isset($image['style']) ? $image['style'] : ''}}" class="{{ isset($image['class']) ? $image['class'] : ''}}"
                 @if (isset($image['directoryDynamic']) && $image['directoryDynamic'])
                 src="{{ asset(\App\Classes\UploadFile::$directory.$image['directory'].$image['directoryDynamic']::getDirectoryImage($model).$model->{$image['name']}) }}">

        @else
                 src="{{ asset(\App\Classes\UploadFile::$directory.$image['directory'].$model->{$image['name']}) }}">
        @endif
        @endif
    </div>
@endif
<div class="picture-legend">
    <div class="row mt-2 mb-2">
        <div class="col-md-12 text-center text-title-card">
            <span>{{ $model->{$title} }}</span>
        </div>
    </div>
    <div class="row">
        @if(isset($field))
          @foreach($field as $key => $value)

            <div class="{{ isset($contentClass) ? $contentClass : 'col-6' }} name-card">
                <p class="mt-2">
                    @if($value == 'content')
                        {{ $model->content($key,$model) }}
                    @else
                        {{ vl('general',$key) }} :
                        {{ $value == false ? ($model->{$key} ?? '--') : ($model->{$value} ?? '--') }}
                    @endif

                </p>
            </div>
        @endforeach
        @endif
        {{--                            <p class="mt-2"><b>{{ vl('general','name_template') }} : {{ $layout->name }}</b></p>--}}
        {{--                            <p>{{ vl('general','price_template') }} : {{ number_format($layout->price) }}</p>--}}
        {{--                            <p>{{ vl('general','number_card') }} : {{  $layout->number_invitation_card }}</p>--}}

    </div>
</div>
<div class="row mt-2 mb-2">
    <div class="form-group">
        <div class="button-card-footer">
            @if(isset($rightBottom) && $rightBottom)
                {!! $rightBottom::getCardRightBottom($model) !!}
            @elseif(isset($rightBottomEmpty) && $rightBottomEmpty)
{{--                {!! $rightBottomContent !!}--}}
            @else
                <a href="{{ route('celebration.celebrate.details.show',[$model->slug]) }}" class="btn btn-main-color-light btn-sm btn-sm-main btn-click-detail" data-slug="{{ $model->slug }}" title="{{ vl('general','showDetails') }}">
                    <i class="fa fa-eye"></i>
                </a>
            @endif
        </div>
        <div class="button-card-footer pull-left">
            @if(isset($leftBottom) && $leftBottom)
                {!! $leftBottom::getCardLeftBottom($model) !!}
                @elseif(isset($leftBottomEmpty) && $leftBottomEmpty)
{{--                    {!! $leftBottomEmpty !!}--}}
                @else
                <a href="{{ route('celebration.card.index',[$model->slug]) }}" class="btn btn-main-color btn-sm btn-sm-main" title="{{ vl('general','enter') }}">
                    <i class="fa fa-sign-in-alt"></i>
                </a>
            @endif
        </div>

    </div>
</div>
<div class="mask"></div>
