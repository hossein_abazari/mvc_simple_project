<div class="table-responsive table-design">
    <table class="table table-striped">
        <thead>
        <tr>
            @foreach($inputs as $input)
                <th style="">{{ $input['label'] }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @if($models->isEmpty())
            <tr>
                <td colspan="7">
                    {{ vl('general','nothing') }}
                </td>
            </tr>
        @else
            @foreach($models as $model)
                <tr>
                    @foreach($inputs as $input)
                        <td>{{ $model->{$input['name']} }}</td>
                    @endforeach

                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

{!! \App\Classes\Paginate::renderS($models) !!}
