@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','messageGuests') }}
@endsection

@section('content')
    @include('partial.flash')

    <div class="page-content">
            <div class="col-md-12">
                @component('Celebration.Generals.cards')
                    @slot('data',[
                        'model' => $memories,
                        'title' =>'name',
                        'nameMessage' => 'guestName',
                        'field' => $field,
                        'contentClass' => 'col-12 message-guest',
                        'topButton' => false,
                        'rightBottomEmpty' => true,
                        'leftBottomEmpty' => true,
                        'search' => [
                            'url' => route('messages.card',['slug' => Request()->slug]),
                            'inputs' => [
                               [
                                    'name' => 'name_search',
                                    'type' => 'text',
                                    'label' => false,
                                    'placeholder' => vl('general','nameGuest'),
                                ],
                            ]
                        ]
                    ])
                @endcomponent
            </div>
    </div>
@endsection
