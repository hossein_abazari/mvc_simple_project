<div class="row">

    {{ \App\Classes\FormField::field('title','text',isset($gallery) ? $gallery->title: '' ,'col-md-12 mt-3','form-control','enterType',[]) }}

{{--    {{ \App\Classes\FormField::field('size','text',isset($gallery) ? $gallery->title: '' ,'col-md-6 mt-3','form-control','enterType',[]) }}--}}

{{--    {{ \App\Classes\FormField::field('file','file',isset($gallery) ? $gallery->title: '' ,'col-md-6 mt-3','form-control','enterType',[]) }}--}}
        @if(!isset($gallery))
          {{ \App\Classes\FormField::fieldUploaderFile('file','col-md-12 mt-3') }}
        @endif

</div>
