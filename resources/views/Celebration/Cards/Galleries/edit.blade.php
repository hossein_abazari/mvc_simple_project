@component('Generals.modal')
    @slot('route') {{ route('celebration.card.galleries.update',['slug'=>Request()->slug,'gallery'=>$gallery->slug]) }}  @endslot
    @slot('content')
        @method('PATCH')
        @component('Celebration.Cards.Galleries._form')
            @slot('gallery',$gallery)
        @endcomponent
    @endslot
    @slot('title') {!! $title !!}  @endslot
    @slot('buttonName') {{ vl('general','update') }}  @endslot
    @slot('nameModal') {!! 'edit-gallery' !!}  @endslot
    @slot('buttonClass') {{ 'btn-main-color' }}  @endslot

@endcomponent
