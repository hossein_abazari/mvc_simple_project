<button type="button" class="btn btn-main-color btn-sm btn-sm-main btn-click-edit" title="{{ vl('general','edit') }}"
        data-url="{{ route('celebration.card.galleries.edit',['slug'=>Request()->slug,'gallery'=>$model->slug]) }}"
        data-bs-toggle="dropdown">
    <i class="fa fa-edit "></i>
</button>

