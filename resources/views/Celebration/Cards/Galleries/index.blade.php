@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','galleries') }}
@endsection

@section('headerTop')
    <a class="btn btn-link pull-left text-main-color none-text-decoration" data-bs-toggle="modal" data-bs-target="#insert-gallery">
        {{ vl('general','insertGalleries') }}
        <i class="fa fa-plus"></i>
    </a>
@stop

@section('content')
    @include('partial.flash')

    @component('Celebration.Generals.cards')
        @slot('data',[
            'model' => $galleries,
            'classBeforeData' => 'mansonry',
            'classAfterData' => 'mansonry-item col-sm-3',
            'image' => [
                'name' => 'file',
                'directory' => 'galleries/',
                'directoryDynamic' => \App\Classes\GalleryTools::class,
                'style' => 'width:100%;',
            ],
            'title' =>'title',
         //   'field' => ['title' => false],
            'rightBottomEmpty' => true,
            //'leftBottomEmpty' => true,
            'leftBottom' => \App\Classes\GalleryTools::class,
            'topButton' => false,
        ])
    @endcomponent

    @component('Generals.modal')
        @slot('route') {!! route('celebration.card.galleries.store',['slug' => Request()->slug])!!}  @endslot
        @slot('content')
            @component('Celebration.Cards.Galleries._form')
            @endcomponent
        @endslot
        @slot('title') {!! vl('general','insertGalleries') !!}  @endslot
        @slot('nameModal') {!! 'insert-gallery' !!}  @endslot
        @slot('buttonName') {!! vl('general','create') !!}  @endslot
        @slot('buttonClass') {{ 'btn-main-color' }}  @endslot
{{--        @slot('sizeModal') {{ 'modal-lg' }}  @endslot--}}
        @slot('option',[
            'enctype' =>true
        ])
    @endcomponent

    <div class="content-modal">

    </div>


@endsection

@section('script')
    <script>
        $(document).on('click','.btn-click-edit',function (e) {
            e.preventDefault();
            var url = $(this).data('url');

            $.get(url,function (data) {
                $('.content-modal').html(data);
                $('#edit-gallery').modal('show')
            })
        })
    </script>
@endsection
