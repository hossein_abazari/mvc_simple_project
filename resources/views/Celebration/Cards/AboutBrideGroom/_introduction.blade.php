<div class="container mt-5 padding-container">
    <div class="style-box-holder">
        <div class="content-hold">
            <label class="style-label">{{ vl('general','moment_acquaintance') }}</label>
             @if(empty($introduction))
                @for($i = 1; $i <= 1; $i++)
                    @component('Celebration.Cards.AboutBrideGroom._row_introduction')
                        @slot('number',$i)
                    @endcomponent
                @endfor
                @else
                @foreach($introduction as $key => $value)
                    @component('Celebration.Cards.AboutBrideGroom._row_introduction')
                        @slot('number',$key +1)
                        @slot('aboutBrideGroom',$value)
                    @endcomponent
                    @endforeach
            @endif
        </div>

        <button type="button" class="btn btn-main-color btn-sm-main mt-5 btn-add">
            <i class="fa fa-plus"></i>
        </button>
    </div>
</div>
