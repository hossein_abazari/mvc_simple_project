<div class="row intro-row">

{{--    <div class="mt-3 label-number">{{ $number }}</div>--}}

    {{ \App\Classes\FormField::field('date[]','text',isset($aboutBrideGroom['date']) ? $aboutBrideGroom['date'] : '' ,'col-md-3 mt-3','form-control datePicker datePicker'.$number,'enterType',['id'=>'datePicker'.$number]) }}

    {{ \App\Classes\FormField::field('title[]','text',isset($aboutBrideGroom['title']) ? $aboutBrideGroom['title'] : '' ,'col-md-3 mt-3','form-control','enterType',[]) }}

    {{ \App\Classes\FormField::field('body[]','text',isset($aboutBrideGroom['body']) ? $aboutBrideGroom['body'] : '' ,'col-md-6 mt-3','form-control','enterType',[]) }}

</div>


@section('scriptComponent')
    <script>
        $(".datePicker").persianDatepicker();
    </script>
@endsection


<script>
    $(".datePicker{{ $number }}").persianDatepicker();
</script>
