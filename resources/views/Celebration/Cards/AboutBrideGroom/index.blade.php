@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','aboutBrideGroom') }}
@endsection

@section('content')
    @include('partial.flash')
    <div class="alert alert-main-color-light">{{ vl('general','tip_about') }}</div>
    <form method="POST" action="{{ route('about.bride.groom.store',['slug' => Request()->slug]) }}" class="form" enctype="multipart/form-data">
        @csrf
        <div class="row">
        {{ \App\Classes\FormField::fieldUploaderAvatar('img_groom','col-md-6 mt-3',[
        'url'=>isset($aboutBrideGroom->img_groom) ? \App\Classes\UploadFile::$directory.'about/'.$aboutBrideGroom->celebrate_detail_id.'/'.$aboutBrideGroom->img_groom :
        asset(\App\Classes\UploadFile::$directory.'about/groom_default.png'),
        'label' => vl('general','groom'),
        'labelClass' => 'badge bg-gray-light label-upload label-groom',
        ]) }}

        {{ \App\Classes\FormField::fieldUploaderAvatar('img_bride','col-md-6 mt-3',[
        'url'=>isset($aboutBrideGroom->img_bride) ? \App\Classes\UploadFile::$directory.'about/'.$aboutBrideGroom->celebrate_detail_id.'/'.$aboutBrideGroom->img_bride :
        asset(\App\Classes\UploadFile::$directory.'about/bride_default.png'),
        'label' => vl('general','bride'),
        'labelClass' => 'badge bg-gray-light label-upload label-bride',
        ]) }}

        {{ \App\Classes\FormField::field('name_groom','text',isset($aboutBrideGroom) ? $aboutBrideGroom->name_groom: '' ,'col-md-6 mt-3','form-control','enterType',[]) }}

         {{ \App\Classes\FormField::field('name_bride','text',isset($aboutBrideGroom) ? $aboutBrideGroom->name_bride: '' ,'col-md-6 mt-3','form-control','enterType',[]) }}

        {{ \App\Classes\FormField::field('description_groom','text',isset($aboutBrideGroom) ? $aboutBrideGroom->description_groom: '' ,'col-md-6 mt-3','form-control','enterType',[]) }}

         {{ \App\Classes\FormField::field('description_bride','text',isset($aboutBrideGroom) ? $aboutBrideGroom->description_bride: '' ,'col-md-6 mt-3','form-control','enterType',[]) }}

        @component('Celebration.Cards.AboutBrideGroom._introduction')
            @slot('introduction',isset($aboutBrideGroom->introduction)? $aboutBrideGroom->introduction : [])
            @endcomponent

            {{ \App\Classes\FormField::fieldButton(vl('general','confirmation'),'btn btn-main-color','row mt-4 mb-4',true) }}

    </div>
    </form>

@endsection

@section('script')

    <script>
        $(document).on('click','.btn-add',function () {
            var url = `{{ route('about.bride.groom.introduction',['slug' =>Request()->slug]) }}`;
            let number = $('.intro-row').length;
            $.get(url,{'number':number},function ($data) {
                $('.content-hold').append($data);
            });

        })

    </script>
@endsection

