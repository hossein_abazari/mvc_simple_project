@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','guestInvited') }} : {{$guestsSum}} {{ vl('general','n') }}
@endsection

@section('headerTop')
    <a class="btn btn-link pull-left text-main-color none-text-decoration" data-bs-toggle="modal" data-bs-target="#insert-guest">
        {{ vl('general','inviteGuest') }}
        <i class="fa fa-plus"></i>
    </a>
@stop

@section('content')
    @include('partial.flash')

    <div class="page-content">
            <div class="col-md-12">
                @component('Celebration.Generals.cards')
                    @slot('data',[
                        'model' => $guests,
                        'title' =>'name_fa',
                        'type' =>'family',
                        'field' => $field,
                        'topButton' => false,
                        'rightBottom' => \App\Classes\Helper::class,
                        'leftBottom' => \App\Classes\Helper::class,
                        'search' => [
                            'url' => route('celebration.card.index',['slug' => Request()->slug]),
                            'inputs' => [
                               [
                                    'name' => 'name_search',
                                    'type' => 'text',
                                    'label' => false,
                                    'placeholder' => vl('general','nameGuest'),
                                ],
                                [
                                    'name' => 'family_search',
                                    'type' => 'select',
                                    'label' => false,
                                    'value' => [
                                            'groom' => vl('general','groom'),
                                            'bride' => vl('general','bride'),
                                            ],
                                    'placeholder' => vl('general','allFamily'),
                                ],
                                [
                                    'name' => 'message_search',
                                    'type' => 'select',
                                    'label' => false,
                                    'value' => [
                                            'allMessage' => vl('general','allMessage'),
                                            'openMessage' => vl('general','openMessage'),
                                            'newMessage' => vl('general','newMessage'),
                                            ],
                                    'placeholder' => vl('general','all'),
                                ],

                            ]
                        ]
                    ])
                @endcomponent
            </div>
    </div>

    @component('Generals.modal')
        @slot('route') {!! route('celebration.card.store',['slug' => Request()->slug])!!}  @endslot
        @slot('content')
            @component('Celebration.Cards.Guests._form')
                @endcomponent
        @endslot
        @slot('title') {!! vl('general','insertNewGuest') !!}  @endslot
        @slot('nameModal') {!! 'insert-guest' !!}  @endslot
        @slot('buttonName') {!! vl('general','create') !!}  @endslot
        @slot('buttonClass') {{ 'btn-main-color' }}  @endslot
    @endcomponent

    <div class="content-guest-modal">

    </div>

@endsection


@section('script')
    <script>
        $(document).on('click','.btn-copied',function (e) {
            e.preventDefault();
            var text = $(this).closest('.link-slug').find('.link').val();
            var $txt = $('<textarea />');
            $txt.val(text).css({ width: "1px", height: "1px" }).appendTo('body');
            $txt.select();
            var copied = $(this).closest('.button-card-footer').find('.copied');
            if (document.execCommand('copy')) {
                $txt.remove();
                copied.show();
                copied.delay(3000).slideUp(500);
            }



        });

        $(document).on('click','.btn-click-edit',function (e) {
            e.preventDefault();
            var url = $(this).data('url');

            $.get(url,function (data) {
                $('.content-guest-modal').html(data);
                $('#edit-guest').modal('show')
            })
        })
    </script>
@endsection
