@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','events_ei') }}  {{ $guest->name }}
@endsection

@section('content')
    <div class="page-content">
        {{--        @include('partial.flash)--}}

        <div class="container font3" style="{{ vl('general','direction') }}">
            <div class="row">
                <div class="col-md-12">
                    @component('Celebration.Generals.table')
                            @slot('models' , $logs )
                            @slot('inputs',[
                                [
                                    'name' => 'url',
                                    'label' => vl('general','url_page'),
                                ],
                                [
                                    'name' => 'ip_address',
                                    'label' => vl('general','ip'),
                                ],
                                [
                                    'name' => 'date_created_at',
                                    'label' => vl('general','date'),
                                    'content' => '',
                                ],
                                [
                                    'name' => 'time_created_at',
                                    'label' => vl('general','time'),
                                ],
                            ])
                        @endcomponent

                </div>
            </div>

        </div>
    </div>

@endsection

