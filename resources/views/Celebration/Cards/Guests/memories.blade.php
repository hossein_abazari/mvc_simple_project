@extends('Celebration.layout.main')
@section('title')
    {{ vl('general','messages_ei') }}  {{ $guest->name }}
@endsection

@section('content')


    @if ($memories->isNotEmpty())
        <ul class="chat-list p-20">
        @foreach($memories as $memory)
            @if ($memory->status == 0)
                @php
                    $memory->status = 1;
                     $memory->update();
                @endphp
            @endif


                <li class="reverse box-card-message bg-gray-light">
                    <div class="chat-img "><img src="{{ asset('images/users/default_user.png') }}" alt="user" /></div>
                    <div class="chat-content">
                        <h5><a>{{ $guest->name }}</a></h5>
                        <div class="box">{{ $memory->message }}</div>
                    </div>
                    <div class="chat-time">{{ $memory->formatTypeDate($memory->created_at,'Y/m/d H:i a') }}</div>
                </li>

        @endforeach
        </ul>
    @else
        <li class="">
            {{ vl('general' , 'nothingMessage') }}
        </li>
    @endif
@endsection

