@component('Generals.modal')
    @slot('route') {{ route('celebration.card.update',['slug'=>Request()->slug,'guest'=>$guest->slug]) }}  @endslot
    @slot('content')
        @method('PATCH')
        @component('Celebration.Cards.Guests._form')
            @slot('guest',$guest)
        @endcomponent
    @endslot
    @slot('title') {!! $title !!}  @endslot
    @slot('buttonName') {{ vl('general','update') }}  @endslot
    @slot('nameModal') {!! 'edit-guest' !!}  @endslot
    @slot('buttonClass') {{ 'btn-main-color' }}  @endslot
    @slot('sizeModal') {{ 'modal-lg' }}  @endslot

@endcomponent
