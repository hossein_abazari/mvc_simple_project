<div class="link-slug">
    <input type="hidden" class="link" value="{{ Request::root()."/".$model->celebrateDetail->link_name."/".$model->slug }}
        ،،،،،کارت دعوت {{vl('general',$model->celebrateDetail->type_celebrate)}} {{ $model->celebrateDetail->name }}
            لطفا روی لینک کلیک نموده و از تمام منو ها بازدید نمایید">
    <span class="link"> </span>
    <a href="#" class="btn btn-main-color-light btn-sm btn-sm-main btn-copied "  title="{{ vl('general','copyLink') }}"><i class="fa fa-copy"></i>
        <span style="font-size: 11px; padding-right: 6px">
            {{ vl('general','inviteLink') }}
        </span>
    </a>
</div>
<div class="copied" style="display: none">
    {{ vl('general','copied') }}
</div>
