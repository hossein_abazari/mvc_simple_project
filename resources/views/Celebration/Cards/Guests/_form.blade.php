<div class="row">
    @if(!isset($guest))
         <div class="col-md-12">
        <div class="form-group">
            <label for="slug" class="black-color">{{ vl('general','linkGuest') }}</label>
            <input type="text" name="slug_name" class="form-control" id="slug"
                   value="{{ isset($guest) ? $guest->slug: '' }}"
                   placeholder="{{ vl('general','enterType') }}">
        </div>
    </div>
    @endif
    <div class="col-md-6 mt-3">
        <div class="form-group">
            <label for="name" class="black-color">{{ vl('general','nameGuest') }}</label>
            <input type="text"  name="name" class="form-control" id="name"
                   value="{{ isset($guest) ? $guest->name: '' }}"
                   placeholder="{{ vl('general','enterType') }}">
        </div>
    </div>
    <div class="col-md-6 mt-3">
        <div class="form-group">
            <label for="extra_name" class="black-color">{{ vl('general','extra_name') }}</label>
            <input type="text" name="extra_name" class="form-control" id="extra_name"
                   value="{{ isset($guest) ? $guest->extra_name: '' }}"
                   placeholder="{{ vl('general','enterType') }}">
        </div>
    </div>

    <div class="col-md-6 mt-3">
        <div class="form-group">
            <label for="slug" class="black-color">{{ vl('general','family') }}</label>
            <select name="family" class="form-control">
                <option {{ isset($guest) && $guest->family == 'groom' ? 'selected' : '' }} value="groom">{{ vl('general' , 'groom') }}</option>
                <option {{ isset($guest) && $guest->family == 'bride' ? 'selected' : '' }} value="bride">{{ vl('general' , 'bride') }}</option>
            </select>
        </div>
    </div>

    <div class="col-md-6 mt-3">
        <div class="form-group">
            <label for="slug" class="black-color">{{ vl('general','count_family') }}</label>
            <input type="text" name="count_family" class="form-control"
                   value="{{ isset($guest) ? $guest->count_family : 1 }}"
                   placeholder="{{ vl('general','enterType') }}">
        </div>
    </div>

</div>
