@if($model->memories()->whereStatus(0)->count() > 0)
    <a href="{{ route('celebration.card.show',['slug'=>Request()->slug,'guest'=>$model->slug]) }}">
    <div class="badge bg-Main-color top-message" title="{{ vl('general','newMessage') }}">
        <i class="fa fa-envelope"></i>
        <span class="number-message">{{ $model->memories()->whereStatus(0)->count() }}</span>
    </div>
    </a>
    @elseif($model->memories()->count() > 0)
    <div class="badge bg-Main-color opacity-0-4 top-message" title="{{ vl('general','seeMessage') }}">
        <i class="fa fa-envelope-open"></i>
    </div>
@endif


<button type="button" class="btn btn-main-color btn-sm btn-sm-main dropdown-toggle btn-click-drop" data-bs-toggle="dropdown">
    <i style="color: var(--main-color)" class="fa fa-ellipsis-vertical"></i>
</button>

<ul class="dropdown-menu card-button">
    <li><a class="dropdown-item btn-click-edit" data-url="{{ route('celebration.card.edit',['slug'=>Request()->slug,'guest'=>$model->slug]) }}" href="#"><i class="fa fa-edit"></i> {{vl('general','edit')}}</a></li>
{{--    <li><a class="dropdown-item" href="#"><i class="fa fa-trash"></i> {{vl('general','delete')}}</a></li>--}}
    <li><a class="dropdown-item" href="{{ route('celebration.card.show',['slug'=>Request()->slug,'guest'=>$model->slug]) }}"><i class="fa fa-message"></i> {{ vl('general','messages') }}
            @if($model->memories()->count() > 0)
                  <span class="badge bg-Main-color badge-new-class">{{ $model->memories()->count() }}</span>
             @endif
        </a>
    </li>
    <li><a class="dropdown-item" href="{{ route('celebration.card.log',['slug'=>Request()->slug,'guest'=>$model->slug]) }}"><i class="fa fa-right-from-bracket"></i> {{ vl('general','events_ei') }}</a></li>

    <li><a class="dropdown-item" target="_blank" href="{{ Request::root()."/".$model->celebrateDetail->link_name."/".$model->slug }}"><i class="fa fa-eye"></i> {{ vl('general','open_card') }}</a></li>
</ul>
@section('scriptComponent')
<script>
    $(document).on('click','.btn-click-drop',function () {
        $('.card-item').css('z-index','0');
        $(this).closest('.card-item').css('z-index','1');
    })
</script>
@endsection
