@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','settingCelebrate') }}
@endsection

@section('content')
    @include('partial.flash')

    <form method="POST" action="{{ route('celebrate.setting.update',['slug' => Request()->slug]) }}" class="form" enctype="multipart/form-data">
        @csrf
        <div class="row">
            {{ \App\Classes\FormField::field('name','text',isset($celebrateDetail) ? $celebrateDetail->name: '' ,'col-md-3 mt-3','form-control','enterType',[]) }}
            {{ \App\Classes\FormField::field('name_logo','text',isset($celebrateDetail) ? $celebrateDetail->name_logo: '' ,'col-md-3 mt-3','form-control','enterType',[]) }}

            {{ \App\Classes\FormField::field('date_ceremony','text',isset($celebrateDetail) ? \App\Classes\Helper::printDateTime($celebrateDetail->date_ceremony,'Y/m/d'): '' ,'col-md-3 mt-3','form-control datePicker','enterType',[]) }}
            {{ \App\Classes\FormField::field('time_ceremony','text',isset($celebrateDetail) ? \App\Classes\Helper::printDateTime($celebrateDetail->date_ceremony,'H:i'): '' ,'col-md-3 mt-3','form-control','enterType',[]) }}

            {{ \App\Classes\FormField::fieldUploaderFile('background_img','col-md-12 mt-3',[
            'labelSelectImage' => vl('general','background_img_select'),
            'nameImage' => isset($celebrateDetail->background_img) ? $celebrateDetail->background_img : '',
            'image' =>  isset($celebrateDetail->background_img) ?
            \App\Classes\UploadFile::$directory.$celebrateDetail->id.'/'.$celebrateDetail->background_img: null
            ]) }}

            <div class="style-box-holder mt-5">
                <div class="content-hold card-celebrate-setting">
                    <label class="style-label" style="margin-top: -54px;">{{ vl('general','cardDetails') }}</label>
                        <div class="row ">
                            {{ \App\Classes\FormField::fieldSelect(
                            isset(vl('sample_name')[$celebrateDetail->text_invite]) ? 'text_invite' : 'text_invite_select',
                            vl('sample_name'),
                            'col-md-6 mt-3',
                            'form-control',
                            'text_invite_select',
                            ['id' => 'text-invite-select'],
                                isset(vl('sample_name')[$celebrateDetail->text_invite]) ? $celebrateDetail->text_invite : (isset($celebrateDetail->text_invite) ? "custom" :false)
                            ) }}

                            {{ \App\Classes\FormField::field('text_invite',
                            'text',
                            !isset(vl('sample_name')[$celebrateDetail->text_invite]) ? $celebrateDetail->text_invite : '' ,
                            !isset(vl('sample_name')[$celebrateDetail->text_invite]) ? 'col-md-6 mt-3 text-invite' : 'col-md-6 mt-3 d-none text-invite',
                            'form-control',
                            'enterType',
                            !isset(vl('sample_name')[$celebrateDetail->text_invite]) ? [] : ['disabled' =>'disabled']
                            ) }}
                        </div>
                        <div class="row ">
                            {{ \App\Classes\FormField::fieldSelect(
                             isset(vl('sample_name')[$celebrateDetail->text_invite]) ? 'text_footer' : 'text_footer_select',
                            vl('text_footer'),
                            'col-md-6 mt-3',
                            'form-control',
                            'text_footer_select',
                            ['id' => 'text-footer-select'],
                             isset(vl('text_footer')[$celebrateDetail->text_footer]) ? $celebrateDetail->text_footer : (isset($celebrateDetail->text_invite) ? "custom" :false)
                            ) }}

                            {{ \App\Classes\FormField::field('text_footer',
                            'text',
                            !isset(vl('text_footer')[$celebrateDetail->text_footer]) ? $celebrateDetail->text_footer : '' ,
                            !isset(vl('text_footer')[$celebrateDetail->text_footer]) ? 'col-md-6 mt-3 text-footer' : 'col-md-6 mt-3 d-none text-footer',
                            'form-control',
                            'enterType',
                            !isset(vl('text_footer')[$celebrateDetail->text_footer]) ? [] : ['disabled' =>'disabled']

                            ) }}

                        </div>
                        <div class="row ">
                        {{ \App\Classes\FormField::fieldCheckbox('survey',/* name */
                                    '1', /* value */
                                    'col-md-6 mt-5', /* first div class */
                                     'check-survey pull-right', /* input class */
                                    [], /* set other option like id, data-id */
                                    vl('general','DoYouNeedASurvey'), /* set label*/
                                    false, /* second class for form group */
                                    isset($celebrateDetail) && $celebrateDetail->survey ? true: false, /* is checked */
                                    'checkbox' /* type radio or checkbox */
                                    ) }}

                        {{ \App\Classes\FormField::field('text_survey','text',isset($celebrateDetail) ? $celebrateDetail->text_survey: '' ,isset($celebrateDetail) && $celebrateDetail->survey ? 'col-md-6 mt-3 text-survey': 'col-md-6 mt-3 text-survey d-none','','enterType',[]) }}
                        </div>
                        <div class="row">
                        {{ \App\Classes\FormField::fieldCheckbox('memories',/* name */
                       '1', /* value */
                       'col-md-6 mt-5', /* first div class */
                        'memories pull-right', /* input class */
                       [], /* set other option like id, data-id */
                       vl('general','DoYouNeedToWriteMemoirs'), /* set label*/
                       false, /* second class for form group */
                       isset($celebrateDetail) && $celebrateDetail->memories ? true: false, /* is checked */
                       'checkbox' /* type radio or checkbox */
                       ) }}

                        {{ \App\Classes\FormField::fieldCheckbox('there_is_gallery',/* name */
                                     '1', /* value */
                                     'col-md-6 mt-5', /* first div class */
                                      'pull-right', /* input class */
                                     [], /* set other option like id, data-id */
                                     vl('general','there_is_gallery'), /* set label*/
                                     false, /* second class for form group */
                                     isset($celebrateDetail) && $celebrateDetail->there_is_gallery ? true: false, /* is checked */
                                     'checkbox' /* type radio or checkbox */
                                     ) }}
                    </div>
                </div>
        </div>

            <div class="style-box-holder mt-5">
                <div class="content-hold">
                    <label class="style-label" style="margin-top: -54px;">{{ vl('general','venue_ceremony') }}</label>
                    <div class="row ">
                        {{ \App\Classes\FormField::field('venue_ceremony','text',isset($celebrateDetail) ? $celebrateDetail->venue_ceremony: '' ,'col-md-6 mt-3','form-control','enterType',[]) }}

                        {{ \App\Classes\FormField::field('address','text',isset($celebrateDetail) ? $celebrateDetail->address: '' ,'col-md-6 mt-3','form-control','enterType',[]) }}
                        {{-- set google map for this item --}}
                        {{ \App\Classes\FormField::field('location','text',isset($celebrateDetail) ? $celebrateDetail->location: '' ,'col-md-12 mt-3','form-control','enterType',[]) }}
                    </div>
                </div>
            </div>

            {{ \App\Classes\FormField::fieldButton(vl('general','confirmation'),'btn btn-main-color','row mt-4 mb-4',true) }}

        </div>
    </form>

@endsection

@section('script')
        <script>
            $(".datePicker").persianDatepicker();
            $(document).on('change','.check-survey',function () {
                var is_checked = $(this).is(':checked');
                if (is_checked) {
                    $('.text-survey').removeClass('d-none');
                }
                else {
                    $('.text-survey').addClass('d-none');
                }
            });

            $(document).on('change','#text-invite-select',function () {
                hideShowSelectToInput($(this),'invite');
            });

            $(document).on('change','#text-footer-select',function () {
                hideShowSelectToInput($(this),'footer');
            });

            function hideShowSelectToInput(elm,name) {
                var value = elm.val();

                if (value === 'custom') {
                    elm.attr('name','text_'+name+'_select');
                    $('.text-'+name+' input').prop('disabled', false);
                    $('.text-'+name).removeClass('d-none');
                }
                else {
                    elm.attr('name','text_'+name);
                    $('.text-'+name+' input').prop('disabled', true);
                    $('.text-'+name).addClass('d-none');
                }
            }

        </script>
    @endsection

