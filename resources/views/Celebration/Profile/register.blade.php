@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','register') }}
@endsection
@section('nameIcon')
{{'edit'}}
@endsection

@section('content')
    {{ \App\Classes\FormField::openForm(route('celebration.profile.register.store'),'POST') }}
    <div class="row">

        {{ \App\Classes\FormField::field('name','text','','col-4','form-control','name',[],false,'form-group') }}

        {{ \App\Classes\FormField::field('referral','text','','col-4','form-control','referral',[],false,'form-group') }}

        {{ \App\Classes\FormField::fieldButton(vl('general','continue'),'btn btn-main-color','col-3',true) }}

    </div>
    {{ \App\Classes\FormField::endForm() }}

@endsection

