   @include('partial.flash')
    @component('Generals.wizard')
        @slot('data',[
            'step1'=>[
                'inputs' => [
                    'type_celebrate' => [
                        'type' => 'select',
                        'class' => 'col-md-6',
                        'value' => \App\Models\CelebrateDetail::getSelectType(),
                          'validate_form' => [
                            'required' => true,
                        ],
                        'option' => ['label' => false,'id' => 'type-celebrate-details']
                    ],
                    'name' => [
                        'type' => 'text',
                        'class' => 'col-md-6 d-none name-display',
                        'classInput' => 'name-input',
                         'validate_form' => [
                            'required' => true,
                        ],
                    ],

                ],
                'step_name' => vl('general','celebration'),
                'title' => vl('general','selectTypeCelebration'),
                'titleClass' => 'title-type-celebrate',
            ],
            'step2'=>[
                    'inputs' => [
                        'text_invite_select' => [
                            'type' => 'select',
                            'class' => 'col-md-6',
                            'value' => vl('sample_name'),
                            'option' => ['label' => false,'id' => 'text-invite-select']
                        ],
                        'text_invite' => [
                            'type' => 'text',
                            'class' => 'col-md-6 d-none text-invite',
                            'option' => ['disabled' =>'disabled']
                        ],
                        'text_footer_select' => [
                            'type' => 'select',
                            'class' => 'col-md-6',
                            'value' => vl('text_footer'),
                            'option' => ['label' => false,'id' => 'text-footer-select']
                        ],
                        'text_footer' => [
                            'type' => 'text',
                            'class' => 'col-md-6 d-none text-footer',
                            'option' => ['disabled' =>'disabled']
                        ],
                        'memories' => [
                            'type' => 'checkbox',
                            'class' => 'col-md-6',
                            'label' => 'DoYouNeedToWriteMemoirs',
                        ],
                        'survey' => [
                            'type' => 'checkbox',
                            'class' => 'col-md-6',
                            'label' => 'DoYouNeedASurvey',
                            'classInput' => 'check-survey'
                        ],
                        'text_survey' => [
                            'type' => 'text',
                            'class' => 'col-md-6 d-none text-survey',
                        ],
                        'link_name' => [
                            'type' => 'text',
                            'class' => 'col-md-6',
                            'option' => [
                               'text_manual' => 'nameLinkDescription'
                            ],
                             'validate_form' => [
                                'authentic_english_text' => true,
                                'required' => true,
                            ],
                        ],
                ],

                'step_name' => vl('general','cardDetails'),
                'title' => '',
                'validate_form' => [
                        'required' => true,
                    ],
            ],

            'step3'=>[
                'inputs' => [
                    'venue_ceremony' => [
                        'type' => 'text',
                        'class' => 'col-sm-4',
                    ],
                    'date_ceremony' => [
                        'type' => 'text',
                        'class' => 'col-sm-4',
                        'classInput' => 'datePicker',
                    ],
                    'time_ceremony' => [
                        'type' => 'text',
                        'class' => 'col-sm-4',
                        'option' => [
                            'value' => \Carbon\Carbon::now()->format('H:i')
                        ]
                    ],
                    'address' => [
                        'type' => 'text',
                        'class' => 'col-md-12',
                    ],
                ],

                'step_name' => vl('general','addressAndLocal'),
                'title' => '',

            ],
            'step4'=>[
                'inputs' => [
                    'layout_id' => [
                        'type' => 'dynamic',
                        'class' => 'col-md-6',
                        'content' => $templateLayout->getSamples(),
                        'classContent' => 'content-wizard',
                        'validate_form' => [
                            'required' => true,
                        ],
                    ],
                ],

                'step_name' => vl('general','template'),
                'title' => '',
            ],
            'step5'=>[
                'inputs' => [
                    'card_pack' => [
                        'type' => 'dynamic',
                        'class' => 'col-md-6',
                        'content' => $templatePackCard->getSamples()
                    ],
                ],

                'step_name' => vl('general','pack'),
                'title' => '',
            ],
            'finish'=>[
            'content' => '<span class="">'.vl('general' ,'congratulationsText' ).'</span>',
            'step_name' => vl('general','finish'),
            'title' => '',
            ],

        ])
        @slot('validate' , [
        true
        ])
        @slot('route' , route('celebration.celebrate.details.store'))
        @slot('button_labels',[
          'previous' => vl('general','previous'),
          'next' => vl('general','next'),
          'finish' => vl('general','insert'),
          'current' => '' ,
      ])
        @slot('classForm' , 'mb-xl-1')
        @slot('nameCSS' , 'celebrate_details')
    @endcomponent


    {{--   st--}}
@section('cssComponent')
    <style>
        @media (max-width: 990px) {
            .steps{
                display: none;
            }

            .content .current {
                padding-top: 10px;
            }

            .content {
                overflow: inherit !important;
            }
            .select-radio-card {
                font-size: 9px !important;
            }
            .name-card {
                font-size: 9px !important;
            }
        }

        @media (min-width: 576px) and (max-width: 780px){
            .card-image-item.col-sm-4 {
                flex: 0 0 50%;
                max-width: 50%;
            }
        }
    </style>
@endsection

@section('scriptComponent')
    <script src="{{ asset('js/script.ajax.js') }}"></script>
    <script>

        function displayOperation(none = 1 , none1 = 1 , none2 = 1){
            if (none === 1) {
                $('.name-display').addClass('d-none');
            }
            else{
                $('.name-display').removeClass('d-none');
            }

            // if (none1 === 1) {
            //     $('.name-groom-display').addClass('d-none');
            // }
            // else{
            //     $('.name-groom-display').removeClass('d-none');
            // }
            //
            // if (none2 === 1) {
            //     $('.name-bride-display').addClass('d-none');
            // }
            // else{
            //     $('.name-bride-display').removeClass('d-none');
            // }
        }

        $(document).on('change','#type-celebrate-details',function () {
            var type = $(this).val();

            var type_wedding = `{{ \App\Classes\TypeCelebration::TYPE_WEDDING }}`;
            var type_birth = `{{ \App\Classes\TypeCelebration::TYPE_BIRTH }}`;
            var type_other = `{{ \App\Classes\TypeCelebration::TYPE_OTHER }}`;

            if(type === type_wedding){
                $('.name-input').attr('placeholder',`{{ vl('general','nameGroomAndBride') }}`);
                displayOperation(0)
            }
            else if(type === type_birth || type === type_other){
                $('.name-input').attr('placeholder',`{{ vl('general','name') }}`);
                displayOperation(0);
            }
            else {
                $('.name-input').attr('placeholder',`{{ vl('general','name') }}`);
                displayOperation();
            }

            getSampleLayout(type)
        })

        $(document).on('change','.check-survey',function () {
            var is_checked = $(this).is(':checked');
            if (is_checked) {
                $('.text-survey').removeClass('d-none');
            }
            else {
                $('.text-survey').addClass('d-none');
            }
        });


        function getSampleLayout($type) {
            var url = `{{ route('celebration.details.layout') }}`;

            $.get(url,{'type':$type},function (data) {
                $('.content-wizard').html(data);
            })
        }

        $(document).on('change','#text-invite-select',function () {
            hideShowSelectToInput($(this),'invite');
        });

        $(document).on('change','#text-footer-select',function () {
            hideShowSelectToInput($(this),'footer');
        });

        function hideShowSelectToInput(elm,name) {
            var value = elm.val();

            if (value === 'custom') {
                elm.attr('name','text_'+name+'_select');
                $('.text-'+name+' input').prop('disabled', false);
                $('.text-'+name).removeClass('d-none');
            }
            else {
                elm.attr('name','text_'+name);
                $('.text-'+name+' input').prop('disabled', true);
                $('.text-'+name).addClass('d-none');
            }
        }

        $(document).on('click','.finish',function (e) {
            e.preventDefault();
            ajaxJquery($(this),false,2)
        });
    </script>
@endsection
