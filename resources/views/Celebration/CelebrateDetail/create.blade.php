@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','newOrder') }}
@endsection
@section('nameIcon')
    {{'edit'}}
@endsection

@section('content')
    @component('Celebration.CelebrateDetail._form_wizard')
        @slot('templatePackCard' , $templatePackCard)
        @slot('templateLayout' , $templateLayout)
    @endcomponent

@endsection
