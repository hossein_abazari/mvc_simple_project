@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','youCardsInvite') }}
@endsection
@section('nameIcon')
    {{'edit'}}
@endsection

@section('content')
    @include('partial.flash')
    @component('Celebration.Generals.cards')
            @slot('data',[
                'model' => $celebrateDetails,
                'image' => [
                    'name' => 'image_layout',
                    'directory' => 'templates/',
                    'directoryDynamic' => '',
                    'style' => 'width:100%;',
                ],
                'title' =>'name',
                'type' =>'type_celebrate',
                'field' => $field,
            ])
        @endcomponent
@endsection

