<div class="picture-legend">
    <div class="row">
        <div class="col-md-12 text-center text-title-card">
            <span>{{ $celebrateDetail->name }}</span>
        </div>
    </div>
    <div class="row detail-card-show">
        <div class="col-12 name-card">
            <p class="mt-2">
                <b>{{ vl('general','text_invite') }}</b> : {{ $celebrateDetail->text_invite_show }}
            </p>
        </div>
        <div class="col-12 name-card">
            <p class="mt-2">
                <b>{{ vl('general','text_footer') }} </b> : {{ $celebrateDetail->text_footer_show }}
            </p>
        </div>
        <div class="col-4 name-card">
            <p class="mt-2">
                <b>{{ vl('general','survey') }} </b> : {{ vl('method_boolean')[$celebrateDetail->survey] }}
            </p>
        </div>
        @if($celebrateDetail->survey == true)
        <div class="col-8 name-card">
            <p class="mt-2">
                <b>{{ vl('general','text_survey') }} </b> : {{ $celebrateDetail->text_survey }}
            </p>
        </div>
        @endif
        <div class="col-4 name-card">
            <p class="mt-2">
                <b>{{ vl('general','memories') }} </b> : {{ vl('method_boolean')[$celebrateDetail->memories] }}
            </p>
        </div>
        <div class="col-6 name-card">
            <p class="mt-2">
                <b>{{ vl('general','link_name') }} </b> : {{ $celebrateDetail->link_name }}
            </p>
        </div>
        <div class="col-12 name-card">
            <p class="mt-2">
                <b>{{ vl('general','address') }} </b> : {{ $celebrateDetail->address }}
            </p>
        </div>
        <div class="col-12 name-card">
            <p class="mt-2">
                <b>{{ vl('general','status') }} </b> :
                {!! toDebtCredit($celebrateDetail->transactionDetail()->sum('total_amount'),
                route('celebration.payment',[$celebrateDetail->slug])
                ) !!}
            </p>
        </div>

    </div>
</div>
<div class="row mt-2 mb-2">
    <div class="form-group">
        <div class="button-card-footer">
            <a href="{{ route('celebration.details.card',[$celebrateDetail->slug]) }}" class="btn btn-main-color-light btn-sm btn-sm-main btn-click-card" title="{{ vl('general','back') }}">
                <i class="fa fa-backspace"></i>
            </a>
        </div>
    </div>
</div>
<div class="mask"></div>
