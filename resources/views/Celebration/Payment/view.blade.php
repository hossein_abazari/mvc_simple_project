@extends('Celebration.layout.main')

@section('title')
    {{ vl('general','cardPayment')." ".
    \App\Models\CelebrateDetail::getSelectType()[$celebrateDetail->type_celebrate]." ".
    $celebrateDetail->name
     }}
@endsection
@section('nameIcon')
{{'credit-card'}}
@endsection

@section('content')
    @component('Celebration.Payment._view')
        @slot('celebrateDetail',$celebrateDetail)
    @endcomponent
@endsection

