<div class="row mt-4 mb-4">
    <div class="body-section">
        <div class="row">
            <div class="col-6">
                <h2 class="heading">{{vl('general','invoice_code')}}:
                    @foreach($celebrateDetail->invoiceDetail()->get() as $invoiceDetail)
                        {{ ','.$invoiceDetail->invoice->invoice_code }}
                    @endforeach
                </h2>
                <p class="sub-heading">{{ vl('general','pricePaid') }} : {{ $celebrateDetail->total_amount }}</p>
                {{--            <p class="sub-heading">Order Date: 20-20-2021 </p>--}}
                {{--            <p class="sub-heading">Email Address: customer@gfmail.com </p>--}}
            </div>
            <div class="col-6">
                <p class="sub-heading">{{ vl('general','name') }}: {{ Auth::getUser()->celebration->name }}  </p>
                <p class="sub-heading text-danger">{{ vl('general','amountPayable') }} : {{ number_format($celebrateDetail->transactionDetail()->sum('total_amount')) }}</p>
                {{--            <p class="sub-heading">Phone Number:  </p>--}}
                {{--            <p class="sub-heading">City,State,Pincode:  </p>--}}
            </div>
        </div>
    </div>

    <div class="body-section">
{{--        <h3 class="heading">Ordered Items</h3>--}}
        <br>
        <table class="table-bordered">
            <thead>
            <tr>
                <th>{{ vl('general','productName') }}</th>
                <th class="w-20">{{ vl('general','price') }}</th>
                <th class="w-20">{{ vl('general','Quantity') }}</th>
                <th class="w-20">{{ vl('general','total_amount') }}</th>
                <th class="w-20">{{ vl('general','paymentStatus') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($celebrateDetail->invoiceDetail()->get() as $invoiceDetail)
                <tr>
                    <td>{{ $invoiceDetail->invoice->name }}</td>
                    <td>{{ $invoiceDetail->amount_str }}</td>
                    <td>{{ $invoiceDetail->number }}</td>
                    <td>{{ $invoiceDetail->total_amount_str }}</td>
                    <td>{!! \App\Traits\PaymentTrait::setTransaction($invoiceDetail->invoice->transactionDetail()->first()->transaction_id)->type !!}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="3" class="text-right">{{ vl('general','amountPayable') }}</td>
                <td> {{ number_format($celebrateDetail->transactionDetail()->sum('total_amount')) }}</td>
            </tr>
{{--            <tr>--}}
{{--                <td colspan="3" class="text-right">Tax Total %1X</td>--}}
{{--                <td> 2</td>--}}
{{--            </tr>--}}
{{--            <tr>--}}
{{--                <td colspan="3" class="text-right">Grand Total</td>--}}
{{--                <td> 12.XX</td>--}}
{{--            </tr>--}}
            </tbody>
        </table>
        <br>
{{--        <h3 class="heading">Payment Status: Paid</h3>--}}
{{--        <h3 class="heading">Payment Mode: Cash on Delivery</h3>--}}
    </div>

    <div class="body-section">
        <p>

            <a href="{{ route('celebration.port',[$celebrateDetail->slug]) }}" class="btn btn-main-color" onclick="event.preventDefault();
                                        document.getElementById('port-celebration').submit();">
                {{ vl('general','payment') }}
                <form id="port-celebration" action="{{ route('celebration.port',[$celebrateDetail->slug]) }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
        </p>
    </div>

</div>


@section('css')
    <style>

        .brand-section{
            background-color: #0d1033;
            padding: 10px 40px;
        }
        .logo{
            width: 50%;
        }


        .text-white{
            color: #fff;
        }
        .company-details{
            float: right;
            text-align: right;
        }
        .body-section{
            padding: 16px;
            border: 1px solid gray;
        }
        .heading{
            font-size: 20px;
            margin-bottom: 08px;
        }
        .sub-heading{
            color: #262626;
            margin-bottom: 05px;
        }
        table{
            background-color: #fff;
            width: 100%;
            border-collapse: collapse;
        }
        table thead tr{
            border: 1px solid #111;
            background-color: #f2f2f2;
        }
        table td {
            vertical-align: middle !important;
            text-align: center;
        }
        table th, table td {
            padding-top: 08px;
            padding-bottom: 08px;
            padding-right: 8px;
        }
        .table-bordered{
            box-shadow: 0px 0px 5px 0.5px gray;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
        }
        .text-right{
            text-align: end;
        }
        .w-20{
            width: 20%;
        }
        .float-right{
            float: right;
        }
    </style>
    @endsection

