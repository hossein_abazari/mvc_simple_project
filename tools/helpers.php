<?php
/* random string for slug */
function str_slug_random($number = null)
{
    if ($number != null && ! is_string($number)) {
        return substr(md5(uniqid(mt_rand(), true)), 0, $number);
    }
    return substr(md5(uniqid(mt_rand(), true)), 0, 8);
}


/* validation language for easy code */
function vl($name1,$name2=null,$name3=null){
    $trans = 'validation.custom.'.$name1;

    if ($name2){
        $trans .= '.'.$name2;
    }
    if ($name2 && $name3){
        $trans .= '.'.$name3;
    }

    $trans = trans($trans);

    if (is_array($trans)){
        $result = $trans;
    }
    else {
        $result = strpos($trans, 'validation.custom') !== false ? false : $trans;
    }

    $result = $result ? $result : ($name3 ? $name3 : $name2);

    return $result;
}

/* flash message */
function flash($message, $level = 'success')
{
    session()->flash('flash_message', $message);
    session()->flash('flash_message_level', $level);
}

function toMoney($number){

    return number_format($number)." ".labelMoney();
}

function labelMoney(){

    $labelMoney = vl('variable','money');

    return vl('general',$labelMoney);
}

function toDebtCredit($amount,$url='#'){

    $mode = '';
    $class = '';
    $href = '';
    if ($amount > 0){
        $mode = vl('admin','debtor');
        $class = "text-danger";
        $href= $url;
    }
    if ($amount < 0){
        $mode = vl('admin','creditor');
        $class = "text-success";
        $href='#';
    }

    return "<a href='".$href."' class='".$class."'>".$mode." ".toMoney(str_replace('-','',$amount))."</a>";
}

function templateDirectory($directoryId,$mark='/')
{
    $directory = $mark.$directoryId.$mark;
    return 'Samples'.(string)$directory;
}



function timeExpire($expireDay)
{
    $time = time() + (60 * 60 * 24 * $expireDay);

    $expire_date = \Carbon\Carbon::parse($time)->format('Y-m-d H:i:s');

    return $expire_date;
}

/**
 * Unset All Array
 *
 * @param $names
 * @param $array
 * @return mixed
 */
function unsetAllArray($array,$names){
    foreach ($names as $name){
        unset($array[$name]);
    }

    return $array;
}

/**
 * @param $input
 * @return mixed
 */
function convertPersianNumbersToEnglish($input)
{
    $persian = ['۰', '۱', '۲', '۳', '۴', '٤', '۵', '٥', '٦', '۶', '۷', '۸', '۹'];
    $english = [ 0 ,  1 ,  2 ,  3 ,  4 ,  4 ,  5 ,  5 ,  6 ,  6 ,  7 ,  8 ,  9 ];
    return str_replace($persian, $english, $input);
}



